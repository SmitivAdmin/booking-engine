import {TENANT} from './common-info';
// import PropUtil from 'PropUtil';
/**
 * A utility class to help maintain and manage the cookies for the website.
 * @type {Object}
 */
class CookieUtil {
    /**
     *  A simple function to set a cookie
     * @param cname
     * @param cvalue
     * @param exdays
     */

    setCookie(cname, cvalue, exdays) {
        let d = new Date();
        d.setTime(d.getTime() + ( exdays * 24 * 60 * 60 * 1000 ));
        const expires = "expires=" + d.toUTCString();
        // Delete existing language cookie
        document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
        // Write new language cookie

        //console.log('ca_cookie',cname,cvalue,TENANT,expires);
        //document.cookie = cname + "=" + cvalue + "; path=/" + PropUtil.URLS.getCurrentTenant() + "/; " + expires;
        document.cookie = cname + "=" + cvalue + "; path=/"+TENANT+"/; " + expires;
    }

    /**
     * A simple function to retrieve the value of the cookies as a string
     * @param cname
     * @returns {*}
     */
    getCookie(cname) {
        const name = cname + "=",  ca = document.cookie.split(';');
        console.log('ca_cookie',ca);

        //alert(document.cookie);
        for (let i = 0; i < ca.length; i++) {
            let c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return -1;
    }

    /**
     * A simple function to remove cookies
     * @param  {[type]} value [description]
     * @return {[type]}       [description]
     */
    removeCookie(value) {
        this.setCookie(value, "", -1);
    }
}

export default new CookieUtil();