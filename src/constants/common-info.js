import CookieUtil from './CookieUtil';

var base64 = require('base-64');

var all_token = document.cookie.split(";").reduce( (ac, cv, i) => Object.assign(ac, {[cv.split('=')[0]]: cv.split('=')[1]}), {});
export const millisecods = Math.floor(Date.now() / 1000);

export const TENANT = "sistic";

// Authentication Token
export const AUTH_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaGFubmVscyI6WyJzaXN0aWNfYm94X29mZmljZSJdLCJzY29wZSI6WyJtYnMiLCJzaXN0aWMiLCJwcm9kdWN0IiwiY2FydCIsImhvdGVsIl0sImV4cCI6MzY1Mjc3NDE5NSwianRpIjoiOGQ0YzZjODctMjY3MS00NTEyLWI5YzItNGQyMjJjMGNhZjBiIiwiY2xpZW50X2lkIjoic2lzdGljLXN0aXhjbG91ZCJ9.mDfZ5iVoeWlS8nauih3NtIrHAEgkuzVIdxEcLiaujIk";

// Authentication Token
export const AUTH_TOKEN2 = CookieUtil.getCookie(TENANT + '_token');

//export const AUTH_TOKEN2 = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJzaXN0aWMiLCJwcm9kdWN0IiwicGF0cm9uX2Fub255bW91cyIsInBhY2thZ2UiLCJtZW1iZXJzaGlwIl0sImV4cCI6MTYwMTg3ODA0NSwianRpIjoiYzcyZDkyYzMtNTNmYy00ZGJlLTkyYzMtMjFjOWMyYWQxYTgwIiwiY2xpZW50X2lkIjoic2lzdGljMS1iZSJ9.2O3zTsvwOvpci1HKMLu4VgRqM6vXQ_8rbSHiEwQ09q0";

//console.log('AUTH_TOKEN2', AUTH_TOKEN2);

export const LOGIN_AUTH_KEY = base64.encode("sistic1-api:56JKCUXmrK0DqqyfywptcQ_U1ga");

export const BASE_URL = "https://api.stixcloudtest.com/api/v0/"+TENANT.toUpperCase();
export const LOGIN_BASE_URL = "https://auth.stixcloudtest.com/auth/v0/"+TENANT.toUpperCase();
export const SITE_URL = "https://api.stixcloudtest.com";
export const TICKETING_URL = "https://ticketing.stixcloudtest.com/"+TENANT;    // Need to change finally

/* ====== Java API Urls ==== */
export const PRODUCT_ID = TENANT+"/patron/products/productid";
export const CAT_SECTION = TENANT+"/patron/products/catsection";
export const SEAT_Availability = TENANT+"/products/seatmap/availability";
export const SEAT_CONFIRMATION = TENANT+"/patron/products/confirmation";
export const SHOPPING_CART = TENANT+"/confirm/shoppingcart";
export const ADD_CART = TENANT+"/patron/cart";
export const LOGIN_API = TENANT+"/login";
export const CHECKCART = TENANT+"/patron/checkcart";
export const PROMOTION = TENANT+"/action/promotion";
export const CONFIRM_ORDER = TENANT+"/action/confirmorder";
export const USER_LOGIN = TENANT+"/login";
export const CART_DELETE = TENANT+"/cart/delete";
export const SEAT_AVAILABLE = TENANT+"/products/seatmap/availability";


export const SELECT_PACKAGE = TENANT+"/patron/package/selectPackage";
export const SELECT_PACKAGE_PRODUCTID = TENANT+"/patron/package/products/productid";

export const PACKAGE_AVAILABILITY = TENANT+"/products/package/seatmap/availability";
export const PACKAGE_CONFIRMATION = TENANT+"/patron/package/products/confirmation";
export const PACKAGE_SELECTION = TENANT+"/patron/package/selection";
export const GET_PACKAGE_SELECTION = TENANT+"/patron/package/getPkgSelections?_="+millisecods;
export const REMOVE_SELECTION = TENANT+"/patron/package/removeSelection";
export const CART_PACKAGE = TENANT+"/patron/cartPackage";
export const EVOUCHERVALIDATION=TICKETING_URL+"/evoucher/validation";


export const VERSION_API = TENANT+"/version?_="+millisecods;
export const PROP_API = TENANT+"/prop?v="+millisecods;
export const SERVER_API = TENANT+"/prop/server?v="+millisecods;
export const LANG_API = TENANT+"/lang/en_us?v="+millisecods;

/* ====== Java API Urls ==== */


export const AVAILABILE = "Available,available_cls,#07b3f0";
export const COMING_SOON = "Coming Soon,coming_cls,#07b3f0";
export const SINGLE_SEATS = "Single Seats,single_cls,#ef5858";
export const LIMITED_SEATS = "Selling Fast,selling_cls,#f2b119";
export const SHOW_RECONFIG = "Show Reconfig,reconf_cls,#07b3f0";
export const SHOW_CANCELLED = "Show Cancelled,cancel_cls,#ef5858";
export const SHOW_POSTPONED = "Show Postponed,postpone_cls,#c7c7c7";
export const SOLD_OUT = "Sold Out,sold_cls,#c7c7c7";
export const NO_DATE = "No Show,sold_cls,#c7c7c7";

export const AMOUNT_INSURED = parseFloat("10").toFixed(2);
export const API_KEY = "sandbox_ktd37673_rpbzbxq2vdzt5x9f";
export const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
export const dayMapping = {
    0: "Sun",
    1: "Mon",
    2: "Tue",
    3: "Wed",
    4: "Thu",
    5: "Fri",
    6: "Sat"
};

export const ADD_ON = {
    "E_TICKET": "E-Ticket",
    "MBS_BOX": "Collection from Marina Bay Sands Box Office",
    "OUTLET_PICKUP": "Collection from Singapore Authorised Agents",
    "MAIL": "Mail",
    "REGISTERED_MAIL": "Registered Mail",
    "COURIER": "Courier",
    "MASTERCARD_PICKUP": "Mastercard Pickup",
};

export function DeliverMethodTitle(method_code){
    switch (method_code) {
        case 'E_TICKET':
            return 'E-Ticket';
        case 'MBS_BOX':
            return "Collection from Marina Bay Sands Box Office";
        case 'OUTLET_PICKUP':
            return "Collection from Singapore Authorised Agents";
        case 'MAIL':
            return "Mail";
        case 'REGISTERED_MAIL':
            return "Registered Mail";
        case 'COURIER':
            return "Courier";
        case 'MASTERCARD_PICKUP':
            return "Mastercard Pickup";
        default:
            return '';
    }
}

export function PaymentMethod(method_code){
    var payment_methods = '';
    
    switch (method_code) {
        case 'AMEX_MBS_SISTIC':
            return payment_methods = {'payment_title':'American Express', 'payment_value': 'amex', 'payment_code': method_code}
        case 'MASTER':
            return payment_methods = {'payment_title':'Mastercard', 'payment_value': 'mastercard', 'payment_code': method_code}
        case 'MASTER_MBS_SISTIC':
                return payment_methods = {'payment_title':'Mastercard', 'payment_value': 'mastercard', 'payment_code': method_code}
        case 'VISA':
            return payment_methods = {'payment_title':'Visa', 'payment_value': 'visa', 'payment_code': method_code}
        case 'VISA_MBS_SISTIC':
                return payment_methods = {'payment_title':'Visa', 'payment_value': 'visa', 'payment_code': method_code}
        default:
            return payment_methods = '';
    }
    
}

export function DeliveryMethodDesc(method_code) {
    switch (method_code) {
        case 'E_TICKET':
            return '<p>You can now conveniently present your e-ticket on your phone for entry.</p>';
        case 'MBS_BOX':
            return '';
        case 'OUTLET_PICKUP':
            return '<p>Your ticket(s) is available for your collection only at Singapore SISTIC Authorised Agents. Check Agent location and operation hours here.</p>';
        case 'MAIL':
            return '<p>Mailing is available through Singpost for Singapore addresses and will take up to 7 workings days. Please note that SISTIC is not liable for undelivered mail.</p><span className="cont-bg"><span className="clearfix">10 Eunos Road 8 #03-04 Singapore 408600</span> <a href="javascript:;">Edit Mailing Details</a></span>';
        case 'REGISTERED_MAIL':
            return '<p>Registered Mail is available through SingPost for local delivery to Singapore addresses only and will take up to 7 workings days.  Registered Mailing is a Sign-For service that requires a signature on delivery. No overseas delivery is applicable under this option.</p><span className="cont-bg"><span className="clearfix">10 Eunos Road 8 #03-04 Singapore 408600</span> <a href="javascript:;">Edit Mailing Details</a></span>';
        case 'COURIER':
            return '<p>Courier is a door-to-door service available through a third party provider for Singapore addresses. SISTIC will be contacting you to arrange for delivery once the tickets are ready. Courier delivery is a Sign-For service that requires a signature on delivery.</p><span className="cont-bg"><span className="clearfix">10 Eunos Road 8 #03-04 Singapore 408600</span> <a href="javascript:;">Edit Mailing Details</a></span>';
        case 'MASTERCARD_PICKUP':
            return '<p>Collect your ticket(s) at the venue starting from one hour before the event time. Venue Collection is at $1.50 per event in one transaction.</p>';
        default:
            return '';
    }
}

export const PAYMENT_LANG_OBJ = {
    "cart.payment.dbs.title" : "Sponsorship by DBS Bank",
    "cart.payment.dbs.no-waive.label": "The event(s) that you have selected is/are not participating in the SISTIC booking fee waiver promotion with DBS Bank. Booking fee of {{fee}} will be charged accordingly."
}

export const PACKAGE_RULES = {
    PACKAGE_EVENT_RULE_MINIMUM_REQUIREMENTS: "package.event.rule.minimum.requirements",
    PACKAGE_EVENT_RULE_MAXIMUM_REQUIREMENTS: "package.event.rule.maximum.requirements",
    PACKAGE_EVENT_RULE_MINIMUM_AMOUNT: "package.event.rule.minimum.amount",
    PACKAGE_EVENT_RULE_MAXIMUM_AMOUNT: "package.event.rule.maximum.amount",
    PACKAGE_EVENT_RULE_MINIMUM_QUANTITY: "package.event.rule.minimum.quantity",
    PACKAGE_EVENT_RULE_EXACT_QUANTITY: "package.event.rule.exact.quantity",
    PACKAGE_EVENT_RULE_MAXIMUM_QUANTITY: "package.event.rule.maximum.quantity",
    PACKAGE_PRODUCT_RULE_SAME_QUANTITY: "package.product.rule.same.quantity",
    PACKAGE_PRODUCT_RULE_SAME_PRICE_CATEGORY: "package.product.rule.same.price.category",
    PACKAGE_EVENT_RULE_SAME_TIME: "package.event.rule.same.time",
    PACKAGE_EVENT_RULE_SAME_DATE: "package.event.rule.same.date",

    // Package Requirements level
    PACKAGE_REQUIREMENTS_EVENT_RULE_MINIMUM_PRODUCTS: "package.requirements.event.rule.minimum.products",
    PACKAGE_REQUIREMENTS_EVENT_RULE_MAXIMUM_PRODUCTS: "package.requirements.event.rule.maximum.products",
    PACKAGE_REQUIREMENTS_EVENT_RULE_SAME_DATE: "package.requirements.event.rule.same.date",
    PACKAGE_REQUIREMENTS_EVENT_RULE_SAME_TIME: "package.requirements.event.rule.same.time",
    PACKAGE_REQUIREMENTS_PRODUCT_RULE_MINIMUM_QUANTITY_PER_PRODUCT: "package.requirements.product.rule.minimum.quantity.per.product",
    PACKAGE_REQUIREMENTS_PRODUCT_RULE_EXACT_QUANTITY_PER_PRODUCT: "package.requirements.product.rule.exact.quantity.per.product",
    PACKAGE_REQUIREMENTS_PRODUCT_RULE_MAXIMUM_QUANTITY_PER_PRODUCT: "package.requirements.product.rule.maximum.quantity.per.product",
    PACKAGE_REQUIREMENTS_PRODUCT_RULE_SAME_QUANTITY: "package.requirements.product.rule.same.quantity",
    PACKAGE_REQUIREMENTS_PRODUCT_RULE_SAME_PRICE_CATEGORY: "package.requirements.product.rule.same.price.category",

    SAME_DATE: "package.same.date",
    SAME_TIME: "package.same.time",
}

//export const SERVER_URL = "http://localhost:5001/checkout";   //For Local
//export const SERVER_URL = "http://ec2-54-251-142-179.ap-southeast-1.compute.amazonaws.com:5001/checkout"; //For Server

export const HTMLENTITY_CODES = /&amp;|&gt;|&lt;|&quot;|&aacute;|&eacute;|&iacute;|&oacute;|&uacute;/g;


