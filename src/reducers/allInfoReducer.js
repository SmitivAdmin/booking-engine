import {
    SHOW_INFO, UPDATE_QUANTITY, ADD_QUANTITY, CHOOSE_DATE, BREAD_CRUMBS, UPDATE_TIMER, CAL_VIEW, SEAT_MAP_OVERVIEW,
    SEAT_SELECTION, ADD_TO_CART, EMPTY_CART, REMOVE_RESERVED_CART, REMOVE_CART, REMOVE_MULTIPLE_ITEM_CART, PAYMENT_SUCCESS, ON_ADDON_SELECT, PAYMENT_METHODS, INSURANCE_CHOSEN, UPDATE_TOTAL_PRICE, CHOOSE_TIME, SEAT_STAGE_VIEW, SEAT_STAGE_COORDS, ADD_TO_RESERVED_CART, USER_DATA, UPDATE_TICKET_TYPE, UPDATE_CURRENT_QUANTITY, COMBINE_CART_ON_TICKETTYPE, PRICE_TABLE, OVERVIEW_SELECTED_AREA, 
    SHOPPING_CART, UPDATE_SEAT_FRND, UPDATE_USER_BILING_DETAILS, UPDATE_CROSS_SELL, UPDATE_UP_SELL, RESERVED_FRIENDS_GA, PACAKAGE_SHOW_LIST, PACK_SHOWTIME_LIST, CHOOSE_PACKAGE, PACKAGE_CART_INFO, UPDATE_EVENT_TYPE, CONFIRM_ORDER, CHECKCART, CHOOSE_CAL_DATE
} from '../actions/actionsTypes'

const initialState = {
    showInfo: {},
    quantity: 0, adult_qty: 0, child_qty: 0, nsf_qty: 0, sectz_qty: 0, ticketTypesSelected:[],
    quantity_visit: 0, datetime_visit: 0, seats_visit: 0, delivery_visit: 0, payment_visit: 0, confirmantion_visit: 0, 
    time: {}, seconds: 900000, timerAutoStart: false,
    nav : {},
    chosenProductId: '',
    error: null,
    showTimeFlag : false,
    showCalFlag: false,
    hideDefaultDateView: true,
    showTimes : [],
    showActive: '',
    dateChosen: '',
    timeChosen: '',
    seatMapInfo: {},
    seatSelectionInfo: {},
    seatSelectionArr: [],
    cartInfo:[],
    mergedCartInfo:[],
    reservedcartInfo:[],
    chosenPriceCatId:'',
    checkout_btn_enable: false,
    index: 0,
    update_seat_select: 0,
    selected_adult_qty: 0,
    selected_child_qty: 0,
    selected_nsf_qty: 0,
    selected_sectz_qty: 0,
    transactionTime: '',
    transactionDate: '',
    transactionId : '',
    chosenAddon:'',
    chosenAddonPrice: '',
    chosenAddon:'',
    chosenAddonMethod: '',
    paymentMethods: {},
    insuranceAmount: 0,
    totalPriceToBePaid:0,
    cardType:'',
    levelsCoordinatesList: [],
    ticketType:[],
    access_token: localStorage.getItem('access_token'),
    patronName: localStorage.getItem('patronName'),
    email: localStorage.getItem('email'),
    firstName: localStorage.getItem('firstName'),
    lastName: localStorage.getItem('lastName'),
    accountNo: localStorage.getItem('accountNo'),
    accountType: localStorage.getItem('accountType'),
    currentQuantity: 0,
    priceTableObj: [],
    selectedFilterArea: {},
    prdTicketType: '',
    seatForFrnds : false,
    userBilingDetails: null,
    crossSellName: "",
    crossSellAmt: 0,
    upsellCartItems: [],
    isEventWithSurvey: false,
    packageShowListObj: {},
    packShowTimeListObj: {},
    pkgReqId: '',
    prdtIdList: '',
    choose_showTitle: '',
    choose_venue: '',
    choose_showDate: '',
    packageCartInfo: [],
    checkcart_info: '',

    conf_hasLiveStreamEvent: {},
    conf_error:"",
    conf_errorId:"",
    conf_hasPkgDine:"",
    conf_hasPkgStay:"",
    conf_hasHotelPackage:"",
    conf_isEvoucherFlow:"",
    conf_wufooFormField:"",
    conf_isEventWithSurvey:"",
    conf_cartTotalQty:"",
    confirmation_analytics: '',
    confirmation_lineitemMsg: '',
    confirmation_transOrder: ''
}

export function showInfoReducer(state = initialState, action) {
    console.log('action', action);
    // TODO: Deepa revisit update quantity
    switch(action.type) {
        case SHOW_INFO:
            return {
                ...state,
                showInfo: action.showInfo
            };
        case UPDATE_QUANTITY:
            return {
                ...state,
                ticketTypesSelected: action.ticketTypesSelected,
                currentQuantity: action.currentQuantity
        };
        case RESERVED_FRIENDS_GA:
            //console.log('reservedcartInfo',action.reservedcartInfo);
            return {
                ...state,
                reservedcartInfo: action.reservedcartInfo,
                currentQuantity: action.currentQuantity
        };
        case ADD_QUANTITY:
            return {
                ...state,
                quantity: action.updatedQuantity,
            };
        case CHOOSE_DATE:
            return {
                ...state,
                showTimeFlag: action.showTimeFlag,
                showTimes: action.showTimes,
                chosenProductId: action.productId,
                showActive: action.showActive,
                dateChosen: action.dateChosen,
                prdTicketType: action.prdTicketType,
                isEventWithSurvey: action.isEventWithSurvey,
                choose_showTitle: action.choose_showTitle,
                choose_venue: action.choose_venue,
                choose_showDate: action.choose_showDate
            };
        case CHOOSE_CAL_DATE:
            return {
                ...state,
                showTimeFlag: action.showTimeFlag,
                dateChosen: action.dateChosen,
                choose_showTitle: action.choose_showTitle,
                choose_venue: action.choose_venue,
                showTimes: action.showTimes
            };
        case CHOOSE_TIME:
            return {
                ...state,
                timeChosen: action.chosenTime,
                chosenProductId: action.chosenProductId
            };
        case CAL_VIEW:
            return {
                ...state,
                showCalFlag: action.showCalFlag,
                hideDefaultDateView: action.hideDefaultDateView,
                showTimeFlag:action.showTimeFlag
            };
        case BREAD_CRUMBS:
            return {
                ...state,
                [action.field]: action.value,
            };
        case UPDATE_TIMER:
            return {
                ...state,
                [action.field]: action.value,
            };
        case SEAT_MAP_OVERVIEW:
            return {
                ...state,
                seatMapInfo: action.seatMapInfo
            };
        case SEAT_SELECTION:
            //let seat_Selection_Arr = [...state.seatSelectionArr, action.seatSelectionArr]
            return {
                ...state,
                seatSelectionInfo: action.seatSelectionInfo,
            };
        
        case PAYMENT_SUCCESS:
            return {
                ...state,
                transactionId: action.transactionId,
                transactionTime: action.transactionTime,
                transactionDate:action.transactionDate,
                cardType:action.cardType
            };
        case ON_ADDON_SELECT:
            return {
                ...state,
                chosenAddon: action.chosenAddon,
                chosenAddonPrice: action.chosenAddonPrice,
                chosenAddonMethod: action.chosenAddonMethod
            };
        case PAYMENT_METHODS:
            return {
                ...state,
                paymentMethods: action.paymentMethods
            };
        case ADD_TO_CART:
            let cart_info_arr = [...state.cartInfo, action.cartInfo];
            return {
                ...state,
                cartInfo: cart_info_arr
            };
        case ADD_TO_RESERVED_CART:
            let reserverCartArr =  [...state.reservedcartInfo, action.reservedcartInfo];
            return {
                ...state,
                reservedcartInfo: reserverCartArr
            };
        case EMPTY_CART:
            return {
                ...state,
                cartInfo: [],
                reservedcartInfo: action.reservedcartInfo,
                currentQuantity: 0,
                ticketTypesSelected : [],
                quantity: 0,
                ticketType:[],
            };
        case INSURANCE_CHOSEN:
            return {
                ...state,
                insuranceAmount: action.insuranceAmount
            };
        case UPDATE_TOTAL_PRICE:
            return {
                ...state,
                totalPriceToBePaid: action.totalPriceToBePaid
            };
        case REMOVE_CART:
            let newState = [...state.cartInfo];
            newState.splice(action.index, 1);
            return {
                ...state,
                cartInfo: newState
            };
        case REMOVE_MULTIPLE_ITEM_CART:
            var new_State = action.isFrnd ? [...state.reservedcartInfo] : [...state.cartInfo];
            for(var k=action.index.length-1; k>=0; k--){
                new_State.splice(action.index[k], 1);
            }
        return {
            ...state,
            [action.isFrnd ? "reservedcartInfo" : "cartInfo"]: new_State
        };
        case REMOVE_RESERVED_CART:
            let newResState = [...state.reservedcartInfo];
            newResState.splice(action.index, 1);
            return {
                ...state,
                reservedcartInfo: newResState
            };
        case SEAT_STAGE_VIEW:
            return {
                ...state,
                seatSelectionArr: action.seatSelectionArr,
            };
        case SEAT_STAGE_COORDS:
            return {
                ...state,
                levelsCoordinatesList: action.levelsCoordinatesList
            };
        case USER_DATA:
            return {
                ...state,
                // access_token: action.access_token,
                // token_type: action.token_type,
                // refresh_token: action.refresh_token,
                // expires_in: action.expires_in,
                // scope: action.scope,
                // patronId: action.patronId,
                // email: action.email,
                // firstName: action.firstName,
                // lastName: action.lastName,
                // accountNo: action.accountNo,
                // hasMembership: action.hasMembership,
                // accountType: action.accountType,
                // jti: action.jti,
                access_token: action.access_token,
                patronName: action.patronName,
                email: action.email,
                firstName: action.firstName,
                lastName: action.lastName,
                accountNo: action.accountNo,
                accountType: action.accountType
            };
        case UPDATE_TICKET_TYPE:
            return {
                ...state,
                ticketType: action.ticketTypeList,
                chosenPriceCatId: action.chosenPriceCatId
            };
        case UPDATE_CURRENT_QUANTITY:
            return {
                ...state,
                currentQuantity: action.currentQuantity
            };
        case COMBINE_CART_ON_TICKETTYPE: {
            return {
                ...state,
                mergedCartInfo: action.mergedCartInfo
            };
        }
        case PRICE_TABLE: {
            return {
                ...state,
                priceTableObj: action.priceTableObj
            };
        }
        case OVERVIEW_SELECTED_AREA: {
            return {
                ...state,
                selectedFilterArea: action.selectedFilterArea
            };
        }
        case SHOPPING_CART: {
            return {
                ...state,
                shoppingCart: action.shoppingCart
            };
        }
        case UPDATE_SEAT_FRND:
            return {
                ...state,
                seatForFrnds : action.boolean
            };
        case UPDATE_USER_BILING_DETAILS:
            return {
                ...state,
                userBilingDetails: action.userData
            };
        case UPDATE_CROSS_SELL:
            return {
                ...state,
                crossSellName: action.name,
                crossSellAmt: action.amount
            };        
        case UPDATE_UP_SELL:
            let upSell = [...state.upsellCartItems];
            if(action.actionType==="ADD"){
                upSell.push(action.item);
                return {
                    ...state,
                    upsellCartItems: upSell
                };
            }
            else if(action.actionType==="EMPTY"){
                return {
                    ...state,
                    upsellCartItems: []
                };
            }
            else{
                upSell.splice(action.item, 1);
                return {
                    ...state,
                    upsellCartItems: upSell
                };
            }
        case PACAKAGE_SHOW_LIST:
            return {
                ...state,
                packageShowListObj: action.packageShowListObj,
            };
        case PACK_SHOWTIME_LIST:
            return {
                ...state,
                packShowTimeListObj: action.packShowTimeListObj,
            };
        case CHOOSE_PACKAGE:
            return {
                ...state,
                pkgReqId: action.pkgReqId,
                prdtIdList: action.prdtIdList,
            };
        case PACKAGE_CART_INFO:
            return {
                ...state,
                packageCartInfo: action.packageCartInfo,
            };
        case UPDATE_EVENT_TYPE:
            return {
                ...state,
                prdTicketType: action.prdTicketType,
            };
        case CONFIRM_ORDER:
            return {
                ...state,
                [action.field]: action.value,
            };
        case CHECKCART:
            return {
                ...state,
                checkcart_info: action.checkcart_info,
            };

        default:
            return state;
    }
}

