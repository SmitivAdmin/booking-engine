import React, {Component, Fragment} from 'react'
import {TENANT, SHOPPING_CART} from './constants/common-info';
import { HashRouter, Route, BrowserRouter } from "react-router-dom";
import ScrollTop from './ScrollTop'
import { withRouter} from 'react-router';
import createHistory from 'history/createBrowserHistory';
// import {createBrowserHistory} from 'history';


import Home from "./components/events/Home";
import Booking from './components/events/Booking'
import Delivery from './components/events/Delivery'
import Payments from './components/events/Payments'
import OrderSummary from './components/events/OrderSummary'
import Logout from './components/events/Logout'
import PackageEvent from './components/events/PackageEvent'


export const history = createHistory();

class Routes extends Component {
    constructor(props) {
        super(props);
        this.state = {hasError: false};
    }

    componentDidCatch(error, info) {
        // Display fallback UI
        this.setState({hasError: true});
    }
    render() {
        return (
            <Fragment>

                <BrowserRouter history={history}>
                    <ScrollTop />
                    <Route exact path={`/`} component={Home} />
                    <Route exact path={`/${TENANT}/booking/:id`} component={() => <Booking {...this.props} />} />
                    <Route exact path={`/${TENANT}/package/:id`} component={() => <PackageEvent {...this.props} />} />
                    <Route exact path={`/${TENANT}/confirm/shoppingcart`} component={() => <Delivery {...this.props} />} />
                    <Route exact path={`/${TENANT}/confirm/payment`} component={() => <Payments {...this.props} />} />
                    {/* <Route exact path="/booking-confirm" component={() => <BookingConfirm {...this.props} />} /> */}
                    <Route exact path={`/${TENANT}/confirmorder/processPayment`} component={() => <OrderSummary {...this.props} />} />
                    <Route exact path={`/${TENANT}/confirmorder/postconfirmation`} component={() => <OrderSummary {...this.props} />} />
                    <Route exact path={`/${TENANT}/logout`} component={() => <Logout {...this.props} />} />
                </BrowserRouter>
            </Fragment>
        )
    }
}

export default withRouter(Routes);