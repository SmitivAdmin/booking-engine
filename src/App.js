import React, {Component} from 'react'
import { Provider } from 'react-redux'
import {showInfoReducer} from "./reducers/allInfoReducer";
import { createStore} from 'redux';
import Routes from './routes'
import * as actionCreators from "./actions/actionCreator"
import * as API from "./service/events/eventsInfo";
import jQuery from 'jquery';

//TODO: Deepa fix this
let store = createStore(showInfoReducer);
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }
  componentWillMount() {
    //var icc_code = "asmod12200221";   // GA - Example id
    //var icc_code = "patrick1220"; // RS - Example id
    console.log('icc_code',window.location.pathname.split('/')[3]);
    localStorage.setItem("event_code", '');
    localStorage.setItem("event_type", '');
    
    if(window.location.pathname.split('/')[2] === "booking"){
      var icc_code = window.location.pathname.split('/')[3];
      localStorage.setItem("event_code", icc_code);
      localStorage.setItem("event_type", "booking");
    }
    if(window.location.pathname.split('/')[2] === "package"){
      var icc_code = window.location.pathname.split('/')[3];
      localStorage.setItem("event_code", icc_code);
      localStorage.setItem("event_type", "package");
    }

    API.checkCart().then(response =>{
      if(response && response.status === 200){
        localStorage.setItem("email", response.data.patron.email);
        localStorage.setItem("firstName", response.data.patron.firstName);
        localStorage.setItem("lastName", response.data.patron.lastName);
        localStorage.setItem("accountNo", response.data.patron.accountNo);
        localStorage.setItem("accountType", response.data.patron.patronType);

        //this.props.userData(response.data.patron);

        jQuery(".login_indicate").addClass("green_clr");
        window.jQuery('#sistic-login-modal').modal('hide');
        jQuery(".login_err").html('');

        store.dispatch(actionCreators.userInfo(response.data.patron));
      } else{
        store.dispatch(actionCreators.userInfo({access_token:'', patronName: '', email:'', firstName: '', lastName: '', accountNo: '', accountType: ''}));
      }
      
    });

    var group_booking = window.location.search;

    if(localStorage.getItem("event_code")){
      API.getInfo(localStorage.getItem("event_code"), localStorage.getItem("event_type"), group_booking).then(response =>{
        store.dispatch(actionCreators.fetchShowInfo(response));
      }); 
    }
     

  }

  componentDidCatch(error, info) {
    // Display fallback UI
    this.setState({ hasError: true });
  }

  componentDidMount() {
    var scrollComponent = this;
    document.addEventListener("scroll", function(e) {
        scrollComponent.toggleVisibility();
    });
  }

  toggleVisibility() {
    if(document.getElementsByClassName("float-wrap")[0]){
      var element = document.getElementsByClassName("float-wrap")[0];
      if (window.pageYOffset > 250) {
          element.classList.add("sticky");
      } else {
          element.classList.remove("sticky");
      }
    }
  }

  render(){
    return(
        <Provider store={store}>
          <Routes {...this.props} />
        </Provider>
    )
  }
}

