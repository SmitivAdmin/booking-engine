import React, {Component} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../actions/actionCreator"

import Countdown from 'react-countdown-now';

import jQuery from 'jquery';

class TimeCounter extends Component {
    constructor(props){
        super(props);
    }

    

    update(value, timePercent) {
        let length = Math.PI * 2 * 100;
        var offset = - length - length * value / (timePercent);
        
        for(var i=0; i<document.getElementsByClassName('e-c-progress').length; i++){
            document.getElementsByClassName('e-c-progress')[i].style.strokeDashoffset = offset; 
            document.getElementsByClassName('e_pointer_cls')[i].style.transform = `rotate(${360 * value / (timePercent)}deg)`; 
        }
    }

    render(){
        let wholeTime = 30 * 30;
        //this.update(wholeTime,wholeTime);
        var checkcart_timer = this.props.checkcart_info;

        console.log('checkcart_timer', checkcart_timer);

        const renderer = ({ hours, minutes, seconds,milliseconds, completed }) => {
            if (completed) {
              // Render a completed state              
              return '00:00';
            } else {
                //console.log('seconds_test', milliseconds)
            //   this.props.updateTimer('seconds',seconds);
              // Render a countdown
              

              return <span>{minutes}:{seconds}</span>;
            }
        };
          
        return (
        <div className="countdown-container">
            <div className="circle">
                <svg viewBox="0 0 220 220" xmlns="http://www.w3.org/2000/svg">
                    <g transform="translate(110,110)">
                        <circle r="100" className="e-c-base"/>
                        <g transform="rotate(-90)">
                            <circle r="100" className="e-c-progress" />
                            <g id="e-pointer" className="e_pointer_cls">
                                <circle cx="98" cy="0" r="10" className="e-c-pointer"/>
                            </g>
                        </g>
                    </g>
                </svg>
            </div>
            <div className="controlls">
            {/* {this.props.time.m}:{this.props.time.s} */}
                <div className="display-remain-time">
                    <Countdown  
                        date={Date.now() + this.props.seconds} 
                        autoStart = {this.props.timerAutoStart}
                        renderer={renderer} 
                        onTick={({ hours, minutes, seconds, completed }) => {
                            let milli_seconds = minutes * 60000 + seconds * 1000;
                            let length = Math.PI * 2 * 100;

                            for(var i=0; i<document.getElementsByClassName('e-c-progress').length; i++){
                                document.getElementsByClassName('e-c-progress')[i].style.strokeDasharray = length;
                            }
                            
                            if(milli_seconds === 25000){
                                window.jQuery("#time-up").modal('show');
                            }
                            

                            this.update(milli_seconds/1000,wholeTime);
                            
                            this.props.updateTimer('seconds',milli_seconds);
                        }}
                    />
                </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    //console.log('timer_state',state.seconds);
    return {
        time: state.time,
        seconds: state.seconds,
        timerAutoStart: state.timerAutoStart,
        checkcart_info: state.checkcart_info,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateTimer: (field, value) => {
            dispatch(actionCreators.updateTimer(field, value));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TimeCounter);