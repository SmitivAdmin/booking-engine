import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"

import {Link} from "react-router-dom"



class Subscription extends Component {
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.breadCrumbData('payment_visit',1);
    }
    render(){
        return (
            <Fragment>
                <section className="container-fluid insured-sec">
                    <div className="container mob-px-0">
                        <h4 className="title-bdr">Subscription</h4>
                        <p className="lead mt-4">I wish to receive marketing information by email from, and agree to the
                            collection, use and disclosure of my personal data for such marketing by:</p>
                        <div className="checkbox-wrap mt-4">
                            <label className="custom-checkbox w-100">
                                <input type="checkbox" name="one"/>Promoter(s) and/or their marketing agents for the
                                event(s) purchased
                                <span className="checkmark"></span>
                            </label>
                            <label className="custom-checkbox w-100">
                                <input type="checkbox" name="two"/>Venue Manager(s) and/or their marketing agents for
                                the event(s) purchased
                                <span className="checkmark"></span>
                            </label>
                            <label className="custom-checkbox w-100">
                                <input type="checkbox" name="three"/>SISTIC and its marketing agents for the event(s)
                                purchased and other sales, promotions, discounts, contests, or events by SISTIC
                                <span className="checkmark"></span>
                            </label>
                        </div>
                        <p className="terms mt-3">(Note: You may unsubscribe by unchecking the options above or by
                            updating your preferences in your SISTIC profile at any time.)</p>
                    </div>
                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        breadCrumbData: (field,value) => {
            dispatch(actionCreators.breadCrumbs(field,value));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Subscription);
