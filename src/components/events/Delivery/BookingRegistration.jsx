import React, { Component, Fragment } from 'react'
import * as actionCreators from "../../../actions/actionCreator";
import { connect } from "react-redux";
import * as API from "../../../service/events/eventsInfo";

import $ from 'jquery';

class BookingRegistration extends Component {
    constructor(props) {
        super(props);
    }


    render() {

        return (
        <Fragment>
            
            <section className="container-fluid delivery-reg-cnt pt-4">  
                <section className="container">
                    <h4 className="title-bdr">Registration Details</h4>

                    {/* =========== Row Start ======== */}
                    <div className="shopRegRow row">
                        <div className="col-12 no_pad">
                            <img src={process.env.PUBLIC_URL + "/assets/images/user.svg"} className="partImg" alt="" title="" />
                            <label className="user-name participent">Participent</label>
                        </div>

                        <div className="col-12 p-0">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label>Prefix</label>
                                        <select type="text" className="form-control">
                                            <option value="">Select</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Mrs">Mrs</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group"><label>Name (as in NRIC/Passport/FIN)</label><input type="text" className="form-control" placeholder="" /></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 p-0">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>Gender</label>
                                    <select type="text" className="form-control">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Others">Others</option>
                                    </select>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>Birth Date</label>
                                    <div className="row">
                                        <div className="col-4"><input type="text" className="form-control text-center" placeholder="DD" /></div>
                                        <div className="col-4"><input type="text" className="form-control text-center" placeholder="MM" /></div>
                                        <div className="col-4"><input type="text" className="form-control text-center" placeholder="YYYY" /></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 no_pad">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>T-Shirt Size</label>
                                    <select type="text" className="form-control">
                                        <option value="">Select</option>
                                        <option value="S">S - 28</option>
                                        <option value="M">M - 30</option>
                                        <option value="L">L - 34</option>
                                        <option value="XL">XL - 38</option>
                                    </select>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>Preferred Start Time</label>
                                    <select type="text" className="form-control">
                                        <option value="">Select</option>
                                        <option value="8AM">8AM</option>
                                        <option value="9AM">9AM</option>
                                        <option value="10AM">10AM</option>
                                        <option value="11AM">11AM</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* =========== Row End ======== */}

                    {/* =========== Row Start ======== */}
                    <div className="shopRegRow row no_bdr">
                        <div className="col-12 no_pad">
                            <img src={process.env.PUBLIC_URL + "/assets/images/user.svg"} className="partImg" alt="" title="" />
                            <label className="user-name participent">Participent</label>
                        </div>

                        <div className="col-12 p-0">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                        <label>Prefix</label>
                                        <select type="text" className="form-control">
                                            <option value="">Select</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Mrs">Mrs</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group"><label>Name (as in NRIC/Passport/FIN)</label><input type="text" className="form-control" placeholder="" /></div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 p-0">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>Gender</label>
                                    <select type="text" className="form-control">
                                        <option value="">Select</option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                        <option value="Others">Others</option>
                                    </select>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>Birth Date</label>
                                    <div className="row">
                                        <div className="col-4"><input type="text" className="form-control text-center" placeholder="DD" /></div>
                                        <div className="col-4"><input type="text" className="form-control text-center" placeholder="MM" /></div>
                                        <div className="col-4"><input type="text" className="form-control text-center" placeholder="YYYY" /></div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 no_pad">
                            <div className="row">
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>T-Shirt Size</label>
                                    <select type="text" className="form-control">
                                        <option value="">Select</option>
                                        <option value="S">S - 28</option>
                                        <option value="M">M - 30</option>
                                        <option value="L">L - 34</option>
                                        <option value="XL">XL - 38</option>
                                    </select>
                                    </div>
                                </div>
                                <div className="col-12 col-md-6">
                                    <div className="form-group">
                                    <label>Preferred Start Time</label>
                                    <select type="text" className="form-control">
                                        <option value="">Select</option>
                                        <option value="8AM">8AM</option>
                                        <option value="9AM">9AM</option>
                                        <option value="10AM">10AM</option>
                                        <option value="11AM">11AM</option>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    {/* =========== Row End ======== */}


                    <div className="row col-12 add_terms_cond">
                        <div className="row col-12">
                            <label className="user-name participent col-12 no_pad">Additional Terms & Conditions</label>
                            <p className="add_req_text">Star Wars Virtual Run SouthEast Asia 2020 Waiver<span>*</span></p>
                        </div>

                        <div className="add_text">
                            <p>All participants must read, acknowledge and agree to waive, discharge and release from liability. In consideration of my participation in any way in Star Wars Virtual Run 2020 (the "Event") declare that I (or anyone that I am paying for). I will not participate in the Event unless medically fit to do so, have not been advised against participating by a certified medical professional, and acknowledge that the Event will be a test of physical and mental resolve.</p>
                        </div>
                        <label className="custom-checkbox w-100">
                            <input type="checkbox" name="three" id="terms-conds"/>I have read and accept the Terms and Conditions
                            <span className="checkmark"></span>
                        </label>
                    </div>

                </section>
            </section>

        </Fragment>

        )
    }
}
const mapStateToProps = (state, ownProps) => {
    //console.log('cartInfo', state.cartInfo)
    return {
        // chosenAddon: state.chosenAddon,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        // chosenAddon: (addon, addonPrice) => {
        //     dispatch(actionCreators.chosenAddon(addon, addonPrice));
        // },
        
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingRegistration);
