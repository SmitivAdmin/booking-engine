import React, { Component, Fragment } from 'react'
import * as actionCreators from "../../../actions/actionCreator";
import { connect } from "react-redux";
import { ADD_ON, DeliverMethodTitle, DeliveryMethodDesc } from "../../../constants/common-info"
import * as API from "../../../service/events/eventsInfo";
import HTMLParser from 'react-html-parser'

import $ from 'jquery';

class DeliveryMethods extends Component {
    constructor(props) {
        super(props);

        this.state = {
            ShoppingCartModel: null,
            E_TICKET: "ticket-lg.svg",
            OUTLET_PICKUP: "avatar-lg.svg",
            MAIL: "mailbox-lg.svg",
            REGISTERED_MAIL: "email-lg.svg",
            COURIER: "delivery-truck-lg.svg",
            MBS_BOX: "mailbox-lg.svg",
            MASTERCARD_PICKUP: "tent-lg.svg",
        }
    }
    componentDidMount() {
        // TODO: Deepa fix this
        // API.getPaymentMethods().then(response =>{
        //     this.props.getPaymentMethods(response);
        // });
        let shoppingCartString = $("#shoppingCartId").attr("data-commonDeliveryMethod");
        if (shoppingCartString !== "") {
            let parsedShoppingCart = JSON.parse(shoppingCartString);
            console.log("shoppingCart from java test", parsedShoppingCart);
            this.setState({
                ShoppingCartModel: parsedShoppingCart
            })
            // this.props.shoppingcartinfo(parsedShoppingCart);
        }

        // Save user Billing details here
        // It available only here from JAVA api.
        let userBillingDetails = $("#shoppingCartId").attr("data-confirmorderform");
        if (userBillingDetails !== "") {
            let parsedData = JSON.parse(userBillingDetails);
            console.log("user Billing details", parsedData);
            this.props.updateUserBillingDetails(parsedData);
        }

        // Set default as E-TICKET
        this.props.chosenAddon("E-Ticket", "SGD 0.00", "E_TICKET");
    }


    // TODO: Deepa change this
    chooseAddon(method_title, method_price, method_code) {
        console.log('delivery_methods', method_title+' '+method_price+' '+method_code)
        this.props.chosenAddon(method_title, method_price, method_code);
    }

    toggleDeliveryMethod(toggle_id, e){
        e.preventDefault();
        $(".card-header > a").addClass("collapsed");
        $(".collapse").slideUp(300);

        $("#del-"+toggle_id+" > a").removeClass("collapsed");
        $("#collapse"+toggle_id).slideDown(500);
    }

    // confirmCart(e){
    //     e.preventDefault();
    //     API.confirmCart().then(response =>{
    //         console.log('confrim_cart', response);
    //     })
    // }

    render() {
        let shoppingCart;;

        shoppingCart = this.state.ShoppingCartModel;

        // Static response ===> need to remove after work
        // shoppingCart = {
        //     "deliveryMethodList": [{"code":"E_TICKET","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"order":1,"addressRequired":false,"feeWaived":false},{"code":"MBS_BOX","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"order":2,"addressRequired":false,"feeWaived":false},{"code":"OUTLET_PICKUP","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":0.5},"order":2,"addressRequired":false,"feeWaived":false},{"code":"REGISTERED_MAIL","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":3,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":3},"order":3,"localMailingCountry":"SG","addressRequired":true,"feeWaived":false},{"code":"COURIER","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":15,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":15},"order":4,"localMailingCountry":"SG","addressRequired":true,"feeWaived":false}]
        // }

        return (
        <Fragment>
            
            <section className="container-fluid delivery-method pt-4">   
                <div className="container mob-px-0">
                    {/* <button className={`btn check-out-btn`} id="confirm-type" type="submit" onClick={(e) => this.confirmCart(e)}>
                    Shopping Cart
                    <img className="ml-4" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} alt="icon"/>
                </button> */}

                    <h4 className="title-bdr">Select a Delivery Method</h4>
                    <div className="d-none d-sm-none d-md-block">
                        <div className="nav nav-pills">
                            {
                                shoppingCart !== null && shoppingCart.deliveryMethodList.map((delivery_item, index) => {
                                    var method_title = DeliverMethodTitle(delivery_item.code);
                                    return (
                                        <a className={`nav-item ${index === 0 && 'active'}`} href={`#del-${index + 1}`} data-toggle="tab" role="tab" name={delivery_item.code} onClick={this.chooseAddon.bind(this, method_title, `${delivery_item.charge.currency.currencyCode} ${delivery_item.charge.number.toFixed(2)}`, delivery_item.code)}>
                                            <img src={`${process.env.PUBLIC_URL}/assets/images/${this.state[delivery_item.code] ? this.state[delivery_item.code] : "ticket-lg.svg"}`} alt="icon" />
                                            <span className="tag">{method_title}</span>
                                            <span className="price">{delivery_item.charge.currency.currencyCode} {delivery_item.charge.number.toFixed(2)}</span>
                                        </a>
                                    )
                                })
                            }
                        </div>
                        <div className="tab-content" id="nav-tabContent">
                        {
                            shoppingCart !== null && shoppingCart.deliveryMethodList.map((delivery_item, index) => {
                                var method_desc = DeliveryMethodDesc(delivery_item.code);
                                return (
                                    <div className={`tab-pane fade ${index === 0 && 'show active'}`} id={`del-${index + 1}`} role="tabpanel">
                                        {HTMLParser(method_desc)}
                                    </div>
                                )
                            })
                        }
                        </div>
                    </div>

                    <div id="accordion" className="delivery-accordion d-block d-sm-block d-md-none">
                        {
                            shoppingCart !== null && shoppingCart.deliveryMethodList.map((delivery_item, index) => {
                                var method_title = DeliverMethodTitle(delivery_item.code);
                                return(
                                <div class="card">
                                    <div className="card-header" id={`del-${index + 1}`} onClick={this.toggleDeliveryMethod.bind(this, index + 1)}>
                                        <a className={index > 0 && 'collapsed'} data-toggle="collapse" data-target={`#collapse-${index + 1}`} aria-expanded={index === 1 ? true : false} aria-controls={`#collapse-${index + 1}`} name={delivery_item.code} onClick={this.chooseAddon.bind(this, method_title, `${delivery_item.charge.currency.currencyCode} ${delivery_item.charge.number.toFixed(2)}`, delivery_item.code)}>
                                            <div className="nav-left">
                                                <div className="radio-check" name={delivery_item.code}></div>
                                                <img src={`${process.env.PUBLIC_URL}/assets/images/${this.state[delivery_item.code] ? this.state[delivery_item.code] : "ticket-lg.svg"}`} alt="icon" />
                                                <span className="tag">{method_title}</span>
                                            </div>
                                            <span className="price">{delivery_item.charge.currency.currencyCode} {delivery_item.charge.number.toFixed(2)}</span>
                                        </a>
                                    </div>

                                    <div id={`collapse${index + 1}`} className={`collapse ${index === 0 && 'show'}`} aria-labelledby={`del-${index + 1}`} data-parent="#accordion">
                                        <div className="card-body">
                                            {HTMLParser(DeliveryMethodDesc(delivery_item.code))}
                                        </div>
                                    </div>
                                </div>
                                )
                            })
                        }
                    </div>

                </div>
            </section>
        </Fragment>

        )
    }
}
const mapStateToProps = (state, ownProps) => {
    //console.log('cartInfo', state.cartInfo)
    return {
        chosenAddon: state.chosenAddon,
        shoppingCart: state.shoppingCart
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        chosenAddon: (addon, addonPrice, addonMethod) => {
            dispatch(actionCreators.chosenAddon(addon, addonPrice, addonMethod));
        },
        shoppingcartinfo: (dataObject) => {
            dispatch(actionCreators.shoppingCartInfo(dataObject));
        },
        getPaymentMethods: (response) => {
            dispatch(actionCreators.getPayments(response))
        },
        updateUserBillingDetails: (details) => {
            dispatch(actionCreators.updateUserBillingDetails(details))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryMethods);
