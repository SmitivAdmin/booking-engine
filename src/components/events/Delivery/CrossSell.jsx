import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"

class CrossSel extends Component {

    constructor(props) {
        super(props);
    }

    addToCart = () =>{
        var CrossName = "Grab Redemption Voucher", crossAmt = 5;

        var self = this;
        if(this.props.crossSellName !== ""){
            this.setState({isAddToBtnActive: false},()=>{
                self.props.updateCrossSellDetails("", 0);
            })
        }
        else{
            this.setState({isAddToBtnActive: true},()=>{
                self.props.updateCrossSellDetails(CrossName, crossAmt);
            })
        }

    }


    render() {
        return (
            <Fragment>
               <section className="container-fluid insured-sec">
                        <div className="container mob-px-0">
                            <h4 className="title-bdr mb-4">Do You Need a Ride After the Show?</h4>
                            <div className="row">
                            <div className="col-md-4">
                                <img className="crossSelImg" src={process.env.PUBLIC_URL + "/assets/images/crossSellImg.png"} alt="icon"/>
                            </div>
                            <div className="col-md-7">
                                <span className="crossSellDesc">Tired after a show? You do not have to worry about missing the train or getting stuck at traffic. Purchase our Grab redemption voucher and get $10 off from your Grab ride to any destination! Redeem your voucher at our Redemption Counter, Basement 1.</span>
                                <div className="crossSellContainer">
                                    <span className="crossSellVoucherName">Grab Redemption Voucher</span>
                                    <br />
                                    <span className="CrossSellAmt">SGD 5.00</span>
                                </div>
                                <button className={this.props.crossSellName !== "" ? "removeCrossSellBtn" : "crossSellBtn"} onClick={()=>this.addToCart()}>
                                    <span className="CrossBtnTxt">{this.props.crossSellName !== "" ? "Remove From Cart" : "Add to Cart"}</span>
                                </button>
                            </div>
                            <div className="col-md-1">

                            </div>
                            </div>
                        </div>
                    </section>
            </Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        crossSellName: state.crossSellName,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateCrossSellDetails : (name, amount) =>{
            dispatch(actionCreators.updateCrossSellDetails(name, amount));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CrossSel);