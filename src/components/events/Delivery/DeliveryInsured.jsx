import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import {Link} from "react-router-dom"
import jQuery from 'jquery'
import {AMOUNT_INSURED} from '../../../constants/common-info'

import * as API from "../../../service/events/eventsInfo";

class DeliveryInsured extends Component {
    constructor(props){
        super(props);
    }
    validateDelivery(e, ref){
        if (jQuery('input[name="yes"]:checked').length != 0) {
            let radioValue = jQuery("input[name='yes']:checked").val();
            //jQuery('.btn.btn-primary').removeClass('in-active');
            if(radioValue === 'yes'){
                ref.props.updateInsuredAmount(AMOUNT_INSURED);
            } 
            // if(radioValue === 'no'){
            //     ref.props.updateInsuredAmount(0);
            // }
        } else{
            ref.props.updateInsuredAmount(0);
        }
    }

    addCartDetails(cart_list, e){
        e.preventDefault();
        var patronInfo = {}, cartItemList = [], deliveryMethod = {}, cartItemTotal = {}, remarks = '', total_price = 0, seat_price_currency = '';

        patronInfo = {"patronType":this.props.accountType,"accountNo":this.props.accountNo,"email":this.props.email,"password":"Rgmobiles@123","firstname":this.props.firstName,"lastName":this.props.lastName,"contacts":[{"contactType":"MOBILE", "areaCode":"+65", "phoneNumber":"93257860"}]}

        cart_list.length > 0 && (
            cart_list.map((cart_data, index) => {

                total_price += parseInt(cart_data.seat_price);
                var seat_price = cart_data.seat_price;
                seat_price_currency = cart_data.seat_price_currency;

                cartItemList.push({productId:cart_data.product_id, seatSectionId: cart_data.seat_section_id, priceCatId: cart_data.price_cat_id, mode: cart_data.mode, priceClassList: [{priceClassCode: cart_data.seat_row, promoPassword: '', quantity: 1, subTotal:{amount: parseInt(cart_data.seat_price), currency: cart_data.seat_price_currency}}], productTotal: {amount: parseInt(cart_data.seat_price), currency: cart_data.seat_price_currency}})
            })
        )

        deliveryMethod = {code: this.props.chosenAddon, charge: {amount: parseInt(this.props.chosenAddonPrice), currency:seat_price_currency}};

        cartItemTotal = {amount:total_price, currency:seat_price_currency}

        remarks= "";

        API.preOrder(patronInfo,cartItemList,deliveryMethod,cartItemTotal,remarks).then((response) =>{
            console.log("pre_order_res", response);
        })
    }
    render(){
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        let cart_list = this.props.cartInfo;

        console.log('cart_list',cart_list);

        return (
        <Fragment>
        <section className="container-fluid insured-sec">
            <div className="container mob-px-0">
                <h4 className="title-bdr">Do you want your tickets insured?</h4>
                <p className="lead mt-4">Protect the value of your tickets with Ticket Protector. It insures your tickets in case you and your companion(s) are unable to attend an event.</p>
                <ul className="list-inline insured-item">
                    <li className="list-inline-item">
                        <img src={process.env.PUBLIC_URL + "/assets/images/tick-gray.svg"} alt="icon"/>
                        Sickness or injury
                    </li>
                    <li className="list-inline-item">
                        <img src={process.env.PUBLIC_URL + "/assets/images/tick-gray.svg"} alt="icon"/>
                        Unexpected Overseas Business Trip
                    </li>
                    <li className="list-inline-item">
                        <img src={process.env.PUBLIC_URL + "/assets/images/tick-gray.svg"} alt="icon"/>
                        Accidents and more
                    </li>
                </ul>
                <div className="radio-btn-wrap mt-5">
                    <label className="custom-checkbox radio mr-3">
                        <input type="checkbox" name="yes" value="yes" onClick={(e) => this.validateDelivery(e, this, "e_ticket")}/> Yes, I want to <span className="label-primary">(SGD 10.00)</span>
                        <span className="checkmark"></span>
                    </label>
                    {/* <label className="custom-checkbox radio">
                        <input type="radio" name="yes" value="no"  onClick={(e) => this.validateDelivery(e, this, "e_ticket")}/> No thanks
                        <span className="checkmark"></span>
                    </label> */}
                </div>
                <p className="terms mt-3">By including Ticket Protector, you acknowledge that you have read the Policy Wording, that you understand Ticket Protector is offered by Allianz Global Assistance, and that it is underwritten by Tokio Marine Insurance Singapore Ltd. SISTIC shall not be liable for any loss, damage or expense in relation to Ticket Protector.</p>
                <p className="terms"><strong>* Ticket Protector is not applicable for DBS Paylah! and UnionPay payment method.</strong></p>

                {/* <div className="col-12 px-0 text-right pt-3 proceed-lg"> */}
                    {/* <button className="btn btn-primary">
                        Proceed to Payment
                        <img className="ml-3" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} />
                    </button> */}

                    {/* <Link to={`/payments/${iccCode}`} className="btn btn-primary in-active">Proceed to Payment
                        <img className="ml-3" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} /></Link> */}
                        
                    {/* <a href="javascript:;" className="btn btn-primary in-active" onClick={this.addCartDetails.bind(this,cart_list)}>Proceed to Payment
                        <img className="ml-3" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"}/></a> */}

                {/* </div> */}

            </div>
        </section>

        {/* <a href="javascript:;" className="btn btn-primary proceed" onClick={this.addCartDetails.bind(this,cart_list)}>
            Proceed to Payment
            <img className="ml-3" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"}/>
        </a> */}

        </Fragment>

        )
    }
}

const mapStateToProps = (state) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        patronName: state.patronName,
        email: state.email,
        firstName: state.firstName,
        lastName: state.lastName,
        accountNo: state.accountNo,
        accountType: state.accountType,        
        cartInfo: state.cartInfo,
        chosenAddon: state.chosenAddon,
        chosenAddonPrice: state.chosenAddonPrice,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        updateInsuredAmount: (amount) => {
            dispatch(actionCreators.updateInsured(amount));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(DeliveryInsured);
