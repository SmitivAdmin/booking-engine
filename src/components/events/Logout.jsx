import React, {Component, Fragment} from 'react'
import {Link} from "react-router-dom";
import {connect} from 'react-redux'
import Footer from '../common/Footer'
import Header from '../common/Header'


class Logout extends Component {

    constructor(props) {
        super(props);

    }
    render() {
        return (

            <Fragment>
                <Header/>

                <main>
                <section className="container-fluid logoutPage">
                    <div className="row">
                        <div className="container">
                            <span className="col-12 signoutText">You have signed out successfully.</span>
                            <a href="http://portaluat.sistic.com.sg" className={`btn check-out-btn`} id="confirm-type">Continue Browsing</a>
                        </div> 
                    </div>
                </section>               
                </main>

                <Footer/>
            </Fragment>
        );
    }
}


const mapStateToProps = (state) => {
    return state
};
//
// export default connect(mapStateToProps, (dispatch) => {
//         return {
//             fetchData: (dataObject) => {
//                 dispatch(actionCreators.fetchShowInfo(dataObject));
//             }
//         }
//     }
// )(Home);

export default connect(mapStateToProps)(Logout);

