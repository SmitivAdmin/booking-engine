import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import moment from 'moment';
import _ from "lodash";
import filter from "lodash/filter"
import {Link} from "react-router-dom"
import jQuery from 'jquery'
import {Redirect} from 'react-router'
import {
    SITE_URL,
    AVAILABILE,
    LIMITED_SEATS,
    COMING_SOON,
    SHOW_CANCELLED,
    SHOW_POSTPONED,
    SHOW_RECONFIG,
    SINGLE_SEATS,
    SOLD_OUT,
    TENANT,
    TICKETING_URL,
    monthNames
} from '../../../constants/common-info'

import he from 'he';
import HTMLParser from 'react-html-parser'
import * as API from "../../../service/events/eventsInfo"
import {Modal, Button} from 'react-bootstrap';
import CalendarView from './CalendarView'

class ShowDateTime extends Component {

    constructor(props) {
        super(props);

        this.state ={
            noticeModel:false,
            productAdvisory:'',
            groupBookShowTime:0,
            groupBookOverview: 0,
            groupseatSelection: 0,
            reservedFrndError: '',
            errorMsg: ''
        }
    }

    toggleNoticeModel = (boolean) =>{
        // If true model will appear else not.
        
        this.setState({noticeModel: boolean});
    }

    async chosenTime(timeSelected, chooseProductId, e) {
        e.preventDefault();
        //alert(chooseProductId);
        jQuery('.time-sec > date-item').removeClass('active');
        e.currentTarget.classList.add('active');
        this.props.chosenProductTime(timeSelected, chooseProductId);

        if(chooseProductId != "" && chooseProductId!= undefined){
            var product_id = chooseProductId;   
            let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';

            var group_booking = window.location.search;
            var searchParams = new URLSearchParams(group_booking);
            var bookingCode = searchParams && searchParams.get('groupBookingCode');
            if(bookingCode){
                bookingCode = bookingCode;
            } else{
                bookingCode = '';
            }

            console.log('prdTicketType',this.props.prdTicketType);

            API.setProductId(product_id, iccCode, bookingCode).then(response =>{
                console.log('set_product_id', response);
                if(response && response === 200){    
                    if(bookingCode !==  ""){
                        console.log('bookingCode', bookingCode);
                        API.getSeatAvailableGroup(product_id, group_booking).then(response => {
                            if (response !== undefined) {
                                console.log('group_available',response);
                                // $("#seatDesiableToggle").css("display", "none");
                                // $(".seat-img.selected").removeClass("selected");
                                if(response && response.length > 0){
                                    var price_cat_id = response[0].priceCategoryId;
                                    var seat_section_id = response[0].seatSectionId;

                                    API.catSection(price_cat_id, seat_section_id, 'SP', false).then(async response => {
                                        console.log('cat_section', response);
                                        if(response && response.message === 200){
                                        }
                                        await API.getSeatAvailable(product_id, price_cat_id, seat_section_id, 0, 'SP', bookingCode).then(response => {
                                            console.log('availablity_section', response);
                                            if(response && response.status === 200){
                                                console.log('group_seat_selection', response);
                                                jQuery(".pictarea").addClass('pointer_event');
                                                this.props.fetchSeatSelection(response.resObj);
                                            }
            
                                        });
        
                                    });
                                }
                                
                            }
                        });
                    }                            
                }
            });

            API.getSeatMapOverView(product_id).then(async response =>{
                console.log("overview_response", response);
                if(response && response.seatMapInfo.mode === "SP"){

                    jQuery(".filter-level img").removeClass('filter_cls');
                    jQuery(".filter-level .tooltip").removeClass('active');
                    // let stage_view = '';
                    // this.props.fetchSeatSelection(stage_view);
                    jQuery('.seat-map').removeClass('hide');

                    this.props.updateEventType('RS');

                    this.props.fetchSeatMapOverview(response);
                    
                    var top_position = jQuery('#seatMapTtl').offset();
                    jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
    
                    await API.getPriceTable(product_id).then(response =>{
                        this.props.fetchPriceTabele(response);
                    });
                    
                } else if(response && response.seatMapInfo.mode === "BA"){

                    this.props.updateEventType('GA');

                    this.props.fetchSeatMapOverview(response);
                    var priceCategoryId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].priceCategoryId;
                    var seatSectionId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].seatSectionId;
                    var mode = "GA";
                    var isMembership = response.seatMapInfo.isMembership === 0 ? false : true;

                    await API.catSection(priceCategoryId, seatSectionId, mode, isMembership, this.props.isEventWithSurvey).then(response => {
                        console.log('cat_section', response);
                    })

                    await API.ticketType(product_id, priceCategoryId, this.props.prdTicketType).then(response => {
                        console.log("Ticket Type Fetched Successfully", response.data);
                        if(response.status === 200){
                            this.props.updateTicketType(response.data);                    
                        }
    
                        jQuery('.ticket-quantity-upgraded').removeClass('hide');
                        setTimeout(function(){
                            var top_position = jQuery('.ticket-quantity-upgraded').offset();
                            jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
                        }, 1000);
                        
                    });
                } else{
                    //alert("Error Occuring!");
                    let ErrorMsg = response && response.seatMapInfo && response.seatMapInfo.error_description;
                    console.log("chooseTime",ErrorMsg);
                    this.setState({errorMsg: ErrorMsg},()=>{
                        this.toggleNoticeModel(true);
                    })
                }
            }); 
        } 
       
    }

    render() {
        return (
        <Fragment>
             <div className="container mt-4">
                <h4 className="title-bdr" id="selectTimeId">Select a time</h4>
                <div className="time-sec col-md-12 mt-4">
                    <div className="clearfix">
                        {                        
                        this.props.showTimes && this.props.showTimes.length > 0 && this.props.showTimes.map((time, index) => (
                            <Fragment>
                                {index<=6 &&  <div className="date-item" id={`chosen-time`} onClick={this.chosenTime.bind(this, time.show_time, time.product_id)}>
                                    <span className="time">{time.show_time}</span>
                                    <span className="status">{time.availabilityStatus}</span>
                                </div>}
                                {index >=7 &&  <div className="date-item hide">
                                    <span className="time">{time.show_time}</span>
                                    <span className="status">{time.availabilityStatus}</span>
                                </div>}
                            </Fragment>
                            )
                        )}
                    </div>
                    {
                    this.props.showTimes && this.props.showTimes.length > 7 &&
                    <div className="clearfix text-center mt-3">
                        <button className="btn btn-primary view-all" id ="view_all_timings" onClick={this.displayAll.bind(this)}>
                            View All Timings
                            <img className="ml-1"
                                    src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"}
                                    alt="icon"/>
                        </button>
                    </div>
                    }
                    {/* {
                    this.props.showTimes && this.props.showTimes.length < 7 &&
                    <div className="clearfix text-center mt-3">
                        <button className="btn btn-primary btn-rounded view-all less in-active" onClick={this.displayAll.bind(this)}>
                            View All Timings
                            <img className="ml-1"
                                    src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"}
                                    alt="icon"/>
                        </button>
                    </div>
                    } */}

                </div>
            </div>
        </Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        productsObj: state.showInfo && state.showInfo.productsObj && state.showInfo.productsObj,
        totalQuantity: state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        adult_qty: state.adult_qty,
        child_qty: state.child_qty,
        nsf_qty: state.nsf_qty,
        sectz_qty: state.sectz_qty,
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        showTimeFlag: state.showTimeFlag,
        showCalFlag: state.showCalFlag,
        hideDefaultDateView: state.hideDefaultDateView,
        showTimes: state.showTimes,
        showActive: state.showActive,
        chosenProductId: state.chosenProductId,
        timeChosen: state.timeChosen,
        prdTicketType: state.prdTicketType,
        isEventWithSurvey: state.isEventWithSurvey
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        chosenProductTime: (time, chooseProductId) => {
            dispatch(actionCreators.chooseTime(time, chooseProductId));
        },
        fetchSeatMapOverview: (dataObject) => {
            dispatch(actionCreators.fetchSeatMapInfo(dataObject));
        },
        fetchPriceTabele: (dataObject) => {
            dispatch(actionCreators.fetchPriceTabele(dataObject));
        },
        fetchSeatSelection: (dataObject) => {
            dispatch(actionCreators.fetchSeatSelInfo(dataObject));           
        },
        updateTicketType: (list) => {
            dispatch(actionCreators.updateTicketType(list));
        },
        updateEventType: (event_type) => {
            dispatch(actionCreators.updateEventType(event_type));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowDateTime);