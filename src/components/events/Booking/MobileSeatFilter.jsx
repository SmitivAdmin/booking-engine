import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../actions/actionCreator"
import {SITE_URL} from '../../constants/common-info'
import * as API from "../../service/events/eventsInfo"

import $ from 'jquery';

class MobileSeatFilter extends Component {
    constructor(props){
        super(props);
    }

    getSeatColorCode(price_cat_num){
        switch(price_cat_num){
            case 1:
                return "#FECD84";
            case 2:
                return "#A5C7F5";
            case 3:
                return "#71BA64";
            case 4:
                return "#D097F0";
            case 5:
                return "#F7F19F";
            case 6:
                return "#B5EDF3";
            case 7:
                return "#D9C297";
            case 8:
                return "#A6F0B9";
            case 9:
                return "#FB9CC0";
            case 10:
                return "#D6CC1B";
            case 11:
                return "#929E92";
            case 12:
                return "#4AD1B1";
            case 13:
                return "#8C65A0";
            case 14:
                return "#A80717";
            case 15:
                return "#FF0049";
            case 16:
                return "#D89D61";
            case 17:
                return "#3D7717";
            case 18:
                return "#AE9FD6";
            case 19:
                return "#EC826A";
            case 20:
                return "#FFC7CE";
            default:
                return "#feec66";
        }
    }

    getAvailabity(availability_status){
        switch(availability_status){
            case 0:
                return "Sold Out";
            case 1:
                return "Available";
            case 2:
                return "Limited Seats";
            case 3:
                return "Single Seat";
        }
    }

    getSeats(price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode){
        if(this.props.chosenProductId != "" && this.props.chosenProductId != undefined){
            var product_id = this.props.chosenProductId;
        } else{
            var product_id = 1080755;
        }
        var product_id = 1080755;

        var qty = this.props.totalQuantity
        if(qty){
            qty = qty
        } else{ 
            qty = 1;
        }

        API.getSeatSelection(product_id, price_category_id, seat_section_id, mode, qty, price_cat_alias, seat_section_alias, seat_section_type, price_amount, price_currency, price_formatted).then(response =>{
            if(response){
                this.props.fetchSeatSelection(response);
            }
            
        });

    }

    async getSeatsAll(seats_overview, e){
        $(".filter-encl").removeClass('actv_cls');

        e.currentTarget.parentElement.parentElement.classList.add('actv_cls');

        //document.getElementsByClassName("stage_loader")[0].classList.remove('hide');

        var qty = this.props.totalQuantity
        if(qty){
            qty = qty
        } else{
            qty = 1;
        }

        if(this.props.chosenProductId != "" && this.props.chosenProductId != undefined){
            var product_id = this.props.chosenProductId;

            var result_arr = [];
            var i = 1;

            for(const item of seats_overview){
                //console.log("stalls_item",item)
                await API.getSeatSelection(product_id, item.price_category_id, item.seat_section_id, item.mode, qty, item.price_cat_alias, item.seat_section_alias, item.seat_section_type, item.price_amount, item.price_currency, item.price_formatted, item.seat_level_alias).then(response =>{      
                    //console.log('stalls_response', response)              
                    if(response != undefined){
                        //console.log('stalls_response', response)
                        result_arr.push(response)
                        //this.props.fetchSeatSelection(response);
                    }
                    if(i === seats_overview.length){
                        //console.log('stalls_overview',result_arr);
                        this.props.fetchSeatStageView(result_arr);
                    }
                });

                i++;
            }
        }
    }

    showTooltip(text, evt) {
        //console.log('evt',evt.pageY);
        let tooltip = document.getElementById("seat_tooltip");
        let getDivPst = $(".ticket-quantity").offset();
        var divTop = getDivPst.top;
        
        tooltip.innerHTML = text;
        tooltip.style.display = "block";
        tooltip.style.top = (evt.pageY - 185 - divTop) + 'px';
    }
    
    hideTooltip() {
        var tooltip = document.getElementById("seat_tooltip");
        tooltip.style.display = "none";
    }

    showFilter(e){
        e.preventDefault();
        $(".filter-mobile .filter-level").toggle();
    }

    render(){

        let imageURL = '', imageAvailable = '', seatSectionList = [], availabilityList = [], cat_amount_arr = [], cat_amount_arr2 = [], cat_amount_arr3 = [], price_cat_currency = 'SGD', mode = '', stalls_overview = [], stall_map = new Map(), dress_overview = [], dress_map = new Map(), grand_overview = [], grand_map = new Map();

        var seat_level_alias = '';

        if(this.props.seatMapInfo){
            imageURL =  this.props.seatMapInfo.imageURL;
            imageAvailable =  this.props.seatMapInfo.imageAvailable;
            seatSectionList =  this.props.seatMapInfo.seatSectionList;
            mode = this.props.seatMapInfo.mode;
        }
        if(this.props.availabilityObj){
            availabilityList =  this.props.availabilityObj;
            //console.log('availabilityList',availabilityList)
        }

        return (
        <Fragment>
        <section class="filter-mobile filter-level-inner">
            <div class="container">
                <div class="row mt-3">
                    <div class="filter-layers" onClick={this.showFilter.bind(this)}>
                        <i class="flaticon-layers" aria-hidden="true"></i>
                        <p class="mb-0">Levels</p>
                    </div>
                    <div class="filter-slider-sec">
                        <div class="filter-layer-horizontal owl-carousel owl-theme" id="filter-layer-slider">
                                <div class="item">
                                    <div class="all-sec filter-main all-filter">
                                        <div class="all-color">
                                            <div class="all-color-inner">
                                                <span class="red"></span>
                                                <span class="blue"></span>
                                            </div>
                                            <div class="all-color-inner">
                                                <span class="yellow"></span>
                                                <span class="green"></span>
                                            </div>
                                        </div>
                                        <p>All</p>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="filter-category-txt filter-main">
                                        <div class="filter-category-inner">
                                            <span class="red"></span>
                                            <p>VIP</p>
                                        </div>
                                        <div class="filter-category-right">
                                            <p>SGD 258</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="filter-category-txt filter-main">
                                        <div class="filter-category-inner">
                                            <span class="navy"></span>
                                            <p>A Reserve</p>
                                        </div>
                                        <div class="filter-category-right">
                                            <p>SGD 228</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="filter-category-txt filter-main">
                                        <div class="filter-category-inner">
                                            <span class="blue"></span>
                                            <p>B Reserve</p>
                                        </div>
                                        <div class="filter-category-right">
                                            <p>SGD 198</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="filter-category-txt filter-main">
                                        <div class="filter-category-inner">
                                            <span class="green"></span>
                                            <p>C Reserve</p>
                                        </div>
                                        <div class="filter-category-right">
                                            <p>SGD 148</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="filter-category-txt filter-main">
                                        <div class="filter-category-inner">
                                            <span class="yellow"></span>
                                            <p>D Reserve</p>
                                        </div>
                                        <div class="filter-category-right">
                                            <p>SGD 118</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="filter-category-txt filter-main">
                                        <div class="filter-category-inner">
                                            <span class="brown"></span>
                                            <p>E Reserve</p>
                                        </div>
                                        <div class="filter-category-right">
                                            <p>SGD 88</p>
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>

                <div class="filter-level">
                    <a href="javascript:;" class="col-md-12 filter-encl">
                        <div class="filter-img-sec">
                            {/* <img src={process.env.PUBLIC_URL + "/assets/images/grand_circle.svg"} alt="Grand circle" class="img-fluid grand-circle" data-newsrc={process.env.PUBLIC_URL + "/assets/images/grand_circle_bg.svg"}  data-oldsrc={process.env.PUBLIC_URL + "/assets/images/grand_circle.svg"} /> */}

                            <svg viewBox="370 350 350 58" className="svg-content"  width="98%">
                            {
                                seatSectionList && seatSectionList.length > 0 && seatSectionList.map((seat_seaction, index) => (


                                    seat_seaction.SeatLevel.map((seat_level, level_index) => {


                                        if((seat_level.seatLevelAlias.toLowerCase() === "circle 1" || seat_level.seatLevelAlias.toLowerCase() === "grand circle") && seat_level.coordinatesList && seat_level.price.amount > 0){
                                            var price_cat_alias = seat_seaction.priceCatAlias;
                                            var price_cat_amount = seat_seaction.priceCatAmount.amount;
                                            var price_cat_currency = seat_seaction.priceCatAmount.currency;
                                            var price_cat_formatted = seat_seaction.priceCatAmount.formatted;

                                            var price_cat_clr = this.getSeatColorCode(seat_level.priceCategoryNum);

                                            seat_level_alias = seat_level.seatLevelAlias;
                                            var seat_section_id = seat_level.seatSectionId;
                                            var seat_section_alias = seat_level.seatSectionAlias;
                                            var seat_section_type = seat_level.seatSectionType;
                                            var seat_entrance = seat_level.seatEntrance;
                                            var price_category_num = seat_level.priceCategoryNum;
                                            var price_category_id = seat_level.priceCategoryId;
                                            var price_amount = seat_level.price.amount;
                                            var price_currency = seat_level.price.currency;
                                            var price_formatted = seat_level.price.formatted;
                                            var avail_status = '';

                                            var availability_text = '';
                                            var avail_clr = '';

                                            availabilityList.length > 0 && availabilityList.map((availability, sect_index) => {
                                                if(availability.seatSectionId === seat_section_id){

                                                    var avail_status = availability.seatSectionAvailability;
                                                    if(avail_status === 0){
                                                        avail_clr = 'red';
                                                    }
                                                    if(avail_status === 1){
                                                        avail_clr = 'green';
                                                    }
                                                    if(avail_status === 2){
                                                        avail_clr = 'blue';
                                                    }
                                                    if(avail_status === 3){
                                                        avail_clr = 'brown';
                                                    }
                                                    availability_text = this.getAvailabity(avail_status);

                                                    console.log('availability_text', availability_text)
                                                }
                                            })
                                            


                                            cat_amount_arr.push(price_amount);

                                            if(!grand_map.has(price_category_id) || !grand_map.has(seat_section_id)){
                                                grand_map.set(price_category_id, true); 
                                                grand_map.set(seat_section_id, true); 

                                                grand_overview.push({price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode, seat_level_alias})
                                            }

                                            return(<Fragment>
                                                {/* <g className="blockStyle" 
                                                data-product_id={this.props.chosenProductId} data-seat_level={seat_level_alias} data-seat_section_id={seat_section_id} data-price_category_id={price_category_id}
                                                onMouseMove={this.showTooltip.bind(this, 'Category: '+price_cat_alias+'<br />Name: '+seat_section_alias+'<br />Type: '+seat_section_type+'<br />Price: '+price_currency+' '+price_formatted.replace('$', '')+'<br /><center style="color:'+avail_clr+'">'+availability_text+'</center>')} 
                                                onMouseOut={this.hideTooltip.bind(this)} 
                                                onClick={this.getSeats.bind(this,price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode, seat_level_alias)}>
                                                    <polygon points={seat_level.coordinatesList} className="poly" style={{fill: price_cat_clr}}></polygon>
                                                </g> */}

                                            <g className="blockStyle" 
                                                data-product_id={this.props.chosenProductId} data-seat_level={seat_level_alias} data-seat_section_id={seat_section_id} data-price_category_id={price_category_id}
                                                onMouseMove={this.showTooltip.bind(this, 'Category: '+price_cat_alias+'<br />Name: '+seat_section_alias+'<br />Type: '+seat_section_type+'<br />Price: '+price_currency+' '+price_formatted.replace('$', '')+'<br /><center style="color:'+avail_clr+'">'+availability_text+'</center>')} 
                                                onMouseOut={this.hideTooltip.bind(this)}>
                                                    <polygon points={seat_level.coordinatesList} className="poly" style={{fill: price_cat_clr}}></polygon>
                                            </g> 
                                            </Fragment>)
                                        } else{
                                            return('')
                                        }

                                    })
                                )
                            )}
                            </svg>
                            <div className="map_view grand-circle" onClick={this.getSeatsAll.bind(this, grand_overview)}></div>

                        </div>
                        <div class="filter-content-sec" onClick={this.getSeatsAll.bind(this, grand_overview)}>
                            <div class="filter-content-txt">
                                <p>{seat_level_alias}</p>
                                {/* {console.log('cat_amount_arr', cat_amount_arr)} */}
                                <p>{price_cat_currency} {Math.min(...cat_amount_arr)} - {price_cat_currency} {Math.max(...cat_amount_arr)}</p>  
                            </div>
                            
                        </div>
                    </a>
                    <a href="javascript:;" class="col-md-12 filter-encl">
                        <div class="filter-img-sec">
                            {/* <img src={process.env.PUBLIC_URL + "/assets/images/grand_circle.svg"} alt="Grand circle" class="img-fluid grand-circle" data-newsrc={process.env.PUBLIC_URL + "/assets/images/grand_circle_bg.svg"}  data-oldsrc={process.env.PUBLIC_URL + "/assets/images/grand_circle.svg"} /> */}

                            <svg viewBox="390 260 560 58" className="svg-content"  width="98%">
                            {
                                seatSectionList && seatSectionList.length>0 && seatSectionList.map((seat_seaction, index) => (
                                    seat_seaction.SeatLevel.map((seat_level, level_index) => {

                                        if((seat_level.seatLevelAlias.toLowerCase() === "circle 2" || seat_level.seatLevelAlias.toLowerCase() === "dress circle") && seat_level.coordinatesList && seat_level.price.amount > 0){
                                            var price_cat_clr = this.getSeatColorCode(seat_level.priceCategoryNum);

                                            var price_cat_alias = seat_seaction.priceCatAlias;
                                            var price_cat_amount = seat_seaction.priceCatAmount.amount;
                                            var price_cat_currency = seat_seaction.priceCatAmount.currency;
                                            var price_cat_formatted = seat_seaction.priceCatAmount.formatted;

                                            seat_level_alias = seat_level.seatLevelAlias;
                                            var seat_section_id = seat_level.seatSectionId;
                                            var seat_section_alias = seat_level.seatSectionAlias;
                                            var seat_section_type = seat_level.seatSectionType;
                                            var seat_entrance = seat_level.seatEntrance;
                                            var price_category_num = seat_level.priceCategoryNum;
                                            var price_category_id = seat_level.priceCategoryId;
                                            var price_amount = seat_level.price.amount;
                                            var price_currency = seat_level.price.currency;
                                            var price_formatted = seat_level.price.formatted;

                                            var availability_text = '';
                                            var avail_clr = '';

                                            availabilityList.length > 0 && availabilityList.map((availability, sect_index) => {
                                                if(availability.seatSectionId === seat_section_id){

                                                    var avail_status = availability.seatSectionAvailability;
                                                    if(avail_status === 0){
                                                        avail_clr = 'red';
                                                    }
                                                    if(avail_status === 1){
                                                        avail_clr = 'green';
                                                    }
                                                    if(avail_status === 2){
                                                        avail_clr = 'blue';
                                                    }
                                                    if(avail_status === 3){
                                                        avail_clr = 'brown';
                                                    }
                                                    availability_text = this.getAvailabity(avail_status);

                                                    console.log('availability_text', availability_text)
                                                }
                                            })
                                            

                                            cat_amount_arr2.push(price_amount);

                                            if(!dress_map.has(price_category_id) || !dress_map.has(seat_section_id)){
                                                dress_map.set(price_category_id, true); 
                                                dress_map.set(seat_section_id, true); 

                                                dress_overview.push({price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode, seat_level_alias})
                                            }


                                            return(<Fragment>
                                                {/* <g className="blockStyle" 
                                                data-product_id={this.props.chosenProductId} data-seat_level={seat_level_alias} data-seat_section_id={seat_section_id} data-price_category_id={price_category_id}
                                                onMouseMove={this.showTooltip.bind(this, 'Category: '+price_cat_alias+'<br />Name: '+seat_section_alias+'<br />Type: '+seat_section_type+'<br />Price: '+price_currency+' '+price_formatted.replace('$', '')+'<br /><center style="color:'+avail_clr+'">'+availability_text+'</center>')} onMouseOut={this.hideTooltip.bind(this)} onClick={this.getSeats.bind(this,price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode)}>
                                                    <polygon points={seat_level.coordinatesList} className="poly" style={{fill: price_cat_clr}}></polygon>
                                                </g> */}
                                                <g className="blockStyle" 
                                                data-product_id={this.props.chosenProductId} data-seat_level={seat_level_alias} data-seat_section_id={seat_section_id} data-price_category_id={price_category_id}
                                                onMouseMove={this.showTooltip.bind(this, 'Category: '+price_cat_alias+'<br />Name: '+seat_section_alias+'<br />Type: '+seat_section_type+'<br />Price: '+price_currency+' '+price_formatted.replace('$', '')+'<br /><center style="color:'+avail_clr+'">'+availability_text+'</center>')} onMouseOut={this.hideTooltip.bind(this)}>
                                                    <polygon points={seat_level.coordinatesList} className="poly" style={{fill: price_cat_clr}}></polygon>
                                                </g>
                                            </Fragment>)
                                        } else{
                                            return('')
                                        }

                                    })
                                )
                            )}
                            </svg>
                            <div className="map_view dress-circle" onClick={this.getSeatsAll.bind(this, dress_overview)}></div>                    
                        </div>
                        <div class="filter-content-sec" onClick={this.getSeatsAll.bind(this, dress_overview)}>
                            <div class="filter-content-txt">
                                <p>{seat_level_alias}</p>
                                {console.log('cat_amount_arr2', cat_amount_arr2)}
                                <p>{price_cat_currency} {Math.min(...cat_amount_arr2)} - {price_cat_currency} {Math.max(...cat_amount_arr2)}</p>  
                            </div>                            
                        </div>
                    </a>
                    <a href="javascript:;" class="col-md-12 filter-encl">
                        <div class="filter-img-sec">
                            {/* <img src={process.env.PUBLIC_URL + "/assets/images/grand_circle.svg"} alt="Grand circle" class="img-fluid grand-circle" data-newsrc={process.env.PUBLIC_URL + "/assets/images/grand_circle_bg.svg"}  data-oldsrc={process.env.PUBLIC_URL + "/assets/images/grand_circle.svg"} /> */}

                            <svg viewBox="370 820 750 58" className="svg-content"  width="98%">
                            {
                                seatSectionList && seatSectionList.length>0 && seatSectionList.map((seat_seaction, index) => (
                                        seat_seaction.SeatLevel.map((seat_level, level_index) => {

                                            // if((seat_level.seatLevelAlias === "Stalls" || seat_level.seatLevelAlias === "Foyer Stalls") && seat_level.coordinatesList && seat_level.price.amount > 0){
                                            if((seat_level.seatLevelAlias.toLowerCase() === "stalls") && seat_level.coordinatesList && seat_level.price.amount > 0){
                                                var price_cat_clr = this.getSeatColorCode(seat_level.priceCategoryNum);

                                                var price_cat_alias = seat_seaction.priceCatAlias;
                                                var price_cat_amount = seat_seaction.priceCatAmount.amount;
                                                var price_cat_currency = seat_seaction.priceCatAmount.currency;
                                                var price_cat_formatted = seat_seaction.priceCatAmount.formatted;

                                                seat_level_alias = seat_level.seatLevelAlias;
                                                var seat_section_id = seat_level.seatSectionId;
                                                var seat_section_alias = seat_level.seatSectionAlias;
                                                var seat_section_type = seat_level.seatSectionType;
                                                var seat_entrance = seat_level.seatEntrance;
                                                var price_category_num = seat_level.priceCategoryNum;
                                                var price_category_id = seat_level.priceCategoryId;
                                                var price_amount = seat_level.price.amount;
                                                var price_currency = seat_level.price.currency;
                                                var price_formatted = seat_level.price.formatted;

                                                var availability_text = '';
                                                var avail_clr = '';

                                                availabilityList.length > 0 && availabilityList.map((availability, sect_index) => {
                                                    if(availability.seatSectionId === seat_section_id){

                                                        var avail_status = availability.seatSectionAvailability;
                                                        if(avail_status === 0){
                                                            avail_clr = 'red';
                                                        }
                                                        if(avail_status === 1){
                                                            avail_clr = 'green';
                                                        }
                                                        if(avail_status === 2){
                                                            avail_clr = 'blue';
                                                        }
                                                        if(avail_status === 3){
                                                            avail_clr = 'brown';
                                                        }
                                                        availability_text = this.getAvailabity(avail_status);

                                                        console.log('availability_text', availability_text)
                                                    }
                                                })
                                                

                                                cat_amount_arr3.push(price_amount);
                                                
                                                if(!stall_map.has(price_category_id) || !stall_map.has(seat_section_id)){
                                                    stall_map.set(price_category_id, true); 
                                                    stall_map.set(seat_section_id, true); 

                                                    stalls_overview.push({price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode, seat_level_alias})
                                                }

                                                //stalls_overview.push({price_cat_alias, seat_level_alias, seat_section_id, price_category_id, price_amount, price_currency, mode})

                                                return(<Fragment>
                                                    {/* {seat_level.coordinatesList} */}
                                                    {/* <g className="blockStyle" 
                                                    data-product_id={this.props.chosenProductId} data-seat_level={seat_level_alias} data-seat_section_id={seat_section_id} data-price_category_id={price_category_id}
                                                    onMouseMove={this.showTooltip.bind(this, 'Category: '+price_cat_alias+'<br />Name: '+seat_section_alias+'<br />Type: '+seat_section_type+'<br />Price: '+price_currency+' '+price_formatted.replace('$', '')+'<br /><center style="color:'+avail_clr+'">'+availability_text+'</center>')} onMouseOut={this.hideTooltip.bind(this)} onClick={this.getSeats.bind(this,price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode, seat_level_alias)}>
                                                        <polygon points={seat_level.coordinatesList} className="poly" style={{fill: price_cat_clr}}></polygon>
                                                    </g> */}

                                                    <g className="blockStyle" 
                                                    data-product_id={this.props.chosenProductId} data-seat_level={seat_level_alias} data-seat_section_id={seat_section_id} data-price_category_id={price_category_id}
                                                    onMouseMove={this.showTooltip.bind(this, 'Category: '+price_cat_alias+'<br />Name: '+seat_section_alias+'<br />Type: '+seat_section_type+'<br />Price: '+price_currency+' '+price_formatted.replace('$', '')+'<br /><center style="color:'+avail_clr+'">'+availability_text+'</center>')} 
                                                    onMouseOut={this.hideTooltip.bind(this)}>
                                                        <polygon points={seat_level.coordinatesList} className="poly" style={{fill: price_cat_clr}}></polygon>
                                                    </g>
                                                </Fragment>)
                                            } else{
                                                return('')
                                            }

                                        })
                                    )
                                )}
                            </svg>
                            <div className="map_view stall-circle" onClick={this.getSeatsAll.bind(this, stalls_overview)}></div>
                    
                        </div>
                        <div class="filter-content-sec" onClick={this.getSeatsAll.bind(this, stalls_overview)}>
                            <div class="filter-content-txt">
                                <p>{seat_level_alias}</p>
                                {console.log('cat_amount_arr3', cat_amount_arr3)}
                                <p>{price_cat_currency} {Math.min(...cat_amount_arr3)} - {price_cat_currency} {Math.max(...cat_amount_arr3)}</p> 
                            </div>
                            
                        </div>
                    </a>
                </div>
            </div>
        </section>
        </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    console.log('seat_state',state)
    return {
        seatMapInfo: state.seatMapInfo.seatMapInfo,
        availabilityObj: state.seatMapInfo.availabilityObj,
        chosenProductId: state.chosenProductId,
        totalQuantity:  state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSeatSelection: (dataObject) => {
            dispatch(actionCreators.fetchSeatSelInfo(dataObject));
        },
        fetchSeatStageView: (dataObject) => {
            dispatch(actionCreators.fetchSeatStageViewInfo(dataObject));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MobileSeatFilter);
