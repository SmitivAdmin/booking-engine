import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"

import {SITE_URL} from '../../../constants/common-info'
import {Link} from "react-router-dom"
import TimeCounter from './../TimeCounter'

import he from 'he';
import HTMLParser from 'react-html-parser'

import $ from 'jquery';
import _ from "lodash";

class CartDetails extends Component {
    constructor(props){
        super(props);
    }

    delCart(evt, index, inventry_id, slct_seat_type, qtyIndex){  
        // Remove seat selection from UI based on inventry_id.
        inventry_id.map((item, index)=>{
            $(`#seatno_${item}`).removeClass("selected");
        }); 
        
        // Update the selected ticket type based on deletion Qty.
        var qtyField = this.props.ticketTypesSelected[qtyIndex][Object.keys(this.props.ticketTypesSelected[qtyIndex])[1]];
        var id ="quantity-"+qtyField;

        var temp = this.props.ticketTypesSelected;
        
        if(_.find(temp,function(o) { return o.key == qtyField} )) {
            _.find(temp,function(o) { return o.key == qtyField} )[qtyField] = _.find(temp,function(o) { return o.key == qtyField})[qtyField] - (inventry_id.length);
        }

        // Update the (quantity, currentQuantity, cart/ReservedCart) in REDUX.
        this.props.addQuantity(this.props.currentQuantity, inventry_id.length*-1);
        this.props.updateQtyData(temp, this.props.currentQuantity, inventry_id.length*-1);
        this.props.removeMultiCartInfo(index, false);

        // Disable the Quantity INC/DEC button based on Current quantity.
        setTimeout(function(){
            if(document.getElementById(id).value > 0){
                $("#qtyPrcCls"+qtyField).addClass('active');
                $("#cart-qty-minus"+qtyField).removeClass('in-active');
            } else{
                $("#qtyPrcCls"+qtyField).removeClass('active');
                $("#cart-qty-minus"+qtyField).addClass('in-active');
            }
        }, 500);
    }

    delCartGA(evt, index, qtyIndex, qty_type){
        var qtyField, temp;
        // Update the selected ticket type based on deletion Qty.
        if(qty_type === "Normal"){
            qtyField = this.props.ticketTypesSelected[qtyIndex][Object.keys(this.props.ticketTypesSelected[qtyIndex])[1]];
            temp = this.props.ticketTypesSelected;
        } else{
            qtyField = this.props.reservedCartInfo[qtyIndex][Object.keys(this.props.reservedCartInfo[qtyIndex])[1]];
            temp = this.props.reservedCartInfo;
        }
        console.log('qtyField',qtyField);
        var id ="quantity-"+qtyField;
        
        if(_.find(temp,function(o) { return o.key == qtyField} )) {
            _.find(temp,function(o) { return o.key == qtyField} )[qtyField] = _.find(temp,function(o) { return o.key == qtyField})[qtyField] - (index.length);
        }

        // Update the (quantity, currentQuantity, cart/ReservedCart) in REDUX.
        this.props.addQuantity(this.props.currentQuantity, index.length*-1);
        if(qty_type === "Normal"){
            this.props.updateQtyData(temp, this.props.currentQuantity, index.length*-1);
        } else{
            document.getElementById(id).value = document.getElementById(id).value - index.length;
            this.props.addReservedCartInfoGA(temp, this.props.currentQuantity, -1);
        }
        this.props.removeMultiCartInfo(index, false);

        // Disable the Quantity INC/DEC button based on Current quantity.
        setTimeout(function(){
            if(document.getElementById(id).value > 0){
                $("#qtyPrcCls"+qtyField).addClass('active');
                $("#cart-qty-minus"+qtyField).removeClass('in-active');
            } else{
                $("#qtyPrcCls"+qtyField).removeClass('active');
                $("#cart-qty-minus"+qtyField).addClass('in-active');
            }
        }, 500);
    }

    delFrndCart(evt, index, inventry_id, slct_seat_type){  
        debugger
        inventry_id.map((item, index)=>{
            $(`#seatno_${item}`).removeClass("selected");
        });

        this.props.removeMultiCartInfo(index, true);
    }


    viewDetDesk(e){
        e.preventDefault();
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".d-md-block.sticky .total-sec, .d-md-block.sticky .ticket-detail-sec, .d-md-block.sticky .view_ticket, .d-md-block.sticky .close_ticket, .d-md-block.sticky .stickyTotal").toggleClass('hide');
        $(".d-md-block.sticky .media-body").toggleClass('mt-0');
        
    }
    viewDetMob(e){
 
        e.preventDefault();
       
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".mob-tic-info").slideToggle();
    }

    scrollToDate() {
        // Note - this works well on the small screens
        //$('html,body').animate({scrollTop: $(".title-bdr").offset().top},'slow');
        var top_position = $('.title-bdr').offset();
        $('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
    }



    render(){
        let cart_list = 0;
        //console.log('eventInfo',this.props.eventInfo && this.props.eventInfo);

        let title = this.props.eventInfo?this.props.eventInfo.title: '';
        let venue =  this.props.eventInfo? this.props.eventInfo.venue: undefined;
        let summaryImagePath = this.props.eventInfo?this.props.eventInfo.summaryImagePath: undefined;

        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';

        let ticketTypes = this.props.ticketType;

        var productsObj = this.props.productsObj ? this.props.productsObj.showTimingList : '';
        console.log('productsObj',productsObj);

        title = productsObj && productsObj.length > 0 ? productsObj[0].showTitle : '';
        venue =  productsObj && productsObj.length > 0 ? productsObj[0].venue : '';

        console.log('show_title',title);

        // Reserved Cart data manipulation start
        let reservedCart_list = [];

        let reserverCartQty = this.props.reservedCartInfo.length;
        let resstartingQty = 0;

        console.log('cart_ticketTypes',ticketTypes);

        if (ticketTypes === "RS") {
            if(reserverCartQty > 0){
                let lclCart = {type: "-",
                    quantity: reserverCartQty,
                    seat_no: [],
                    inventory_id: [],
                    selected_index : []
                    };
                for(let j=0;j<reserverCartQty;j++){
                    let cartDetails = this.props.reservedCartInfo[j+resstartingQty]
                    lclCart.seat_no.push(cartDetails.seat_no);
                    lclCart.inventory_id.push(cartDetails.inventory_id);
                    lclCart.selected_index.push(j+resstartingQty);

                    if(j===reserverCartQty-1){
                            lclCart.seat_price= 0.00;
                            lclCart.seat_price_currency= cartDetails.seat_price_currency;
                            lclCart.seat_level=cartDetails.seat_level;
                            lclCart.price_cat_alias= cartDetails.price_cat_alias;
                            lclCart.seat_section= cartDetails.seat_section;
                            lclCart.seat_row= cartDetails.seat_row;
                            lclCart.totalAmt = 0.00;

                            reservedCart_list.push(lclCart);
                            resstartingQty = resstartingQty+(reserverCartQty-1);
                    }
                    
                }
            }
        } else{
            for (let m = 0; m < reserverCartQty; m++) {
                var ticketGrp = this.props.reservedCartInfo;
                var quantityLcl = ticketGrp[m][Object.keys(ticketGrp[m])[0]];
                if (quantityLcl > 0) {
                    var lclCart = {
                        type: ticketGrp[m].name,
                        quantity: quantityLcl,
                        totalAmt: ticketGrp[m].price * (quantityLcl),
                        seat_section: "General Admission",
                        seat_price_currency: "SGD",
                        seat_price: ticketGrp[m].price,
                        selected_index : [],
                        qtyIndex: m
                    };
                    for(let j=0;j<quantityLcl;j++){
                        lclCart.selected_index.push(j+resstartingQty);
                    }

                    reservedCart_list.push(lclCart);
                }
            }
        }
        // Reserved Cart data manipulation Ends

        if(title != ""){
            //console.log('parser123',he.decode(title));
            var decode_title = he.decode(title);
        }

        // Cart Data manipulation starts
        let quantity = this.props.currentQuantity;

        console.log('cart_info',this.props.ticketTypesSelected);
        
        if (quantity > 0 && this.props.ticketTypesSelected.length > 0) {
            

            let tickets = this.props.ticketTypesSelected;

            let filteredCart = [];
            let startingQty = 0;

            if (ticketTypes === "RS") {
                for (let i = 0; i < tickets.length; i++) {
                    let quantityLcl = this.props.ticketTypesSelected[i][Object.keys(this.props.ticketTypesSelected[i])[0]];
                    if (quantityLcl > 0 && this.props.cartInfo.length > 0) {
                        let lclCart = {
                            type: this.props.ticketTypesSelected[i].name,
                            quantity: quantityLcl,
                            totalAmt: this.props.ticketTypesSelected[i].price * (quantityLcl),
                            seat_no: [],
                            inventory_id: [],
                            selected_index : [],
                            qtyIndex: i
                        };
                        for (let j = 0; j < quantityLcl; j++) {
                            let cartDetails = this.props.cartInfo[j + startingQty]
                            lclCart.seat_no.push(cartDetails.seat_no);
                            lclCart.inventory_id.push(cartDetails.inventory_id);
                            lclCart.selected_index.push(j+startingQty);

                            if (j === quantityLcl - 1) {
                                lclCart.seat_price = this.props.ticketTypesSelected[i].price;
                                lclCart.seat_price_currency = cartDetails.seat_price_currency;
                                lclCart.seat_level = cartDetails.seat_level;
                                lclCart.price_cat_alias = cartDetails.price_cat_alias;
                                lclCart.seat_section = cartDetails.seat_section;
                                lclCart.seat_row = cartDetails.seat_row;

                                filteredCart.push(lclCart);
                                startingQty = startingQty + quantityLcl;
                            }

                        }
                    }
                }
                cart_list = filteredCart;
            }
            else {
                for (let k = 0; k < tickets.length; k++) {
                    var ticketGrp = this.props.ticketTypesSelected;
                    var quantityLcl = ticketGrp[k][Object.keys(ticketGrp[k])[0]];
                    if (quantityLcl > 0) {
                        var lclCart = {
                            type: ticketGrp[k].name,
                            quantity: quantityLcl,
                            totalAmt: ticketGrp[k].price * (quantityLcl),
                            seat_section: "General Admission",
                            seat_price_currency: "SGD",
                            seat_price: ticketGrp[k].price,
                            selected_index : [],
                            qtyIndex: k
                        };
                        for (let m = 0; m < quantityLcl; m++) {
                            lclCart.selected_index.push(m+startingQty);
                        }

                        filteredCart.push(lclCart);
                    }
                    
                }
                cart_list = filteredCart;
            }

        }
        else {
            cart_list = [];
        }
        // Cart Data manipulation Ends
       
        // if(cart_list.length > 0){
        //     console.log('cart_list', cart_list[0].slct_seat_type);
        // }

        console.log('cart_list_data',cart_list);
        
        
        var total_price = 0, total_price2 = 0, seat_price_currency = '';

        console.log('reservedCart_list', reservedCart_list);


        return (
        <Fragment>
            <section className="show-info seats">
                <div className="container">
                    <div className="row">
                        <div className="media col-md-11 col-12">
                            { summaryImagePath ?
                                (<div className="media-img mr-4">
                                    <img className="img-fluid" src={SITE_URL + summaryImagePath} alt=""/>
                                </div>): ''
                            }
                            <div className="media-body col-md-12">
                                <h5 className="mt-0 ft">{HTMLParser(decode_title)}</h5>
                                <p className="ft-1">
                                    {   
                                        venue && (
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon"/>
                                                {venue}
                                            </span>
                                        )
                                    }
                                    
                                    {
                                        this.props.currentQuantity > 0 && 
                                        (
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/ticket-icon.svg"} alt="icon"/>
                                                {this.props.currentQuantity} Ticket{this.props.currentQuantity > 1 ? 's': ''}
                                            </span>
                                        )
                                    }
                                    
                                    {
                                        this.props.dateChosen !== "" ? (
                                            <span>
                                                <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon"/>
                                                {this.props.dateChosen+", "+this.props.timeChosen}
                                            </span>
                                        ) : (
                                            <a href="javascript:;" onClick={this.scrollToDate.bind(this)}>
                                                <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"}
                                                        alt="icon"/>
                                                Select A Date
                                            </a>
                                        )
                                    }
                                    {
                                        this.props.cartInfo.length>0 && <span>&nbsp;Row&nbsp;:&nbsp;</span>
                                    }
                                    {
                                        
                                        this.props.cartInfo.map((value,index)=>{
                                            return(
                                            <span>{value.seat_row+" "+value.seat_no} </span>
                                            )
                                        })
                                    
                                    }
                                </p>
                                <div className="ticket-detail-sec table-responsive">
                                    <table className="table">
                                        <tbody>
                                        { 
                                            cart_list.length > 0 ? (
                                            <Fragment>
                                            {
                                                cart_list.map((cart_data, index) => {

                                                     total_price += parseFloat(cart_data.totalAmt);
                                                    var seat_price = cart_data.seat_price;

                                                    seat_price_currency = cart_data.seat_price_currency;
                                                    
                                                    return(
                                                    <tr>
                                                        <td>
                                                            <p>{parseInt(index) + 1}.</p>
                                                            <span></span>
                                                        </td>
                                                        <td className="type">
                                                            <p>Type</p>
                                                            <span>{cart_data.type}</span>
                                                        </td>
                                                        {
                                                            ticketTypes === "RS" && (
                                                            <td className="lvl">
                                                                <p>Level</p>
                                                                <span>{cart_data.seat_level}</span>
                                                            </td>
                                                            )
                                                        }
                                                        
                                                        {
                                                            ticketTypes === "RS" ? (
                                                                <td className="sec">
                                                                    <p>Section</p>
                                                                    <span>{cart_data.price_cat_alias}</span>
                                                                </td>
                                                            ) : (
                                                                <td className="sec">
                                                                    <p>Section</p>
                                                                    <span>{cart_data.seat_section}</span>
                                                                </td>
                                                            )
                                                        }
                                                        
                                                        {
                                                            ticketTypes === "RS" && (
                                                            <Fragment>
                                                                <td className="lane">
                                                                    <p>Row</p>
                                                                    <span>{cart_data.seat_row}</span>
                                                                </td>
                                                                <td className="seat-no">
                                                                    <p>Seat(s)</p>
                                                                    <span>{cart_data.seat_no.toString()}</span>
                                                                </td>
                                                            </Fragment>
                                                            )
                                                        }
                                                        <td className="unit-pr">
                                                            <p>Unit Price</p>
                                                            <span>{cart_data.seat_price_currency} {cart_data.seat_price}</span>
                                                        </td>
                                                            {
                                                                ticketTypes !== "RS" && (
                                                                    <td className="quantity">
                                                                        <p>Quantity</p>
                                                                        <span>{cart_data.quantity}</span>
                                                                    </td>
                                                                )
                                                            }
                                                        <td className="total-amt">
                                                            <p></p>
                                                            <span className="amount">{cart_data.seat_price_currency} {(cart_data.totalAmt).toFixed(2)}</span>
                                                            {
                                                                ticketTypes === "RS" ? (
                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCart(e, cart_data.selected_index, cart_data.inventory_id, cart_data.type, cart_data.qtyIndex)}>
                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                                                    </i>
                                                                ) : (
                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCartGA(e, cart_data.selected_index, cart_data.qtyIndex, 'Normal')}>
                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                                                    </i>
                                                                )
                                                            }
                                                            
                                                        </td>
                                                    </tr>                                                   
                                                )

                                               })
                                            }
                                            </Fragment>) : ''
                                        }

                                                
                                        {/* reserved friends */}                                               
                                        {
                                            reservedCart_list.length > 0 ? (
                                        
                                                <Fragment>
                                                        
                                                    {
                                                        reservedCart_list.map((cart_data, index) => {

                                                            total_price += cart_data.totalAmt;
                                                            var seat_price = cart_data.totalAmt;

                                                            seat_price_currency = cart_data.seat_price_currency;
                                                            
                                                            return (
                                                                
                                                                <Fragment>
                                                                    
                                                                            
                                                                    {index === 0 &&
                                                                                                                                
                                                                        <tr><td colspan="9" className="reservdForFrndTxt">
                                                                            <hr className="tline"/>
                                                                            <b>Reserved for Friends</b></td>
                                                                            </tr>
                                                                    }
                                                                    <tr>
                                                                        <td>
                                                                            <p>{parseInt(index) + 1 + cart_list.length}.</p>
                                                                            <span></span>
                                                                        </td>
                                                                        <td className="type">
                                                                            <p>Type</p>
                                                                            { cart_data.type === "Reserved for Friends" ? (
                                                                                <span>-</span>
                                                                            ) : (
                                                                                <span>{cart_data.type ? cart_data.type : "PRODUCT"}</span>
                                                                                )
                                                                            }
                                                                            
                                                                        </td>
                                                                        {
                                                                            ticketTypes === "RS" && (
                                                                                <td className="lvl">
                                                                                    <p>Level</p>
                                                                                    <span>{cart_data.seat_level}</span>
                                                                                </td>
                                                                            )
                                                                        }

                                                                        {
                                                                            ticketTypes === "RS" ? (
                                                                                <td className="sec">
                                                                                    <p>Section</p>
                                                                                    <span>{cart_data.price_cat_alias}</span>
                                                                                </td>
                                                                            ) : (
                                                                                    <td className="sec">
                                                                                        <p>Section</p>
                                                                                        <span>{cart_data.seat_section}</span>
                                                                                    </td>
                                                                                )
                                                                        }

                                                                        {
                                                                            ticketTypes === "RS" && (
                                                                                <Fragment>
                                                                                    <td className="lane">
                                                                                        <p>Row</p>
                                                                                        <span>{cart_data.seat_row}</span>
                                                                                    </td>
                                                                                    <td className="seat-no">
                                                                                        <p>Seat(s)</p>
                                                                                        <span>{cart_data.seat_no.toString()}</span>
                                                                                    </td>
                                                                                </Fragment>
                                                                            )
                                                                        }
                                                                        <td className="unit-pr">
                                                                            <p>Unit Price</p>
                                                                            <span>{cart_data.seat_price_currency} 
                                                                            {
                                                                            cart_data.seat_price > 0 ? (cart_data.seat_price).toFixed(2) : cart_data.seat_price
                                                                            }</span>
                                                                        </td>
                                                                        {
                                                                            ticketTypes !== "RS" && (
                                                                                <td className="quantity">
                                                                                    <p>Quantity</p>
                                                                                    <span>{cart_data.quantity}</span>
                                                                                </td>
                                                                            )
                                                                        }
                                                                        <td className="total-amt">
                                                                            <p></p>
                                                                            <span className="amount">{cart_data.seat_price_currency} {seat_price.toFixed(2)}</span>
                                                                            {
                                                                                ticketTypes === "RS" ? (
                                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delFrndCart(e, cart_data.selected_index, cart_data.inventory_id, cart_data.type)}>
                                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon" />
                                                                                    </i>
                                                                                ) : (
                                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCartGA(e, cart_data.selected_index, cart_data.qtyIndex, 'Reserve')}>
                                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon" />
                                                                                    </i>
                                                                                )
                                                                            }
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                </Fragment>
                                                            )

                                                        })
                                                    }
                                                </Fragment>) : ''
                                                }

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-1">
                            <TimeCounter />
                        </div>
                    </div>
                </div>
                <div className="mob-tic-info pr-3">
                    <hr />

                    { 
                    cart_list.length > 0 ? (

                    
                        cart_list.map((cart_data, index) => {

                            seat_price_currency = cart_data.seat_price_currency;
                        
                            return(
                            <div className="col-12 mb-3 px-0">
                                <div className="row title">
                                    <span className="col-6">{cart_data.type}</span>
                                    <span className="col-6 text-right">{cart_data.seat_price_currency} {cart_data.seat_price}</span>
                                </div>
                                <div className="row tic-cont">
                                    <div className="col-4">                                        
                                        {
                                            ticketTypes === "RS" && (
                                            <Fragment>
                                                <span className="row">
                                                    <span className="col-4">Level</span>
                                                    <span className="col-8">{cart_data.seat_level}</span>
                                                </span>
                                                <span className="row">
                                                    <span className="col-4">Row</span>
                                                    <span className="col-8">{cart_data.seat_row}</span>
                                                </span>
                                            </Fragment>
                                            )
                                        }
                                    </div>
                                    <div className="col-8">
                                    {
                                        ticketTypes === "RS" ? (
                                        <Fragment>
                                            <span className="row">
                                                <span className="col-5">Section</span>
                                                <span className="col-7 pl-0">{cart_data.price_cat_alias}</span>
                                            </span>
                                            <span className="row">
                                                <span className="col-5">Seats(s)</span>
                                                <span className="col-7 pl-0">{cart_data.seat_no.toString()}</span>
                                            </span>
                                        </Fragment>
                                        ) : (
                                            <span className="row">
                                                <span className="col-5">Section</span>
                                                <span className="col-7 pl-0">{cart_data.seat_section}</span>
                                            </span>
                                        )
                                    }
                                        <span className="row">
                                            <span className="col-5">Unit Price</span>
                                            <span className="col-7 pl-0">{cart_data.seat_price_currency} {cart_data.seat_price}</span>
                                        </span>
                                        <span className="row">
                                            <span className="col-5">Quantity</span>
                                            <span className="col-7 pl-0">{cart_data.quantity}</span>
                                        </span>
                                    </div>
                                    { ticketTypes === "RS" ? (
                                        <a href="javascript:;" className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCart(e, cart_data.selected_index, cart_data.inventory_id,cart_data.type, cart_data.qtyIndex)}>
                                            <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                        </a>) : (
                                        <a href="javascript:;" className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCartGA(e, cart_data.selected_index, cart_data.qtyIndex, 'Normal')}>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                        </a>
                                        )
                                    }
                                </div>
                            </div>
                            )

                        })
                        
                        ) : ''

                    }

                    {/* ========== Reserved Friends ===========  */}
                    { 
                    reservedCart_list.length > 0 ? (

                    
                        reservedCart_list.map((cart_data, index) => {

                            seat_price_currency = cart_data.seat_price_currency;
                        
                            return(
                            <div className="col-12 mb-3 px-0">
                                <div className="row title">
                                    <span className="col-6">{cart_data.type === "Reserved for Friends" && '-'}</span>
                                    <span className="col-6 text-right">{cart_data.seat_price_currency} {cart_data.seat_price}</span>
                                </div>
                                <div className="row tic-cont">
                                    <div className="col-4">                                        
                                        {
                                            ticketTypes === "RS" && (
                                            <Fragment>
                                                <span className="row">
                                                    <span className="col-4">Level</span>
                                                    <span className="col-8">{cart_data.seat_level}</span>
                                                </span>
                                                <span className="row">
                                                    <span className="col-4">Row</span>
                                                    <span className="col-8">{cart_data.seat_row}</span>
                                                </span>
                                            </Fragment>
                                            )
                                        }
                                    </div>
                                    <div className="col-8">
                                    {
                                        ticketTypes === "RS" ? (
                                        <Fragment>
                                            <span className="row">
                                                <span className="col-5">Section</span>
                                                <span className="col-7 pl-0">{cart_data.price_cat_alias}</span>
                                            </span>
                                            <span className="row">
                                                <span className="col-5">Seats(s)</span>
                                                <span className="col-7 pl-0">{cart_data.seat_no.toString()}</span>
                                            </span>
                                        </Fragment>
                                        ) : (
                                            <span className="row">
                                                <span className="col-5">Section</span>
                                                <span className="col-7 pl-0">{cart_data.seat_section}</span>
                                            </span>
                                        )
                                    }
                                        <span className="row">
                                            <span className="col-5">Unit Price</span>
                                            <span className="col-7 pl-0">{cart_data.seat_price_currency} {cart_data.seat_price}</span>
                                        </span>
                                        <span className="row">
                                            <span className="col-5">Quantity</span>
                                            <span className="col-7 pl-0">{cart_data.quantity}</span>
                                        </span>
                                    </div>
                                    { ticketTypes === "RS" ? (
                                        <a href="javascript:;" className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCart(e, cart_data.selected_index, cart_data.inventory_id,cart_data.type, cart_data.qtyIndex)}>
                                            <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                        </a>) : (
                                        <a href="javascript:;" className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCartGA(e, cart_data.selected_index, cart_data.qtyIndex, 'Reserve')}>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                        </a>
                                        )
                                    }
                                </div>
                            </div>
                            )

                        })
                        
                        ) : ''

                    }
                </div>
                { 
                    cart_list.length > 0 || reservedCart_list.length > 0 ? (
                    <Fragment>
                        <div className="total-sec">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className={`col-8 booking-fee ${ summaryImagePath ? '' : 'pl-3'}`}>
                                        <h6>Total</h6>
                                        <p className="mb-0">excl.booking fee</p>
                                    </div>
                                    <div className="col-3 check-out-sec">
                                        <h6>{seat_price_currency} {total_price.toFixed(2)}</h6>
                                        {/* { 
                                            this.props.checkout_btn_enable ? 
                                            (
                                                // <Link to={`/delivery/${iccCode}`} className="btn check-out-btn">Check Out</Link>
                                                this.props.patronName !== undefined && this.props.patronName !== "" ? (
                                                    <Link to={`/delivery/${iccCode}`} className="btn check-out-btn">Check Out</Link>
                                                ): (
                                                    <a href="javascript:;" className="btn check-out-btn" data-toggle="modal" data-target="#sistic-login-modal">Check Out</a>
                                                )
                                                

                                            ) : (<a href="javascript:;" className="btn check-out-btn grey">Check Out</a>)
                                        } */}
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        {/* <div className="view-detail d-block d-sm-block d-md-none">
                            <button className="btn btn-view" onClick={this.viewDetails.bind(this)}>
                                <span>View Ticket Details</span>
                                <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon"/>
                            </button>
                        </div> */}

                        <div className="view-detail d-block d-sm-block d-md-none">
                            <button className="btn btn-view" onClick={this.viewDetMob.bind(this)}>
                                <span>View Ticket Details</span>
                                <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon"/>
                            </button>
                        </div>
                    
                    </Fragment>
                        
                    ) : ''

                }                
            </section>
            
            <section className="show-info seats float-wrap d-none d-sm-none d-md-block">
                <div className="container">
                    <div className="row">
                        <div className="media col-md-11 col-12">
                            <div className={`media-body`}>
                                <h5 className="mt-0 ft">{HTMLParser(decode_title)}</h5>
                                <p className="ft-1">
                                    <span className="mr-3">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon"/>
                                        {venue}
                                    </span>
                                    {
                                        this.props.currentQuantity > 0 && (
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/ticket-icon.svg"} alt="icon"/>
                                                {this.props.currentQuantity} Ticket{this.props.currentQuantity > 1 ? 's': ''}
                                            </span>
                                        )
                                    }

                                    {
                                        this.props.dateChosen !== "" ? (
                                            <span>
                                                <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon"/>
                                                {this.props.dateChosen+", "+this.props.timeChosen}
                                            </span>
                                        ) : (
                                            <a href="javascript:;" onClick={this.scrollToDate.bind(this)}>
                                                <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"}
                                                        alt="icon"/>
                                                Select A Date
                                            </a>
                                        )
                                    }
                                    {
                                        this.props.cartInfo.length>0 && <span>&nbsp;Row&nbsp;:&nbsp;</span>
                                    }
                                    {
                                        
                                        this.props.cartInfo.map((value,index)=>{
                                            return(
                                            <span>{value.seat_row+" "+value.seat_no} </span>
                                            )
                                        })
                                      
                                    }

                                    {/* {                                            
                                        cart_list.length > 0 && cart_list.map((cart_data, index) => {
                                            total_price2 += parseInt(cart_data.seat_price);
                                            var seat_price = cart_data.seat_price;

                                            seat_price_currency = cart_data.seat_price_currency;
                                        })
                                    } */}
                                    {
                                        total_price > 0 && (
                                            <div className="stickyTotal"><h5 className="mt-0 ft">Total {seat_price_currency} {total_price.toFixed(2)}</h5></div>
                                        )
                                    }                        
                                </p>
                                
                                <div className={`ticket-detail-sec hide`}>
                                    <table className="table table-responsive">
      
                                    <tbody>
                                        {                                            
                                            cart_list.length > 0 ? 
                                            
                                                cart_list.map((cart_data, index) => {

                                                    total_price2 += parseInt(cart_data.seat_price);
                                                    var seat_price = cart_data.seat_price;

                                                    seat_price_currency = cart_data.seat_price_currency;

                                                    return(
                                                    <tr>
                                                        <td>
                                                            <p>{parseInt(index) + 1}.</p>
                                                            <span></span>
                                                        </td>
                                                        <td className="type">
                                                            <p>Type</p>
                                                            <span>{cart_data.type}</span>
                                                        </td>
                                                        
                                                        {
                                                            ticketTypes === "RS" && (
                                                                <td className="lvl">
                                                                    <p>Level</p>
                                                                    <span>{cart_data.seat_level}</span>
                                                                </td>
                                                            )
                                                        }

                                                        {
                                                            ticketTypes === "RS" ? (
                                                                <td className="sec">
                                                                    <p>Section</p>
                                                                    <span>{cart_data.price_cat_alias}</span>
                                                                </td>
                                                            ) : (
                                                                <td className="sec">
                                                                    <p>Section</p>
                                                                    <span>{cart_data.seat_section}</span>
                                                                </td>
                                                            )
                                                        }

                                                        {
                                                            ticketTypes === "RS" && (
                                                                <Fragment>
                                                                    <td className="lane">
                                                                        <p>Row</p>
                                                                        <span>{cart_data.seat_row}</span>
                                                                    </td>
                                                                    <td className="seat-no">
                                                                        <p>Seat(s)</p>
                                                                        <span>{cart_data.seat_no.toString()}</span>
                                                                    </td>
                                                                </Fragment>
                                                            )
                                                        }
                                                            <td className="unit-pr">
                                                                <p>Unit Price</p>
                                                                <span>{cart_data.seat_price_currency} {cart_data.seat_price}</span>
                                                            </td>
                                                            {
                                                                ticketTypes !== "RS" && (
                                                                    <td className="quantity">
                                                                        <p>Quantity</p>
                                                                        <span>{cart_data.quantity}</span>
                                                                    </td>
                                                                )
                                                            }
                                                        <td className="total-amt">
                                                            <p></p>
                                                            <span className="amount">{cart_data.seat_price_currency} {(cart_data.totalAmt).toFixed(2)}</span>
                                                            {
                                                                ticketTypes === "RS" ? (
                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCart(e, cart_data.selected_index, cart_data.inventory_id,cart_data.type, cart_data.qtyIndex)}>
                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                                                    </i>
                                                                ) : (
                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCartGA(e, cart_data.selected_index, cart_data.qtyIndex, 'Normal')}>
                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon"/>
                                                                    </i>
                                                                )
                                                            }
                                                        </td>
                                                    </tr>                                                   
                                                )

                                               }) 
                                               : ''
                                                }

                                                {/* reserved friends */}
                                                {reservedCart_list.length > 0 ? (
                                                        <Fragment>
                                                            {
                                                                reservedCart_list.map((cart_data, index) => {
                                                                    total_price2 += parseInt(cart_data.seat_price);
                                                                    var seat_price = cart_data.totalAmt;

                                                                    seat_price_currency = cart_data.seat_price_currency;

                                                                    return (
                                                                        <tr style={{backgroundColor:'#f7f7f7'}}>
                                                                            <td>
                                                                                <p>{parseInt(index) + 1 + cart_list.length}.</p>
                                                                                <span></span>
                                                                            </td>
                                                                            <td className="type">
                                                                                <p>Type</p>
                                                                                <span>{cart_data.type === "Reserved for Friends" ? '-' : cart_data.type}</span>
                                                                            </td>

                                                                            {
                                                                                ticketTypes === "RS" && (
                                                                                    <td className="lvl">
                                                                                        <p>Level</p>
                                                                                        <span>{cart_data.seat_level}</span>
                                                                                    </td>
                                                                                )
                                                                            }

                                                                            {
                                                                                ticketTypes === "RS" ? (
                                                                                    <td className="sec">
                                                                                        <p>Section</p>
                                                                                        <span>{cart_data.price_cat_alias}</span>
                                                                                    </td>
                                                                                ) : (
                                                                                        <td className="sec">
                                                                                            <p>Section</p>
                                                                                            <span>{cart_data.seat_section}</span>
                                                                                        </td>
                                                                                    )
                                                                            }

                                                                            {
                                                                                ticketTypes === "RS" && (
                                                                                    <Fragment>
                                                                                        <td className="lane">
                                                                                            <p>Row</p>
                                                                                            <span>{cart_data.seat_row}</span>
                                                                                        </td>
                                                                                        <td className="seat-no">
                                                                                            <p>Seat(s)</p>
                                                                                            <span>{cart_data.seat_no.toString()}</span>
                                                                                        </td>
                                                                                    </Fragment>
                                                                                )
                                                                            }
                                                                            <td className="unit-pr">
                                                                                <p>Unit Price</p>
                                                                                <span>{cart_data.seat_price_currency} {cart_data.seat_price}</span>
                                                                            </td>
                                                                            {
                                                                                ticketTypes !== "RS" && (
                                                                                    <td className="quantity">
                                                                                        <p>Quantity</p>
                                                                                        <span>{cart_data.quantity}</span>
                                                                                    </td>
                                                                                )
                                                                            }
                                                                            <td className="total-amt">
                                                                                <p></p>
                                                                                <span className="amount">{cart_data.seat_price_currency} {seat_price.toFixed(2)}</span>
                                                                                {
                                                                                ticketTypes === "RS" ? (
                                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delFrndCart(e, cart_data.selected_index, cart_data.inventory_id, cart_data.type)}>
                                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon" />
                                                                                    </i>
                                                                                ):(
                                                                                    <i className="trash-icon" onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCartGA(e, cart_data.selected_index, cart_data.qtyIndex, 'Reserve')}>
                                                                                        <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon" />
                                                                                    </i>
                                                                                )
                                                                            }
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                    
                                                                })
                                            }
                                            </Fragment>) : ''
                                        }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-1 d-none d-sm-none d-md-block">
                            <TimeCounter />
                        </div>
                    </div>
                </div>

                { 
                    cart_list.length > 0 ? (
                    <Fragment>
                        <div className="total-sec col-12 mob hide">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className="col-6 booking-fee">
                                        <h6>Total</h6>
                                        <p className="mb-0">excl.booking fee</p>
                                    </div>
                                    <div className="col-5 check-out-sec">
                                        <h6>{seat_price_currency} {total_price.toFixed(2)}</h6>                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="view-detail d-block d-sm-none d-md-block">
                            <button className="btn btn-view" onClick={this.viewDetDesk.bind(this)}>
                                <span className="view_ticket">View Ticket Details</span>
                                <span className="close_ticket hide">Close Ticket Details</span>
                                <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon"/>
                            </button>
                        </div>
                    </Fragment>
                    ) : ''

                }
            </section>
        </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    //console.log('cartInfo', state.cartInfo)
    return {
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        totalQuantity:  state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        dateChosen: state.dateChosen,
        cartInfo: state.cartInfo,
        quantity: state.quantity,
        mergedCartInfo:state.mergedCartInfo,
        checkout_btn_enable: state.checkout_btn_enable,
        update_seat_select: state.update_seat_select,
        selected_adult_qty: state.selected_adult_qty,
        selected_child_qty: state.selected_child_qty,
        selected_nsf_qty: state.selected_nsf_qty,
        selected_sectz_qty: state.selected_sectz_qty,
        timeChosen:state.timeChosen,
        patronName: state.patronName,
        propsObj: ownProps,
        currentQuantity: state.currentQuantity,
        ticketTypesSelected:state.ticketTypesSelected,
        reservedCartInfo : state.reservedcartInfo,
        ticketType: state.prdTicketType,
        productsObj: state.showInfo && state.showInfo.productsObj && state.showInfo.productsObj,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeMultiCartInfo: (value, isFrnd) => {
            dispatch(actionCreators.removeMultiCartInfo(value, isFrnd));
        },
        updateQtyData: (field,value,factor) => {
            dispatch(actionCreators.updateQty(field,value, factor));
        },
        addQuantity: (current, increment) => {
            dispatch(actionCreators.addQuantity(current,increment));
        },
        addReservedCartInfoGA: (obj, current, increment) => {
            dispatch(actionCreators.addReservedCartInfoGA(obj, current, increment));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CartDetails);