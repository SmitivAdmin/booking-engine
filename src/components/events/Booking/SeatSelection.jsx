import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import SeatsOverView from './SeatsOverview'
import SeatsStageView from './SeatsStageView'
import * as actionCreators from "../../../actions/actionCreator"
import {SITE_URL} from '../../../constants/common-info'
import * as API from "../../../service/events/eventsInfo"

class SeatSelection extends Component {

    constructor(props) {
        super(props);
    }
   

    render() {

        return (

            <Fragment>
                <div className="seat-map hide">
                    <div className="container">
                        <div className="row">
                            <div className="col-8 pl-0">
                                <h4 className="title-bdr" id="seatMapTtl">Seat Selection</h4>
                                {/* <h4>Select Your seat(s) </h4>
                                <p>To shift seats, unselect the chosen seats and select your preferred seats.</p> */}
                            </div>
                            <div className="col-4 right-align-column hide">
                                <button type="button" className="btn get-seat-btn get-seat-desktop">Get the best seats available</button>
                                <button type="button" className="btn get-seat-btn get-seat-mobile">Best seats</button>
                            </div>
                        </div>
                        
                        <div className="row seat-selection">
                            <SeatsOverView />
                            <SeatsStageView />
                        </div>
                        
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SeatSelection);