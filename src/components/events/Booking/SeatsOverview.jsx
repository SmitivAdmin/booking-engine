import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import {SITE_URL} from '../../../constants/common-info'
import * as API from "../../../service/events/eventsInfo"

import $ from 'jquery';
import {Modal, Button} from 'react-bootstrap';

class SeatsOverView extends Component {
    constructor(props){
        super(props);
        this.state = {hoveredArea: null, seatAvailable: '', isShowModel: false, layerX: null, layerY: null, area: null}
    }

    componentDidMount(){
        require('jquery-mousewheel');
        require('malihu-custom-scrollbar-plugin');
    
        $(".mscroll-x").mCustomScrollbar({
            axis:"x",
            scrollEasing:"linear",
            scrollInertia: 0,
            autoHideScrollbar: false,
            autoExpandScrollbar: false,
            alwaysShowScrollbar: 2,
            theme: "dark-3"
        });
    }

    componentDidUpdate(){
        window.$('#overviewSeatMap').pictarea(
            {
                rescaleOnResize: true,
                maxSelections: 1,
                normal: {
                    fillStyle: 'transparent',
                    strokeStyle: 'transparent',
                    lineWidth: 1
                },
                active: {
                    fillStyle: 'transparent',
                    strokeStyle: '#0098ff',
                    lineWidth: 1
                },
                hover: {
                    fillStyle: 'transparent',
                    strokeStyle: '#0098ff',
                    lineWidth: 1,
                    shadowColor: 'transparent',
                    shadowBlur: 0
                },
                areaValueKey: "target"
            }
        );
    }

    getSeatColorCode(price_cat_num){
        switch(price_cat_num){
            case 1:
                return "bg_cls1";
            case 2:
                return "#bg_cls1";
            case 3:
                return "#71BA64";
            case 4:
                return "#D097F0";
            case 5:
                return "#F7F19F";
            case 6:
                return "#B5EDF3";
            case 7:
                return "#D9C297";
            case 8:
                return "#A6F0B9";
            case 9:
                return "#FB9CC0";
            case 10:
                return "#D6CC1B";
            case 11:
                return "#929E92";
            case 12:
                return "#4AD1B1";
            case 13:
                return "#8C65A0";
            case 14:
                return "#A80717";
            case 15:
                return "#FF0049";
            case 16:
                return "#D89D61";
            case 17:
                return "#3D7717";
            case 18:
                return "#AE9FD6";
            case 19:
                return "#EC826A";
            case 20:
                return "#FFC7CE";
            default:
                return "#feec66";
        }
    }

    getAvailabity(availability_status){
        switch(availability_status){
            case 0:
                return "Sold Out";
            case 1:
                return "Available";
            case 2:
                return "Limited Seats";
            case 3:
                return "Single Seat";
        }
    }

    showTooltip(text, evt) {
        //console.log('evt',evt.pageY);
        let tooltip = document.getElementById("seat_tooltip");
        let getDivPst = $(".ticket-quantity").offset();
        var divTop = getDivPst.top;

        tooltip.innerHTML = text;
        tooltip.style.display = "block";
        tooltip.style.top = (evt.pageY - 185 - divTop) + 'px';
    }

    hideTooltip() {
        var tooltip = document.getElementById("seat_tooltip");
        tooltip.style.display = "none";
    }

    getSeats(price_cat_alias, price_cat_amount, price_cat_currency, price_cat_formatted, seat_section_id, seat_section_alias, seat_section_type, seat_entrance, price_category_num, price_category_id, price_amount, price_currency, price_formatted, mode){
        var qty = this.props.totalQuantity
        if(qty){
            qty = qty
        } else{
            qty = 1;
        }

        if(this.props.chosenProductId != "" && this.props.chosenProductId != undefined){
            var product_id = this.props.chosenProductId;

            API.getSeatSelection(product_id, price_category_id, seat_section_id, mode, qty, price_cat_alias, seat_section_alias, seat_section_type, price_amount, price_currency, price_formatted).then(response =>{
                if(response){
                    this.props.fetchSeatSelection(response);
                }
                
            });
        } 
    }

    moveOnArea(evt) {
        evt.preventDefault();
        //const coords = { x: evt.nativeEvent.layerX, y: evt.nativeEvent.layerY };
        document.getElementsByClassName('tooltip')[0].style.left = parseInt(40) + evt.nativeEvent.layerX+'px';
        document.getElementsByClassName('tooltip')[0].style.top =  evt.nativeEvent.layerY - 30 +'px';
	}


    async enterArea(area, evt) {
        evt.preventDefault();
        this.setState({
            hoveredArea: area,
            seatAvailable: ''
        });
        console.log("area_cls",area.class_name);
        var current_cls = area.class_name;

        $("."+current_cls).trigger("hover");
        
        console.log("current_len",$("."+current_cls).length);
        // var getId = $(".tooltip").attr("id");
        // if(getId === area.tooltip_id){
        //     $(".tooltip").addClass('active');
        // } else{
        //     $(".tooltip").removeClass('active');
        // }
        var qty = 1, seats_available = 0;

        var isMembership = this.props.seatMapInfo.isMembership === 0 ? false : true;

        // await API.catSection(area.priceCategoryId, area.seatSectionId, area.mode, isMembership, this.props.isEventWithSurvey).then(response => {
        //     console.log('cat_section', response);
        // });
        
        await API.getSeatSelection(area.productId, area.priceCategoryId, area.seatSectionId, area.mode, qty, area.priceCatAlias, area.seatSectionAlias, area.seatSectionType, area.priceCatAmount, area.priceCatCurrency, area.priceCatFormatted, area.seatLevelAlias, area.pkgReqId).then(response =>{                 
            if(response !== undefined){
                //console.log('seats_available', response)
                if(area.pkgReqId){
                    seats_available = response.seatSelection.seatSectionAvailability && response.seatSelection.seatSectionAvailability;
                } else{
                    seats_available = response.seatSelection.seatsAvailableList && response.seatSelection.seatsAvailableList.length
                }
                
                this.setState({
                    seatAvailable: seats_available
                });
                //$(".tooltip").addClass('active');
            }
        });

        // await API.getSeatAvailable(area.productId, area.priceCategoryId, area.seatSectionId, qty, area.mode, area.priceCatAlias, area.seatSectionAlias, area.seatSectionType, area.priceCatAmount, area.priceCatCurrency, area.priceCatFormatted, area.seatLevelAlias, area.pkgReqId).then(response => {

        //     if(response && response.status === 200){
        //         seats_available = response.resObj.seatSelection.seatsAvailableList && response.resObj.seatSelection.seatsAvailableList.length;
        //         this.setState({
        //             seatAvailable: seats_available
        //         });
        //     } else{
        //         API.getSeatSelection(area.productId, area.priceCategoryId, area.seatSectionId, area.mode, qty, area.priceCatAlias, area.seatSectionAlias, area.seatSectionType, area.priceCatAmount, area.priceCatCurrency, area.priceCatFormatted, area.seatLevelAlias, area.pkgReqId).then(response => {
        //             if (response !== undefined) {
        //                 // Empty the cart item When select load new seats
        //                 //console.log('seats_available', response)
        //                 if(area.pkgReqId){
        //                     seats_available = response.seatSelection.seatSectionAvailability && response.seatSelection.seatSectionAvailability;
        //                 } else{
        //                     seats_available = response.seatSelection.seatsAvailableList && response.seatSelection.seatsAvailableList.length
        //                 }
        //                 this.setState({
        //                     seatAvailable: seats_available
        //                 });
        //                 //$(".tooltip").addClass('active');
        //             }
        //         });
        //     }
        // });

    }

    leaveArea(evt) {
        evt.preventDefault();
		this.setState({
            hoveredArea: null,
            seatAvailable: ''
        });
        //$(".tooltip").removeClass('active');
    }

    toggleModel = (boolean) =>{
        this.setState({isShowModel : boolean});
    }

    clickFromPrompt = async ()=>{
        let area = this.state.area;
        this.toggleModel(false);

            // this.setState({
            //     hoveredArea: area
            // });

            $(".tooltip").attr('id', area.tooltip_id);
            $(".tooltip").attr('data-shape', area.shape);
            $(".tooltip").attr('data-coords', area.coords);
            $(".tooltip").attr('data-fill-color', area.fillColor);

            document.getElementsByClassName('tooltip')[0].style.left = parseInt(40) + this.state.layerX + 'px';
            document.getElementsByClassName('tooltip')[0].style.top = this.state.layerY - 30 + 'px';

            if (document.getElementsByClassName("stage_loader")[0] != undefined) {
                document.getElementsByClassName("stage_loader")[0].classList.remove('hide');
            }

            //$(".filter-level img").addClass('filter_cls');

            var top_position = $('.right-seat-map').offset();
            $('html,body').animate({ scrollTop: (top_position.top) }, 700);

            var qty = 1, seats_available = 0;

            var isMembership = this.props.seatMapInfo.isMembership === 0 ? false : true;

            await API.catSection(area.priceCategoryId, area.seatSectionId, area.mode, isMembership, this.props.isEventWithSurvey).then(response => {
                console.log('cat_section', response);
            });            

            await API.getSeatAvailable(area.productId, area.priceCategoryId, area.seatSectionId, qty, area.mode, area.priceCatAlias, area.seatSectionAlias, area.seatSectionType, area.priceCatAmount, area.priceCatCurrency, area.priceCatFormatted, area.seatLevelAlias, area.pkgReqId).then(response => {
                if(response && response.status === 200){
                    // Empty the cart item When select load new seats
                    this.props.updateCartForFrnd(false);
                    $("#seatDesiableToggle").css("display", "none");
                    if($(".seat-img.selected") != undefined)
                    $(".seat-img.selected").removeClass("selected");
                    this.props.emptyCartInfo();
                    
                    //console.log('seats_available', response)
                    seats_available = response.resObj.seatSelection.seatsAvailableList && response.resObj.seatSelection.seatsAvailableList.length
                    this.setState({
                        seatAvailable: seats_available
                    });
                    this.props.overviewSelectedArea({ area, seats_available });
                    //result_arr.push(response)
                    this.props.fetchSeatSelection(response.resObj);

                } else{
                    API.getSeatSelection(area.productId, area.priceCategoryId, area.seatSectionId, area.mode, qty, area.priceCatAlias, area.seatSectionAlias, area.seatSectionType, area.priceCatAmount, area.priceCatCurrency, area.priceCatFormatted, area.seatLevelAlias, area.pkgReqId).then(response => {
                        if (response !== undefined) {
                            // Empty the cart item When select load new seats
                            this.props.updateCartForFrnd(false);
                            $("#seatDesiableToggle").css("display", "none");
                            if($(".seat-img.selected") != undefined)
                            $(".seat-img.selected").removeClass("selected");
                            this.props.emptyCartInfo();
                            
                            //console.log('seats_available', response)
                            seats_available = response.seatSelection.seatsAvailableList.length
                            this.setState({
                                seatAvailable: seats_available
                            });
                            this.props.overviewSelectedArea({ area, seats_available });
                            //result_arr.push(response)
                            this.props.fetchSeatSelection(response);

                        }
                    });
                }
            });

    }

    async clicked(area, evt) {
        evt.preventDefault();
        this.setState({layerX : evt.nativeEvent.layerX, layerY: evt.nativeEvent.layerY, area: area}, async ()=>{
        if ($("#seatDesiableToggle").is(':hidden')) {
            this.toggleModel(false);

            // this.setState({
            //     hoveredArea: area
            // });

            $(".tooltip").attr('id', area.tooltip_id);
            $(".tooltip").attr('data-shape', area.shape);
            $(".tooltip").attr('data-coords', area.coords);
            $(".tooltip").attr('data-fill-color', area.fillColor);

            document.getElementsByClassName('tooltip')[0].style.left = parseInt(40) + this.state.layerX + 'px';
            document.getElementsByClassName('tooltip')[0].style.top = this.state.layerY - 30 + 'px';

            if (document.getElementsByClassName("stage_loader")[0] != undefined) {
                document.getElementsByClassName("stage_loader")[0].classList.remove('hide');
            }
            if(document.getElementsByClassName("defaultStageImg")[0] != undefined){
            document.getElementsByClassName("defaultStageImg")[0].classList.remove('hide');
            }
            //$(".filter-level img").addClass('filter_cls');

            var top_position = $('.right-seat-map').offset();
            $('html,body').animate({ scrollTop: (top_position.top) }, 700);

            var qty = 1, seats_available = 0;

            var isMembership = this.props.seatMapInfo.isMembership === 0 ? false : true;

            await API.catSection(area.priceCategoryId, area.seatSectionId, area.mode, isMembership, this.props.isEventWithSurvey).then(response => {
                console.log('cat_section', response);
            })

            await API.getSeatAvailable(area.productId, area.priceCategoryId, area.seatSectionId, qty, area.mode, area.priceCatAlias, area.seatSectionAlias, area.seatSectionType, area.priceCatAmount, area.priceCatCurrency, area.priceCatFormatted, area.seatLevelAlias, area.pkgReqId).then(response => {
                if(response && response.status === 200){
                    // Empty the cart item When select load new seats
                    this.props.updateCartForFrnd(false);
                    $("#seatDesiableToggle").css("display", "none");
                    if($(".seat-img.selected") != undefined )
                    $(".seat-img.selected").removeClass("selected");
                    this.props.emptyCartInfo();
                    
                    //console.log('seats_available', response)
                    seats_available = response.resObj.seatSelection.seatsAvailableList && response.resObj.seatSelection.seatsAvailableList.length
                    this.setState({
                        seatAvailable: seats_available
                    });
                    this.props.overviewSelectedArea({ area, seats_available });
                    //result_arr.push(response)
                    this.props.fetchSeatSelection(response.resObj);

                } else{
                    API.getSeatSelection(area.productId, area.priceCategoryId, area.seatSectionId, area.mode, qty, area.priceCatAlias, area.seatSectionAlias, area.seatSectionType, area.priceCatAmount, area.priceCatCurrency, area.priceCatFormatted, area.seatLevelAlias, area.pkgReqId).then(response => {
                        if (response !== undefined) {
                            // Empty the cart item When select load new seats
                            this.props.updateCartForFrnd(false);
                            $("#seatDesiableToggle").css("display", "none");
                            if($(".seat-img.selected") != undefined)
                            $(".seat-img.selected").removeClass("selected");
                            this.props.emptyCartInfo();
                            
                            //console.log('seats_available', response)
                            seats_available = response.seatSelection.seatsAvailableList &&  response.seatSelection.seatsAvailableList.length
                            this.setState({
                                seatAvailable: seats_available
                            });
                            this.props.overviewSelectedArea({ area, seats_available });
                            //result_arr.push(response)
                            this.props.fetchSeatSelection(response);

                        }
                    });
                }
            });

        }
        else {
            this.toggleModel(true);
        }
    });
    }


    render(){
        //console.log('imageURL',this.props.seatMapInfo ? this.props.seatMapInfo.imageURL : '');
        //console.log('availabilityObj',this.props.availabilityObj)
        let imageURL = '', imageAvailable = '', seatSectionList = [], availabilityList = [],  mode = '', area = {};

        var price_cat_clr = '', jparse_coords = '', levelPrice = [], seatLevelAlias = '', seatSectionAlias = '', seatSectionId = '', priceCategoryNum = '', priceCategoryId = '', seatSectionType = '', seats_available = 0, priceCatAmount, priceCatCurrency = '', priceCatFormatted = '', priceCatAlias = '', product_id = '', seat_available_map = new Map(), seat_select_list_arr = '', seat_color = '', category_filter = [], price_cat_alias = '', category_map = new Map(), disp_tooltip = '';

        var pkgReqId = this.props.pkgReqId ? this.props.pkgReqId : '';

        var self = this;

        if(this.props.seatMapInfo){
            imageURL =  this.props.seatMapInfo.imageURL;
            imageAvailable =  this.props.seatMapInfo.imageAvailable;
            seatSectionList =  this.props.seatMapInfo.seatSectionList;
            mode = this.props.seatMapInfo.mode;
        }

        if(this.props.chosenProductId != "" && this.props.chosenProductId != undefined){
            product_id = this.props.chosenProductId;
        }

        if(this.props.availabilityObj){
            availabilityList =  this.props.availabilityObj;
            //console.log('availabilityList',availabilityList)
        }

        this.state.hoveredArea ? (
            disp_tooltip = 'active'
        ): (
            disp_tooltip = ''
        )

        return (
        <Fragment>
            <div className="col-md-4 col-xs-12 col-sm-12 filter-level-inner pl-0 filter-desktop mt-12">
                <h6>Step 1<br /><span>Please select your preferred section</span></h6>

                <div class="row filter-level" style={{ position: "relative" }}>
                    {
                        imageAvailable === 1 ? 
                        (
                            <img className="img-fluid overView" src={SITE_URL+imageURL} id="overviewSeatMap" useMap="#overview" alt="No Preview Available" />
                        ) : ('')
                    }

                    <map name="overview">
                        {
                             seatSectionList && seatSectionList.length > 0 && seatSectionList.map((seat_seaction, index) => (

                                seat_seaction.SeatLevel.map((seat_level, level_index) => {

                                    priceCategoryNum = seat_level.priceCategoryNum;
                                    price_cat_clr = this.getSeatColorCode(priceCategoryNum);
                                    seatLevelAlias = seat_level.seatLevelAlias;
                                    seatSectionAlias = seat_level.seatSectionAlias;
                                    priceCategoryId = seat_level.priceCategoryId;
                                    seatSectionId = seat_level.seatSectionId;
                                    seatSectionType = seat_level.seatSectionType;
                                    priceCatAmount = seat_seaction.priceCatAmount.amount;
                                    priceCatCurrency =  seat_seaction.priceCatAmount.currency;
                                    priceCatFormatted = seat_seaction.priceCatAmount.formatted;
                                    priceCatAlias = seat_seaction.priceCatAlias;
                                    
                                    levelPrice.push(seat_level.price.amount);

                                    const seatCatID = priceCategoryId+"_"+seatSectionId;
                
                                    if(!seat_available_map.has(priceCategoryId) || !seat_available_map.has(seatSectionId)){
                                        seat_available_map.set(priceCategoryId, true); 
                                        seat_available_map.set(seatSectionId, true);
                                    }
                
                                    if(!category_map.has(seat_seaction.priceCatAlias) || !category_map.has(seat_seaction.priceCatAmount.amount)){
                
                                        category_map.set(seat_seaction.priceCatAlias, true); 
                                        category_map.set(seat_seaction.priceCatAmount.amount, true); 
                    
                                        var price_cat_alias = seat_seaction.priceCatAlias;
                                        var price_amount = seat_seaction.priceCatAmount.amount;
                                        var price_currency = seat_seaction.priceCatAmount.currency;
                    
                    
                                        category_filter.push({price_cat_alias,price_amount,price_currency, price_cat_clr})
                                    }
                
                                    // if(seat_level.coordinatesList){
                                    //     jparse_coords = JSON.parse("[" + seat_level.coordinatesList + "]");
                
                                    //     area = {productId: product_id, mode: mode, seatLevelAlias: seatLevelAlias,  seatSectionId: seatSectionId, seatSectionAlias: seatSectionAlias, seatSectionType: seatSectionType, priceCategoryId: priceCategoryId, priceCategoryNum: priceCategoryNum, priceCatAlias: priceCatAlias, priceCatAmount: priceCatAmount, priceCatCurrency: priceCatCurrency, priceCatFormatted: priceCatFormatted, minAmount: Math.min(...levelPrice), maxAmount: Math.max(...levelPrice), shape: "poly", preFillColor: "", fillColor: price_cat_clr, lineWidth: 1, coords:jparse_coords, tooltip_id:'tooltip_id_'+index+level_index, pkgReqId}
                
                                    // }
                                    return(
                                        <Fragment>
                                            {
                                                seat_level.coordinatesList && seat_level.coordinatesList.map((coords,c_index) => {

                                                    area = {productId: product_id, mode: mode, seatLevelAlias: seatLevelAlias,  seatSectionId: seatSectionId, seatSectionAlias: seatSectionAlias, seatSectionType: seatSectionType, priceCategoryId: priceCategoryId, priceCategoryNum: priceCategoryNum, priceCatAlias: priceCatAlias, priceCatAmount: priceCatAmount, priceCatCurrency: priceCatCurrency, priceCatFormatted: priceCatFormatted, minAmount: Math.min(...levelPrice), maxAmount: Math.max(...levelPrice), shape: "poly", preFillColor: "", fillColor: price_cat_clr, lineWidth: 1, coords:coords, tooltip_id:'tooltip_id_'+index+level_index, pkgReqId, class_name:"area_cls_"+seatSectionId+"_"+priceCategoryId}

                                                    return(
                                                        <area
                                                            shape="poly" data-section-id={seatSectionId}
                                                            className={`area_cls_${seatSectionId && seatSectionId+"_"+priceCategoryId}`}
                                                            coords={coords}
                                                            tabIndex="0"
                                                            href="#"
                                                            key={(index + "_" + level_index + "_" + c_index)}
                                                            target={`area_cls_${seatSectionId && seatSectionId+"_"+priceCategoryId}`}
                                                            id={seatSectionId+"_"+priceCategoryId+"_"+c_index}               
                                                            name={seatSectionId}
                                                            data-seatcatsecid={seatCatID}
                                                            className={"cinema " + seatCatID}
                                                            onClick={this.clicked.bind(this, area)}
                                                            onMouseEnter={this.enterArea.bind(this, area)}
                                                            onMouseLeave={this.leaveArea.bind(this)}
                                                            onMouseMove={this.moveOnArea.bind(this)}
                                                            fillColor= "blue"
                                                        />
                                                    )
                                                })
                                            }                                            
                                        </Fragment>
                                    );
                                })

                             ))
                            
                        }                        
                    </map>

                    <span className={`tooltip mb_hide ${disp_tooltip}`} data-shape="" data-coords="" data-fill-color="">
                        {/* { this.state.hoveredArea && this.state.hoveredArea.name} */}
                        {
                            this.state.hoveredArea &&
                            (
                            <Fragment>
                                <span className="tooltip_sec_head">Section<br /><span>{this.state.hoveredArea.seatSectionAlias}</span></span>
                                <span className="tooltip_sec_cnt">
                                    <span className="seatClr" style={{background:this.state.hoveredArea.fillColor}}></span>
                                    <span className="tooltip_text"><b>{this.state.hoveredArea.priceCatAlias} Circle {this.state.hoveredArea.priceCategoryNum}</b><br/>{this.state.hoveredArea.priceCatCurrency} {this.state.hoveredArea.priceCatAmount.toFixed(2)}</span>
                                    {
                                        this.state.seatAvailable != '' ? (
                                            <span className="avail_seats">{this.state.seatAvailable} Seats Available</span>
                                        ) : (<span className="avail_seats"><img src={process.env.PUBLIC_URL + "/assets/images/seat_aval_loader.gif"} alt="Loader" title=""/></span>)
                                    }
                                    
                                </span>
                            </Fragment>
                            )
                        }
                    </span>

                </div>

                <a href="javascript:;" className="prcTable" data-target="#view_price_table" data-toggle="modal">View Price Table</a>

                <div className="modal fade sistic-login" id="view_price_table" tabindex="-1" role="dialog">
                    <div className="modal-dialog modal-dialog-centered" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h2>Price Table</h2>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div className="modal-body text-center">
                            <div className="prcTblOut">

                            <div className="mscroll-x">
                                <table className="prcTblCnt" cellPadding="7" cellSpacing="5">
                                    <thead>
                                        <th className="prcClsHead"></th>
                                        {                                 
                                            category_filter.length > 0 && category_filter.map((category_data, cat_index) => {
                                                return(
                                                    <th>
                                                         <span className="catClr" style={{background: category_data.price_cat_clr}}></span><br />
                                                        {category_data.price_cat_alias}
                                                    </th>
                                                )
                                            })
                                        }
                                    </thead>
                                    <tbody>
                                    {

                                        this.props.priceTableObj && this.props.priceTableObj.length > 0 && this.props.priceTableObj.map((priceTable, index) => {
                                            var price_table_map = new Map();

                                            return(
                                            <tr>
                                                <td className="priceClass">{priceTable.priceClassAlias}</td>
                                                {
                                                    priceTable.priceCatList.map((priceCat, cat_index) =>{

                                                        if(!price_table_map.has(priceCat.priceCategoryId) || !price_table_map.has(priceCat.priceValue.amount)){

                                                            price_table_map.set(priceCat.priceCategoryId, true); 
                                                            price_table_map.set(priceCat.priceValue.amount, true); 


                                                            return(
                                                                <td>{priceCat.priceValue.amount > 0 ? priceCat.priceValue.currency+' '+priceCat.priceValue.amount.toFixed(2) : '-'}</td>
                                                            )
                                                        }
                                                    })
                                                }
                                            
                                            </tr>)
                                        })
                                    
                                    }
                                        
                                    </tbody>
                                </table>
                                </div>
                            </div>

                            <div className="contactTxt">For patrons who require special access, please contact the SISTIC Hotline at +65 6348 5555</div>
                        </div>
                    </div>
                    </div>
                </div>  

                <Modal show={this.state.isShowModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleModel(false)} centered>
                    <Modal.Header closeButton>
                        <Modal.Title><p className="modal-title">Change Selection</p></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div><span>Would you like to release your current selection and search again?</span></div>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button onClick={()=>{
                        self.clickFromPrompt();
                    }}>Ok</Button>
                        <Button onClick={()=>this.toggleModel(false)}>Cancel</Button>
                    </Modal.Footer>
                </Modal>

            </div>

        </Fragment>
        )
    }
}


const mapStateToProps = (state, ownProps) => {
    //console.log('seat_state',state)
    return {
        seatMapInfo: state.seatMapInfo.seatMapInfo,
        availabilityObj: state.seatMapInfo.availabilityObj,
        priceTableObj: state.priceTableObj,
        chosenProductId: state.chosenProductId,
        totalQuantity:  state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        isEventWithSurvey:state.isEventWithSurvey,
        pkgReqId: state.pkgReqId,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSeatSelection: (dataObject) => {
            if(dataObject){
                dispatch(actionCreators.fetchSeatSelInfo(dataObject));
            }            
        },
        fetchSeatStageView: (dataObject) => {
            dispatch(actionCreators.fetchSeatStageViewInfo(dataObject));
        },
        fetchSeatsLevelsCoords: (dataObject) => {
            dispatch(actionCreators.fetchSeatLevelsCoords(dataObject));
        },
        overviewSelectedArea: (dataObject) => {
            dispatch(actionCreators.overviewSelectedArea(dataObject));
        },
        emptyCartInfo: ()=> {
            dispatch(actionCreators.emptyCartInfo());
        },
        updateCartForFrnd: (boolean) => {
            dispatch(actionCreators.updateCartForFrnd(boolean));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(SeatsOverView);
