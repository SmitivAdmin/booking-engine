import React, {Component, Fragment} from 'react'
import ReactDOM from 'react-dom';
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import moment from 'moment';
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import interactionPlugin from '@fullcalendar/interaction';
import _ from "lodash";
import filter from "lodash/filter"
import {Link} from "react-router-dom"
import jQuery, { event } from 'jquery'
import {Redirect} from 'react-router'
import {
    SITE_URL,
    AVAILABILE,
    LIMITED_SEATS,
    COMING_SOON,
    SHOW_CANCELLED,
    SHOW_POSTPONED,
    SHOW_RECONFIG,
    SINGLE_SEATS,
    SOLD_OUT,
    NO_DATE
} from '../../../constants/common-info'

class CalendarView extends Component {
    calendarComponentRef = React.createRef();

    constructor(props) {
        super(props);

        this.state ={
            noticeModel:false,
            productAdvisory:'',
            groupBookShowTime:0,
            groupBookOverview: 0,
            groupseatSelection: 0,
            reservedFrndError: '',
            errorMsg: '',
        }
    }

    toggleNoticeModel = (boolean) =>{
        // If true model will appear else not.
        
        this.setState({noticeModel: boolean});
    }

    componentDidMount(){
        let products = this.props.productsObj ? this.props.productsObj.showTimingList : 0;
        console.log('productsObj_data',products);
        let calendarApi = this.calendarComponentRef.current.getApi();

        if(jQuery(window).width() > 767){
            calendarApi.setOption('height', 530);
        } else{
            calendarApi.setOption('height', 'auto');
        }
        
        var today = moment().format('YYYY-MM-DD hh:mm'); 

        if(products && products.length > 0){
            console.log("first_date",typeof moment(products[0].showDateTime).format("YYYY-MM-DD hh:mm")+"_"+today);

            if(moment(products[0].showDateTime).format("YYYY-MM-DD hh:mm") > today){
                calendarApi.gotoDate(products[0].showDateTime);
            }
            
            //calendarApi.setOption('height', 500);
            products.map((show_time, index) => {
                var availability = this.getAvailability(show_time.availabilityStatus);
                var split_avail = availability.split(',');
                var bgColor = split_avail[2];
                var show_date =  show_time.showDateTime ? moment(show_time.showDateTime).format("YYYY-MM-DD") : '';
    
                console.log("show_date_data",show_date);
                setTimeout(function(){
                    jQuery(".fc-scrollgrid-sync-table").find('.fc-daygrid-day[data-date=' + show_date + ']').addClass('availDates');
                    jQuery(".fc-scrollgrid-sync-table").find('.fc-daygrid-day[data-date=' + show_date + ']').addClass(split_avail[1]);
                },500);
                
            });
        }
        
    }

    

    getAvailability(statusCode) {
        if (statusCode == "20") {
            return COMING_SOON
        } else if (statusCode == "19") {
            return AVAILABILE
        } else if (statusCode == "18") {
            return LIMITED_SEATS
        } else if (statusCode == "17") {
            return SINGLE_SEATS
        } else if (statusCode == "16") {
            return SOLD_OUT
        } else if (statusCode == "-6") {
            return SHOW_RECONFIG
        } else if (statusCode == "-7") {
            return SHOW_CANCELLED
        } else if (statusCode == "-8") {
            return SHOW_POSTPONED
        } else {
            return NO_DATE
        }
    }

    chooseCalDate(all_events, e){
        var THIS = this, selected_Date = e.dateStr, showTitle = '', venue = '', showTimesArr = [];
        console.log("event_data", e.dateStr);
        console.log("events_data", all_events);

        let products = THIS.props.productsObj ? THIS.props.productsObj.showTimingList : 0;
        console.log("products_Data",products);
        jQuery(".fc-scrollgrid-sync-table").find('.fc-daygrid-day').removeClass('active_cls');

        if(products && products.length > 0){
            //calendarApi.setOption('height', 500);
            products.map((show_time, index) => {
                var availability = THIS.getAvailability(show_time.availabilityStatus);
                var split_avail = availability.split(',');
                var show_date =  show_time.showDateTime ? moment(show_time.showDateTime).format("YYYY-MM-DD") : '';
                console.log("show_date_data_cal",typeof show_date+'_'+typeof selected_Date);
                if(show_date === selected_Date){
                    showTitle = show_time.showTitle;
                    venue = show_time.venue;
                    showTimesArr.push({'show_time':moment(show_time.showDateTime.substring(11, 16), ["HH:mm"]).format("hh:mm A"), 'product_id': show_time.productId, 'availabilityStatus': split_avail[0]})
                    jQuery(".fc-scrollgrid-sync-table").find('.fc-daygrid-day[data-date=' + selected_Date + ']').addClass('active_cls');
                }
            });
            //console.log("showTimesArr", showTimesArr);
            THIS.props.chooseShowDate(true, selected_Date, showTitle, venue, showTimesArr);
        }
    }

    render() {
        let products = this.props.productsObj ? this.props.productsObj.showTimingList : undefined;
        console.log('productsObj',this.props.productsObj);
        var events = [];
        var default_date = products && products.length > 0 && products[0].showDateTime;
        products && products.map((show_time, index) => {
            var availability = this.getAvailability(show_time.availabilityStatus);
            var split_avail = availability.split(',');
            var bgColor = split_avail[2];
            var show_date =  show_time.showDateTime ? moment(show_time.showDateTime).format("YYYY-MM-DD") : '';

            console.log("show_date",show_date);

            events.push({date:show_date, title: split_avail[0], productId: show_time.productId, isPromo: show_time.isPromo, showTitle: show_time.showTitle, ticketType: show_time.ticketType, venue: show_time.venue})
        });
        return (
        <Fragment>
            <div className="calendar-grid col-md-12">
                <FullCalendar
                    id="calendar"
                    selectable={false}
                    defaultView="dayGridMonth"
                    plugins={[dayGridPlugin, interactionPlugin]}
                    ref={this.calendarComponentRef}
                    events={events}
                    headerToolbar={{start: '',center: 'title',end: ''}}
                    dateClick={this.chooseCalDate.bind(this, events)}
                    fixedWeekCount={false}
                    showNonCurrentDates={false}
                />
                <div className="date-legend d-block d-sm-block d-md-none">
                    <span className="avail-state">Available</span>
                    <span className="fast-state">Selling Fast</span>
                    <span className="insuff-state">Sold Out</span>
                </div>
            </div>
        </Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        productsObj: state.showInfo && state.showInfo.productsObj && state.showInfo.productsObj,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        chooseShowDate: (flag, dateChosen, showTitle, venue, showTimesArr) => {
            dispatch(actionCreators.chooseShowDate(flag, dateChosen, showTitle, venue, showTimesArr));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CalendarView);

