import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import moment from 'moment';
import _ from "lodash";
import filter from "lodash/filter"
import {Link} from "react-router-dom"
import jQuery from 'jquery'
import {Redirect} from 'react-router'
import {
    SITE_URL,
    AVAILABILE,
    LIMITED_SEATS,
    COMING_SOON,
    SHOW_CANCELLED,
    SHOW_POSTPONED,
    SHOW_RECONFIG,
    SINGLE_SEATS,
    SOLD_OUT,
    TENANT,
    TICKETING_URL,
    monthNames
} from '../../../constants/common-info'

import he from 'he';
import HTMLParser from 'react-html-parser'
import * as API from "../../../service/events/eventsInfo"
import {Modal, Button} from 'react-bootstrap';
import CalendarView from './CalendarView'
import ShowTime from './ShowTime'

class ShowDateTime extends Component {

    constructor(props) {
        super(props);

        this.state ={
            noticeModel:false,
            productAdvisory:'',
            groupBookShowTime:0,
            groupBookOverview: 0,
            groupseatSelection: 0,
            reservedFrndError: '',
            errorMsg: ''
        }
    }

    toggleNoticeModel = (boolean) =>{
        // If true model will appear else not.
        
        this.setState({noticeModel: boolean});
    }

    componentWillMount() {
        this.props.breadCrumbData('datetime_visit', 1);
        this.props.breadCrumbData('timerAutoStart',true);

        var group_booking = window.location.search;

       
      

        // if(group_booking !== ""){
        //     let products = this.props.productsObj && this.props.productsObj.showTimingList ? this.props.productsObj.showTimingList[0] : '';
        //     if(products){
        //         this.showTime(this, products.productId, products.productId, dateChosen, ticket_type, isEventWithSurvey);
        //     }
        // }
        
        // let products = this.props.productsObj ? this.props.productsObj.showTimingList : undefined;
        // this.showTime(this, obj, productId, dateChosen, ticket_type, isEventWithSurvey);
        // {this.showTime.bind(this, eachProduct.showDateTime, eachProduct.dateTimeObj, eachProduct.productId, eachProduct.date+ " "+eachProduct.month+ " "+eachProduct.year, eachProduct.ticketType, eachProduct.isEventWithSurvey)}
    }

    componentDidUpdate(){
        //console.log('productsObj_data',this.props.productsObj);
        if(this.state.productAdvisory === ""){
            if(this.props.productsObj && this.props.productsObj.advisory){
                let newAdvisory = this.props.productsObj.advisory.replace(/\\/g, "").replace("/public/SISTIC/AdvisoryMessageImage/20-05-2020/1589956062742/original_gift-card.png", `/assets/images/original_gift-card.png`);
                this.setState({productAdvisory: newAdvisory, errorMsg: '', reservedFrndError: ''},()=>{
                    this.toggleNoticeModel(true);
                })
            }
        }

        if(this.state.reservedFrndError === ""){
            if(this.props.productsObj && this.props.productsObj.httpStatus === "400"){
                let ErrorMsg = this.props.productsObj.statusMessage;
                this.setState({reservedFrndError: ErrorMsg, productAdvisory: "", errorMsg: ''},()=>{
                    this.toggleNoticeModel(true);
                })
            }
        }        
    }

    // if its 16 - SOLD
    getAvailability(statusCode) {
        if (statusCode == "20") {
            return COMING_SOON
        } else if (statusCode == "19") {
            return AVAILABILE
        } else if (statusCode == "18") {
            return LIMITED_SEATS
        } else if (statusCode == "17") {
            return SINGLE_SEATS
        } else if (statusCode == "16") {
            return SOLD_OUT
        } else if (statusCode == "-6") {
            return SHOW_RECONFIG
        } else if (statusCode == "-7") {
            return SHOW_CANCELLED
        } else if (statusCode == "-8") {
            return SHOW_POSTPONED
        } else {
            return undefined
        }
    }

    showTime(key, obj, productId, dateChosen, ticket_type, isEventWithSurvey, e) {
        e.preventDefault();
        this.props.fetchSeatMapOverview("");
        this.props.chosenProductTime("");
        this.props.emptyCartInfo();
        //var split_ticket_type = ticket_type.split(','), prd_ticket_type;
        var prd_ticket_type;
        //alert(isEventWithSurvey);
        var group_booking = window.location.search;
        
        if(isEventWithSurvey === true){
            prd_ticket_type = "RS";
        } else if(isEventWithSurvey === false && group_booking != ""){
            prd_ticket_type = "RS";
        }else{
            prd_ticket_type = "GA";
        }
        //prd_ticket_type = "GA";
        this.props.chosenProductDate(true, obj[key.substring(0, 10)], productId, dateChosen, prd_ticket_type, isEventWithSurvey);
        //console.log('productId', productId);
        jQuery('.dateSec .date-item').removeClass("active");
        jQuery("#"+productId).addClass("active");
        
    }

    async chosenTime(timeSelected,e) {
        e.preventDefault();
        
        jQuery('.time-sec > date-item').removeClass('active');
        e.currentTarget.classList.add('active');
        this.props.chosenProductTime(timeSelected);

        if(this.props.chosenProductId != "" && this.props.chosenProductId != undefined){
            var product_id = this.props.chosenProductId;   
            let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';

            var group_booking = window.location.search;
            var searchParams = new URLSearchParams(group_booking);
            var bookingCode = searchParams && searchParams.get('groupBookingCode');
            if(bookingCode){
                bookingCode = bookingCode;
            } else{
                bookingCode = '';
            }

            console.log('prdTicketType',this.props.prdTicketType);

            API.setProductId(product_id, iccCode, bookingCode).then(response =>{
                console.log('set_product_id', response);
                if(response && response === 200){    
                    if(bookingCode !==  ""){
                        console.log('bookingCode', bookingCode);
                        API.getSeatAvailableGroup(product_id, group_booking).then(response => {
                            if (response !== undefined) {
                                console.log('group_available',response);
                                // $("#seatDesiableToggle").css("display", "none");
                                // $(".seat-img.selected").removeClass("selected");
                                if(response && response.length > 0){
                                    var price_cat_id = response[0].priceCategoryId;
                                    var seat_section_id = response[0].seatSectionId;

                                    API.catSection(price_cat_id, seat_section_id, 'SP', false).then(async response => {
                                        console.log('cat_section', response);
                                        if(response && response.message === 200){
                                        }
                                        await API.getSeatAvailable(product_id, price_cat_id, seat_section_id, 0, 'SP', bookingCode).then(response => {
                                            console.log('availablity_section', response);
                                            if(response && response.status === 200){
                                                console.log('group_seat_selection', response);
                                                jQuery(".pictarea").addClass('pointer_event');
                                                this.props.fetchSeatSelection(response.resObj);
                                            }
            
                                        });
        
                                    });
                                }
                                
                            }
                        });
                    }                            
                }
            });

            API.getSeatMapOverView(product_id).then(async response =>{
                console.log("overview_response", response);
                if(response && response.seatMapInfo.mode === "SP"){

                    jQuery(".filter-level img").removeClass('filter_cls');
                    jQuery(".filter-level .tooltip").removeClass('active');
                    // let stage_view = '';
                    // this.props.fetchSeatSelection(stage_view);
                    jQuery('.seat-map').removeClass('hide');

                    this.props.updateEventType('RS');

                    this.props.fetchSeatMapOverview(response);
                    
                    var top_position = jQuery('#seatMapTtl').offset();
                    jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
    
                    await API.getPriceTable(product_id).then(response =>{
                        this.props.fetchPriceTabele(response);
                    });
                    
                } else if(response && response.seatMapInfo.mode === "BA"){

                    this.props.updateEventType('GA');

                    this.props.fetchSeatMapOverview(response);
                    var priceCategoryId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].priceCategoryId;
                    var seatSectionId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].seatSectionId;
                    var mode = "GA";
                    var isMembership = response.seatMapInfo.isMembership === 0 ? false : true;

                    await API.catSection(priceCategoryId, seatSectionId, mode, isMembership, this.props.isEventWithSurvey).then(response => {
                        console.log('cat_section', response);
                    })

                    await API.ticketType(product_id, priceCategoryId, this.props.prdTicketType).then(response => {
                        console.log("Ticket Type Fetched Successfully", response.data);
                        if(response.status === 200){
                            this.props.updateTicketType(response.data);                    
                        }
    
                        jQuery('.ticket-quantity-upgraded').removeClass('hide');
                        setTimeout(function(){
                            var top_position = jQuery('.ticket-quantity-upgraded').offset();
                            jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
                        }, 1000);
                        
                    });
                } else{
                    //alert("Error Occuring!");
                    let ErrorMsg = response && response.seatMapInfo && response.seatMapInfo.error_description;
                    console.log("chooseTime",ErrorMsg);
                    this.setState({errorMsg: ErrorMsg},()=>{
                        this.toggleNoticeModel(true);
                    })
                }
            }); 
        } 
       
    }


    calculateTime(key, selected, dateChosen,ticket_type, isEventWithSurvey, e) {
        e.preventDefault();
        let products = this.props.productsObj ? this.props.productsObj.showTimingList : undefined;
        let filteredArray = _.find(products, function (product) {
            return (product.showDateTime.includes(key) && product.availabilityStatus == "19")
        });
        // let showTime = filteredArray.length > 0 ? filteredArray[0].showDateTime : undefined;
        let productId = filteredArray? filteredArray.productId : undefined;
        let productArray = this.getDateTimeObj(filteredArray);
        let dateObj = productArray&& productArray.length > 0? productArray[0].dateTimeObj: undefined;
        this.props.chosenProductDate(true, dateObj, productId, dateChosen, ticket_type, isEventWithSurvey);
        jQuery('.date-item').removeClass("active");
        (jQuery(`#${selected}`)).addClass("active");
    }

    showCal(e) {
        e.preventDefault();
        jQuery('.calViewSection').removeClass('hide');
        jQuery('.listViewSection').addClass('hide');
        jQuery(".event_time_list ul li").removeClass('actv');

        let products = this.props.productsObj ? this.props.productsObj.showTimingList : undefined;
        let result = filter(products, function (product) {
            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-0" + "3") && product.availabilityStatus == "16")
        }).length > 0;
        
        // document.getElementsByClassName('cal_view')[0].classList.toggle('hide');
        // document.getElementsByClassName('slider_view')[0].classList.toggle('hide');
        //console.log('classList', e.target.parentNode.classList);
        
        if(e.target.parentNode.classList.contains('actv') || e.target.classList.contains('actv')){
            if(!this.props.showCalFlag && this.props.hideDefaultDateView){
                this.props.showCalendarView(true, false, false);
            }
            if(this.props.showCalFlag && !this.props.hideDefaultDateView){
                this.props.showCalendarView(false, true, false);
            }

            jQuery('.cal_view').toggleClass('hide');
            jQuery('.slider_view').toggleClass('hide');
        } else{
            
        }

        // jQuery('.dateTimeTitle .btn-primary').removeClass('actv');
        // jQuery('.calViewSection .calendar_view_btn').addClass('actv');
    }

    showList(e){        
        e.preventDefault();
        jQuery('.calViewSection').addClass('hide');
        jQuery('.listViewSection').removeClass('hide');

        jQuery('.dateTimeTitle .btn-primary').removeClass('actv');
        jQuery('.listViewSection .list_view_btn').addClass('actv');

        jQuery('.cal_view').addClass('hide');
        jQuery('.slider_view').removeClass('hide');
         
        this.props.showCalendarView(true, false, false);
    }

   

    getDaysInMonth(month, year) {
        return new Date(year, month + 1, 0).getDate();
    }

    displayAll(e){
        e.preventDefault();
        jQuery('.date-item').toggleClass("hide");
        e.target.classList.toggle('less');
    }

    getDateTimeObj(products) {
        let productArray = [];
        let dataTimeObj = {};
        // TODO: Deepa clean this up
        if (products && products.length > 0) {
            for (let id in products) {
                let actualProduct = products[id];
                let dateString = actualProduct && actualProduct.showDateTime ? actualProduct.showDateTime.substring(0, 10): undefined;
                let timeArray = _.filter(products, function (product) {
                    return (product.showDateTime.includes(dateString) && product.availabilityStatus == "19")
                }).map((value) => {
                    return moment(value.showDateTime.substring(11, 16), ["HH:mm"]).format("hh:mm A");
                });
                dataTimeObj[dateString] = timeArray;
                let formattedDate = moment(actualProduct.showDateTime).format('llll').split(',');
                let year = dateString.split("-");
                var dateMonth = formattedDate && formattedDate[1] ? formattedDate[1].split(' ') : formattedDate;
                //TODO: Deepa move this out
                var availability = this.getAvailability(actualProduct.availabilityStatus);
                if(!productArray || !_.find(productArray, function (obj) {
                        return obj.showDateTime.includes(dateString)
                    })){
                    productArray.push({
                        day: formattedDate[0],
                        date: dateMonth[2],
                        month: dateMonth[1],
                        availability: availability,
                        showDateTime: actualProduct.showDateTime,
                        dateTimeObj: dataTimeObj,
                        productId: actualProduct.productId,
                        year:year[0],
                        ticketType:actualProduct.ticketType,
                        isEventWithSurvey:actualProduct.isEventWithSurvey
                    })
                }
            }
            return productArray;
        } else {
            let actualProduct = products;
            let timeArray = [];
            let formattedDate;
            if(actualProduct){
                timeArray.push(moment(actualProduct.showDateTime.substring(11, 16), ["HH:mm"]).format("hh:mm A"));
                // TODO: Deepa remove this
                formattedDate = moment(actualProduct.showDateTime).format('llll').split(',');
            }
            var dateMonth = formattedDate && formattedDate[1] ? formattedDate[1].split(' ') : formattedDate;
            //TODO: Deepa move this out
            var availability = this.getAvailability(actualProduct.availabilityStatus);
            productArray.push({
                day: formattedDate[0],
                date: dateMonth[2],
                month: dateMonth[1],
                availability: availability,
                showDateTime: actualProduct.showDateTime,
                dateTimeObj: timeArray,
                productId: actualProduct.productId,
                ticketType:actualProduct.ticketType,
                isEventWithSurvey:actualProduct.isEventWithSurvey
            })
        }
        return productArray;

    }

    render() {
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        // TODO: Deepa revisit this cond
        // if (this.props.timeChosen) {
        //     return <Redirect to={`/seats/${iccCode}`}/>
        // }

        var group_booking = window.location.search;
        var searchParams = new URLSearchParams(group_booking);
        var bookingCode = searchParams && searchParams.get('groupBookingCode');

        if(group_booking && this.props.productsObj && this.props.productsObj.showTimingList && this.props.productsObj.showTimingList[0]){
            var productId = this.props.productsObj.showTimingList[0].productId;
            if(this.state.groupBookShowTime === 0){
                this.setState({groupBookShowTime: 1},()=>{
                    setTimeout(function(){
                        jQuery(".date-slide .owl-item #"+productId).trigger("click");
                    },1500);
                });
            }
            if(this.state.groupBookOverview === 0 && this.state.groupBookShowTime === 1){
                this.setState({groupBookOverview: 1},()=>{
                    setTimeout(function(){
                        jQuery("#chosen-time").trigger("click");
                    },1500);
                });
            }            
            
        }

        if(this.props.showTimeFlag === true){
            setTimeout(function(){
                var top_position = jQuery('#selectTimeId').offset();
                if(top_position !== undefined){
                    jQuery('html,body').animate({scrollTop: (top_position.top)}, 700);
                }                
            },500)
        }
        
        let title = this.props.eventInfo ? this.props.eventInfo.title : '';
        console.log('eventInfo',this.props.eventInfo);

        let summaryImagePath = this.props.eventInfo ? this.props.eventInfo.summaryImagePath : undefined;
        let venue = this.props.eventInfo ? this.props.eventInfo.venue : undefined;
        let products = this.props.productsObj ? this.props.productsObj.showTimingList : undefined;
       
        let productArray = !this.props.showCalFlag && products && products.length > 0? this.getDateTimeObj(products) : null;
        
        console.log('productsObj',this.props.productsObj);
        // Date time mapping
        let dataTimeObj = {};

        let date = new Date();
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        let numberOfRows = 5;
        //let currentMonth = date.getMonth() + 1;
        
        var getFirstDate = products !== undefined ? products[0].showDateTime : '';
        var momentDate =  getFirstDate ? moment(getFirstDate).format("ddd,DD,MM,YYYY,hh:mm A").split(',') : [];
        let currentMonth = momentDate.length > 0 ? momentDate[2] : date.getMonth() + 1;
        
        //let year = date.getFullYear();
        let year = momentDate.length > 0 ? momentDate[3] : date.getFullYear();

        let tempArray = [];
        for (var j = 1; j < 32; j++) {
            let date = "" + year + "-" + currentMonth + "-" + j;
            let numberOfDatesMatched = _.filter(products, function (product) {
                return (product.showDateTime.includes(date) && product.availabilityStatus == "19")
            });
            if (numberOfDatesMatched > 0 && numberOfDatesMatched.length > 0) {
                tempArray.push({
                    availability: true,
                    index: j
                });
            }
        }
        //TODO: Deepa add a if case 30,31,28 and also the logic for the dates
        let arrayOfDaysMap = [];
        arrayOfDaysMap.push([0, 0, 1, 2, 3, 4, 5]);
        arrayOfDaysMap.push([6, 7, 8, 9, 10, 11, 12]);
        arrayOfDaysMap.push([13, 14, 15, 16, 17, 18, 19]);
        arrayOfDaysMap.push([20, 21, 22, 23, 24, 25, 26]);
        arrayOfDaysMap.push([27, 28, 29, 30, 31]);
        // firstDay = firstDay.split(" ")[0];
        let numberOfDays = this.getDaysInMonth(10, year);
        console.log('numberOfDays', numberOfDays);

        let rows = [], i = 0, len = numberOfDays;
        while (++i <= len) rows.push(i);

        if(title != ""){
            //console.log('parser123',he.decode(title));
            var decode_title = he.decode(title);
        }

        console.log("productAdvisory", this.state.productAdvisory);
        let form;
        
        form = (
        <Fragment>

        <section className="container-fluid ticket-quantity calViewSection">
            <div className="row">
                <div className="container mb-5 dateSec">
                    <h4 className="title-bdr dateTimeTitle">
                        Select A Date
                        <a href="javascript:;" className="btn btn-primary round-btn calendar_view_btn actv"
                            onClick={this.showCal.bind(this)}>
                            <img src={process.env.PUBLIC_URL + "/assets/images/calendar-btn.svg"} className="cal_view hide"
                                    alt="Calender View"/>
                            <img src={process.env.PUBLIC_URL + "/assets/images/calendar-list.svg"}
                                    alt="Slider View" className="slider_view"/>
                        </a>

                        <a href="javascript:;" className="btn btn-primary round-btn list_view_btn"
                            onClick={this.showList.bind(this)}>
                            <img src={process.env.PUBLIC_URL + "/assets/images/list_view_icon.svg"} className="list_view" alt="List View"/>
                        </a>
                    </h4>
                    {this.props.hideDefaultDateView &&
                    <div id="dektop-date-slide"
                            className="date-slide owl-carousel mt-4 owl-loaded owl-drag">
                        <div className="owl-stage-outer">
                            <div className="owl-stage"
                                    style={{
                                        transform: "translate3d(0px, 0px, 0px)",
                                        transition: "all 0s ease 0s",
                                        width: "2006px"
                                    }}>
                                {productArray && productArray.length > 0 && productArray.map((eachProduct) => {
                                    var availability = eachProduct.availability.split(',');
                                    return(
                                    <Fragment>
                                        {!eachProduct.availability && 
                                            (<div className="owl-item in-active" data-availability={eachProduct.availability} style={{
                                                width: "134.286px", "margin-right": "20px"}}>
                                                <div className={'date-item in-active'} data-eventsurvey={eachProduct.isEventWithSurvey} id={eachProduct.productId} onClick={this.showTime.bind(this, eachProduct.showDateTime, eachProduct.dateTimeObj, eachProduct.productId, eachProduct.date+ " "+eachProduct.month+ " "+eachProduct.year, eachProduct.ticketType, eachProduct.isEventWithSurvey)}>
                                                    <span className="day">{eachProduct.day}</span>
                                                    <span className="date">{eachProduct.date}</span>
                                                    <span className="month">{eachProduct.month}</span>
                                                    <span className={`status ${availability[1]}`}>{availability[0]}</span>
                                                </div>
                                            </div>)
                                        }
                                        {eachProduct.availability && 
                                            (<div className="owl-item" data-availability={eachProduct.availability} style={{width: "134.286px","margin-right": "20px"}} onClick={this.showTime.bind(this, eachProduct.showDateTime, eachProduct.dateTimeObj, eachProduct.productId, eachProduct.date+ " "+eachProduct.month+ " "+eachProduct.year, eachProduct.ticketType, eachProduct.isEventWithSurvey)}>
                                                <div className={"date-item"} id={eachProduct.productId}>
                                                    <span className="day">{eachProduct.day}</span>
                                                    <span className="date">{eachProduct.date}</span>
                                                    <span className="month">{eachProduct.month}</span>
                                                    <span className={`status ${availability[1]}`}>{availability[0]}</span>
                                                </div>
                                            </div>)
                                        }

                                    </Fragment>
                                    )                                                
                                })
                                }
                            </div>
                        </div>
                    </div>}
                    {this.props.hideDefaultDateView &&
                    <div id="mobile-date-slide" className="date-slide owl-carousel mt-4">
                        <div className="date-enclose">
                            {productArray && productArray.length > 0 && productArray.map((eachProduct) => {
                                var availability = eachProduct.availability.split(',');

                                return (
                                <Fragment>
                                    {!eachProduct.availability &&
                                    <div className={'date-item in-active'} data-availability={eachProduct.availability} id={eachProduct.productId} onClick={this.showTime.bind(this, eachProduct.showDateTime, eachProduct.dateTimeObj, eachProduct.productId, eachProduct.date+ " "+eachProduct.month+ " "+eachProduct.year, eachProduct.ticketType, eachProduct.isEventWithSurvey)}>
                                        <span className="date">{eachProduct.date}</span>
                                        <span className="month">{eachProduct.month}, </span>
                                        <span className="day">{eachProduct.day}</span>
                                        <span className={`status ${availability[1]}`}>{availability[0]}</span>
                                    </div>
                                    }
                                    {eachProduct.availability &&
                                    <div className={"date-item"} data-availability={eachProduct.availability} id={eachProduct.productId} onClick={this.showTime.bind(this, eachProduct.showDateTime, eachProduct.dateTimeObj, eachProduct.productId, eachProduct.date+ " "+eachProduct.month+ " "+eachProduct.year, eachProduct.ticketType, eachProduct.isEventWithSurvey)}>
                                        <span className="date">{eachProduct.date}</span>
                                        <span className="month">{eachProduct.month}, </span>
                                        <span className="day">{eachProduct.day}</span>
                                        <span className={`status ${availability[1]}`}>{availability[0]}</span>
                                    </div>
                                    }

                                </Fragment>
                                )
                            })
                        }
                        </div>
                    </div>
                    }
                    
                    {
                        this.props.showCalFlag && 
                        <CalendarView />
                    }

                    
                </div>
                {
                    this.props.showTimeFlag && (
                        <ShowTime />
                    )
                }

            </div>
        </section>


            <Modal show={this.state.noticeModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleNoticeModel(false)} centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">{this.state.productAdvisory != "" ? 'Notice' : 'Error'}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="modal_error_msg">
                        {
                            this.state.productAdvisory !== "" && this.state.errorMsg === "" && (
                                <div dangerouslySetInnerHTML={{ __html: this.state.productAdvisory }}></div>
                            )
                        }
                        {
                            this.state.reservedFrndError !== "" && (
                                <div dangerouslySetInnerHTML={{ __html: this.state.reservedFrndError}}></div>
                            )
                        }
                        {
                            this.state.errorMsg !== "" && (
                                <div dangerouslySetInnerHTML={{ __html: this.state.errorMsg}}></div>
                            )
                        }
                    </div>
                </Modal.Body>
                <Modal.Footer>
                {
                    (this.state.productAdvisory !== "" || this.state.errorMsg !== "") && (
                        <Button onClick={()=>this.toggleNoticeModel(false)}>Ok</Button>
                    )
                }
                {
                    this.state.reservedFrndError != "" && (
                    //    <Button onClick={()=>this.toggleNoticeModel(false)}>Ok</Button>
                        <a href={iccCode} className="btn btn-primary">Ok</a>
                    )
                }
                </Modal.Footer>
            </Modal>

            {/* <Footer/> */}
        </Fragment>
        );
        return (
        <Fragment>
            {form}
        </Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        productsObj: state.showInfo && state.showInfo.productsObj && state.showInfo.productsObj,
        totalQuantity: state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        adult_qty: state.adult_qty,
        child_qty: state.child_qty,
        nsf_qty: state.nsf_qty,
        sectz_qty: state.sectz_qty,
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        showTimeFlag: state.showTimeFlag,
        showCalFlag: state.showCalFlag,
        hideDefaultDateView: state.hideDefaultDateView,
        showTimes: state.showTimes,
        showActive: state.showActive,
        chosenProductId: state.chosenProductId,
        timeChosen: state.timeChosen,
        prdTicketType: state.prdTicketType,
        isEventWithSurvey: state.isEventWithSurvey
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchShowInfoData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        chosenProductDate: (flag, times, productId, dateChosen, ticket_type, isEventWithSurvey) => {
            dispatch(actionCreators.chooseDate(flag, times, productId, 'active', dateChosen, ticket_type, isEventWithSurvey));
        },
        chosenProductTime: (time) => {
            dispatch(actionCreators.chooseTime(time));
        },
        emptyCartInfo: ()=> {
            dispatch(actionCreators.emptyCartInfo());
        },
        showCalendarView: (flag, hide) => {
            dispatch(actionCreators.showCalendarView(flag, hide));
        },
        breadCrumbData: (field, value) => {
            dispatch(actionCreators.breadCrumbs(field, value));
        },
        fetchSeatMapOverview: (dataObject) => {
            dispatch(actionCreators.fetchSeatMapInfo(dataObject));
        },
        fetchPriceTabele: (dataObject) => {
            dispatch(actionCreators.fetchPriceTabele(dataObject));
        },
        fetchSeatSelection: (dataObject) => {
            dispatch(actionCreators.fetchSeatSelInfo(dataObject));           
        },
        updateTicketType: (list) => {
            dispatch(actionCreators.updateTicketType(list));
        },
        updateEventType: (event_type) => {
            dispatch(actionCreators.updateEventType(event_type));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowDateTime);

