import React, { Component, Fragment, useEffect} from 'react'
import { connect } from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import { Modal, Button } from 'react-bootstrap';

import { Link } from "react-router-dom"
import _ from "lodash";
import jQuery from 'jquery';
import * as API from "../../../service/events/eventsInfo";
import { Redirect } from 'react-router';
import { TENANT, SHOPPING_CART } from '../../../constants/common-info';

class TicketType extends Component {
    constructor(props) {
        super(props);
        this.state = {
            hidediv: false,
            promoQtyVal: 0,
            promoQtyId: '',
            promoErrorMsg: "",
            errorModel:false,
            errorMsg: ''
        };
    }
    toggleErrorModel = (boolean) =>{
        // If true model will appear else not.
        this.setState({errorModel: boolean});
    }
    componentDidUpdate(){
        window.jQuery('[data-toggle="tooltip"]').tooltip()
    }

    confirmCheckout(ticket_type, e) {
        e.preventDefault();
        console.log('patronEmail',this.props.patronEmail);

        var promo_pwd = jQuery("#promotionInput").val();
        var promo_type_id = jQuery("#qtyTypeId").val();
        var replace_type_id = promo_type_id.replace('quantity-','');

        var promoter_list = this.props.seatMapInfo && this.props.seatMapInfo.promoterNameList && this.props.seatMapInfo.promoterNameList;

        var selected_cart_items = this.props.ticketTypesSelected && this.props.ticketTypesSelected; 
        console.log('cart_ticketTypesSelected',selected_cart_items);

        var groupBookingQuantity = '';

        var reservedCartInfo = this.props.reservedCartInfo && this.props.reservedCartInfo; 
        console.log('reservedcartInfo2',reservedCartInfo);
        var reservedQty = reservedCartInfo.length > 0 ? reservedCartInfo[0][Object.keys(reservedCartInfo[0])[0]] : 0;
        console.log('reservedQty',reservedQty);
        
        var priceClassMap = [];
        var add_promo_pwd = '';
        selected_cart_items && selected_cart_items.map((cart_items, index) => {
            var get_qty = cart_items[Object.keys(cart_items)[0]];
            if(get_qty > 0){
                if(promo_pwd !="" && cart_items.isPwd === 1){
                    add_promo_pwd = "|"+promo_pwd;
                } else{
                    add_promo_pwd = '';
                }
                priceClassMap.push(cart_items.classCode+':'+get_qty+add_promo_pwd);
            }            
        });
        console.log('priceClassMap_data',priceClassMap);
        if(priceClassMap.length > 0){
            var cart_info;
            if(reservedQty > 0){
                cart_info = { "priceClassMap": priceClassMap.toString(), "isHidePrice": 0, "isShowEndTime":0, "promoterNameList": promoter_list, "groupBookingQuantity": reservedQty};
            } else{
                cart_info = { "priceClassMap": priceClassMap.toString(), "isHidePrice": 0, "isShowEndTime":0, "promoterNameList": promoter_list};
            }
            console.log('cart_info_data', cart_info);
        }

        API.addCart(cart_info).then(response => {
            console.log('add_cart', response);
            // window.location = '/'+SHOPPING_CART;
            if (response && response.status === 200) {
                //alert("success");
                window.location = '/' + SHOPPING_CART;
            } else {
                //alert("failure");
                console.log('cart_failure', response);
                if(response && response.data && response.data.errorCode === "error.generic"){
                    this.setState({errorMsg: "An error has occurred. Please refresh the page and try again or contact our Ticketing Hotline at +65 6348 5555 for further assistance."},()=>{
                        this.toggleErrorModel(true);
                    })                    
                } else{
                    let error_msg = response && response.data && response.data.statusMessage;
                    if(error_msg){
                        this.setState({errorMsg: error_msg},()=>{
                            this.toggleErrorModel(true);
                        })
                    }
                    //window.location = '/'+SHOPPING_CART;
                } 
            }
        })

        if(this.props.patronEmail === undefined || this.props.patronEmail === ""){
            window.jQuery("#sistic-login-modal").modal("show");
        } else{
            if(jQuery("#qtyTypeVal").val() > 0 && jQuery("#promotionInput").val() === ""){
                var promo_position = jQuery('.ticket-quantity-upgraded').offset();
                jQuery('html,body').animate({scrollTop: (promo_position.top - 50)}, 700);
    
                jQuery("#promotionInput").addClass('error_bdr');
                this.setState({
                    promoErrorMsg: "Please enter promo password when this ticket type is selected."
                });
            }
            else{
                jQuery("#promotionInput").removeClass('error_bdr');
                this.setState({
                    promoErrorMsg: ""
                });
                
                if (ticket_type === "RS") {
                    if (this.props.quantity === this.props.currentQuantity) {
                        var cartArray = this.props.cartInfo;
                        var total_price = 0;
                        var ref = this;
                        let temp = [];
                        var ticketTypeObj = this.props.ticketTypesSelected;
                        _.each(ticketTypeObj, function (ticketType) {
                            var cartObj = {};
                            let length = parseInt(_.get(ticketType, ticketType.key));
                            for (var i = length - 1; i >= 0; i--) {
                                var newObj = cartArray[i];
                                total_price += parseInt(ticketType.price);
                                cartObj = _.mergeWith(newObj, cartObj, function customizer(objValue, srcValue) {
                                    if (_.isArray(objValue)) {
                                        return objValue.concat(srcValue);
                                    }
                                });
                                cartObj['price'] = ticketType.price;
                                cartObj['type'] = ticketType.name;
                                _.pullAt(cartArray, i);
                            }
                            cartObj['totalPrice'] = total_price;
                            temp.push(cartObj);
        
                        });
                        this.props.combineCartDetails(temp);
                    }
                }
                if(ticket_type === "GA"){
                    if(this.props.currentQuantity > 0){
                        var cartArray = this.props.cartInfo;
                        var total_price = 0;
                        var ref = this;
                        let temp = [];
                        var ticketTypeObj = this.props.ticketTypesSelected;
                        _.each(ticketTypeObj,function(ticketType){
                            var cartObj ={};
                            let length = parseInt(_.get(ticketType, ticketType.key));
                            for (var i = length - 1; i >= 0; i--) {
                                var newObj = cartArray[i];
                                total_price+=parseInt(ticketType.price);
                                cartObj = _.mergeWith(newObj, cartObj, function customizer(objValue, srcValue) {
                                    if (_.isArray(objValue)) {
                                        return objValue.concat(srcValue);
                                    }
                                });
                                cartObj['price']=ticketType.price;
                                cartObj['type']=ticketType.name;
                               _.pullAt(cartArray, i);
                            }
                            cartObj['totalPrice'] = total_price;
                            temp.push(cartObj);
            
                        });
                        this.props.combineCartDetails(temp);
                    } 
                }
              
            } 
        }

               
        
    }

    updateQty(qtyType, qtyField, id, price, name, classCode, isPwd, e) {
        //e.preventDefault();
        console.log('upd_qty',qtyType+' '+qtyField+' '+id+' '+price+' '+name);

        if (qtyType === "increment") {
            var qty_value = parseInt(document.getElementById(id).value) + 1;           

            if(name === "Reserved for Friends"){
                document.getElementById(id).value = qty_value;
                var temp = this.props.reservedCartInfo;
                var json = {};
                if (_.find(temp, function (o) { return o.key == qtyField })) {
                    _.find(temp, function (o) { return o.key == qtyField })[qtyField] = _.find(temp, function (o) { return o.key == qtyField })[qtyField] + 1;
                }
                else {
                    json[qtyField] = qty_value;
                    json['key'] = qtyField;
                    json['name'] = name;
                    json['price'] = price;
                    json['classCode'] = classCode;
                    json['isPwd'] = isPwd;                    
                    temp.push(json);
                }
                this.props.addReservedCartInfoGA(temp, this.props.currentQuantity, 1);

            } else{
                var temp = this.props.ticketTypesSelected;
                var json = {};

                if(isPwd === 1){
                    this.setState({
                        promoQtyVal:qty_value,
                        promoQtyId: id
                    });                        
                }

                if (_.find(temp, function (o) { return o.key == qtyField })) {
                    _.find(temp, function (o) { return o.key == qtyField })[qtyField] = _.find(temp, function (o) { return o.key == qtyField })[qtyField] + 1;
                }
                else {
                    json[qtyField] = qty_value;
                    json['key'] = qtyField;
                    json['name'] = name;
                    json['price'] = price;
                    json['classCode'] = classCode;
                    json['isPwd'] = isPwd;                   
                    temp.push(json);
                }

                this.props.updateQtyData(temp, this.props.currentQuantity, 1);
            }
            
            // if(this.props.quantity === qty_value){
            //     jQuery("#cart-qty-plus").prop("disabled", true);
            // }
        }
        if (qtyType === "decrement") {
            if (parseInt(document.getElementById(id).value) <= 0) {
                var qty_value = 0;
            } else {
                var qty_value = document.getElementById(id).value - 1;
            }
            
            if(name === "Reserved for Friends"){
                document.getElementById(id).value = document.getElementById(id).value - 1;
                var temp = this.props.reservedCartInfo;
                var json = {};
                if (_.find(temp, function (o) { return o.key == qtyField })) {
                    _.find(temp, function (o) { return o.key == qtyField })[qtyField] = _.find(temp, function (o) { return o.key == qtyField })[qtyField] - 1;
                }
                else {
                    json[qtyField] = qty_value;
                    json['key'] = qtyField;
                    temp.push(json);
                }
                this.props.addReservedCartInfoGA(temp, this.props.currentQuantity, -1);
            
            } else{
                var temp = this.props.ticketTypesSelected;
                var json = {};
                if(isPwd === 1){
                    this.setState({
                        promoQtyVal:qty_value,
                        promoQtyId: id
                    });  
                }
                if (_.find(temp, function (o) { return o.key == qtyField })) {
                    _.find(temp, function (o) { return o.key == qtyField })[qtyField] = _.find(temp, function (o) { return o.key == qtyField })[qtyField] - 1;
                }
                else {
                    json[qtyField] = qty_value;
                    json['key'] = qtyField;                    
                    temp.push(json);
                }
                this.props.updateQtyData(temp, this.props.currentQuantity, -1);
            }
            
        }

        setTimeout(function () {
            var promoQty = jQuery("#qtyTypeVal").val();
            if (document.getElementById(id).value > 0) {
                jQuery("#qtyPrcCls" + qtyField).addClass('active');
                jQuery("#cart-qty-minus" + qtyField).removeClass('in-active');
            } else {
                jQuery("#qtyPrcCls" + qtyField).removeClass('active');
                jQuery("#cart-qty-minus" + qtyField).addClass('in-active');
            }
            if(parseInt(promoQty) > 0){
                jQuery(".promocodeContainer").removeClass('hide');
            } else{
                jQuery(".promocodeContainer").addClass('hide');
                jQuery("#qtyTypeId").val('');
                jQuery("#promotionInput").val('');
            }
        }, 500)

    }

    // selectSeats(ticketTypesSelected) {

    //     var temp = this.props.ticketTypesSelected;
    //     if (temp === "true") {
    //         this.props.addReservedCartInfo({ temp });

    //     } else {
    //         this.props.cartData({ temp });
    //     }
    //     var cartInfo = this.props.ticketTypesSelected;
    //     cartInfo.map((cart_data, index) => {
    //         if (cart_data.ticketTypesSelected === ticketTypesSelected) {
    //             this.props.removeResCartData(index);
    //         }
    //     })


    // handleClick(qtyType, qtyField, id, price, name, e) {
    //     var qty_value = + 1;
    //     var temp = this.props.ticketTypesSelected;
    //     console.log('//////////', temp)
    //     var json = {};
    //     {
    //         json[qtyField] = qty_value;
    //         json['key'] = qtyField;
    //         json['name'] = name;
    //         json['price'] = price;
    //         temp.push(json);
    //     }
    //     this.props.updateQtyData(temp, this.props.currentQuantity, 1);

    //     this.setState({
    //         hidediv: true
    //     });


    // }
    routeChange(e) {
        e.preventDefault();
        this.props.history.push('/date-time/' + this.props.match.params.id)
    }
    

    render() {
        var groupBookingMode = 0;
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        let ticketTypes = this.props.ticketType ? this.props.ticketType : [];
        let prdTicketType = this.props.prdTicketType ? this.props.prdTicketType : '';
        let overView = this.props.seatMapInfo ? this.props.seatMapInfo : '';

        if(overView){
            groupBookingMode = overView.groupBookingMode ? overView.groupBookingMode : 0;
        }

        var group_booking = window.location.search;
        var searchParams = new URLSearchParams(group_booking);
        var bookingCode = searchParams && searchParams.get('groupBookingCode');
        if(bookingCode){
            bookingCode = bookingCode;
        } else{
            bookingCode = '';
        }
        

        console.log('quantity', this.props.quantity);

        console.log('error_msg', this.state.errorMsg);

        //console.log('ticketTypesSelected_state', this.props.ticketTypesSelected);

        // console.log('ticketTypes_test', ticketTypes);
        // console.log('ticketTypesSelected_test',this.props.ticketTypesSelected);

        // console.log('mergedCartInfo_data',this.props.mergedCartInfo)

        //  if (this.props.mergedCartInfo && this.props.mergedCartInfo.length > 0) {
        //      //return <Redirect to={`/delivery/${iccCode}`}/>
        //      return <Redirect to={`/${SHOPPING_CART}`} />
        //  }
        return (
            <div>
            <section className="container-fluid ticket-quantity-upgraded hide">
                <div className="row">
                    <div className="container">
                        <h4 className="title-bdr">Select Your Ticket Type</h4>

                        <div className="promocodeContainer hide">
                            <span className="promocodeTitle">Have a Promotion Password?</span>                            
                            <input autocomplete="none" className="promotionInput" id="promotionInput" spellcheck="false" type="text" placeholder="Enter Password" ></input>
                            {
                                prdTicketType && prdTicketType === "RS" ? (
                                    <button className={`promotionBtn ${(this.props.quantity != 0 && this.props.quantity == this.props.currentQuantity) ? 'active' : 'in-active'}`} onClick={(e) => this.confirmCheckout(prdTicketType, e)} type="button">Validate</button>
                                ) : (
                                    <button className={`promotionBtn ${(this.props.currentQuantity > 0) ? 'active' : 'in-active'}`} onClick={(e) => this.confirmCheckout(prdTicketType, e)} type="button">Validate</button>
                                )
                            }
                            
                            <input type="hidden" name="qtyTypeId" id="qtyTypeId" value={this.state.promoQtyId} />
                            <input type="hidden" name="qtyTypeVal" id="qtyTypeVal" value={this.state.promoQtyVal} />
                            {
                                this.state.promoErrorMsg && (
                                    <span className="errMsg">{this.state.promoErrorMsg}</span>
                                )
                            }
                        </div>
                        <div className="col-md-12 ticket-list px-0">
                            {
                                ticketTypes && ticketTypes.length > 0 && ticketTypes.map((eachType) => (

                                    <div className="tic-list-item col-md-12 px-0" id={`qtyPrcCls${eachType.priceClassId}`} data-params={eachType.priceClassCode +','+ eachType.passwordRequired}>

                                        <div className="ticket-bar">
                                            <span className="tic-left">
                                                <div className="tt-align">
                                                <b >{eachType.priceClassAlias}</b>
                                                {
                                                    eachType.priceClassDescription && (
                                                        <i class="tootltip-icon test" aria-hidden="true" data-toggle="tooltip" data-placement="right" title={eachType.priceClassDescription}>
                                                            <img  src={process.env.PUBLIC_URL + "/assets/images/info-tooltip.svg"} alt="icon"/>
                                                        </i>
                                                    )
                                                }
                                                </div>
                                                
                                                <br />{eachType.priceValueAmount.currency + " " + eachType.priceValueAmount.amount.toFixed(2)}
                                            </span>
                                            {
                                                prdTicketType && prdTicketType === "RS" && (
                                                    <div className="tic-right button-container rs">
                                                        <button className={`cart-qty-minus in-active`} id={`cart-qty-minus${eachType.priceClassId}`} type="button" value="-"
                                                            onClick={(e) => this.updateQty('decrement', `${eachType.priceClassId}`, `${"quantity-" + eachType.priceClassId}`, `${eachType.priceValueAmount.amount}`, `${eachType.priceClassAlias}`, eachType.priceClassCode, eachType.passwordRequired, e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/minus.svg"} alt="icon" />
                                                        </button>

                                                        <input type="text" name={'quantity-' + eachType.priceClassId} id={'quantity-' + eachType.priceClassId} className="qty form-in" maxLength="12"
                                                            value={(this.props.ticketTypesSelected && this.props.ticketTypesSelected.length > 0) ? _.get(_.find(this.props.ticketTypesSelected, function (o) { return o.key == eachType.priceClassId }), eachType.priceClassId) : 0} />

                                                        {this.props.quantity >= this.props.currentQuantity &&
                                                        <button className={`cart-qty-plus ${this.props.quantity === this.props.currentQuantity ? 'in-active' : ''}`} id={`cart-qty-plus${eachType.priceClassId}`} type="button" value="+"
                                                            onClick={(e) => this.updateQty('increment', `${eachType.priceClassId}`, `${"quantity-" + eachType.priceClassId}`, `${eachType.priceValueAmount.amount}`, `${eachType.priceClassAlias}`, eachType.priceClassCode, eachType.passwordRequired, e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/plus.svg"} alt="icon" />
                                                        </button>}
                                                        
                                                    </div>
                                                )
                                            }
                                            {

                                                prdTicketType && prdTicketType === "GA" && (
                                                    <div className="tic-right button-container ga" >
                                                        <button className={`cart-qty-minus in-active`} id={`cart-qty-minus${eachType.priceClassId}`} type="button" value="-"
                                                            onClick={(e) => this.updateQty('decrement', `${eachType.priceClassId}`, `${"quantity-" + eachType.priceClassId}`, `${eachType.priceValueAmount.amount}`, `${eachType.priceClassAlias}`, eachType.priceClassCode, eachType.passwordRequired, e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/minus.svg"} alt="icon" />
                                                        </button>

                                                        <input type="text" name={'quantity-' + eachType.priceClassId} id={'quantity-' + eachType.priceClassId} className="qty form-in" maxLength="12"
                                                            value={(this.props.ticketTypesSelected && this.props.ticketTypesSelected.length > 0) ? _.get(_.find(this.props.ticketTypesSelected, function (o) { return o.key == eachType.priceClassId }), eachType.priceClassId) : 0} />


                                                        <button className={`cart-qty-plus`} id={`cart-qty-plus${eachType.priceClassId}`} type="button" value="+"
                                                            onClick={(e) => this.updateQty('increment', `${eachType.priceClassId}`, `${"quantity-" + eachType.priceClassId}`, `${eachType.priceValueAmount.amount}`, `${eachType.priceClassAlias}`, eachType.priceClassCode, eachType.passwordRequired, e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/plus.svg"} alt="icon" />
                                                        </button>
                                                    </div>
                                                )
                                            }

                                        </div>
                                    </div>
                                ))
                            }
                            <p className="qtyNote">* price excludes booking fee</p>

                            {
                            this.props.prdTicketType && this.props.prdTicketType === "GA" && groupBookingMode === 1 &&
                            (
                                <Fragment>
                                    {
                                        bookingCode !== "" ? (
                                            <div className="tic-list-item col-md-12 px-0">
                                                <div className="ticket-bar">
                                                    <span className="tic-left"><b>I would like to process with my own booking instead</b><br /></span>
                                                    <div className="tic-right ">
                                                        <a className="btn confirm-btn" href={`/${TENANT}/booking/${iccCode}`}>Confirm</a>
                                                    </div>
                                                </div>
                                            </div>
                                        ) : (
                                            <div className="tic-list-item col-md-12 px-0" id="qtyPrcCls0">

                                                <div className="ticket-bar">

                                                    <span className="tic-left">
                                                        <b>How many tickets did you want to reserve for friends?</b>
                                                        <i class="tootltip-icon" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="You can reserve seats up to 8 friends to this event! Check your confirmation email after purchase for a unique link to invite friends. Reserved seats are held for 24 hours.">
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/info-tooltip.svg"} alt="icon"/>
                                                        </i>
                                                        <br />SGD 0.00
                                                    </span>


                                                    <div className="tic-right button-container ga">

                                                        <button className={`cart-qty-minus in-active`} id={`cart-qty-minus0`} type="button" value="-" onClick={(e) => this.updateQty('decrement', '0', `${"quantity-0"}`, "0.00", 'Reserved for Friends', '', '', e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/minus.svg"} alt="icon" />
                                                        </button>

                                                        <input type="text" name={'quantity-0'} id={'quantity-0'} className="qty form-in" maxLength="12" defaultValue={0} />

                                                        <button className={`cart-qty-plus`} id={`cart-qty-plus0`} type="button" value="+" onClick={(e) => this.updateQty('increment', '0', "quantity-0", "0.00", 'Reserved for Friends', '', '', e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/plus.svg"} alt="icon" />
                                                        </button>

                                                    </div>


                                                </div>
                                            </div>
                                
                                        )
                                    }
                                    
                                {/* 
                                    <div className="tic-list-item col-md-12 px-0" id="qtyPrcCls0">

                                        <div className="ticket-bar">

                                            <span className="tic-left">
                                                <b>How many tickets did you want to reserve for friends?</b>
                                                <i class="tootltip-icon" aria-hidden="true" data-toggle="tooltip" data-placement="right" title="You can reserve seats up to 8 friends to this event! Check your confirmation email after purchase for a unique link to invite friends. Reserved seats are held for 24 hours.">
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/info-tooltip.svg"} alt="icon"/>
                                                </i>
                                                <br />SGD 0.00
                                            </span>


                                            <div className="tic-right button-container ga">
                                                <button className={`cart-qty-minus in-active`} id={`cart-qty-minus0`} type="button" value="-" onClick={(e) => this.updateQty('decrement', '0', `${"quantity-0"}`, "0.00", 'Reserved for Friends', '', '', e)}>
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/minus.svg"} alt="icon" />
                                                </button>

                                                <input type="text" name={'quantity-0'} id={'quantity-0'} className="qty form-in" maxLength="12" defaultValue={0} />

                                                <button className={`cart-qty-plus`} id={`cart-qty-plus0`} type="button" value="+" onClick={(e) => this.updateQty('increment', '0', "quantity-0", "0.00", 'Reserved for Friends', '', '', e)}>
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/plus.svg"} alt="icon" />
                                                </button>

                                            </div>


                                        </div>
                                    </div> */}
                                </Fragment>
                            )

                        }

                        {this.props.seatForFrnds &&
                            <p className="resSeatAlert">Reserved seats not reflected here</p>
                        }
                            
                    </div>


                        <div className="col-md-12 px-0 text-right mt-5 checkOutBtn">
                            {

                                <Fragment>
                                    {
                                        prdTicketType && prdTicketType === "RS" && (
                                            this.props.patronEmail !== undefined && this.props.patronEmail !== "" ? (
                                                <button className={`btn check-out-btn ${(this.props.quantity != 0 && this.props.quantity == this.props.currentQuantity) ? 'active' : 'in-active'} `} id="confirm-type" type="submit" onClick={(e) => this.confirmCheckout(prdTicketType, e)}>
                                                    Checkout
                                                    <img className="ml-4" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} alt="icon" />
                                                </button>
                                            ) : (
                                                <button className={`btn check-out-btn ${(this.props.quantity != 0 && this.props.quantity == this.props.currentQuantity) ? 'active' : 'in-active'} `} id="confirm-type" onClick={(e) => this.confirmCheckout(prdTicketType, e)}>
                                                    Checkout
                                                    <img className="ml-4" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} alt="icon" />
                                                </button>
                                            )
                                            
                                        )
                                    }

                                    {
                                        prdTicketType && prdTicketType === "GA" && (
                                            this.props.patronEmail !== undefined && this.props.patronEmail !== "" ? (
                                                <button className={`btn check-out-btn ${(this.props.currentQuantity > 0) ? 'active' : 'in-active'} `} id="confirm-type" type="submit" onClick={(e) => this.confirmCheckout(prdTicketType, e)}>
                                                    Checkout
                                                    <img className="ml-4" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} alt="icon" />
                                                </button>
                                            ) : (
                                                <button className={`btn check-out-btn ${(this.props.currentQuantity > 0) ? 'active' : 'in-active'} `} id="confirm-type" onClick={(e) => this.confirmCheckout(prdTicketType, e)}>
                                                    Checkout
                                                    <img className="ml-4" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} alt="icon" />
                                                </button>
                                            )
                                        )
                                    }
                                </Fragment>
                            }

                            <Modal show={this.state.errorModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleErrorModel(false)} centered>
                                <Modal.Header closeButton>
                                    <Modal.Title id="contained-modal-title-vcenter">Error</Modal.Title>
                                </Modal.Header>
                                <Modal.Body>
                                    <div dangerouslySetInnerHTML={{ __html: this.state.errorMsg }}></div>
                                </Modal.Body>
                                <Modal.Footer>
                                    <Button onClick={()=>this.toggleErrorModel(false)}>Ok</Button>
                                </Modal.Footer>
                            </Modal>

                        </div>
                    </div>
                </div>
            </section>
            </div>)
    }
}
const mapStateToProps = (state, ownProps) => {
    //console.log('ticketTypesSelected_state', state.ticketTypesSelected);
    return {
        productsObj: state.showInfo && state.showInfo.productsObj && state.showInfo.productsObj,
        seatMapInfo: state.seatMapInfo.seatMapInfo,
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        ticketType: state.ticketType,
        currentQuantity: state.currentQuantity,
        cartInfo: state.cartInfo,
        quantity: state.quantity,
        reservedCartInfo: state.reservedcartInfo,
        mergedCartInfo: state.mergedCartInfo,
        adult_qty: state.adult_qty,
        child_qty: state.child_qty,
        nsf_qty: state.nsf_qty,
        sectz_qty: state.sectz_qty,
        ticketTypesSelected: state.ticketTypesSelected,
        patronName: state.patronName,
        patronEmail: state.email,
        seatForFrnds: state.seatForFrnds,
        prdTicketType: state.prdTicketType,
        ...ownProps
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        cartData: (cart_values, reserved_list) => {
            dispatch(actionCreators.addCartInfo(cart_values));
        },
        addReservedCartInfoGA: (obj, current, increment) => {
            dispatch(actionCreators.addReservedCartInfoGA(obj, current, increment));
        },
        removeResCartData: (value) => {
            dispatch(actionCreators.removeReservedCartInfo(value));
        },
        updateQtyData: (obj, current, increment) => {
            dispatch(actionCreators.updateQty(obj, current, increment));
        },
        updateCurrentQuantity: (current, increment) => {
            dispatch(actionCreators.updateCurrentQuantity(current, increment));
        },
        combineCartDetails: (obj) => {
            dispatch(actionCreators.combineCartDetails(obj));
        },
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(TicketType);