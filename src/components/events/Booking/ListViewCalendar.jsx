import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import moment from 'moment';
import _ from "lodash";
import filter from "lodash/filter"
import {Link} from "react-router-dom"
import jQuery from 'jquery'
import {Redirect} from 'react-router'
import {
    SITE_URL,
    AVAILABILE,
    LIMITED_SEATS,
    COMING_SOON,
    SHOW_CANCELLED,
    SHOW_POSTPONED,
    SHOW_RECONFIG,
    SINGLE_SEATS,
    SOLD_OUT,
    TENANT,
    TICKETING_URL,
    monthNames
} from '../../../constants/common-info'

import he from 'he';
import HTMLParser from 'react-html-parser'
import * as API from "../../../service/events/eventsInfo"



class ListViewCalendar extends Component {

    constructor(props) {
        super(props);
    }

    getAvailability(statusCode) {
        var status_clr = "#343434";
        var status_text = ""
        if (statusCode == "20") {
            status_clr = "#4ee014";
            status_text =  COMING_SOON;
        } else if (statusCode == "19") {
            status_clr = "#07b3f0";
            status_text = AVAILABILE
        } else if (statusCode == "18") {
            status_clr = "#f2b119";
            status_text = LIMITED_SEATS
        } else if (statusCode == "17") {
            status_clr = "#fc5d5d";
            status_text = SINGLE_SEATS
        } else if (statusCode == "16") {
            status_clr = "#b5b5b5";
            status_text = SOLD_OUT
        } else if (statusCode == "-6") {
            status_clr = "#343434"
            status_text = SHOW_RECONFIG
        } else if (statusCode == "-7") {
            status_clr = "#343434"
            status_text = SHOW_CANCELLED
        } else if (statusCode == "-8") {
            status_clr = "#343434"
            status_text = SHOW_POSTPONED
        } else {
            status_clr = ""
            status_text = ""
        }
        return status_text+','+status_clr;
    }

    showCal_list(e) {
        e.preventDefault();
        jQuery('.calViewSection').removeClass('hide');
        jQuery('.listViewSection').addClass('hide');

        jQuery(".event_time_list ul li").removeClass('actv');

        let products = this.props.productsObj ? this.props.productsObj.showTimingList : undefined;
        let result = filter(products, function (product) {
            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-0" + "3") && product.availabilityStatus == "16")
        }).length > 0;
        if(!this.props.showCalFlag && this.props.hideDefaultDateView){
            this.props.showCalendarView(true, false, false);
        }
        if(this.props.showCalFlag && !this.props.hideDefaultDateView){
            this.props.showCalendarView(false, true, false);
        }
        // document.getElementsByClassName('cal_view')[0].classList.toggle('hide');
        // document.getElementsByClassName('slider_view')[0].classList.toggle('hide');

        if(e.target.parentNode.classList.contains('actv') || e.target.classList.contains('actv')){
            if(!this.props.showCalFlag && this.props.hideDefaultDateView){
                this.props.showCalendarView(true, false, false);
            }
            if(this.props.showCalFlag && !this.props.hideDefaultDateView){
                this.props.showCalendarView(false, true, false);
            }

            jQuery('.cal_view').toggleClass('hide');
            jQuery('.slider_view').toggleClass('hide');
        } else{
            
        }

        jQuery('.dateTimeTitle .btn-primary').removeClass('actv');
        jQuery('.calViewSection .calendar_view_btn').addClass('actv');
    }

    showList_list(e){
        e.preventDefault();
        jQuery('.calViewSection').addClass('hide');
        jQuery('.listViewSection').removeClass('hide');

        // jQuery('.dateTimeTitle .btn-primary').removeClass('actv');
        // jQuery('.listViewSection .list_view_btn').addClass('actv');
    }

    async chooseEvent(productId, choose_date, choose_time, ticket_type, isEventWithSurvey, e){
        e.preventDefault();
        // var split_ticket_type = ticket_type.split(',');
        // if(isEventWithSurvey === true){
        //     split_ticket_type = "RS";
        // } else{
        //     split_ticket_type = "GA";
        // }
        var prd_ticket_type;
        //alert(isEventWithSurvey);
        var group_booking = window.location.search;
        
        if(isEventWithSurvey === true){
            prd_ticket_type = "RS";
        } else if(isEventWithSurvey === false && group_booking != ""){
            prd_ticket_type = "RS";
        }else{
            prd_ticket_type = "GA";
        }
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';

        this.props.chosenProductDate(false, [], productId, '', choose_date, prd_ticket_type, isEventWithSurvey);
        this.props.chosenProductTime(choose_time);

        // jQuery(".filter-level img").removeClass('filter_cls');
        // jQuery(".filter-level .tooltip").removeClass('active');
        // let stage_view = {};
        // this.props.fetchSeatSelection(stage_view);

        jQuery(".event_time_list ul li").removeClass('actv');
        e.currentTarget.classList.add('actv');

        // await API.setProductId(productId, iccCode, '').then(response =>{
        //     console.log('set_product_id', response);
        // })

        var group_booking = window.location.search;
        var searchParams = new URLSearchParams(group_booking);
        var bookingCode = searchParams && searchParams.get('groupBookingCode');
        if(bookingCode){
            bookingCode = bookingCode;
        } else{
            bookingCode = '';
        }

        console.log('prdTicketType',this.props.prdTicketType);

        // if(productId != "" && prd_ticket_type === "RS"){
        //     var product_id = productId;
        //     jQuery(".filter-level img").removeClass('filter_cls');
        //         jQuery(".filter-level .tooltip").removeClass('active');
        //         // let stage_view = '';
        //         // this.props.fetchSeatSelection(stage_view);
        //         jQuery('.seat-map').removeClass('hide');
                
        //         API.getSeatMapOverView(product_id, prd_ticket_type).then(response =>{
        //         if(response){

        //             this.props.fetchSeatMapOverview(response);
                    
        //             var top_position = jQuery('#seatMapTtl').offset();
        //             jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
    
        
        //             API.getPriceTable(product_id).then(response =>{
        //                 this.props.fetchPriceTabele(response);
        //             });
                    
        //             API.setProductId(product_id, iccCode, bookingCode).then(response =>{
        //                 console.log('set_product_id', response);
        //                 if(response && response === 200){                                
        //                 }
        //                 if(bookingCode !==  ""){
        //                     console.log('bookingCode', bookingCode);
        //                     API.getSeatAvailableGroup(product_id, group_booking).then(response => {
        //                         if (response !== undefined) {
        //                             console.log('group_available',response);
        //                             // $("#seatDesiableToggle").css("display", "none");
        //                             // $(".seat-img.selected").removeClass("selected");
        //                             if(response && response.length > 0){
        //                                 var price_cat_id = response[0].priceCategoryId;
        //                                 var seat_section_id = response[0].seatSectionId;

        //                                 API.catSection(price_cat_id, seat_section_id, 'SP', false).then(async response => {
        //                                     console.log('cat_section', response);
        //                                     if(response && response.message === 200){
        //                                     }
        //                                     await API.getSeatAvailable(product_id, price_cat_id, seat_section_id, 0, 'SP', bookingCode).then(response => {
        //                                         console.log('availablity_section', response);
        //                                         if(response && response.status === 200){
        //                                             console.log('group_seat_selection', response);
        //                                             jQuery(".pictarea").addClass('pointer_event');
        //                                             this.props.fetchSeatSelection(response.resObj);
        //                                         }
                
        //                                     });
            
        //                                 });
        //                             }
                                    
        //                         }
        //                     });
        //                 }
                        
        //             });
        //         }
        //     });        
        // } else {
        //     var priceCatId = '';
        //     var product_id = productId;
        //     API.setProductId(product_id, iccCode).then(response =>{
        //         console.log('set_product_id', response);
        //         if(response && response === 200){                                
        //         }
        //         API.getSeatMapOverView(product_id, prd_ticket_type).then(async response =>{
        //             console.log('seat_map_overview', response);
        //             if(response && response.seatMapInfo && response.seatMapInfo.seatSectionList && response.seatMapInfo.seatSectionList[0] && response.seatMapInfo.seatSectionList[0].SeatLevel[0] && response.seatMapInfo.seatSectionList[0].SeatLevel[0].priceCategoryId){
        //                 this.props.fetchSeatMapOverview(response);
        //                 var priceCategoryId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].priceCategoryId;
        //                 var seatSectionId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].seatSectionId;
        //                 var mode = "GA";
        //                 var isMembership = response.seatMapInfo.isMembership === 0 ? false : true;

        //                 await API.catSection(priceCategoryId, seatSectionId, mode, isMembership, this.props.isEventWithSurvey).then(response => {
        //                     console.log('cat_section', response);
        //                 })

        //                 await API.ticketType(product_id, priceCategoryId, prd_ticket_type).then(response => {
        //                     console.log("Ticket Type Fetched Successfully", response.data);
        //                     if(response.status === 200){
        //                         this.props.updateTicketType(response.data);                    
        //                     }
        
        //                     jQuery('.ticket-quantity-upgraded').removeClass('hide');
        //                     setTimeout(function(){
        //                         var top_position = jQuery('.ticket-quantity-upgraded').offset();
        //                         jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
        //                     }, 1000);
                            
        //                 });
                        
        //             }
        //         });
        //     });
        // }

        var product_id = productId;

        API.setProductId(product_id, iccCode, bookingCode).then(response =>{
            console.log('set_product_id', response);
            if(response && response === 200){    
                if(bookingCode !==  ""){
                    console.log('bookingCode', bookingCode);
                    API.getSeatAvailableGroup(product_id, group_booking).then(response => {
                        if (response !== undefined) {
                            console.log('group_available',response);
                            // $("#seatDesiableToggle").css("display", "none");
                            // $(".seat-img.selected").removeClass("selected");
                            if(response && response.length > 0){
                                var price_cat_id = response[0].priceCategoryId;
                                var seat_section_id = response[0].seatSectionId;

                                API.catSection(price_cat_id, seat_section_id, 'SP', false).then(async response => {
                                    console.log('cat_section', response);
                                    if(response && response.message === 200){
                                    }
                                    await API.getSeatAvailable(product_id, price_cat_id, seat_section_id, 0, 'SP', bookingCode).then(response => {
                                        console.log('availablity_section', response);
                                        if(response && response.status === 200){
                                            console.log('group_seat_selection', response);
                                            jQuery(".pictarea").addClass('pointer_event');
                                            this.props.fetchSeatSelection(response.resObj);
                                        }
        
                                    });
    
                                });
                            }
                            
                        }
                    });
                }                            
            }
        });

        API.getSeatMapOverView(product_id).then(async response =>{
            console.log("overview_response", response);
            if(response && response.seatMapInfo.mode === "SP"){

                jQuery(".filter-level img").removeClass('filter_cls');
                jQuery(".filter-level .tooltip").removeClass('active');
                // let stage_view = '';
                // this.props.fetchSeatSelection(stage_view);
                jQuery('.seat-map').removeClass('hide');

                this.props.updateEventType('RS');

                this.props.fetchSeatMapOverview(response);
                
                var top_position = jQuery('#seatMapTtl').offset();
                jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);

                await API.getPriceTable(product_id).then(response =>{
                    this.props.fetchPriceTabele(response);
                });
                
            } else if(response && response.seatMapInfo.mode === "BA"){

                this.props.updateEventType('GA');

                this.props.fetchSeatMapOverview(response);
                var priceCategoryId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].priceCategoryId;
                var seatSectionId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].seatSectionId;
                var mode = "GA";
                var isMembership = response.seatMapInfo.isMembership === 0 ? false : true;

                await API.catSection(priceCategoryId, seatSectionId, mode, isMembership, this.props.isEventWithSurvey).then(response => {
                    console.log('cat_section', response);
                })

                await API.ticketType(product_id, priceCategoryId, this.props.prdTicketType).then(response => {
                    console.log("Ticket Type Fetched Successfully", response.data);
                    if(response.status === 200){
                        this.props.updateTicketType(response.data);                    
                    }

                    jQuery('.ticket-quantity-upgraded').removeClass('hide');
                    setTimeout(function(){
                        var top_position = jQuery('.ticket-quantity-upgraded').offset();
                        jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
                    }, 1000);
                    
                });
            } else{
                alert("Error Occuring!");
            }
        }); 
    }

    render() {
        let date_map = new Map();
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        let showDateTimeList = this.props.productsObj ? this.props.productsObj.showTimingList : '';

        return (

            <Fragment>
                <section className="container-fluid ticket-quantity listViewSection hide">
                    <div className="row">
                        <div className="container mb-5">
                            <h4 className="title-bdr dateTimeTitle">
                                Select An Event
                                <a href="javascript:;" className="btn btn-primary calendar_view_btn round-btn" onClick={this.showCal_list.bind(this)}>
                                    <img src={process.env.PUBLIC_URL + "/assets/images/calendar-btn.svg"} className="cal_view"
                                            alt="Calender View"/>
                                    <img src={process.env.PUBLIC_URL + "/assets/images/calendar-list.svg"}
                                            alt="Slider View" className="slider_view hide"/>
                                </a>

                                <a href="javascript:;" className="btn btn-primary list_view_btn round-btn" onClick={this.showList_list.bind(this)}>
                                    <img src={process.env.PUBLIC_URL + "/assets/images/list_view_icon.svg"} className="list_view" alt="List View"/>
                                </a>
                            </h4>

                            <div className="col-12 list_view_cnt">
                            {
                                showDateTimeList && showDateTimeList.length > 0 && showDateTimeList.map((dateTimeList, index) =>{

                                    var date_time = dateTimeList.showDateTime;
                                    var getDate = date_time.substring(0,10);
                                    var formattedDate = moment(date_time).format("ddd,DD,MMM,YYYY,hh:mm A").split(',');
                                    // var avail_status = this.getAvailability(dateTimeList.availabilityStatus);

                                    if(!date_map.has(getDate)){
                                        date_map.set(getDate, true); 

                                        return(
                                        <div className="row list_view_row">
                                            <div class="date-item">
                                                <span class="day">{formattedDate[0]}</span>
                                                <span class="date">{formattedDate[1]}</span>
                                                <span class="month">{formattedDate[2]}</span>
                                                {/* <span class="status">{avail_status}</span> */}
                                            </div>
                                            <div className="event_time_list">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/list_view_time_arw.svg"} alt="" title="" className="time_left_arw" />
                                                
                                                <ul className="col-12">
                                                {
                                                    showDateTimeList && showDateTimeList.length > 0 && showDateTimeList.map((timeList, time_index) =>{

                                                    var date_time_2 = timeList.showDateTime;
                                                    var getDate_2 = date_time_2.substring(0,10);
                                                    var formattedDate_2 = moment(date_time_2).format("ddd,DD,MMM,YYYY,hh:mm A").split(',');
                                                    var avail_status_2 = this.getAvailability(timeList.availabilityStatus);
                                                    var split_status = avail_status_2.split(',');
                                                    var showTitle = timeList.showTitle;
                                                    var venue = timeList.venue;

                                                    var choose_date = formattedDate[1]+' '+formattedDate[2]+', '+formattedDate[3];
                                                    var choose_time = formattedDate_2[4];

                                                    if(getDate_2 === getDate){
                                                        return(
                                                            <li className={`${timeList.availabilityStatus === 16 ? 'soldout' : ''}`} onClick={this.chooseEvent.bind(this, timeList.productId, choose_date, choose_time,  timeList.ticketType, timeList.isEventWithSurvey)}>
                                                                <input type="radio" name="event_id"/> 
                                                                <span class="event_opt"></span>
                                                                <span className ="event_text">
                                                                    {showTitle}: {venue}  |  {choose_time}<br />
                                                                <span className="aval_text" style={{color: split_status[1]}}>{split_status[0]}</span>
                                                                </span>
                                                            </li>
                                                        )
                                                    }
                                                    
                                                    })
                                                }

                                                </ul>
                                            </div>
                                        </div>
                                        )
                                    }
                                    
                                })
                                
                            }
                                
                            </div>
                        </div>
                    </div>
                </section>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    console.log('state_prdTicketType', state.prdTicketType)
    return {
        productsObj: state.showInfo && state.showInfo.productsObj,
        totalQuantity: state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        adult_qty: state.adult_qty,
        child_qty: state.child_qty,
        nsf_qty: state.nsf_qty,
        sectz_qty: state.sectz_qty,
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        showTimeFlag: state.showTimeFlag,
        showCalFlag: state.showCalFlag,
        hideDefaultDateView: state.hideDefaultDateView,
        showTimes: state.showTimes,
        showActive: state.showActive,
        chosenProductId: state.chosenProductId,
        timeChosen: state.timeChosen,
        prdTicketType: state.prdTicketType,
        isEventWithSurvey: state.isEventWithSurvey
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        chosenProductDate: (flag, times, productId, showTime, dateChosen, ticket_type, isEventWithSurvey) => {
            dispatch(actionCreators.chooseDate(flag, times, productId, showTime, dateChosen, ticket_type, isEventWithSurvey));
        },
        chosenProductTime: (time) => {
            dispatch(actionCreators.chooseTime(time));
        },
        showCalendarView: (flag, hide) => {
            dispatch(actionCreators.showCalendarView(flag, hide));
        },
        fetchSeatMapOverview: (dataObject) => {
            dispatch(actionCreators.fetchSeatMapInfo(dataObject));
        },
        fetchPriceTabele: (dataObject) => {
            dispatch(actionCreators.fetchPriceTabele(dataObject));
        },
        fetchSeatSelection: (dataObject) => {
            dispatch(actionCreators.fetchSeatSelInfo(dataObject));         
        },
        updateTicketType: (list) => {
            dispatch(actionCreators.updateTicketType(list));
        },
        updateEventType: (event_type) => {
            dispatch(actionCreators.updateEventType(event_type));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListViewCalendar);

