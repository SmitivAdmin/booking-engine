import React, {Component} from 'react'

export class Articles extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <section className="sec-3 article-wrap">
                <div className="card pos-3">
                    <div className="container-fluid pad-0">
                        <div className="row pt-5">
                            <div className="col-md-8 col-8"><h3 className="head">Articles</h3></div>
                            <div className="col-md-4 col-4 text-right">
                                <a href="javascript:;" className="see-all">
                                    See all
                                    <img src="assets/images/chevron-right-blue.svg" alt="icon"/>
                                </a>
                            </div>
                        </div>
                        <div className="row pb-4 mob-slide">
                            <div className="col-md-4 slide-item">
                                <div className="card pos-img">
                                    <img className="card-img-top" src="assets/images/article-1.png" alt="Card image cap" />
                                    <div className="card-body px-0">
                                        <h5 className="card-title">A Guide to SIFA 2019 Music-centric Gems</h5>
                                        <p className="card-text">Artists who are ground-breaking in their fields have always inspired me ...<a href="javascript:;">More</a></p>

                                    </div>
                                </div>


                            </div>
                            <div className="col-md-4 slide-item">
                                <div className="card pos-img">
                                    <img className="card-img-top" src="assets/images/article-2.png" alt="Card image cap" />
                                    <div className="card-body px-0">
                                        <h5 className="card-title">Best Ever Opera Lorem Ipsum Sit Dolor Amet</h5>
                                        <p className="card-text">Artists who are ground-breaking in their fields have always inspired me ...<a href="javascript:;">More</a></p>

                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4 slide-item">
                                <div className="card pos-img">
                                    <img className="card-img-top" src="assets/images/article-3.png" alt="Card image cap" />
                                    <div className="card-body px-0">
                                        <h5 className="card-title">Mauris malesuada nisi sit amet augue</h5>
                                        <p className="card-text">Artists who are ground-breaking in their fields have always inspired me ...<a href="javascript:;">More</a></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}




