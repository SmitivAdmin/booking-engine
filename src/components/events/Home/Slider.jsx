import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Link} from "react-router-dom"
import * as actionCreators from "../../../actions/actionCreator"
import {TENANT} from '../../../constants/common-info';

class Slider extends Component {
    constructor(props){
        super(props);
    }

    render(){
        let internetContentCode = "patrick1220";

        return (
            <section className="sec-1">
                <div className="col-md-12">
                    <div className="row">
                        <div className="col-lg-8 col-md-12 px-0">
                            <div className="show-slider">
                                <div id="big" className="owl-carousel owl-theme">
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-1.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-2.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-3.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-1.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-2.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-3.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-1.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-2.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-3.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/banner-img-2.jpg"} />
                                    </div>
                                </div>
                                <div id="thumbs" className="owl-carousel owl-theme">
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-1.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-2.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-3.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-1.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-2.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-3.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-1.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-2.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-3.jpg"} />
                                    </div>
                                    <div className="item">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/thumbnail-2.jpg"} />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="col-lg-4 col-md-12 float-right mtt-5 res-mtt0 mob-px-0 bannRgt">
                            <div className="col-md-12 ml-0 pl-0 pr-0 mt-2 float-left">
                                <div className="col-md-12 mt-2 clearfix vert_middle">
                                    <p className="d-none d-sm-none d-md-block breadCrumb">Home / Musicals</p>
                                    <div
                                        className="d-block d-sm-block d-md-none label-sec float-left vert_middle">
                                        <span className="badge badge-primary">Musical</span>
                                        <span className="badge badge-outline">12+ Years</span>
                                    </div>
                                    <a href="#" className="float-right share-hdr"><img width="100%" height="50%" src={process.env.PUBLIC_URL + "/assets/images/share-icon.png"} /></a>
                                    <a href="#"
                                        className="d-block d-sm-block d-md-none float-right share-hdr"><img
                                        width="100%" height="50%" src={process.env.PUBLIC_URL + "/assets/images/info-icon.png"} /></a>
                                </div>
                                <div
                                    className="d-none d-sm-none d-md-block col-md-12 mt-4 label-sec vert_middle">
                                    <span className="badge badge-primary">Musical</span>
                                    <span className="badge badge-outline">Dance</span>
                                </div>

                                <div className="col-md-12">
                                    <h4 className="mt-4 font-weight-bold show-title">Aladdin - The Hit Broadway
                                        Musical</h4>
                                </div>

                                <div className="col-md-12 mt-3 f-rubik clearfix">
                                    <div>
                                        <img className="mt-0 mr-2 float-left vert_middle"
                                                src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.png"}/>
                                        <p className="text-muted float-left mb-0 dateCnt">Fri, 19 Apr - Sun, 19
                                            May 2019<a href="javascript:;"
                                                        className="d-block d-sm-block d-md-none">View all Dates &
                                                Time</a></p>

                                    </div>
                                </div>
                                <div className="col-md-12 mt-3 f-rubik clearfix d-block d-sm-block d-md-none">
                                    <div>
                                        <img className="mt-0 mr-2 float-left vert_middle"
                                                src={process.env.PUBLIC_URL + "/assets/images/map-icon-grey.png"} />
                                        <p className="text-muted float-left mb-0 viewAllVenues">Sands Theatre
                                            Marina Bay Sands<a href="javascript:;" className="d-block">View all
                                                Venues</a></p>
                                    </div>
                                </div>
                                <div className="col-md-12 mt-3 f-rubik d-block d-sm-block d-md-none">
                                    <div>
                                        <img className="mt-0 mr-2 float-left vert_middle"
                                                src={process.env.PUBLIC_URL + "/assets/images/seat-grey-icon.png"}/>
                                        <p className="text-muted seatMapOut"><a href="javascript:;"
                                                                                className="d-block">Seat Map</a>
                                        </p>
                                    </div>
                                </div>
                                <div className="col-md-12 mt-3 f-rubik d-block d-sm-block d-md-none">
                                    <div>
                                        <img className="mt-0 mr-2 float-left vert_middle"
                                                src={process.env.PUBLIC_URL + "/assets/images/dollar-grey-icon.png"}/>
                                        <p className="text-muted prc_cnt">S$45 - S$380</p>
                                    </div>
                                </div>
                                <div className="d-none d-sm-none d-md-block">
                                    <div className="col-md-12 mt-3 f-rubik">
                                        <div>
                                            <img className="mt-0  mr-2 ml-0 float-left"
                                                    src={process.env.PUBLIC_URL + "/assets/images/map-icon.png"} />
                                            <p className="ml-3 locationTxt">Sands Theatre Marina Bay Sands</p>
                                        </div>
                                    </div>

                                    <div className="col-md-12 mt-4">
                                        <div>
                                            <h6 className="font-weight-bold text-muted">Price</h6>
                                            <h4 className="font-weight-bold price">S$45 - S$380</h4>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-12 mt-4 mb-4">
                                    <Link to={`/${TENANT}/booking/${internetContentCode}`}>
                                        <div className="col-md-12 btn-primary my-2">
                                            Buy Tickets
                                        </div>
                                    </Link>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        eventInfo: state.showInfo?state.showInfo.eventInfo:''
    }
};


export default connect(mapStateToProps, (dispatch) => {
        return {
            fetchData: (dataObject) => {
                dispatch(actionCreators.fetchShowInfo(dataObject));
            }
        }
    }
)(Slider);