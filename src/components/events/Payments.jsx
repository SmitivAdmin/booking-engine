import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../actions/actionCreator"

import {Link} from "react-router-dom"
import Footer from '../common/Footer'
import Header from '../common/Header'

import PaymentCartDetails from './Payment/PaymentCartDetails'
import PaymentMethods from './Payment/PaymentMethods'

class Payments extends Component {
    constructor(props){
        super(props);
    }

    componentWillMount(){
        this.props.breadCrumbData('payment_visit',1);
    }
    render(){
        return (
            <Fragment>
                <Header/>

                <main className="payment_page">

                    <PaymentCartDetails />

                    <PaymentMethods {...this.props.propsObj}/>

                    {/* <button class="btn btn-primary proceed">
                        Confirm Order
                        <img class="ml-3" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} />
                    </button> */}
                </main>

                {/* <Footer/> */}
            </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        breadCrumbData: (field,value) => {
            dispatch(actionCreators.breadCrumbs(field,value));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Payments);
