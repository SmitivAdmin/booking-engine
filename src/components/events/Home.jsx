import React, {Component, Fragment} from 'react'
import {Link} from "react-router-dom";
import {connect} from 'react-redux'
import Footer from '../common/Footer'
import Header from '../common/Header'
import {Articles} from "./Home/Articles";
import {Description} from "./Home/Description";
import Slider from "./Home/Slider";
import {SimilarPicks} from "./Home/SimilarPicks";


class Home extends Component {

    constructor(props) {
        super(props);

    }
    render() {
        return (

            <Fragment>
                <Header/>

                <main>
                    <Slider />

                    <Description/>

                    <section className="button-sec container-fluid mob-px-0">
                        <hr/>
                        <div className="container-fluid btm-badge mb-5 pl-3">
                            <span className="ml-3 badge badge-outline">Entertainment</span>
                            <span className="badge badge-outline">Family</span>
                            <span className="badge badge-outline">Fun</span>
                        </div>

                    </section>

                    <Articles/>

                    <SimilarPicks />

                </main>

                <Footer/>
            </Fragment>
        );
    }
}


const mapStateToProps = (state) => {
    return state
};
//
// export default connect(mapStateToProps, (dispatch) => {
//         return {
//             fetchData: (dataObject) => {
//                 dispatch(actionCreators.fetchShowInfo(dataObject));
//             }
//         }
//     }
// )(Home);

export default connect(mapStateToProps)(Home);

