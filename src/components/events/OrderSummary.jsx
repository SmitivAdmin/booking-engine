import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../actions/actionCreator"
import Footer from '../common/Footer'
import Header from '../common/Header'
import OrderedCartDetails from './OrderSummary/OrderedCartDetails'
import BookingReference from './OrderSummary/BookingReference'
import $ from 'jquery'

class BookingConfirm extends Component {
    constructor(props){
        super(props);
        this.state = {
            confirmation_transOrder: ''
        }
    }

    componentWillMount(){
        this.props.breadCrumbData('confirmantion_visit',1);
    }

    componentDidMount() {
        
        var conf_hasLiveStreamEvent = $("#orderconfirmation").attr("data-hasLiveStreamEvent");
        if(conf_hasLiveStreamEvent){
            console.log("conf_hasLiveStreamEvent", conf_hasLiveStreamEvent);
            this.props.confirmOrderData('conf_hasLiveStreamEvent',conf_hasLiveStreamEvent);
        }
        var conf_error = $("#orderconfirmation").attr("data-error");
        if(conf_error){
            console.log("conf_error", conf_error);
            this.props.confirmOrderData('conf_error',conf_error);
        }
        var conf_errorId = $("#orderconfirmation").attr("data-errorId");
        if(conf_errorId){
            console.log("conf_errorId", conf_errorId);
            this.props.confirmOrderData('conf_errorId',conf_errorId);
        }
        var confirmation_analytics = $("#orderconfirmation").attr("data-analytics");
        if(confirmation_analytics){
            console.log("confirmation_analytics", confirmation_analytics);
            this.props.confirmOrderData('confirmation_analytics',JSON.parse(confirmation_analytics));
        }
        var confirmation_lineitemMsg = $("#orderconfirmation").attr("data-lineItemMessageList");
        if(confirmation_lineitemMsg){
            console.log("confirmation_lineitemMsg", confirmation_lineitemMsg);
            this.props.confirmOrderData('confirmation_lineitemMsg',JSON.parse(confirmation_lineitemMsg));
        }
        var confirmation_transOrder = $("#orderconfirmation").attr("data-transactionOrder");
        if(confirmation_transOrder){
            console.log("confirmation_transOrder", confirmation_transOrder);
            this.props.confirmOrderData('confirmation_transOrder',JSON.parse(confirmation_transOrder));
        }
        var conf_hasPkgDine = $("#orderconfirmation").attr("data-hasPkgDine");
        if(conf_hasPkgDine){
            console.log("conf_hasPkgDine", conf_hasPkgDine);
            this.props.confirmOrderData('conf_hasPkgDine',conf_hasPkgDine);
        }
        var conf_hasPkgStay = $("#orderconfirmation").attr("data-hasPkgStay");
        if(conf_hasPkgStay){
            console.log("conf_hasPkgStay", conf_hasPkgStay);
            this.props.confirmOrderData('conf_hasPkgStay',conf_hasPkgStay);
        }
        var conf_hasHotelPackage = $("#orderconfirmation").attr("data-hasHotelPackage");
        if(conf_hasHotelPackage){
            console.log("conf_hasHotelPackage", conf_hasHotelPackage);
            this.props.confirmOrderData('conf_hasHotelPackage',conf_hasHotelPackage);
        }
        var conf_isEvoucherFlow = $("#orderconfirmation").attr("data-isEvoucherFlow");
        if(conf_isEvoucherFlow){
            console.log("conf_isEvoucherFlow", conf_isEvoucherFlow);
            this.props.confirmOrderData('conf_isEvoucherFlow',conf_isEvoucherFlow);
        }
        var conf_wufooFormField= $("#orderconfirmation").attr("data-wufooFormField");
        if(conf_wufooFormField){
            console.log("conf_wufooFormField", conf_wufooFormField);
            this.props.confirmOrderData('conf_wufooFormField',conf_wufooFormField);
        }
        var conf_isEventWithSurvey= $("#orderconfirmation").attr("data-isEventWithSurvey");
        if(conf_isEventWithSurvey){
            console.log("conf_isEventWithSurvey", conf_isEventWithSurvey);
            this.props.confirmOrderData('conf_isEventWithSurvey',conf_isEventWithSurvey);
        }
        var conf_cartTotalQty= $("#orderconfirmation").attr("data-cartTotalQty");
        if(conf_cartTotalQty){
            console.log("conf_cartTotalQty", conf_cartTotalQty);
            this.props.confirmOrderData('conf_cartTotalQty',conf_cartTotalQty);
        }
  
    }
    render(){
        var confirmation_transOrder = this.props.confirmation_transOrder;

        return (
            <div className="confirm_page">
                <Header/>

                <main>

                    <section class="confirmation_sec mb-5 mt-4">
                        <div class="container">
                            <div class="confirm_top_sec clearfix">
                                <div class="pull-left w80">
                                    <div class="confirm_title">
                                        <h2><img src={process.env.PUBLIC_URL + "/assets/images/confirm-check.svg"} alt="icon"/> Your Booking is Confirmed! </h2>
                                    </div>
                                    <div class="confirm_msg">
                                        <p>Dear {confirmation_transOrder && confirmation_transOrder.firstName},</p>
                                        <p>Your email confirmation has been sent to your email at <b>{confirmation_transOrder && confirmation_transOrder.emailAddress}</b>. Please use this email to log in on your next visit.</p>
                                    </div>
                                </div>
                                <div class="pull-right w20">
                                    <div class="transac_detail">
                                        <p>Transaction ID</p>
                                        <b>{confirmation_transOrder && confirmation_transOrder.transactionRefNumber}</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <Fragment>
                        <OrderedCartDetails />
                        <BookingReference />
                    </Fragment>

                </main>

                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        transactionId:state.transactionId,
        transactionTime: state.transactionTime,
        confirmation_transOrder: state.confirmation_transOrder,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        breadCrumbData: (field,value) => {
            dispatch(actionCreators.breadCrumbs(field,value));
        },
        confirmOrderData: (field,value) => {
            dispatch(actionCreators.confirmOrder(field,value));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingConfirm);
