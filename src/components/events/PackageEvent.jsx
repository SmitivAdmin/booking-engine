import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import Footer from '../common/Footer'
import Header from '../common/Header'
import PackageCartDetails from './Package/PackageCartDetails'
import PackageShowList from './Package/PackageShowList';
import * as actionCreators from "../../actions/actionCreator"
import moment from 'moment';
import _ from "lodash";
import filter from "lodash/filter"
import {Link} from "react-router-dom"
import jQuery from 'jquery'
import {Redirect} from 'react-router'
import {SITE_URL, TENANT, TICKETING_URL} from '../../constants/common-info'
import {monthNames} from '../../constants/common-info'

import he from 'he';
import HTMLParser from 'react-html-parser'
import * as API from "../../service/events/eventsInfo"
import {Modal, Button} from 'react-bootstrap';

class PackageEvent extends Component {

    constructor(props) {
        super(props);

        this.state ={
            noticeModel:false,
            errorMsg: '',
        }
    }

    toggleNoticeModel = (boolean) =>{
        // If true model will appear else not.
        this.setState({noticeModel: boolean});
    }

    componentWillMount() {
        this.props.breadCrumbData('timerAutoStart',true);
    }

    getPackageShow(pkgId, pkgAlias, info, description, e){
        e.preventDefault();
        jQuery(".packList ul li").removeClass('actv');
        e.currentTarget.classList.add('actv');
        jQuery(".show_title").html(pkgAlias);

        API.getPackageShow(pkgId, pkgAlias, info, description).then(response => {
            console.log('pack_show_list', response);
            if(response && response.status === 200){
                this.props.packageShowList(response.data);
                
                var top_position = jQuery('.packShowCnt').offset();
                jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
            } else{
                let ErrorMsg = response && response.data.statusMessage;
                this.setState({errorMsg: ErrorMsg},()=>{
                    this.toggleNoticeModel(true);
                })
            }
        });
    }

    render() {
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        let title = this.props.eventInfo ? this.props.eventInfo.title : '';
        let summaryImagePath = this.props.eventInfo ? this.props.eventInfo.summaryImagePath : undefined;
        let venue = this.props.eventInfo ? this.props.eventInfo.venue : undefined;
        let packageList = this.props.productsObj ? this.props.productsObj : undefined;

        console.log('packageList', packageList);

        if(title != ""){
            //console.log('parser123',he.decode(title));
            var decode_title = he.decode(title);
        }
        let form;

        form = (
            <Fragment>
                <Header/>

                <main>

                    <PackageCartDetails />

                    <section className="container-fluid packListCnt">
                        <div className="row">
                            <div className="container mb-5">
                                <h4 className="title-bdr dateTimeTitle">
                                    Select An Package
                                </h4>

                                <div className="col-12 list_view_cnt packList">
                                    {
                                        packageList && packageList.length > 0 && (
                                        <div className="row list_view_row">
                                            <div class="date-item">
                                                <span class="date">SELECT ONE</span>
                                            </div>
                                            <div className="event_time_list">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/list_view_time_arw.svg"} alt="" title="" className="time_left_arw" />
                                                
                                                <ul className="col-12">
                                                    {
                                                       packageList.map((pack_data, index) =>{
                                                            var package_info = (pack_data.info).replace(/\r\n/g,'<li>');
                                                            var list_info = '<li>'+package_info;
                                                            return(
                                                            <li className="" onClick={this.getPackageShow.bind(this,pack_data.pkgId, pack_data.pkgAlias, pack_data.info, pack_data.description)}>
                                                                <span className="downArw" data-toggle="collapse" data-target={`#${pack_data.pkgId}`} aria-expanded="false" aria-controls={pack_data.pkgId}></span>
                                                                
                                                                <input type="radio" name="event_id"/> 
                                                                <span class="event_opt"></span>
                                                                <span className ="event_text">{pack_data.pkgAlias}</span>
                                                                <span className ="event_info collapse" id={pack_data.pkgId}>
                                                                    <ul>{HTMLParser(list_info)}</ul>
                                                                </span>
                                                            </li>
                                                            )
                                                        })
                                                    }
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        )                                    
                                    }

                                </div>
                            </div>
                        </div>
                    </section>


                    <PackageShowList />  

                </main>

                <Modal show={this.state.noticeModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleNoticeModel(false)} centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">Error</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            this.state.errorMsg && (
                                <div dangerouslySetInnerHTML={{ __html: this.state.errorMsg}}></div>
                            )
                        }
                    </Modal.Body>
                    <Modal.Footer>
                    {
                        this.state.errorMsg && (
                           <Button onClick={()=>this.toggleNoticeModel(false)}>Ok</Button>
                        )
                    }
                    </Modal.Footer>
                </Modal>

                {/* <Footer/> */}
            </Fragment>
        );
        return (
            <Fragment>
                {form}
            </Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        productsObj: state.showInfo && state.showInfo.productsObj && state.showInfo.productsObj,
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        prdTicketType: state.prdTicketType
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchShowInfoData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        updateTicketType: (list) => {
            dispatch(actionCreators.updateTicketType(list));
        },
        breadCrumbData: (field, value) => {
            dispatch(actionCreators.breadCrumbs(field, value));
        },
        packageShowList: (dataObject) => {
            dispatch(actionCreators.packageShowList(dataObject));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PackageEvent);

