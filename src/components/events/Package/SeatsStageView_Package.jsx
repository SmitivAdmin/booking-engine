import React, {Component, Fragment, useEffect} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import {SITE_URL, TENANT} from '../../../constants/common-info'
import * as API from "../../../service/events/eventsInfo"
import $ from 'jquery';
import CookieUtil from '../../../constants/CookieUtil';
import {Modal, Button} from 'react-bootstrap';

const MOVE_VALUE = 30;

class PackageSeatsStageView extends Component {
    constructor(props){
        super(props);
        this.state = {
            img_width: 0,
            img_height: 0,
            hoveredArea: false,
            reserveFrndsDialog: false,
            errorModel:false,
            errorMsg: ''
        };
    }

    toggleErrorModel = (boolean) =>{
        // If true model will appear else not.
        this.setState({errorModel: boolean});
    }

    // componentDidMount(){
    //     require('jquery-mousewheel');
    //     require('malihu-custom-scrollbar-plugin');
    
    //     $(".mscroll-x").mCustomScrollbar({
    //         axis:"x",
    //         scrollEasing:"linear",
    //         scrollInertia: 0,
    //         autoHideScrollbar: false,
    //         autoExpandScrollbar: false,
    //         alwaysShowScrollbar: 2,
    //         theme: "dark-3"
    //     });
    // }

    getWidth(coord) {
        if (coord.length == 4) {
            return Math.abs(parseInt(coord[2] - coord[0]));
        }
        else {
            let low = parseInt(coord[0]), high = parseInt(coord[0]), currentCoord = 0;
            for (let i = 0; i < coord.length; i += 2) {
                currentCoord = parseInt(coord[i]);

                if (currentCoord < low)
                    low = parseInt(currentCoord);

                if (currentCoord > high)
                    high = currentCoord;
            }

            return Math.abs(high - low);
        }
    }

    getHeight(coord) {
        if (coord.length == 4) {
            return Math.abs(parseInt(coord[3] - coord[1]));
        }
        else {
            let low = parseInt(coord[1]), high = parseInt(coord[1]), currentCoord = 0;
            for (let i = 1; i < coord.length; i += 2) {
                currentCoord = parseInt(coord[i]);

                if (currentCoord < low)
                    low = parseInt(currentCoord);

                if (currentCoord > high)
                    high = currentCoord;
            }

            return Math.abs(high - low);
        }
    }

    getX(coord) {
        let low = parseInt(coord[0]), high = parseInt(coord[0]), currentCoord = 0;
        for (let i = 0; i < coord.length; i += 2) {
            currentCoord = parseInt(coord[i]);

            if (currentCoord < low)
                low = parseInt(currentCoord);
        }

        return low;
    }

    getY(coord) {
        let low = parseInt(coord[1]), high = parseInt(coord[1]), currentCoord = 0;
        for (let i = 1; i < coord.length; i += 2) {
            currentCoord = parseInt(coord[i]);

            if (currentCoord < low)
                low = parseInt(currentCoord);
        }

        low = low;

        return low;
    }

    _onSeatsLoad(evt) {
        evt.preventDefault();
        // if(document.getElementsByClassName("set_px")[0] !== undefined){
        //     var seatSvgW = document.getElementsByClassName("set_px")[0].offsetWidth;
        //     var seatSvgH = document.getElementsByClassName("set_px")[0].offsetHeight;

        //     console.log('seatSvgW',seatSvgW);
        //     document.getElementsByClassName("seat-allocation-image")[0].style.width = seatSvgW;
        //     document.getElementsByClassName("seat-allocation-image")[0].style.height = seatSvgH;
        // }

        //alert($(".set-px").innerWidth());
        
        setTimeout(function(){
            var that = this;

            var getParentWidth = $(".seats_outter").innerWidth();
            var seatSvgW = $(".seat_img_hidden").innerWidth();
            var seatSvgH = $(".seat_img_hidden").innerHeight();
            var decScale = 1;

            var getScreenWidth = $(document).width();

            if(seatSvgW > 0 && seatSvgH > 0){
                $(".seat-allocation-image").width(seatSvgW);
                $(".seat-allocation-image").height(seatSvgH);
                $(".seat_img_hidden").css('display','none');
            }
            if(getParentWidth < seatSvgW){
                decScale = getParentWidth / seatSvgW;
                $(".seat-allocation-image").css({transform:'scale('+decScale+')'});
                $("#scale_value").val(decScale);
            } else{
                decScale = 1;
                $(".seat-allocation-image").css({transform:'scale('+decScale+')'});
                $("#scale_value").val(decScale);
            }

            $(".seatMapCnt").height(decScale * seatSvgH);
            $(".seatMapCnt").width(decScale * seatSvgW);

            $(".seatTbl").width(getParentWidth);
            
            if(document.getElementsByClassName("stage_loader")[0] != undefined){
                document.getElementsByClassName("stage_loader")[0].classList.add('hide');
            }
            
        },1000)
        
    }

    zoomIn(e){
        e.preventDefault();
        var getScaleVal = $("#scale_value").val();
        var THIS = this;
        var getScreenWidth = $(document).width();
        var seatSvgH = $(".seat-allocation-image").innerHeight();
        var seatSvgW = $(".seat-allocation-image").innerWidth();
        var maxScale = 1.32;

        if(getScreenWidth > 767){
            maxScale = 1.32;
        } else{
            maxScale = 0.74;
        }

        if(parseFloat(getScaleVal) <= maxScale){
            var incScale = parseFloat(getScaleVal) + 0.08;
            $(".seat-allocation-image").css({transform:'scale('+incScale+')'});
            $("#scale_value").val(incScale);

            $(".seatMapCnt").height(incScale * seatSvgH);
            $(".seatMapCnt").width(incScale * seatSvgW);
        }        
    }
    zoomOut(e){
        e.preventDefault();
        var THIS = this;

        var getScaleVal = $("#scale_value").val();
        var seatSvgH = $(".seat-allocation-image").innerHeight();
        var seatSvgW = $(".seat-allocation-image").innerWidth();

        var getScreenWidth = $(document).width();

        var minScale = 0.68;

        if(getScreenWidth > 767){
            minScale = 0.68;
        } else{
            minScale = 0.35;
        }

        if(parseFloat(getScaleVal) >= minScale){
            var decScale = parseFloat(getScaleVal) - 0.08;
            $(".seat-allocation-image").css({transform:'scale('+decScale+')'});
            $("#scale_value").val(decScale);

            $(".seatMapCnt").height(decScale * seatSvgH);
            $(".seatMapCnt").width(decScale * seatSvgW);
        }
        
    }

    changePosition(direction) {
        let el = document.getElementById('seatMapId');
        if (el) {
            switch (direction) {
                case "MOVE_LEFT":
                    el.scrollLeft -= MOVE_VALUE;
                    break;

                case "MOVE_RIGHT":
                    el.scrollLeft += MOVE_VALUE;
                    break;

                case "MOVE_DOWN":
                    el.scrollTop += MOVE_VALUE;
                    break;

                case 'MOVE_UP':
                    el.scrollTop -= MOVE_VALUE;
                    break;

                case 'RESET':
                    el.scrollTop = 0;
                    el.scrollLeft = 0;
                    break;
            }
        }
    }

    selectSeats(isGroupReserved, index, inventory_id, seat_section, seat_level, seat_row, seat_no, seat_price, seat_price_currency, product_id, seat_section_id, price_cat_id, price_cat_alias, mode, isGroupBookingReserved, seatType, topLeftCoordinates, coordinates, seatAngle, e){
        e.preventDefault();

        if (e.target.classList.contains('selected') && this.props.seatForFrnds && !isGroupBookingReserved) {

        }
        else {
            //console.log("reserve_frnd1");

            if (isGroupReserved === "true" && !isGroupBookingReserved) {
                this.props.seatSelectionInfo.seatsAvailableList[index].isGroupBookingReserved = true;
                this.props.fetchSeatSelection({ seatSelection: this.props.seatSelectionInfo });
            }

            e.target.classList.toggle('selected');

            if (e.target.classList.contains('selected')) {
                if(isGroupReserved==="true"){
                    //console.log("reserve_frnd2");

                    this.props.addReservedCartInfo({ inventory_id, seat_section, seat_level, seat_row, seat_no, seat_price, seat_price_currency, product_id, seat_section_id, price_cat_id, price_cat_alias, mode, isGroupReserved, seatType, topLeftCoordinates, coordinates, seatAngle });

                    //console.log("reserve_frnd3");
                }
                else{
                    this.props.cartData({ inventory_id, seat_section, seat_level, seat_row, seat_no, seat_price, seat_price_currency, product_id, seat_section_id, price_cat_id, price_cat_alias, mode, isGroupReserved, seatType, topLeftCoordinates, coordinates, seatAngle });
                    
                    this.props.addQuantity(this.props.quantity, 1);
                }

            } else {
                if(isGroupReserved==="true"){
                    var cartInfo = this.props.reservedCartInfo;
                    cartInfo.map((cart_data, index) => {
                        if (cart_data.inventory_id === inventory_id) {
                            this.props.removeResCartData(index);
                        }
                    })
                }
                else{
                    var cartInfo = this.props.cartInfo;
                    cartInfo.map((cart_data, index) => {
                        if (cart_data.inventory_id === inventory_id) {
                            this.props.removeCartData(index);
                        }
                    });
                    this.props.decrementQuantity(this.props.quantity, 1);
                }
                

                //this.props.removeCartData(getIndex);
            }

            // //TODO: Deepa check on this
            // var slct_seat_type = '';
            // var price = 0;
            // var seatNumber = [];
            // seatNumber.push(seat_alias);
            // if(isGroupReserved==="false"){
            //     console.log('quantity_add');
            //     this.props.addQuantity(this.props.quantity, 1);
            // }
            // if (inventory_id != "" && seatSection != "" && seatLevel != "") {
            //     this.props.cartData({
            //         inventry_id: inventory_id,
            //         type: slct_seat_type,
            //         section: seatSection,
            //         level: seatLevel,
            //         row: seat_row,
            //         no: seatNumber,
            //         price: price,
            //         currency: currency,
            //         pid: productId,
            //         mode: mode,
            //         totalPrice:0
            //     });
            // }
            // API.ticketType(productId, priceCatId).then(response => {
            //     console.log("Ticket Type Fetched Successfully", response);
            //     this.props.updateTicketType(response);
            // });
        }
    }

    askToConfirmSeats= ()=>{
        if(!this.props.seatForFrnds && this.props.seatMapInfo.seatMapInfo.groupBookingMode===1){
            this.frndsDialogToggle(true);
        }
        else{
            let seat_select_list="";

            if(this.props.seatSelectionInfo){           
                seat_select_list = this.props.seatSelectionInfo;
                console.log('seat_select_list_data',seat_select_list);
            }

            this.confirmSeats(seat_select_list.product_id, seat_select_list.price_cat_id);
        }
    }

    confirmSeatsForFrnds(){
        this.frndsDialogToggle(false);

        this.props.updateCartForFrnd(true);
    }

    confirmSeats = (productId, priceCatId) => {
        this.frndsDialogToggle(false);

        var cartInfo = this.props.cartInfo;
        var prdTicketType = this.props.prdTicketType;

        //console.log("cartInfo_data",cartInfo)

        let reservedSeatList = [], releasedSeatList = null, groupReservedSeatList = [];
        cartInfo.map((seat, index) => {
            reservedSeatList.push({coordinates:seat.coordinates, inventoryId:seat.inventory_id, isGroupBookingReserved:seat.isGroupReserved, seatAlias:seat.seat_no, seatAngle:seat.seatAngle, seatRowAlias:seat.seat_row, seatType:seat.seatType, topLeftCoordinates:seat.topLeftCoordinates});
        });

        let resCartInfo = this.props.reservedCartInfo;
        resCartInfo.map((seat, index) => {
            groupReservedSeatList.push(seat.inventory_id);
        });
        
        console.log('productId_priceCatId',productId+'-'+priceCatId);

        var confirmation_post_data = {reservedSeatList, releasedSeatList};
        var pkgReqId = this.props.pkgReqId ? this.props.pkgReqId : '';
        var qty_val = cartInfo && cartInfo.length;
        var prodCntID = this.props.prdtIdList && this.props.prdtIdList;

        var productId, priceCategoryId, seatSectionType;
        var seatSelectionInfo = this.props.seatSelectionInfo && this.props.seatSelectionInfo;
        if(seatSelectionInfo){
            productId =  seatSelectionInfo.product_id;
            priceCategoryId =  seatSelectionInfo.price_cat_id;
            seatSectionType  = seatSelectionInfo.seat_section_type;
        }

        var cart_info = '';

        // Static Sample Records ==> Need to delete after
        
        // cart_info = {"packageId":2644,"packageReqSelectionList":[{"packageReqId":2381,"packageItemSelectionList":[{"uniqueId":1,"productId":1141348,"productAlias":"Patrick The Musical","mode":"SP","priceCatId":85041,"seatSectionId":173408,"seatSelection":{"seats":[{"inventoryId":239938608,"seatRowAlias":"M","seatAlias":"1","seatSectionAlias":"STALLS RIGHT","seatType":1},{"inventoryId":239938611,"seatRowAlias":"M","seatAlias":"2","seatSectionAlias":"STALLS RIGHT","seatType":1}],"reservedSeats":[]},"priceClassMap":"Q9:2","showDate":"2020-12-25T19:00:00+08:00","priceCatAlias":"A Reserve","priceCatNum":2,"priceAmount":{"amount":200.00,"currency":"SGD"},"priceClassAlias":"Package of 2 shows","priceClassCode":"Q9","prdtIdList":[1141348,1141351,1141352,1141353],"venueAlias":"Sands Theatre at Marina Bay Sands"}]},{"packageReqId":2380,"packageItemSelectionList":[{"uniqueId":2,"productId":960207,"productAlias":"MAMMA MIA! (TEST DO NOT SELL)","mode":"SP","priceCatId":56736,"seatSectionId":128264,"seatSelection":{"seats":[{"inventoryId":178478396,"seatRowAlias":"N","seatAlias":"36","seatSectionAlias":"STALLS LEFT","seatType":1},{"inventoryId":178478421,"seatRowAlias":"O","seatAlias":"36","seatSectionAlias":"STALLS LEFT","seatType":1}],"reservedSeats":[]},"priceClassMap":"Q9:2","showDate":"2021-11-17T20:00:00+08:00","priceCatAlias":"A RESERVE","priceCatNum":2,"priceAmount":{"amount":200.00,"currency":"SGD"},"priceClassAlias":"Package of 2 shows","priceClassCode":"Q9","prdtIdList":[960203,960207,1135212,1135213],"venueAlias":"Sands Theatre at Marina Bay Sands"}]}],"packagePromoPwdList":[]}
        

        API.packageConfirmation(confirmation_post_data).then(async response => {
            console.log('confirmation_response',response);
              
              await API.ticketType(productId, priceCategoryId, seatSectionType, pkgReqId).then(async response => {
                console.log('ticketType_response',response);
                
                var ticket_type_response = response && response.data && response.data[0];

                // this.props.packageQtyData(cart_info);
                // $("#addButton-"+prodCntID).addClass('hide');
                // $("#prdDateSect-"+prodCntID).addClass('hide');
                // $("#cartItemDetails-"+prodCntID).removeClass('hide');
                // var top_position = $('#show_list_item-'+prodCntID).offset();
                // $('html,body').animate({scrollTop: (top_position.top - 150)}, 700);

                if(ticket_type_response){
                                        
                  var selection_post_data = {'priceClassMap': ticket_type_response.priceClassCode+':'+qty_val,'priceClassAlias':ticket_type_response.priceClassAlias, 'priceClassAmount': (ticket_type_response.priceValueAmount.amount).toString()};

                  await API.packageSelection(selection_post_data).then(async response => {
                    console.log('selection_response',response);
                    //this.props.packageQtyData('');
                    
                    if(response && response.status === 200){
                        await API.getPkgSlctn().then(async response => {
                            console.log('get_package_response',response);
                            if(response.status === 200){
                                this.props.packageQtyData(response.data);
                            } else{
                                this.props.packageQtyData(cart_info);
                            }
        
                            $("#addButton-"+prodCntID).addClass('hide');
                            $("#prdDateSect-"+prodCntID).addClass('hide');
                            $("#cartItemDetails-"+prodCntID).removeClass('hide');
        
                            var top_position = $('#show_list_item-'+prodCntID).offset();
                            $('html,body').animate({scrollTop: (top_position.top - 150)}, 700);
                            //this.props.updateQtyData('', this.props.currentQuantity, qty_val);
                        });
                    } else{
                        let error_msg = response && response.data && response.data.statusMessage;
                        if(error_msg){
                            this.setState({errorMsg: error_msg},()=>{
                                this.toggleErrorModel(true);
                            })
                        }
                    }
                    

                  });
                }

              });
          });


    }

    frndsDialogToggle(isOpen){
        this.setState({
            reserveFrndsDialog : isOpen
        });
    }

    render(){
        let seat_select_list = '', styles = {}, cart_count = 0;
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        if(this.props.seatSelectionInfo){           
            seat_select_list = this.props.seatSelectionInfo;
            //console.log('seat_select_list_data',seat_select_list);
        }

        if(this.props.cartInfo){
            console.log('cartInfo_data',this.props.cartInfo);
            cart_count = this.props.cartInfo.length;
        }

        let selected_filter_area = this.props.selectedFilterArea;

        console.log('reservedCartInfo', this.props.reservedCartInfo);

        console.log('seat_select_list_data',seat_select_list);

        return (
            <Fragment>
                <div class="col-8 right-seat-map">
                    <div id="seatDesiableToggle" className="seatDesiable"></div>
                    {/* <h5>Stage</h5> */}
                    <h6>Step 2<br /><span>Please select your preferred seat(s)</span></h6>

                    <span className={`tooltip ds_hide`}>
                        {/* { this.state.hoveredArea && this.state.hoveredArea.name} */}
                        {
                            selected_filter_area.area && selected_filter_area.area.priceCatAmount != undefined &&
                            (
                            <Fragment>
                                <span className="tooltip_sec_head">Section: <span>{selected_filter_area.area.seatSectionAlias}</span></span>
                                <span className="tooltip_sec_cnt">                                    
                                    <span className="tooltip_text"><span className="seatClr" style={{background:selected_filter_area.area.fillColor}}></span> <b>{selected_filter_area.area.priceCatAlias} Circle {selected_filter_area.area.priceCategoryNum}</b><br/>{selected_filter_area.area.priceCatCurrency} {selected_filter_area.area.priceCatAmount.toFixed(2)}</span>
                                    <span className="avail_seats">{selected_filter_area.seats_available} Seats Available</span>
                                </span>
                            </Fragment>
                            )
                        }
                    </span>

                    <div className="stage_loader hide">
                        <div className="loaderImg"></div>
                    </div>

                    { seat_select_list && seat_select_list != '' ?
                        (
                            <Fragment>
                                <div className="seats_outter" id="seatMapId">
                                    
                                    <img className="seat_img_hidden" src={SITE_URL+seat_select_list.imageURL} />
                                    
                                    <div className="seatTbl" id="seatTblId">

                                        <div className="seatMapCnt">
                                    
                                            <svg className="seat-allocation-image" style={styles}>
                                            <image className="set-px"
                                                xlinkHref={SITE_URL+seat_select_list.imageURL}
                                                onLoad={this._onSeatsLoad.bind(this)}
                                            />
                                            {
                                                seat_select_list.seatsAvailableList && seat_select_list.seatsAvailableList.length > 0 ?
                                                    (
                                                        <Fragment>
                                                            {
                                                                seat_select_list.seatsAvailableList.map((seat, index) => {

                                                                    let seatCoordsArray = JSON.parse("[" + seat.coordinates + "]");
                                                                    let seatLeftcordsArray=JSON.parse("["+seat.topLeftCoordinates+"]");
                                                                    let x = seatLeftcordsArray[0];
                                                                    let y = seatLeftcordsArray[1];

                                                                    if (seat.seatAngle != "0") {
                                                                        x = this.getX(seatCoordsArray);
                                                                        y = this.getY(seatCoordsArray);
                                                                    }

                                                                    let result="";

                                                                    // return(<image className="seat-img"
                                                                    //         onClick={(e) => this.selectSeats(seat.seatRowAlias, seat.seatAlias, seat.seatType, seat.inventoryId, seat_select_list.seat_level_alias, seat_select_list.seat_section_alias, seat_select_list.price_currency, seat_select_list.product_id, seat_select_list.price_cat_id, seat_select_list.mode, e)}
                                                                    //         key={index}
                                                                    //         id={"seatno_" + seat.inventoryId}
                                                                    //         xlinkHref={"https://ticketing.sistic.com.sg/tenant/sistic/images/seats/selected/SeatIcon_Selected_"+seat.seatAngle+".gif"}
                                                                    //         x={ x }
                                                                    //         y={ y }
                                                                    //         style={{
                                                                    //             width: this.getWidth(seatCoordsArray),
                                                                    //             height: this.getHeight(seatCoordsArray)
                                                                    //         }}
                                                                    // />);

                                                                    return(
                                                            
                                                                    <image className="seat-img" data-index="-1" data-grp={seat.isGroupBookingReserved}
                                                                    onClick={
                                                                        this.selectSeats.bind(this, `${this.props.seatForFrnds ? true : false}`, index, seat.inventoryId, seat_select_list.seat_section_alias, seat_select_list.seat_level_alias, seat.seatRowAlias, seat.seatAlias, seat_select_list.price_amount, seat_select_list.price_currency, seat_select_list.product_id, seat_select_list.seat_section_id, seat_select_list.price_cat_id, seat_select_list.price_cat_alias, seat_select_list.mode, seat.isGroupBookingReserved, seat.seatType, seat.topLeftCoordinates, seat.coordinates, seat.seatAngle)
                                                                    }
                                                                        key={index}
                                                                        id={"seatno_" + seat.inventoryId}
                                                                        xlinkHref={`${seat.isGroupBookingReserved ? "https://ticketing.sistic.com.sg/tenant/sistic/images/seats/hold/SeatIcon_Hold_" : "https://ticketing.sistic.com.sg/tenant/sistic/images/seats/selected/SeatIcon_Selected_"}${seat.seatAngle}.gif`}
                                                                        x={ x }
                                                                        y={ y }
                                                                        style={{
                                                                            width: this.getWidth(seatCoordsArray),
                                                                            height: this.getHeight(seatCoordsArray)
                                                                        }}
                                                                    />
                                                                    );

                                                                })
                                                            }
                                                        </Fragment>
                                                    ) : ''
                                            }

                                            {
                                                seat_select_list.seatsUnavailableList && seat_select_list.seatsUnavailableList.length > 0 ?
                                                    (
                                                        <Fragment>
                                                            {
                                                                seat_select_list.seatsUnavailableList.map((seat, index) => {

                                                                    let seatCoordsArray = JSON.parse("[" + seat.coordinates + "]");
                                                                    let seatLeftcordsArray=JSON.parse("["+seat.topLeftCoordinates+"]");
                                                                    let x = seatLeftcordsArray[0];
                                                                    let y = seatLeftcordsArray[1];

                                                                    if (seat.seatAngle != "0") {
                                                                        x = this.getX(seatCoordsArray);
                                                                        y = this.getY(seatCoordsArray);
                                                                    }

                                                                    let result="";

                                                                    return(<image className="seat-img unavail"
                                                                                key={index}
                                                                                id={"seatno_"+ seat.inventoryId}
                                                                                xlinkHref={"https://ticketing.sistic.com.sg/images/seats/unavail/SeatIcon_Unavail_"+seat.seatAngle+".gif"}
                                                                                x={ x }
                                                                                y={ y }
                                                                                style={{
                                                                                    width: this.getWidth(seatCoordsArray),
                                                                                    height: this.getHeight(seatCoordsArray)
                                                                                }}
                                                                    />);

                                                                })
                                                            }
                                                        </Fragment>
                                                    ) : ''
                                            }

                                            {
                                                seat_select_list.groupBookingSeatList && seat_select_list.groupBookingSeatList.length > 0 ?
                                                    (
                                                        <Fragment>
                                                            {
                                                                seat_select_list.groupBookingSeatList.map((seat, index) => {

                                                                    let seatCoordsArray = JSON.parse("[" + seat.coordinates + "]");
                                                                    let seatLeftcordsArray=JSON.parse("["+seat.topLeftCoordinates+"]");
                                                                    let x = seatLeftcordsArray[0];
                                                                    let y = seatLeftcordsArray[1];

                                                                    if (seat.seatAngle != "0") {
                                                                        x = this.getX(seatCoordsArray);
                                                                        y = this.getY(seatCoordsArray);
                                                                    }

                                                                    let result="";

                                                                    return(<image className="seat-img unavail"
                                                                                key={index}
                                                                                id={"seatno_"+ seat.inventoryId}
                                                                                xlinkHref={"https://ticketing.sistic.com.sg/tenant/sistic/images/seats/hold/SeatIcon_Hold_"+seat.seatAngle+".gif"}
                                                                                x={ x }
                                                                                y={ y }
                                                                                style={{
                                                                                    width: this.getWidth(seatCoordsArray),
                                                                                    height: this.getHeight(seatCoordsArray)
                                                                                }}
                                                                    />);

                                                                })
                                                            }
                                                        </Fragment>
                                                    ) : ''
                                            }
                                        </svg>
                                        {/* <img src={SITE_URL+seat_select_list.imageURL} alt="" class="img-fluid" /> */}
                                        
                                        </div>
                                    </div>
                                </div>

                                <div className="seat_identify"><img src={`${process.env.PUBLIC_URL}/assets/images/${this.props.seatForFrnds || seat_select_list.groupBookingSeatList && seat_select_list.groupBookingSeatList.length > 0 ? "seat_identify_friends.jpg":"seat_identify.jpg"}`} /></div>

                                <div class="zoom-btn">
                                    <div className="navig_scroll">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/scroll_navigation_arrow.svg"} alt="icon" className="col-12 top_nav_scl" onClick={this.changePosition.bind(this,'MOVE_UP')}/>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/scroll_navigation_arrow.svg"} alt="icon" className="left_nav_scl" onClick={this.changePosition.bind(this,'MOVE_LEFT')}/>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/scroll_navigation_arrow.svg"} alt="icon" className="right_nav_scl" onClick={this.changePosition.bind(this,'MOVE_RIGHT')}/>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/scroll_navigation_arrow.svg"} alt="icon" className="col-12 bottom_nav_scl" onClick={this.changePosition.bind(this,'MOVE_DOWN')}/>
                                    </div>

                                    <button class="btn btn-primary" onClick={this.zoomIn.bind(this)}>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/zoom-in.svg"} alt="icon"/>
                                    </button>
                                    <button class="btn btn-primary" onClick={this.zoomOut.bind(this)}>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/zoom-out.svg"} alt="icon"/>
                                    </button>
                                    <input type="hidden" value="1" id="scale_value" />
                                </div>
                                
                                <div className="px-0 text-right mt-1 mb-3 flat_btn checkOutBtn">                                    
                                    {/* <a href="javascript:;" onClick={()=>this.confirmSeats(seat_select_list.product_id, seat_select_list.price_cat_id)} className={`btn btn-primary mt-2 ${cart_count === 0 && ('in-active')}`}>Next</a> */}
                                    {
                                        seat_select_list.groupBookingSeatList && seat_select_list.groupBookingSeatList.length > 0 ? (
                                            <Fragment>
                                                <a href="javascript:;" onClick={()=>this.confirmSeats(seat_select_list.product_id, seat_select_list.price_cat_id)} className={`btn btn-primary mt-2 ${cart_count === 0 && ('in-active')}`}>Next</a>
                                                <a href={`/${TENANT}/booking/${iccCode}`} className={`btn btn-primary mt-2 ml-3`}>I Want To Select Other Seats</a>
                                            </Fragment>
                                        ) : (
                                            <a href="javascript:;" onClick={()=>this.askToConfirmSeats()} className={`btn btn-primary mt-2 ${cart_count === 0 && ('in-active')}`}>Next</a>
                                        )
                                    }
                                </div>
                            </Fragment>
                            
                            ) : (<div className="defaultStageImg"><img src={process.env.PUBLIC_URL + "/assets/images/seats_generic.svg"}  /></div>)
                        }

                </div>

                <Modal show={this.state.reserveFrndsDialog} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.frndsDialogToggle(false)} centered>
                    <Modal.Header>
                        <Modal.Title><p className="modal-title">Reserved For Friends</p></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div><span>Do you want to reserve seats for friends?</span></div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={()=>this.confirmSeatsForFrnds()} size="sm">Yes</Button>
                        <Button onClick={()=>this.confirmSeats(seat_select_list.product_id, seat_select_list.price_cat_id)} size="sm">No</Button>
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.errorModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleErrorModel(false)} centered>
                    <Modal.Header closeButton>
                        <Modal.Title id="contained-modal-title-vcenter">Error</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div dangerouslySetInnerHTML={{ __html: this.state.errorMsg }}></div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button onClick={()=>this.toggleErrorModel(false)}>Ok</Button>
                    </Modal.Footer>
                </Modal>

            </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    console.log('seat_select_state',state.seatSelectionInfo.seatSelection)
    return {
        seatSelectionInfo: state.seatSelectionInfo && state.seatSelectionInfo.seatSelection && state.seatSelectionInfo.seatSelection,
        quantity: state.quantity,
        currentQuantity: state.currentQuantity,
        cartInfo: state.cartInfo,
        reservedCartInfo : state.reservedcartInfo,
        selectedFilterArea: state.selectedFilterArea,
        prdTicketType: state.prdTicketType,
        propsObj: ownProps,
        seatMapInfo: state.seatMapInfo,
        seatForFrnds : state.seatForFrnds,
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        pkgReqId: state.pkgReqId,
        prdtIdList: state.prdtIdList,
        chosenProductId: state.chosenProductId,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        cartData: (cart_values, reserved_list) => {
            dispatch(actionCreators.addCartInfo(cart_values));
        },
        addReservedCartInfo: (cart_values) => {
            dispatch(actionCreators.addReservedCartInfo(cart_values));
        },
        removeCartData: (value) => {
            dispatch(actionCreators.removeCartInfo(value));
        },
        removeResCartData : (value) => {
            dispatch(actionCreators.removeReservedCartInfo(value));
        },
        addQuantity: (current, increment) => {
            dispatch(actionCreators.addQuantity(current,increment));
        },
        decrementQuantity: (current, decrement) => {
            dispatch(actionCreators.decrementQuantity(current,decrement));
        },        
        updateTicketType: (list) => {
            dispatch(actionCreators.updateTicketType(list));
        },
        fetchSeatSelection: (dataObject) => {
            dispatch(actionCreators.fetchSeatSelInfo(dataObject));       
        },
        updateCartForFrnd: (boolean) => {
            dispatch(actionCreators.updateCartForFrnd(boolean));
        },
        packageQtyData: (obj) => {
            dispatch(actionCreators.packageQtyInfo(obj));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PackageSeatsStageView);
