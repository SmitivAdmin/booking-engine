import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import {SITE_URL} from '../../../constants/common-info'
import {Link} from "react-router-dom"
import TimeCounter from './../TimeCounter'

import he from 'he';
import HTMLParser from 'react-html-parser'

import moment from 'moment';

import $ from 'jquery';
import _ from "lodash";

class PackageCartDetails extends Component {
    constructor(props){
        super(props);
    }

    delCart(evt, index, inventry_id, slct_seat_type, qtyIndex){  
        // Remove seat selection from UI based on inventry_id.
        inventry_id.map((item, index)=>{
            $(`#seatno_${item}`).removeClass("selected");
        }); 
        
        // Update the selected ticket type based on deletion Qty.
        var qtyField = this.props.ticketTypesSelected[qtyIndex][Object.keys(this.props.ticketTypesSelected[qtyIndex])[1]];
        var id ="quantity-"+qtyField;

        var temp = this.props.ticketTypesSelected;
        
        if(_.find(temp,function(o) { return o.key == qtyField} )) {
            _.find(temp,function(o) { return o.key == qtyField} )[qtyField] = _.find(temp,function(o) { return o.key == qtyField})[qtyField] - (inventry_id.length);
        }

        // Update the (quantity, currentQuantity, cart/ReservedCart) in REDUX.
        this.props.addQuantity(this.props.currentQuantity, inventry_id.length*-1);
        this.props.updateQtyData(temp, this.props.currentQuantity, inventry_id.length*-1);
        this.props.removeMultiCartInfo(index, false);

        // Disable the Quantity INC/DEC button based on Current quantity.
        setTimeout(function(){
            if(document.getElementById(id).value > 0){
                $("#qtyPrcCls"+qtyField).addClass('active');
                $("#cart-qty-minus"+qtyField).removeClass('in-active');
            } else{
                $("#qtyPrcCls"+qtyField).removeClass('active');
                $("#cart-qty-minus"+qtyField).addClass('in-active');
            }
        }, 500);
    }

    delCartGA(evt, index, qtyIndex, qty_type){
        var qtyField, temp;
        // Update the selected ticket type based on deletion Qty.
        if(qty_type === "Normal"){
            qtyField = this.props.ticketTypesSelected[qtyIndex][Object.keys(this.props.ticketTypesSelected[qtyIndex])[1]];
            temp = this.props.ticketTypesSelected;
        } else{
            qtyField = this.props.reservedCartInfo[qtyIndex][Object.keys(this.props.reservedCartInfo[qtyIndex])[1]];
            temp = this.props.reservedCartInfo;
        }
        console.log('qtyField',qtyField);
        var id ="quantity-"+qtyField;
        
        if(_.find(temp,function(o) { return o.key == qtyField} )) {
            _.find(temp,function(o) { return o.key == qtyField} )[qtyField] = _.find(temp,function(o) { return o.key == qtyField})[qtyField] - (index.length);
        }

        // Update the (quantity, currentQuantity, cart/ReservedCart) in REDUX.
        this.props.addQuantity(this.props.currentQuantity, index.length*-1);
        if(qty_type === "Normal"){
            this.props.updateQtyData(temp, this.props.currentQuantity, index.length*-1);
        } else{
            document.getElementById(id).value = document.getElementById(id).value - index.length;
            this.props.addReservedCartInfoGA(temp, this.props.currentQuantity, -1);
        }
        this.props.removeMultiCartInfo(index, false);

        // Disable the Quantity INC/DEC button based on Current quantity.
        setTimeout(function(){
            if(document.getElementById(id).value > 0){
                $("#qtyPrcCls"+qtyField).addClass('active');
                $("#cart-qty-minus"+qtyField).removeClass('in-active');
            } else{
                $("#qtyPrcCls"+qtyField).removeClass('active');
                $("#cart-qty-minus"+qtyField).addClass('in-active');
            }
        }, 500);
    }

    delFrndCart(evt, index, inventry_id, slct_seat_type){  
        debugger
        inventry_id.map((item, index)=>{
            $(`#seatno_${item}`).removeClass("selected");
        });

        this.props.removeMultiCartInfo(index, true);
    }


    viewDetDesk(e){
        e.preventDefault();
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".d-md-block.sticky .total-sec, .d-md-block.sticky .ticket-detail-sec, .d-md-block.sticky .view_ticket, .d-md-block.sticky .close_ticket, .d-md-block.sticky .stickyTotal").toggleClass('hide');
        $(".d-md-block.sticky .media-body").toggleClass('mt-0');
    }
    viewDetMob(e){
        e.preventDefault();
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".mob-tic-info").slideToggle();
    }

    scrollToDate() {
        // Note - this works well on the small screens
        //$('html,body').animate({scrollTop: $(".title-bdr").offset().top},'slow');
        var top_position = $('.title-bdr').offset();
        $('html,body').animate({scrollTop: (top_position.top - 50)}, 700);
    }



    render(){
        let title = this.props.eventInfo?this.props.eventInfo.title: '';
        let venue =  this.props.eventInfo? this.props.eventInfo.venue: undefined;
        let summaryImagePath = this.props.eventInfo?this.props.eventInfo.summaryImagePath: undefined;

        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        
        var cart_info = this.props.packageCartInfo && this.props.packageCartInfo;
        console.log('cart_info',cart_info);
        var packageReqSelectionList = cart_info && cart_info.packageReqSelectionList;

        var productsObj = this.props.productsObj ? this.props.productsObj : '';
        console.log('productsObj',productsObj);

        title = productsObj && productsObj.length > 0 ? productsObj[0].pkgAlias : '';

        console.log('packageReqSelectionList',packageReqSelectionList);

        if(title != ""){
            //console.log('parser123',he.decode(title));
            var decode_title = he.decode(title);
        }

        var total_price = 0, total_price2 = 0, seat_price_currency = '', total_tickets = 0;

        packageReqSelectionList && packageReqSelectionList.map((cart_package_data, index) =>{
            cart_package_data.packageItemSelectionList.map((cart_data, index) => {
                var getQty = (cart_data.priceClassMap).replace('Q:','');
                total_tickets += parseInt(getQty);
            })
        })

        return (
        <Fragment>
            <section className="show-info packCart">
                <div className="container">
                    <div className="row">
                        <div className="media col-md-11 col-12">
                            { summaryImagePath ?
                                (<div className="media-img mr-4">
                                    <img className="img-fluid" src={SITE_URL + summaryImagePath} alt=""/>
                                </div>): ''
                            }
                            <div className="media-body col-md-12">
                                <h5 className="mt-0 ft show_title">{HTMLParser(decode_title)}</h5>
                                <p className="ft-1">
                                    {
                                        venue && (
                                        <span className="mr-3">
                                            <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon"/>
                                            {venue}
                                        </span>
                                        )
                                    }
                                    
                                    {
                                        total_tickets> 0 && 
                                        (
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/ticket-icon.svg"} alt="icon"/>
                                                {total_tickets} Ticket{total_tickets > 1 ? 's': ''}
                                            </span>
                                        )
                                    }
                                </p>
                                {
                                    packageReqSelectionList && packageReqSelectionList.length > 0 && (

                                        <div className="ticket-detail-sec">
                                            {
                                                packageReqSelectionList.map((cart_package_data, index) =>{
                                                    return(
                                                        <Fragment>
                                                        {
                                                            cart_package_data.packageItemSelectionList.map((cart_data, index) => {
                                                                total_price += parseInt(cart_data.priceAmount.amount);
                                                                var show_time = moment(cart_data.showDate.substring(11, 16), ["HH:mm"]).format("hh:mm A");
                                                                var show_date = moment(cart_data.showDate).format('D MMM, Y');

                                                                return(
                                                                <div className="pack_cart_item">
                                                                    <span className="cart_item_txt">{cart_data.productAlias}</span>
                                                                    <span className="cart_item_date">{show_date}, {show_time}</span>
                                                                </div>
                                                                )
                                                            })
                                                        }
                                                        </Fragment>
                                                    )
                                                })
                                            }                                    
                                        </div>
                                    )
                                }
                                
                            </div>
                        </div>
                        <div className="col-md-1">
                            <TimeCounter />
                        </div>
                    </div>
                </div>
                <div className="mob-tic-info pr-3">
                    <hr />

                </div>
                { 
                    packageReqSelectionList && packageReqSelectionList.length > 0 ? (
                    <Fragment>
                        <div className="total-sec">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className={`col-6 booking-fee ${ summaryImagePath ? '' : 'pl-3'}`}>
                                        <h6>Total</h6>
                                        <p className="mb-0">excl.booking fee</p>
                                    </div>
                                    <div className="col-6 check-out-sec">
                                        <h6>{seat_price_currency} {total_price.toFixed(2)}</h6>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="view-detail d-block d-sm-block d-md-none">
                            <button className="btn btn-view" onClick={this.viewDetMob.bind(this)}>
                                <span>View Ticket Details</span>
                                <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon"/>
                            </button>
                        </div>
                    
                    </Fragment>
                        
                    ) : ''

                }
                
            </section>

            <section className="show-info seats float-wrap d-none d-sm-none d-md-block">
                <div className="container">
                    <div className="row">
                        <div className="media col-md-11 col-12">
                            <div className={`media-body col-md-12`}>
                                <h5 className="mt-0 ft">{HTMLParser(decode_title)}</h5>
                                <p className="ft-1">
                                    {
                                        venue && (
                                        <span className="mr-3">
                                            <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon"/>
                                            {venue}
                                        </span>
                                        )
                                    }
                                    
                                    {
                                        total_tickets > 0 && (
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/ticket-icon.svg"} alt="icon"/>
                                                {total_tickets} Ticket{total_tickets > 1 ? 's': ''}
                                            </span>
                                        )
                                    }
                                    
                                    {
                                        total_price > 0 && (
                                            <div className="stickyTotal"><h5 className="mt-0 ft">Total {seat_price_currency} {total_price.toFixed(2)}</h5></div>
                                        )
                                    }                        
                                </p>
                                
                                {
                                    packageReqSelectionList && packageReqSelectionList.length > 0 && (
                                        <div className="ticket-detail-sec sticky_cart hide">
                                            {
                                                packageReqSelectionList.map((cart_package_data, index) =>{
                                                    return(
                                                    <Fragment>
                                                        {
                                                            cart_package_data.packageItemSelectionList.map((cart_data, index) => {
                                                                total_price2 += parseInt(cart_data.priceAmount.amount);
                                                                var show_time = moment(cart_data.showDate.substring(11, 16), ["HH:mm"]).format("hh:mm A");
                                                                var show_date = moment(cart_data.showDate).format('D MMM, Y');
        
                                                                return(
                                                                <div className="pack_cart_item">
                                                                    <span className="cart_item_txt">{cart_data.productAlias}</span>
                                                                    <span className="cart_item_date">{show_date}, {show_time}</span>
                                                                </div>
                                                                )
                                                            })
                                                        }
                                                    </Fragment>
                                                    )                                                    
                                                })
                                            }                                    
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                        <div className="col-md-1 d-none d-sm-none d-md-block">
                            <TimeCounter />
                        </div>
                    </div>
                </div>

                { 
                    packageReqSelectionList  && packageReqSelectionList.length > 0 ? (
                    <Fragment>
                        <div className="total-sec hide">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className="col-4 booking-fee">
                                        <h6>Total</h6>
                                        <p className="mb-0">excl.booking fee</p>
                                    </div>
                                    <div className="col-6 check-out-sec">
                                        <h6>{seat_price_currency} {total_price2.toFixed(2)}</h6>                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="view-detail d-block d-sm-none d-md-block">
                            <button className="btn btn-view" onClick={this.viewDetDesk.bind(this)}>
                                <span className="view_ticket">View Ticket Details</span>
                                <span className="close_ticket hide">Close Ticket Details</span>
                                <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon"/>
                            </button>
                        </div>
                    </Fragment>
                    ) : ''

                }
            </section>
        </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    //console.log('cartInfo', state.cartInfo)
    return {
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        totalQuantity:  state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        dateChosen: state.dateChosen,
        packageCartInfo: state.packageCartInfo,
        quantity: state.quantity,
        mergedCartInfo:state.mergedCartInfo,
        checkout_btn_enable: state.checkout_btn_enable,
        update_seat_select: state.update_seat_select,
        selected_adult_qty: state.selected_adult_qty,
        selected_child_qty: state.selected_child_qty,
        selected_nsf_qty: state.selected_nsf_qty,
        selected_sectz_qty: state.selected_sectz_qty,
        timeChosen:state.timeChosen,
        patronName: state.patronName,
        propsObj: ownProps,
        currentQuantity: state.currentQuantity,
        ticketTypesSelected:state.ticketTypesSelected,
        reservedCartInfo : state.reservedcartInfo,
        ticketType: state.prdTicketType,
        productsObj: state.showInfo && state.showInfo.productsObj && state.showInfo.productsObj
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        removeMultiCartInfo: (value, isFrnd) => {
            dispatch(actionCreators.removeMultiCartInfo(value, isFrnd));
        },
        updateQtyData: (field,value,factor) => {
            dispatch(actionCreators.updateQty(field,value, factor));
        },
        addQuantity: (current, increment) => {
            dispatch(actionCreators.addQuantity(current,increment));
        },
        addReservedCartInfoGA: (obj, current, increment) => {
            dispatch(actionCreators.addReservedCartInfoGA(obj, current, increment));
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PackageCartDetails);