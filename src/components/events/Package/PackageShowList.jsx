import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import moment from 'moment';
import _ from "lodash";
import filter from "lodash/filter"
import {Link} from "react-router-dom"
import jQuery, { merge } from 'jquery'
import {Redirect} from 'react-router'
import SeatsOverview_Package from './SeatsOverview_Package'
import PackageSeatsStageView from './SeatsStageView_Package'
import {
  SITE_URL,
  AVAILABILE,
  LIMITED_SEATS,
  COMING_SOON,
  SHOW_CANCELLED,
  SHOW_POSTPONED,
  SHOW_RECONFIG,
  SINGLE_SEATS,
  SOLD_OUT,
  TENANT,
  TICKETING_URL,
  monthNames,
  PACKAGE_RULES,
  SHOPPING_CART
} from '../../../constants/common-info'
import he from 'he';
import HTMLParser from 'react-html-parser'
import * as API from "../../../service/events/eventsInfo"
import {Modal, Button} from 'react-bootstrap';

class PackageShowList extends Component {

    constructor(props) {
        super(props);

        this.state ={
          noticeModel:false,
          errorMsg: '',
          errorTitle: '',
          errorStaus: '',
          selectPrdId: 0,
          uniqueId: '',
          productId: '',
          checkOutValidation: false
        }
    }

    toggleNoticeModel = (boolean) =>{
        // If true model will appear else not.
        this.setState({noticeModel: boolean});
    }
    toggleValidationModel = (boolean) =>{
      // If true model will appear else not.
      this.setState({checkOutValidation: boolean});
  }

    getAvailability(statusCode) {
        if (statusCode == "20") {
            return COMING_SOON+', coming_soon_cls'
        } else if (statusCode == "19") {
            return AVAILABILE+', available_cls'
        } else if (statusCode == "18") {
            return LIMITED_SEATS+', limited_seats_cls'
        } else if (statusCode == "17") {
            return SINGLE_SEATS+', single_seat_cls'
        } else if (statusCode == "16") {
            return SOLD_OUT+', soldout_cls'
        } else if (statusCode == "-6") {
            return SHOW_RECONFIG+', reconfig_cls'
        } else if (statusCode == "-7") {
            return SHOW_CANCELLED+', cancel_cls'
        } else if (statusCode == "-8") {
            return SHOW_POSTPONED+', post_poned_cls'
        } else {
            return undefined
        }
    }

    addPackagePrd(pkgReqId, prdtIdListStr, e){
        e.preventDefault();
        this.props.fetchSeatMapOverview('');
        var split_prdId = prdtIdListStr.split(',');
        var current_element = e.currentTarget.classList;
        jQuery(".show_list_item .check-out-btn").removeClass('hide');
        jQuery(".prdDateSect .date-slide").addClass('hide');

        this.props.chosenProductDate(false, '', '', '', '', '');
        this.props.chosenPackage(pkgReqId, split_prdId);
        
        API.addPackPrd(pkgReqId, split_prdId).then(response => {
          console.log('prd_list', response);
          if(response && response.status === 200){
              //e.currentTarget.classList.add('hide');
              this.setState({ 
                selectPrdId: split_prdId[0]}, ()=>{
                  this.props.packShowTimeList(response.data);
                  //current_element.add('hide');
                  jQuery("#addButton-"+split_prdId[0]).addClass('hide');
                  jQuery("#dektop-date-slide"+split_prdId[0]).removeClass('hide');
              });

          } else{
              let ErrorMsg = response && response.data.statusMessage;
              this.setState({errorMsg: ErrorMsg},()=>{
                  this.toggleNoticeModel(true);
              })
          }
      });
    }

    showTime(key, obj, productId, dateChosen, ticket_type, isEventWithSurvey, showTitle, venue, e) {
      e.preventDefault();
      //var split_ticket_type = ticket_type.split(','), prd_ticket_type;
      var prd_ticket_type;
      //alert(isEventWithSurvey);
      var group_booking = window.location.search;
      var show_date_time = key;

      //console.log('show_date_time',show_date_time);
      
      if(isEventWithSurvey === true){
          prd_ticket_type = "RS";
      } else if(isEventWithSurvey === false && group_booking != ""){
          prd_ticket_type = "RS";
      }else{
          prd_ticket_type = "GA";
      }
      //prd_ticket_type = "GA";
      this.props.chosenProductDate(true, obj[key.substring(0, 10)], productId, dateChosen, '', isEventWithSurvey, showTitle, venue, show_date_time);
      //console.log('productId', productId);
      jQuery('.owl-stage-outer .date-item').removeClass("active");
      jQuery('.time-sec .date-item').removeClass('active');
      jQuery('.seat-map').addClass('hide');
      
      jQuery("#"+productId).addClass("active");

      setTimeout(function(){
        var top_position = jQuery('#showTimeID').offset();
        jQuery('html,body').animate({scrollTop: (top_position.top - 80)}, 700);
      },1000);
      
    }

    async chosenTime(timeSelected,e) {
        e.preventDefault();
        jQuery('.time-sec .date-item').removeClass('active');
        e.currentTarget.classList.add('active');
        this.props.chosenProductTime(timeSelected);

        if(this.props.chosenProductId != "" && this.props.chosenProductId != undefined){
            var product_id = this.props.chosenProductId;   
            let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';

            var group_booking = window.location.search;
            var searchParams = new URLSearchParams(group_booking);
            var bookingCode = searchParams && searchParams.get('groupBookingCode');
            if(bookingCode){
                bookingCode = bookingCode;
            } else{
                bookingCode = '';
            }

            console.log('prdTicketType',this.props.prdTicketType);


            var pkgReqId = this.props.pkgReqId ? this.props.pkgReqId : '';

            API.setPackageProductId(product_id, iccCode, pkgReqId, this.props.showDateTime, this.props.productAlias, this.props.venueAlias, this.props.prdtIdList).then(response =>{
              console.log('set_product_id', response);
              if(response && response === 200){                                
              }
              //alert(pkgReqId);
              API.getSeatMapOverView(product_id, pkgReqId).then(async response =>{

                if(response && response.seatMapInfo && response.seatMapInfo.mode === "SP"){

                  jQuery(".filter-level img").removeClass('filter_cls');
                  jQuery(".filter-level .tooltip").removeClass('active');
                  // let stage_view = '';
                  // this.props.fetchSeatSelection(stage_view);
                  jQuery('.seat-map').removeClass('hide');

                  this.props.updateEventType('RS');

                  this.props.fetchSeatMapOverview(response);
                  
                  if(this.props.seatMapInfo){
                    var top_position = jQuery('.seat-map').offset();
                    jQuery('html,body').animate({scrollTop: (top_position.top - 50)}, 700);                    
                  }
  
                  await API.getPriceTable(product_id).then(response =>{
                      this.props.fetchPriceTabele(response);
                  });
                  
              } else if(response && response.seatMapInfo && response.seatMapInfo.mode === "BA"){

                  if(response.seatMapInfo.seatSectionList && response.seatMapInfo.seatSectionList[0] && response.seatMapInfo.seatSectionList[0].SeatLevel[0] && response.seatMapInfo.seatSectionList[0].SeatLevel[0].priceCategoryId){

                      this.props.fetchSeatMapOverview(response);

                      this.props.updateEventType('GA');
                      
                      var priceCategoryId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].priceCategoryId;
                      var seatSectionId = response.seatMapInfo.seatSectionList[0].SeatLevel[0].seatSectionId;
                      var mode = "GA";
                      var isMembership = response.seatMapInfo.isMembership === 0 ? false : true;

                      await API.getPackageSeatsAvailable(product_id, pkgReqId).then(response => {
                          console.log('fetchSeatMapAvailable', response);

                          var top_position = jQuery('#packageQtySection').offset();
                          jQuery('html,body').animate({scrollTop: (top_position.top - 120)}, 700);
                      })

                      // await API.catSection(priceCategoryId, seatSectionId, mode, isMembership, this.props.isEventWithSurvey).then(response => {
                      //     console.log('cat_section', response);
                      // })

                      // await API.ticketType(product_id, priceCategoryId, this.props.prdTicketType).then(response => {
                      //     console.log("Ticket Type Fetched Successfully", response.data);
                      //     if(response.status === 200){
                      //         this.props.updateTicketType(response.data);                    
                      //     }
                      // });

                  }
                }
                  
              });
          });

        } 
    }


  calculateTime(key, selected, dateChosen,ticket_type, isEventWithSurvey, e) {
      e.preventDefault();
      let products = this.props.productsObj ? this.props.productsObj.showTimingList : undefined;
      let filteredArray = _.find(products, function (product) {
          return (product.showDateTime.includes(key) && product.availabilityStatus == "19")
      });
      // let showTime = filteredArray.length > 0 ? filteredArray[0].showDateTime : undefined;
      let productId = filteredArray? filteredArray.productId : undefined;
      let productArray = this.getDateTimeObj(filteredArray);
      let dateObj = productArray&& productArray.length > 0? productArray[0].dateTimeObj: undefined;
      this.props.chosenProductDate(true, dateObj, productId, dateChosen, ticket_type, isEventWithSurvey);
      jQuery('.date-item').removeClass("active");
      (jQuery(`#${selected}`)).addClass("active");
  }

  displayAll(e){
    e.preventDefault();
    jQuery('.date-item').toggleClass("hide");
    e.target.classList.toggle('less');
  }

  showCal(e) {
      e.preventDefault();
      // jQuery('.calViewSection').removeClass('hide');
      // jQuery('.listViewSection').addClass('hide');
      // jQuery(".event_time_list ul li").removeClass('actv');

      let products = this.props.packShowTimeListObj ? this.props.packShowTimeListObj.showTimingList : undefined;
      let result = filter(products, function (product) {
          return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-0" + "3") && product.availabilityStatus == "16")
      }).length > 0;
      
      // document.getElementsByClassName('cal_view')[0].classList.toggle('hide');
      // document.getElementsByClassName('slider_view')[0].classList.toggle('hide');
      //console.log('classList', e.target.parentNode.classList);
      
      if(e.target.parentNode.classList.contains('actv') || e.target.classList.contains('actv')){
          if(!this.props.showCalFlag && this.props.hideDefaultDateView){
              this.props.showCalendarView(true, false, false);
          }
          if(this.props.showCalFlag && !this.props.hideDefaultDateView){
              this.props.showCalendarView(false, true, false);
          }

          jQuery('.cal_view').toggleClass('hide');
          jQuery('.slider_view').toggleClass('hide');
      } else{
          
      }

      // jQuery('.dateTimeTitle .btn-primary').removeClass('actv');
      // jQuery('.calViewSection .calendar_view_btn').addClass('actv');
  }

    getDateTimeObj(products) {
        let productArray = [];
        let dataTimeObj = {};
        // TODO: Deepa clean this up
        if (products && products.length > 0) {
            for (let id in products) {
                let actualProduct = products[id];
                let dateString = actualProduct && actualProduct.showDateTime ? actualProduct.showDateTime.substring(0, 10): undefined;
                let timeArray = _.filter(products, function (product) {
                    return (product.showDateTime.includes(dateString) && product.availabilityStatus == "19")
                }).map((value) => {
                    return moment(value.showDateTime.substring(11, 16), ["HH:mm"]).format("hh:mm A");
                });
                dataTimeObj[dateString] = timeArray;
                let formattedDate = moment(actualProduct.showDateTime).format('llll').split(',');
                let year = dateString.split("-");
                var dateMonth = formattedDate && formattedDate[1] ? formattedDate[1].split(' ') : formattedDate;
                //TODO: Deepa move this out
                var availability = this.getAvailability(actualProduct.availabilityStatus);
                if(!productArray || !_.find(productArray, function (obj) {
                        return obj.showDateTime.includes(dateString)
                    })){
                    productArray.push({
                        day: formattedDate[0],
                        date: dateMonth[2],
                        month: dateMonth[1],
                        availability: availability,
                        showDateTime: actualProduct.showDateTime,
                        showTitle: actualProduct.showTitle,
                        venue: actualProduct.venue,
                        dateTimeObj: dataTimeObj,
                        productId: actualProduct.productId,
                        year:year[0],
                        ticketType:actualProduct.ticketType,
                        isEventWithSurvey:actualProduct.isEventWithSurvey
                    })
                }
            }
            return productArray;
        } else {
            let actualProduct = products;
            let timeArray = [];
            let formattedDate;
            if(actualProduct){
                timeArray.push(moment(actualProduct.showDateTime.substring(11, 16), ["HH:mm"]).format("hh:mm A"));
                // TODO: Deepa remove this
                formattedDate = moment(actualProduct.showDateTime).format('llll').split(',');
            }
            var dateMonth = formattedDate && formattedDate[1] ? formattedDate[1].split(' ') : formattedDate;
            //TODO: Deepa move this out
            var availability = this.getAvailability(actualProduct.availabilityStatus);
            productArray.push({
                day: formattedDate[0],
                date: dateMonth[2],
                month: dateMonth[1],
                availability: availability,
                showDateTime: actualProduct.showDateTime,
                dateTimeObj: timeArray,
                productId: actualProduct.productId,
                ticketType:actualProduct.ticketType,
                isEventWithSurvey:actualProduct.isEventWithSurvey
            })
        }
        return productArray;

    }

    getDaysInMonth(month, year) {
      return new Date(year, month, 0).getDate();
    }

    updateQty(qtyType, id, e) {
      if (qtyType === "increment") {
        var qty_value = parseInt(document.getElementById(id).value) + 1;           
        document.getElementById(id).value = qty_value;
        this.props.updateQtyData('', this.props.currentQuantity, 1);
      }

      if (qtyType === "decrement") {
        if (parseInt(document.getElementById(id).value) <= 0) {
            var qty_value = 0;
        } else {
            var qty_value = document.getElementById(id).value - 1;
        }
        document.getElementById(id).value = document.getElementById(id).value - 1;
        this.props.updateQtyData('', this.props.currentQuantity, -1);
      }

    }

    addPackCart(cart_item_data, e){
      e.preventDefault();
      var qty_val = document.getElementById("quantity-"+cart_item_data.seatSectionId).value;
      var cart_info = '';

      // Static Sample Records ==> Need to delete after
      
      // cart_info = {"packageId":2644,"packageReqSelectionList":[{"packageReqId":2381,"packageItemSelectionList":[{"uniqueId":2,"productId":1141348,"productAlias":"Patrick The Musical","mode":"SP","priceCatId":85041,"seatSectionId":173409,"seatSelection":{"seats":[{"inventoryId":239939098,"seatRowAlias":"M","seatAlias":"38","seatSectionAlias":"STALLS LEFT","seatType":1},{"inventoryId":239939097,"seatRowAlias":"M","seatAlias":"39","seatSectionAlias":"STALLS LEFT","seatType":1}],"reservedSeats":[]},"priceClassMap":"Q9:2","showDate":"2020-12-25T19:00:00+08:00","priceCatAlias":"A Reserve","priceCatNum":2,"priceAmount":{"amount":200.00,"currency":"SGD"},"priceClassAlias":"Package of 2 shows","priceClassCode":"Q9","prdtIdList":[1141348,1141351,1141352,1141353],"venueAlias":"Sands Theatre at Marina Bay Sands"}]}],"packagePromoPwdList":[]}

      if(qty_val > 0){
        var cart_item_obj = {"quantity":qty_val}
        Object.keys(cart_item_obj).map(o => cart_item_data[o]= cart_item_obj[o]);
        console.log('cart_item_data',cart_item_data);

        var available_post_data = {'mode':'HS', 'originalMode': cart_item_data.seatSectionType, 'priceCatAlias': cart_item_data.priceCatAlias, 'priceCatId': cart_item_data.priceCategoryId, 'priceCatNum': cart_item_data.priceCategoryNum, 'quantity': qty_val, 'seatSectionAlias': cart_item_data.seatSectionAlias, 'seatSectionId': cart_item_data.seatSectionId};

        API.packageAvailability(available_post_data).then(async response => {
          console.log('available_response',response);

          var available_response = response && response.data && response.data.detailSeatmapResponse && response.data.detailSeatmapResponse.setsReservedList;

          var setsReservedList = available_response && available_response.length > 0 && available_response[0].setsReserved;

          console.log('setsReservedList',setsReservedList);
          var confirmation_post_data = {'packageSectionAlias':cart_item_data.seatSectionAlias, 'releasedSeatList': null, 'reservedSeatList': setsReservedList};

          await API.packageConfirmation(confirmation_post_data).then(async response => {
            console.log('confirmation_response',response);
              
              await API.ticketType(cart_item_data.productId, cart_item_data.priceCategoryId, cart_item_data.seatSectionType, cart_item_data.pkgReqId).then(async response => {
                console.log('ticketType_response',response);
                
                var ticket_type_response = response && response.data && response.data[0];

                if(ticket_type_response){
                  var selection_post_data = {'priceClassAlias':ticket_type_response.priceClassAlias, 'priceClassAmount': cart_item_data.priceCatAmount, 'priceClassMap': ticket_type_response.priceClassCode+':'+qty_val};

                  await API.packageSelection(selection_post_data).then(async response => {
                    console.log('selection_response',response);
                    this.props.packageQtyData('');
                    
                    if(response.status === 200){
                      await API.getPkgSlctn().then(async response => {
                        console.log('get_package_response',response);
                        // if(response.status === 200){
                        if(response){
                          this.props.packageQtyData(response.data);
                        } else{
                          this.props.packageQtyData(cart_info);
                        }

                        jQuery("#addButton-"+cart_item_data.productId).addClass('hide');
                        jQuery("#prdDateSect-"+cart_item_data.productId).addClass('hide');
                        jQuery("#cartItemDetails-"+cart_item_data.productId).removeClass('hide');

                        var top_position = jQuery('#show_list_item-'+cart_item_data.productId).offset();
                        jQuery('html,body').animate({scrollTop: (top_position.top - 150)}, 700);

                        //this.props.updateQtyData('', this.props.currentQuantity, qty_val);
                        
                      });
                    } else{
                        let error_msg = response && response.data && response.data.statusMessage;
                        if(error_msg){
                            this.setState({errorMsg: error_msg},()=>{
                                this.toggleNoticeModel(true);
                            })
                        }
                    }

                  });
                }

              });
          });
        });
      } else{
        jQuery("#addButton-"+cart_item_data.productId).removeClass('hide');
        jQuery("#prdDateSect-"+cart_item_data.productId).removeClass('hide');
        jQuery("#cartItemDetails-"+cart_item_data.productId).addClass('hide');
      }
      
    }

    removeSelection(uniqueId, productId, status, e){
      e.preventDefault();

      if(status === false){
        this.setState({
          errorMsg: "Would you like to release your current selection?",
          errorStaus: "remove_cart",
          errorTitle: "Remove Selection",
          uniqueId: uniqueId,
          productId: productId
          },()=>{this.toggleNoticeModel(true);
        })

      } else{
        this.toggleNoticeModel(false);

        var remove_post_data = {'uniqueId': uniqueId};
      
        API.removeSelection(remove_post_data).then(async response => {
          console.log('get_package_response',response);
            
          // if(response && response.status === 200){
            this.props.packageQtyData('');
            await API.getPkgSlctn().then(async response => {
              console.log('get_package_response',response);

              // if(response && response.status === 200){
                this.props.packageQtyData(response && response.data);
                
                jQuery("#addButton-"+productId).removeClass('hide');
                //jQuery("#prdDateSect-"+productId).removeClass('hide');
                jQuery("#cartItemDetails-"+productId).addClass('hide');
              //}

                this.props.packShowTimeList('');

            });
          //}        
        });
      }
    
    }

    editSelection(uniqueId, productId, status, e){
      e.preventDefault();

      if(status === false){
        this.setState({
          errorMsg: "Would you like to release your current selection and select again?",
          errorStaus: "edit_cart",
          errorTitle: "Edit Selection",
          uniqueId: uniqueId,
          productId: productId
          },()=>{this.toggleNoticeModel(true);
        })
      } else{
        this.toggleNoticeModel(false);
        var remove_post_data = {'uniqueId': uniqueId};
      
        API.removeSelection(remove_post_data).then(async response => {
          console.log('get_package_response',response);
          if(response && response.status === 200){
            this.props.packageQtyData('');

            await API.getPkgSlctn().then(async response => {
              console.log('get_package_response',response);
              if(response && response.status === 200){
                this.props.packageQtyData(response.data);

                jQuery("#addButton-"+productId).removeClass('hide');
                jQuery("#prdDateSect-"+productId).removeClass('hide');
                jQuery("#cartItemDetails-"+productId).addClass('hide');
                
                jQuery("#addButton-"+productId+" > .btn").trigger('click');
              }
            });
          }
        });

      }
      
    }

    mergeArrays(arrays, prop) {
      const merged = {};
      arrays.forEach(arr => {
          arr.forEach(item => {
              merged[item[prop]] = Object.assign({}, merged[item[prop]], item);
          });
      });
      return Object.values(merged);
    }

    checkOutPackage(e){
      e.preventDefault();
      var checkout_data = {'priceClassMap' : ""}
      API.cartPackage(checkout_data).then(response => {
        console.log("checkout_response", response);

        if(this.props.patronName === undefined || this.props.patronName === ""){
            window.jQuery("#sistic-login-modal").modal("show");
        } else{
          if(response && response.data && response.data.errorCode === "error.generic"){
            this.setState({errorMsg: "An error has occurred. Please refresh the page and try again or contact our Ticketing Hotline at +65 6348 5555 for further assistance."},()=>{
                this.toggleNoticeModel(true);
            })                    
          } else if(response && response.status === 200){
              window.location = '/'+SHOPPING_CART;
          }else{
            let error_msg = response && response.data && response.data.statusMessage;
            if(error_msg){
                this.setState({errorMsg: error_msg},()=>{
                    this.toggleNoticeModel(true);
                })
            }
            //window.location = '/'+SHOPPING_CART;
          } 
        }
      })
    }
  
    render() {
        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '', imageURL = '', imageAvailable = 1, seatSectionList = [], mode = '';
        let title = this.props.eventInfo ? this.props.eventInfo.title : '';
        let products = this.props.packShowTimeListObj ? this.props.packShowTimeListObj.showTimingList : undefined;
       
        //let productArray = !this.props.showCalFlag && products && products.length > 0 ? this.getDateTimeObj(products) : null;
        let productArray = products && products.length > 0 ? this.getDateTimeObj(products) : null;

        console.log('productArray', productArray);

        //console.log('seatMapInfo_Data', this.props.seatMapInfo);

        if(this.props.seatMapInfo){
          if(this.props.seatMapInfo.imageAvailable){
            imageURL =  this.props.seatMapInfo.imageURL;
          }          
          imageAvailable =  this.props.seatMapInfo.imageAvailable;
          seatSectionList =  this.props.seatMapInfo.seatSectionList;
          mode = this.props.seatMapInfo.mode;
        }

        // Date time mapping
        let dataTimeObj = {};

        let date = new Date();
        let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
        let numberOfRows = 5;
        //let currentMonth = date.getMonth() + 1;
        
        var getFirstDate = products !== undefined ? products[0].showDateTime : '';
        var momentDate =  getFirstDate ? moment(getFirstDate).format("ddd,DD,MM,YYYY,hh:mm A").split(',') : [];
        let currentMonth = momentDate.length > 0 ? momentDate[2] : date.getMonth() + 1;
        
        //let year = date.getFullYear();
        let year = momentDate.length > 0 ? momentDate[3] : date.getFullYear();

        let tempArray = [];
        for (var j = 1; j < 32; j++) {
            let date = "" + year + "-" + currentMonth + "-" + j;
            let numberOfDatesMatched = _.filter(products, function (product) {
                return (product.showDateTime.includes(date) && product.availabilityStatus == "19")
            });
            if (numberOfDatesMatched > 0 && numberOfDatesMatched.length > 0) {
                tempArray.push({
                    availability: true,
                    index: j
                });
            }
        }
        //TODO: Deepa add a if case 30,31,28 and also the logic for the dates
        let arrayOfDaysMap = [];
        arrayOfDaysMap.push([0, 0, 1, 2, 3, 4, 5]);
        arrayOfDaysMap.push([6, 7, 8, 9, 10, 11, 12]);
        arrayOfDaysMap.push([13, 14, 15, 16, 17, 18, 19]);
        arrayOfDaysMap.push([20, 21, 22, 23, 24, 25, 26]);
        arrayOfDaysMap.push([27, 28, 29, 30, 31]);
        // firstDay = firstDay.split(" ")[0];
        let numberOfDays = this.getDaysInMonth(currentMonth, year);
        let rows = [], i = 0, len = numberOfDays;
        while (++i <= len) rows.push(i);

        let showList = this.props.packageShowListObj ? this.props.packageShowListObj : undefined;
        
        // let showList = {"pkgId":2644,"pkgAlias":"Package of 2 Shows","info":"Package of Mamma Mia and Patrick The Show @ $200 only.\r\nOffers only applicable for A Reserve.\r\nMinimum 1 tickets from each shows.","description":"Package of Mamma Mia and Patrick @ $200 only. Offers only applicable for A Reserve, minimum 1 tickets per show.","pkgCrit":{"minReq":2,"maxReq":2,"qtyRule":1,"qty":2,"sameQty":false,"samePriceCat":false,"sameTime":false,"sameDate":false},"pkgRuleList":[{"pkgRule":"package.event.rule.minimum.requirements","pkgRuleText":"Select minimum of 2 requirement(s) for this package.","ruleParam":2,"ruleDisplayLevel":"EVENT"},{"pkgRule":"package.event.rule.maximum.requirements","pkgRuleText":"Select maximum of 2 requirement(s) for this package.","ruleParam":2,"ruleDisplayLevel":"EVENT"},{"pkgRule":"package.event.rule.minimum.quantity","pkgRuleText":"Select minimum of 2 ticket(s) for this package.","ruleParam":2,"ruleDisplayLevel":"EVENT"}],"pkgReqList":[{"pkgReqId":2381,"pkgReqAlias":"Patrick The Show","pkgReqCrit":{"minPrdts":1,"maxPrdts":1,"qtyRule":2,"qty":2,"sameQty":false,"samePriceCat":false,"sameTime":false,"sameDate":false},"pkgReqRuleList":[{"pkgRule":"package.requirements.event.rule.minimum.products","pkgRuleText":"Select minimum of 1 show(s) for this requirement.","ruleParam":1,"ruleDisplayLevel":"EVENT"},{"pkgRule":"package.requirements.event.rule.maximum.products","pkgRuleText":"Select maximum of 1 show(s) for this requirement.","ruleParam":1,"ruleDisplayLevel":"EVENT"},{"pkgRule":"package.requirements.product.rule.exact.quantity.per.product","pkgRuleText":"Select exactly 2 ticket(s) per selected show.","ruleParam":2,"ruleDisplayLevel":"PRODUCT"}],"prdtGrpList":[{"prdtGrpAlias":"Patrick The Musical 音樂","prdtList":[{"prdtAlias":"Patrick The Musical","prdtIdList":[1141348,1141351,1141352,1141353],"promoPassword":false},{"prdtAlias":"Patrick The Musical - T","prdtIdList":[1141354],"promoPassword":false}]}]},{"pkgReqId":2380,"pkgReqAlias":"Mamma MIa","pkgReqCrit":{"minPrdts":1,"maxPrdts":1,"qtyRule":1,"qty":3,"maxQty":5,"sameQty":false,"samePriceCat":false,"sameTime":false,"sameDate":false},"pkgReqRuleList":[{"pkgRule":"package.requirements.event.rule.minimum.products","pkgRuleText":"Select minimum of 1 show(s) for this requirement.","ruleParam":1,"ruleDisplayLevel":"EVENT"},{"pkgRule":"package.requirements.event.rule.maximum.products","pkgRuleText":"Select maximum of 1 show(s) for this requirement.","ruleParam":1,"ruleDisplayLevel":"EVENT"},{"pkgRule":"package.requirements.product.rule.minimum.quantity.per.product","pkgRuleText":"Select minimum of 3 tickets(s) per selected show.","ruleParam":3,"ruleDisplayLevel":"PRODUCT"},{"pkgRule":"package.requirements.product.rule.maximum.quantity.per.product","pkgRuleText":"Select maximum of 5 ticket(s) per selected show.","ruleParam":5,"ruleDisplayLevel":"PRODUCT"}],"prdtGrpList":[{"prdtGrpAlias":"(DO NOT USE FOR TESTING) MAMMA MIA! (TEST DO NOT SELL)","prdtList":[{"prdtAlias":"MAMMA MIA! (TEST DO NOT SELL)","prdtIdList":[960203,960207,1135212,1135213],"promoPassword":false}]}]}]};
        
        console.log('selectPrdId',this.state.selectPrdId);
        console.log('showCalFlag',this.state.showCalFlag);
        //console.log('arrayOfDaysMap',this.state.arrayOfDaysMap);
        console.log('showList_data',showList);

        
        var cart_info = this.props.packageCartInfo && this.props.packageCartInfo;
        console.log('cart_info',cart_info);
        var packageReqSelectionList = cart_info && cart_info.packageReqSelectionList;

        console.log('packageReqSelectionList',packageReqSelectionList);

        let form, add_pack_cart_data = '', hide_class='', total_tickets = 0, pkgCount = 0, sameQtyValid=false, pkgQtyArr = [], showReq=[], showPrdReq=[], prdQty = 0;

        pkgCount = packageReqSelectionList && packageReqSelectionList.length > 0 && packageReqSelectionList.length;
        packageReqSelectionList && packageReqSelectionList.map((cart_package_data, index) =>{
          prdQty = 0;
          cart_package_data.packageItemSelectionList.map((cart_data, index) => {
              var getQty = (cart_data.priceClassMap).replace(cart_data.priceClassCode+':','');
              pkgQtyArr.push(getQty);
              total_tickets += parseInt(getQty);
              prdQty += parseInt(getQty);
          })
          showReq.push({'pkgReqId':cart_package_data.packageReqId,'prdCount': cart_package_data.packageItemSelectionList.length,'tickets':prdQty});
        })

        console.log('showReq', showReq);

        console.log('pkgQtyArr', pkgQtyArr);
        var getUnqiueVal = [...new Set(pkgQtyArr)];

        console.log('getUnqiueVal', getUnqiueVal);
        if(getUnqiueVal.length > 0){
            sameQtyValid = getUnqiueVal.length > 1 ? false : true;
        }
     
        console.log("sameQtyValid", sameQtyValid);

        var pkgReqListArr = showList.pkgReqList && showList.pkgReqList.length > 0 ? showList.pkgReqList : [];
        console.log('pkgReqListArr', pkgReqListArr);
        var showReqArr = showReq && showReq.length > 0 ? showReq : [];

        let merge_req_arr = this.mergeArrays([pkgReqListArr, showReqArr], 'pkgReqId');
        console.log('merge_req_arr', merge_req_arr);

        var validation_Summary = [], packValidation = [];
        validation_Summary.push(
          <Fragment>
          <ul className="valid_text">
            {
              showList && showList.pkgRuleList && showList.pkgRuleList.map((rule_data,index) =>{

                if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_EVENT_RULE_MINIMUM_REQUIREMENTS){
                  if(pkgCount >= rule_data.ruleParam){
                    packValidation.push(true);
                    var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                  } else{
                    packValidation.push(false);
                    var val_status = 'Not fulfilled'
                  }
                  return(<li>{rule_data.pkgRuleText} 
                    <span>{val_status}</span>
                  </li>)
                }

                if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_EVENT_RULE_MAXIMUM_REQUIREMENTS){
                  if(pkgCount >= 1){
                    packValidation.push(true);
                    var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                  } else{
                    packValidation.push(false);
                    var val_status = 'Not fulfilled'
                  }
                  return(<li>{rule_data.pkgRuleText} 
                    <span>{val_status}</span>
                  </li>)
                }

                if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_EVENT_RULE_MINIMUM_QUANTITY){
                  if(total_tickets >= rule_data.ruleParam){
                    packValidation.push(true);
                    var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                  } else{
                    packValidation.push(false);
                    var val_status = 'Not fulfilled'
                  }
                  return(<li>{rule_data.pkgRuleText} 
                    <span>{val_status}</span>
                  </li>)
                }

                if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_PRODUCT_RULE_SAME_QUANTITY){
                  if(sameQtyValid === rule_data.ruleParam){
                    packValidation.push(true);
                    var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                  } else{
                    packValidation.push(false);
                    var val_status = 'Not fulfilled'
                  }
                  return(<li>{rule_data.pkgRuleText} 
                    <span>{val_status}</span>
                  </li>)
                }

              })
            }
            </ul>

            {
              merge_req_arr && merge_req_arr.length > 0 && merge_req_arr.map((pkgData,index) => {
                return(
                  <Fragment>
                    <h4>{pkgData.pkgReqAlias}</h4>
                    <ul className="valid_text">
                    {
                      pkgData.pkgReqRuleList.map((rule_data,index) => {
                        if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_REQUIREMENTS_EVENT_RULE_MINIMUM_PRODUCTS){
                          if(pkgData.prdCount >= rule_data.ruleParam){
                            packValidation.push(true);
                            var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                          } else{
                            packValidation.push(false);
                            var val_status = 'Not fulfilled'
                          }
                          return(<li>{rule_data.pkgRuleText} 
                            <span>{val_status}</span>
                          </li>)
                        }

                        if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_REQUIREMENTS_EVENT_RULE_MAXIMUM_PRODUCTS){
                          if(pkgData.prdCount <= rule_data.ruleParam){
                            packValidation.push(true);
                            var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                          } else{
                            packValidation.push(false);
                            var val_status = 'Not fulfilled'
                          }
                          return(<li>{rule_data.pkgRuleText} 
                            <span>{val_status}</span>
                          </li>)
                        }
                        if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_REQUIREMENTS_PRODUCT_RULE_MINIMUM_QUANTITY_PER_PRODUCT){
                          if(pkgData.tickets >= rule_data.ruleParam){
                            packValidation.push(true);
                            var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                          } else{
                            packValidation.push(false);
                            var val_status = 'Not fulfilled'
                          }
                          return(<li>{rule_data.pkgRuleText} 
                            <span>{val_status}</span>
                          </li>)
                        }
                        if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_REQUIREMENTS_PRODUCT_RULE_MAXIMUM_QUANTITY_PER_PRODUCT){
                          if(pkgData.tickets <= rule_data.ruleParam){
                            packValidation.push(true);
                            var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                          } else{
                            packValidation.push(false);
                            var val_status = 'Not fulfilled'
                          }
                          return(<li>{rule_data.pkgRuleText} 
                            <span>{val_status}</span>
                          </li>)
                        }
                        if(rule_data.pkgRule === PACKAGE_RULES.PACKAGE_REQUIREMENTS_PRODUCT_RULE_EXACT_QUANTITY_PER_PRODUCT){
                          if(pkgData.tickets === rule_data.ruleParam){
                            packValidation.push(true);
                            var val_status = <img src={process.env.PUBLIC_URL + "/assets/images/pkg-fulfilled-tick.png"} alt="" title="" />
                          } else{
                            packValidation.push(false);
                            var val_status = 'Not fulfilled'
                          }
                          return(<li>{rule_data.pkgRuleText} 
                            <span>{val_status}</span>
                          </li>)
                        }
                      })
                    }
                    </ul>
                </Fragment>)

              })
            }
          </Fragment>
        );

        var getUnqValid = [...new Set(packValidation)];
        var checkValidStatus = getUnqValid && getUnqValid.length === 1 && getUnqValid[0] === true ? true : false;
        var seat_no = [], seat_row = [], seat_row_map = new Map();

        console.log('checkValidStatus', checkValidStatus);

        form = (
            <Fragment>

                <section className="container-fluid packShowCnt">
                  {
                    pkgReqListArr && pkgReqListArr.map((pkgData,index) => {
                      return(
                        <div className="row mt-4" data-pkgReqId={pkgData.pkgReqId}>
                          <div className="container mb-5">
                            <h4 className="title-bdr dateTimeTitle">
                                {pkgData.pkgReqAlias}
                            </h4>
                            {
                              pkgData.prdtGrpList && pkgData.prdtGrpList.length > 0 && pkgData.prdtGrpList.map((prdtGrpData,index) => {
                                return(
                                  <div className="col-12 p-0 list_view_cnt pack_show_list_data">
                                    <h5 className="prd_grp_title">
                                        {prdtGrpData.prdtGrpAlias}
                                    </h5>
                                    {
                                      prdtGrpData.prdtList && prdtGrpData.prdtList.length > 0 && prdtGrpData.prdtList.map((prdtListData,index) => {
                                        var prdtIdListStr = prdtListData.prdtIdList.toString();
                                        
                                        hide_class = '';

                                        return(
                                          <div className="col-12 show_list_item" id={`show_list_item-${prdtListData.prdtIdList[0]}`} data-prdtIdList={prdtIdListStr} data-pkgReqId={pkgData.pkgReqId}>
                                            <span className="show_item_text col-md-10 col-12 p-0">{prdtListData.prdtAlias}</span>
                                            
                                            {
                                              packageReqSelectionList && packageReqSelectionList.map((cart_package_data, index) =>{

                                                if(cart_package_data.packageReqId === pkgData.pkgReqId){

                                                  console.log('cart_packageReqId',cart_package_data.packageReqId+' '+pkgData.pkgReqId);

                                                  return(
                                                    <Fragment>
                                                    {
                                                      cart_package_data.packageItemSelectionList.map((cart_data, index) => {

                                                        console.log('cart_productId',cart_data.productId+' '+prdtListData.prdtIdList[0], getQty, show_time);

                                                        if(prdtListData.prdtIdList.includes(cart_data.productId)){ 

                                                          var levelAlias='', sectionAlias='';

                                                          this.props.cartInfo && this.props.cartInfo.map((temp_cart_data, c_index) => {
                                                            console.log('inventory_id',temp_cart_data.inventory_id+"__"+cart_data.inventoryId);

                                                            if(temp_cart_data.seat_section_id === cart_data.seatSectionId){
                                                              levelAlias = temp_cart_data.seat_level;
                                                              sectionAlias = temp_cart_data.seat_section;
                                                            }                                                            
                                                          })
                                                          
                                                          var getQty = (cart_data.priceClassMap).replace(cart_data.priceClassCode+':','');
                                                          var mode = cart_data.mode;

                                                          var show_time = moment(cart_data.showDate.substring(11, 16), ["HH:mm"]).format("hh:mm A");
                                                          var show_date = moment(cart_data.showDate).format('D MMM, Y');

                                                          //console.log('cart_productId',cart_data.productId+' '+prdtListData.prdtIdList[0], getQty, show_time);

                                                          //hide_class = 'hide';
                                                          jQuery("#addButton-"+prdtListData.prdtIdList[0]).addClass('hide');

                                                          seat_no = [], seat_row = [], seat_row_map = new Map();
                                                          cart_data.seatSelection.seats && cart_data.seatSelection.seats.map((seats_data,index) =>{
                                                            seat_no.push(seats_data.seatAlias);

                                                            if(!seat_row_map.has(seats_data.seatRowAlias)){
                                                              seat_row_map.set(seats_data.seatRowAlias, true); 
                                                              seat_row.push(seats_data.seatRowAlias);
                                                            }
                                                          })
    
                                                          return(
                                                          <div className="cartItemDetails" id={`cartItemDetails-${prdtListData.prdtIdList[0]}`}>
                                                              <div className="cartItemDate">
                                                                  Date<br /><span>{show_date}</span>
                                                              </div>
                                                              <div className="cartItemDate">
                                                                  Time<br /><span>{show_time}</span>
                                                              </div>
                                                              <div className="cartItemDate">
                                                                  Quantity<br /><span>{getQty} {getQty > 1 ? 'Tickets' : 'Ticket' }</span>
                                                              </div>
                                                            
                                                             {
                                                               mode && mode === 'SP' && (
                                                                <Fragment>
                                                                  <div className="cartItemDate">
                                                                    Level<br /><span>{levelAlias && levelAlias}</span>
                                                                  </div>
                                                                  <div className="cartItemDate">
                                                                      Section<br /><span>{sectionAlias && sectionAlias}</span>
                                                                  </div>
                                                                  <div className="cartItemDate">
                                                                      Row<br /><span>{seat_row.toString()}</span>
                                                                  </div>
                                                                  <div className="cartItemDate">
                                                                      Seat(s)<br /><span>{seat_no.toString()}</span>
                                                                  </div>
                                                                </Fragment>
                                                               )
                                                             }
                                                              
              
                                                              <div className="float-right cart_act">
                                                                <a href="javascript:;" className="edit_cls" onClick={e => this.editSelection(cart_data.uniqueId, prdtListData.prdtIdList[0], false, e)}>Edit</a>
                                                                <i className="trash-icon" id={`removeIndex-${cart_data.uniqueId}`} onClick={e => this.removeSelection(cart_data.uniqueId, prdtListData.prdtIdList[0], false, e)}>
                                                                    <img src={process.env.PUBLIC_URL + "/assets/images/pack_delete.svg"} alt="Delete" />
                                                                </i>
                                                              </div>
                                                          </div>
                                                          )
                                                          
                                                        } else{
                                                          hide_class = '';
                                                        }
    
                                                      })
                                                    }
                                                    </Fragment>
                                                  )                                                  
                                                } else{
                                                  //hide_class = '';
                                                }
                                                
                                              })
                                            }

                                            <span className={`addBtnCls col-md-2 col-12 ${hide_class}`}  id={`addButton-${prdtListData.prdtIdList[0]}`}>
                                              <button className="btn check-out-btn" data-id={prdtListData.prdtIdList[0]} onClick={this.addPackagePrd.bind(this, pkgData.pkgReqId, prdtIdListStr)}>Add</button>
                                            </span>
                                            
                                            {
                                            this.state.selectPrdId === prdtListData.prdtIdList[0].toString() && productArray && productArray.length > 0 && (
                                            <div className="col-12 prdDateSect" id={`prdDateSect-${prdtListData.prdtIdList[0]}`} data-id={prdtListData.prdtIdList[0].toString()}>
                                              <h5 className="prdDateTilte">
                                                  Select A Date
                                                  <a href="javascript:;" className="btn btn-primary round-btn calendar_view_btn actv float-right" onClick={this.showCal.bind(this)}>
                                                      <img src={process.env.PUBLIC_URL + "/assets/images/calendar-btn.svg"} className="cal_view" alt="Calender View"/>
                                                      <img src={process.env.PUBLIC_URL + "/assets/images/calendar-list.svg"}
                                                          alt="Slider View" className="slider_view hide"/>
                                                  </a>
                                              </h5>

                                              {
                                                this.props.hideDefaultDateView && (
                                                    <div id={`dektop-date-slide${prdtListData.prdtIdList[0]}`} className="date-slide owl-carousel mt-4 owl-loaded owl-drag hide">

                                                        <div className="owl-stage-outer">
                                                            <div className="owl-stage"
                                                                style={{
                                                                    transform: "translate3d(0px, 0px, 0px)",
                                                                    transition: "all 0s ease 0s",
                                                                    width: "2006px"
                                                                }}>
                                                                {productArray.map((eachProduct) => {
                                                                    var avail_split = eachProduct.availability.split(',');
                                                                    
                                                                    return(
                                                                    <Fragment>
                                                                        {!eachProduct.availability ? 
                                                                            (<div className={`owl-item in-active`}  data-datetime={eachProduct.showDateTime} data-availability={eachProduct.availability} style={{
                                                                                width: "134.286px", "margin-right": "20px"}}>
                                                                                <div className={'date-item in-active'} data-eventsurvey={eachProduct.isEventWithSurvey} id={eachProduct.productId} onClick={this.showTime.bind(this, eachProduct.showDateTime, eachProduct.dateTimeObj, eachProduct.productId, eachProduct.date+ " "+eachProduct.month+ " "+eachProduct.year, eachProduct.ticketType, eachProduct.isEventWithSurvey, eachProduct.showTitle, eachProduct.venue)}>
                                                                                    <span className="day">{eachProduct.day}</span>
                                                                                    <span className="date">{eachProduct.date}</span>
                                                                                    <span className="month">{eachProduct.month}</span>
                                                                                    <span className="status">{avail_split[0]}</span>
                                                                                </div>
                                                                            </div>) : (
                                                                            <div className={`owl-item ${avail_split[1]}`} data-datetime={eachProduct.showDateTime} data-availability={eachProduct.availability} style={{
                                                                                width: "134.286px","margin-right": "20px"}} onClick={this.showTime.bind(this, eachProduct.showDateTime, eachProduct.dateTimeObj, eachProduct.productId, eachProduct.date+ " "+eachProduct.month+ " "+eachProduct.year, eachProduct.ticketType, eachProduct.isEventWithSurvey, eachProduct.showTitle, eachProduct.venue)}>
                                                                                <div className={"date-item"} id={eachProduct.productId}>
                                                                                    <span className="day">{eachProduct.day}</span>
                                                                                    <span className="date">{eachProduct.date}</span>
                                                                                    <span className="month">{eachProduct.month}</span>
                                                                                    <span className="status">{avail_split[0]}</span>
                                                                                </div>
                                                                            </div>
                                                                            )
                                                                        }
                                                                    </Fragment>
                                                                    )
                                                                    
                                                                })
                                                              }
                                                            </div>
                                                        </div>
                                                    </div>                                              
                                                )
                                              }

                                              {this.props.showCalFlag &&
                                                <div className="calendar-grid col-md-12">
                                                    <div className="cal-hdr">
                                                        <p>{monthNames[currentMonth - 1]}</p>
                                                        <div className="day-wrap">
                                                            <p>Sun</p>
                                                            <p>Mon</p>
                                                            <p>Tue</p>
                                                            <p>Wed</p>
                                                            <p>Thu</p>
                                                            <p>Fri</p>
                                                            <p>Sat</p>
                                                        </div>
                                                    </div>
                                                    {arrayOfDaysMap.map((value) => (
                                                        <div className="date-row">
                                                            { // Deepa - check this
                                                                value.map((dateNum) => (
                                                                    dateNum === 0 ? (
                                                                        <div className={"date-item no_bdr"} id={dateNum}>
                                                                            
                                                                        </div>
                                                                    ) : (
                                                                    <Fragment>
                                                                        {dateNum < 10 && filter(products, function (product) {
                                                                            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-0" + dateNum) && product.availabilityStatus == "19" && product.isEventWithSurvey === true)
                                                                        }).length > 0 &&
                                                                        <div className={"date-item "} id={dateNum}
                                                                            onClick={this.calculateTime.bind(this, "" + "2020" + "-" + "12" + "-0" + dateNum, dateNum, year+" "+monthNames[currentMonth-1]+" 0"+dateNum, "RS", true)}>
                                                                            <span className="date">{dateNum}</span>
                                                                            <span className="status">Available</span>
                                                                        </div>
                                                                        }
                                                                        {dateNum < 10 && filter(products, function (product) {
                                                                            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-0" + dateNum) && product.availabilityStatus == "19" && (product.isEventWithSurvey === false))
                                                                        }).length > 0 &&
                                                                        <div className={"date-item "} id={dateNum}
                                                                            onClick={this.calculateTime.bind(this, "" + "2020" + "-" + "12" + "-0" + dateNum, dateNum, year+" "+monthNames[currentMonth-1]+" 0"+dateNum, "GA", false)}>
                                                                            <span className="date">{dateNum}</span>
                                                                            <span className="status">Available</span>
                                                                        </div>
                                                                        }
                                                                        {dateNum < 10 && filter(products, function (product) {
                                                                            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-0" + dateNum) && product.availabilityStatus == "19")
                                                                        }).length === 0 &&
                                                                        <div className="date-item in-active" id={dateNum}>
                                                                            <span className="date">{dateNum}</span>
                                                                        </div>
                                                                        }
                                                                        {dateNum >= 10 && filter(products, function (product) {
                                                                            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-" + dateNum) && product.availabilityStatus == "19" && (product.isEventWithSurvey === true))
                                                                        }).length > 0 &&
                                                                        <div className={"date-item "} id={dateNum}
                                                                            onClick={this.calculateTime.bind(this, "" + "2020" + "-" + "12" + "-" + dateNum, dateNum, year+" "+monthNames[currentMonth-1]+" "+dateNum, "RS", true)}>
                                                                            <span className="date">{dateNum}</span>
                                                                            <span className="status">Available</span>
                                                                        </div>
                                                                        }
                                                                        {dateNum >= 10 && filter(products, function (product) {
                                                                            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-" + dateNum) && product.availabilityStatus == "19" && (product.isEventWithSurvey === false))
                                                                        }).length > 0 &&
                                                                        <div className={"date-item "} id={dateNum}
                                                                            onClick={this.calculateTime.bind(this, "" + "2020" + "-" + "12" + "-" + dateNum, dateNum, year+" "+monthNames[currentMonth-1]+" "+dateNum, "GA", false)}>
                                                                            <span className="date">{dateNum}</span>
                                                                            <span className="status">Available</span>
                                                                        </div>
                                                                        }
                                                                        {dateNum >= 10 && filter(products, function (product) {
                                                                            return (product.showDateTime.includes("" + "2020" + "-" + "12" + "-" + dateNum) && product.availabilityStatus == "19")
                                                                        }).length === 0 &&
                                                                        <div className="date-item in-active" id={dateNum}>
                                                                            <span className="date">{dateNum}</span>
                                                                        </div>
                                                                        }
                                                                    </Fragment>
                                                                  )
                                                                ))
                                                            }
                                                        </div>
                                                    ))
                                                    }
                                                    <div className="date-legend d-block d-sm-block d-md-none">
                                                        <span className="avail-state">Available</span>
                                                        <span className="fast-state">Selling Fast</span>
                                                        <span className="insuff-state">Insufficient Seat(s)</span>
                                                    </div>
                                                </div>
                                              }

                                              {
                                              this.props.showTimeFlag && 
                                                <div className="container p-0 mt-4" id="showTimeID">
                                                  <div className="">
                                                    <h5 className="prdDateTilte">Select a time</h5>
                                                    <div className="time-sec col-md-12 mt-4">
                                                        <div className="clearfix">
                                                            {
                                                            
                                                              this.props.showTimes && this.props.showTimes.length > 0 && this.props.showTimes.map((time, index) => (
                                                                  <Fragment>
                                                                      {index<=6 &&  <div className="date-item"  onClick={this.chosenTime.bind(this, time)}>
                                                                          <span className="time">{time}</span>
                                                                          <span className="status">Available</span>
                                                                      </div>}
                                                                      {index >=7 &&  <div className="date-item hide">
                                                                          <span className="time">{time}</span>
                                                                          <span className="status">Available</span>
                                                                      </div>}
                                                                  </Fragment>
                                                              )
                                                            )}
                                                        </div>
                                                        {
                                                        this.props.showTimes && this.props.showTimes.length > 7 &&
                                                        <div className="clearfix text-center mt-3">
                                                            <button className="btn btn-primary view-all" id ="view_all_timings" onClick={this.displayAll.bind(this)}>
                                                                View All Timings
                                                                <img className="ml-1"
                                                                    src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"}
                                                                    alt="icon"/>
                                                            </button>
                                                        </div>
                                                        }

                                                    </div>
                                                  </div>
                                                </div>
                                              }

                                            {                                              
                                              this.props.prdTicketType && this.props.prdTicketType === "GA" && 
                                              seatSectionList && seatSectionList.length > 0 && seatSectionList.map((seatSectionData,index) => {
                                                var seatSectionId = seatSectionData.SeatLevel && seatSectionData.SeatLevel[0].seatSectionId;
                                                var product_id = prdtListData.prdtIdList[0];
                                                if(seatSectionData.SeatLevel && seatSectionData.SeatLevel.length > 0){                                                
                                                  add_pack_cart_data = {'pkgReqId': pkgData.pkgReqId, 'cartItemName': prdtListData.prdtAlias, 'cartItemDate': this.props.dateChosen, 'cartItemTime': this.props.timeChosen, 'productId': product_id, 'priceCatAlias': seatSectionData.priceCatAlias, 'seatSectionId': seatSectionData.SeatLevel[0].seatSectionId, 'priceCategoryId': seatSectionData.SeatLevel[0].priceCategoryId, 'seatSectionType': seatSectionData.SeatLevel[0].seatSectionType, 'seatSectionAlias': seatSectionData.SeatLevel[0].seatSectionAlias, 
                                                  'priceCategoryNum': seatSectionData.SeatLevel[0].priceCategoryNum, 'priceCatAmount': seatSectionData.priceCatAmount.amount, 'priceCatCurrency': seatSectionData.priceCatAmount.currency, 'priceCatFormatted': seatSectionData.priceCatAmount.formatted};
                                                }
                                                
                                                  return(
                                                    <div className="packageQtySection" id="packageQtySection">

                                                  <div className="float-left slctQtyTxt">{seatSectionList.length > 1 ? seatSectionData.priceCatAlias : 'Select A Quantity'}</div>

                                                      <div className="tic-right button-container">
                                                        <button className={`cart-qty-minus ${(this.props.currentQuantity > 0) ? 'active' : 'in-active'}`} id={`cart-qty-minus`} type="button" value="-"
                                                            onClick={(e) => this.updateQty('decrement', `quantity-${seatSectionId}`, e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/minus.svg"} alt="icon" />
                                                        </button>

                                                        <input type="text" name={`quantity-${seatSectionId}`} id={`quantity-${seatSectionId}`} className="qty form-in" maxLength="12" defaultValue={0} />

                                                        <button className={`cart-qty-plus`} id={`cart-qty-plus`} type="button" value="+"
                                                            onClick={(e) => this.updateQty('increment', `quantity-${seatSectionId}`, e)}>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/plus.svg"} alt="icon" />
                                                        </button>
                                                      </div>

                                                      <div className="float-right addPackBtn"><button className={`btn check-out-btn`} onClick={this.addPackCart.bind(this, add_pack_cart_data)}>Add</button></div>
                                                      
                                                    </div>
                                                  )
                                              })    
                                            }

                                            {                                              
                                              this.props.prdTicketType && this.props.prdTicketType === "RS" && 

                                              <div className="seat-map packageMap hide" style={{'display': this.props.prdTicketType && this.props.prdTicketType === "RS" ? 'inherit':'none'}}>
                                                  <div className="container">
                                                      <div className="row">
                                                          <div className="col-8 pl-0">
                                                            <div className="float-left slctQtyTxt">Seat Selection</div>
                                                              {/* <h4>Select Your seat(s) </h4>
                                                              <p>To shift seats, unselect the chosen seats and select your preferred seats.</p> */}
                                                          </div>
                                                          <div className="col-4 right-align-column hide">
                                                              <button type="button" className="btn get-seat-btn get-seat-desktop">Get the best seats available</button>
                                                              <button type="button" className="btn get-seat-btn get-seat-mobile">Best seats</button>
                                                          </div>
                                                      </div>
                                                      
                                                      <div className="row seat-selection">
                                                          <SeatsOverview_Package />
                                                          <PackageSeatsStageView />
                                                      </div>
                                                      
                                                  </div>
                                              </div>

                                            }

                                            </div>
                                            )
                                          }
                                        </div>
                                        )
                                      })
                                    }
                                  </div>
                                )
                              })
                            }
                            
                          </div>
                        </div>
                      )
                    })
                  }

                  {
                    showList.pkgReqList && showList.pkgReqList.length > 0 && (
                      <div className="container">
                        <div className="col-md-12 px-0 mb-5 text-right checkOutBtn package">
                          {
                            checkValidStatus === false && (
                              <button className={`btn check-out-btn mr-2`} id="confirm-type" onClick={()=>this.toggleValidationModel(true)}>Check Package Requirements</button>
                            )
                          }

                          <button className={`btn check-out-btn ${checkValidStatus === false && 'in-active'}`} id="confirm-type" onClick={(e)=>this.checkOutPackage(e)}>Checkout <img className="ml-4" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} alt="icon" /></button>

                          
                          
                        </div>
                      </div>
                    )
                  }
                  
                    
                </section>

                {/* ========= Notice Modal ======= */}
                <Modal show={this.state.noticeModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleNoticeModel(false)} centered>
                    <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">{this.state.errorTitle ? this.state.errorTitle : 'Error' }</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {
                            this.state.errorMsg && (
                                <div dangerouslySetInnerHTML={{ __html: this.state.errorMsg }}></div>
                            )
                        }
                    </Modal.Body>
                    <Modal.Footer>
                    {   
                        this.state.errorStaus === "remove_cart" && (
                          <Fragment>
                              <Button onClick={(e)=>this.removeSelection(this.state.uniqueId, this.state.productId, true, e)}>Ok</Button>
                              <Button onClick={()=>this.toggleNoticeModel(false)}>Cancel</Button>
                          </Fragment>
                        ) 
                    }
                    {   
                        this.state.errorStaus === "edit_cart" && (
                          <Fragment>
                              <Button onClick={(e)=>this.editSelection(this.state.uniqueId, this.state.productId, true, e)}>Ok</Button>
                              <Button onClick={()=>this.toggleNoticeModel(false)}>Cancel</Button>
                          </Fragment>
                        )
                    }
                    {
                        
                        this.state.errorStaus === '' && (
                           <Button onClick={()=>this.toggleNoticeModel(false)}>Ok</Button>
                        )
                    }
                    </Modal.Footer>
                </Modal>

                <Modal show={this.state.checkOutValidation} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleValidationModel(false)} id={`validationModal`} centered>
                    <Modal.Header closeButton>
                      <Modal.Title id="contained-modal-title-vcenter">{showList && showList.description}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {validation_Summary}
                    </Modal.Body>
                    <Modal.Footer>                    
                      {
                          
                          this.state.errorStaus === '' && (
                            <Button onClick={()=>this.toggleValidationModel(false)}>Ok</Button>
                          )
                      }
                    </Modal.Footer>
                </Modal>
                

                {/* <Footer/> */}
            </Fragment>
        );
        return (
            <Fragment>
                {form}
            </Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        packShowTimeListObj: state.packShowTimeListObj && state.packShowTimeListObj,
        eventInfo: state.showInfo && state.showInfo.eventInfo,
        prdTicketType: state.prdTicketType,
        packageShowListObj: state.packageShowListObj,
        showTimeFlag: state.showTimeFlag,
        showCalFlag: state.showCalFlag,
        hideDefaultDateView: state.hideDefaultDateView,
        showTimes: state.showTimes,
        showActive: state.showActive,
        chosenProductId: state.chosenProductId,
        dateChosen: state.dateChosen,
        timeChosen: state.timeChosen,
        prdTicketType: state.prdTicketType,
        isEventWithSurvey: state.isEventWithSurvey,
        pkgReqId: state.pkgReqId,
        prdtIdList: state.prdtIdList,
        productAlias: state.choose_showTitle,
        venueAlias: state.choose_venue,
        showDateTime: state.choose_showDate,
        currentQuantity: state.currentQuantity,
        seatMapInfo: state.seatMapInfo.seatMapInfo && state.seatMapInfo.seatMapInfo,
        packageCartInfo: state.packageCartInfo,
        patronName: state.patronName,
        cartInfo: state.cartInfo,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchShowInfoData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        updateTicketType: (list) => {
            dispatch(actionCreators.updateTicketType(list));
        },
        chosenProductTime: (time) => {
            dispatch(actionCreators.chooseTime(time));
        },
        breadCrumbData: (field, value) => {
            dispatch(actionCreators.breadCrumbs(field, value));
        },
        packShowTimeList: (dataObject) =>{
          dispatch(actionCreators.packShowTimeList(dataObject));
        },
        showCalendarView: (flag, hide) => {
          dispatch(actionCreators.showCalendarView(flag, hide));
        },
        chosenProductDate: (flag, times, productId, dateChosen, ticket_type, isEventWithSurvey, showTitle, venue, show_date_time) => {
          dispatch(actionCreators.chooseDate(flag, times, productId, 'active', dateChosen, ticket_type, isEventWithSurvey, showTitle, venue, show_date_time));
        },
        chosenPackage: (pkgReqId, prdtIdList) => {
            dispatch(actionCreators.choosePackage(pkgReqId, prdtIdList));
        },
        fetchSeatMapOverview: (dataObject) => {
            dispatch(actionCreators.fetchSeatMapInfo(dataObject));
        },
        fetchPriceTabele: (dataObject) => {
          dispatch(actionCreators.fetchPriceTabele(dataObject));
        },
        fetchSeatSelection: (dataObject) => {
            dispatch(actionCreators.fetchSeatSelInfo(dataObject));           
        },
        updateQtyData: (obj, current, increment) => {
          dispatch(actionCreators.updateQty(obj, current, increment));
        },
        packageQtyData: (obj) => {
          dispatch(actionCreators.packageQtyInfo(obj));
        },
        updateEventType: (event_type) => {
          dispatch(actionCreators.updateEventType(event_type));
        },
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PackageShowList);