import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../actions/actionCreator"
import {Link} from "react-router-dom"
import Header from '../common/Header'
import DeliveryCartDetails from './Delivery/DeliveryCartDetails'
import DeliveryMethods from './Delivery/DeliveryMethods'
import DeliveryInsured from './Delivery/DeliveryInsured'
import Subscription from './Delivery/Subscription';
import CrossSell from './Delivery/CrossSell';
import BookingRegistration from './Delivery/BookingRegistration';
import jQuery from 'jquery';

import {SITE_URL, TENANT} from '../../constants/common-info'

import * as API from "../../service/events/eventsInfo";
import {Modal, Button} from 'react-bootstrap';

class Delivery extends Component {

    constructor(props) {
        super(props);
        this.state ={
            noticeModel:false,
            termsCond: false
        }
    }

    toggleNoticeModel = (boolean) =>{
        this.setState({noticeModel: boolean});
    }

    componentWillMount(){
        this.props.breadCrumbData('delivery_visit',1);
    }

    onTermsConds(boolean) {
        
        if(boolean === false){
            this.setState({termsCond: true});
        } else{
            this.setState({termsCond: false});
        }
        // if (jQuery('#terms-conds').is(':checked')) {
        //     jQuery("#make-payment").removeClass("in-active");
        // } else {
        //     jQuery("#make-payment").addClass("in-active");
        // }
    }

    render() {

        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';
        let cart_list = this.props.cartInfo;

        //console.log('cart_list',cart_list);
        return (

            <Fragment>
                <Header/>

                <main className={`shopingCartCls`}>

                    <DeliveryCartDetails />

                    <BookingRegistration />
                    
                    <DeliveryMethods />

                    <CrossSell />
                    
                    <DeliveryInsured />

                    <Subscription />

                    <section className="container-fluid condition-wrap">
                        <div className="container mob-px-0">
                            <h4 className="title-bdr mb-4">Conditions of Sale, Use or Personal Data:</h4>
                            <label className="custom-checkbox w-100">
                                <input type="checkbox" name="three" id="terms-conds"
                                       onClick={this.onTermsConds.bind(this, this.state.termsCond)}/>I have read and accept SISTIC <a
                                href="javascript:;" onClick={this.toggleNoticeModel.bind(this,true)}>Terms & Conditions of Sale</a> and consent to:
                                <span className="checkmark"></span>
                            </label>
                            <p className="mt-3"><span className="mr-2">1. </span>the collection of my personal data by
                                SISTIC both in its own capacity, and as ticketing agent and data intermediary of the
                                promoter(s) and venue manager(s) of the event(s) purchased; and</p>
                            <p className="mt-2"><span>2. </span>the collection, use and disclosure of such personal data
                                by
                                SISTIC, its agents and the promoter(s) and venue manager(s), in accordance with SISTIC's <a href="javascript:;">Privacy Policy.</a> No refund, exchange of tickets is allowed once your booking is confirmed</p>

                            <div className="col-12 px-0 text-right pt-3 proceed-lg">
                            <Link to={`/${TENANT}/confirm/payment`} id="make-payment" className={`btn btn-primary ${this.state.termsCond === true ? '' : 'in-active'}`}>Proceed to Payment <img className="ml-3" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} /></Link>

                                {/* <button class="btn btn-primary in-active" id="make-payment" onClick={()=>this.proceedPayment()}>
                                 Proceed to Payment
                                    <img class="ml-4"
                                         src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"}/>
                                </button> */}

                                {/* <Link to={`/booking-confirm`} className="btn btn-primary">Confirm Order
                                    <img className="ml-4"
                                         src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"}/></Link> */}
                            </div>
                        </div>
                    </section>

                    <Modal dialogClassName={`terms_cond_cls`}  show={this.state.noticeModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleNoticeModel(false)} centered>
                        <Modal.Body>
                            <p>
                            <b>Terms and Conditions:</b>
                            1. All tickets, etickets, vouchers, memberships and packages (“Tickets”) are sold by SISTIC.com Pte Ltd ("SISTIC") as ticketing agent for and on behalf of the entity or entities (collectively “Provider”) operating, managing, producing, promoting or providing the event, show, performance, screening, webstream, exhibition, conference, transport service, attraction, food and beverage, place of interest, ride, venue access, membership programme, or other product or service for which the Tickets are sold (individually and collectively, “Services”), or the venue, vehicle, craft, equipment, attraction, restaurant, webstream platform in or by which the Services are provided (individually and collectively, “Venue”). All orders are subject to availability and acceptance by SISTIC. SISTIC reserves the right to accept or reject any order. By ordering Tickets you agree, on your own behalf and as agent on behalf of all persons for whom you are purchasing Tickets or who will be holding Tickets purchased by you (you and each such person being a “Ticket Holder”), to be bound by these Terms and Conditions. SISTIC shall be entitled, but not obliged, to conduct verification checks on any order, and shall be entitled to rescind the order if such verification checks reveal any fraud or other irregularity in the order.
                            <br /><br />
                            2. SISTIC may in certain situations allow the sale or distribution of selected Tickets by approved third parties who may or may not include Providers (“Approved Third Parties”) via online websites, mobile applications, or other online platforms (“Third Party Sites”). However, these Third Party Sites are not owned, controlled or managed by SISTIC. Consequently, SISTIC does not warrant, endorse, guarantee or assume responsibility, and shall not be liable for, any information, materials, content, or any other product or service produced or provided by a third party and/or on the Third Party Sites, or for any acts or omissions of the Approved Third Parties. Such Third Party Sites may also have their own terms and conditions and/or privacy policies, and SISTIC does not accept any responsibility or liability for these terms or policies.
                            <br /><br />
                            3. SISTIC has no control over the maintenance or management of the Venue, or the organisation or management of the Services. Where a Service is webstreamed,
                            <br /><br />
                            3.1 SISTIC has no control or management over the Service being webstreamed;
                            <br /><br />
                            3.2 your ability to access the webstream, and the quality of the webstream, are dependent on the webstream service provider, your network service provider, internet traffic and other factors beyond SISTIC's control; and
                            <br /><br />
                            3.3 SISTIC will not be liable for any problems or issues you may have in accessing the webstream. 
                            <br /><br />
                            3.4 SISTIC will not be liable for any content posted by users in the chatroom and/or webstream, and cannot be held responsible for any illegal, infringing, objectionable or offensive text, images, photographs, content, or material uploaded by other users.
                            <br /><br />
                            3.5 You agree that you are solely responsible for all content that you make available on or through the chatroom and/or webstream, and you agree not to submit, upload or publish any content that is obscene, infringing, offensive, illegal, or inflammatory, or otherwise commit any unlawful act or any act designed to impair the operation and functionality of the Services.
                            </p>
                            <Button onClick={()=>this.toggleNoticeModel(false)}><span>Continue Booking</span><span className={`hoverName`}>Checkout</span></Button>
                        </Modal.Body>                        
                    </Modal>
                </main>

                {/* <Footer/> */}
            </Fragment>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        patronName: state.patronName,
        email: state.email,
        firstName: state.firstName,
        lastName: state.lastName,
        accountNo: state.accountNo,
        accountType: state.accountType,        
        cartInfo: state.cartInfo,
        chosenAddon: state.chosenAddon,
        chosenAddonPrice: state.chosenAddonPrice,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchData: (dataObject) => {
            dispatch(actionCreators.fetchShowInfo(dataObject));
        },
        breadCrumbData: (field,value) => {
            dispatch(actionCreators.breadCrumbs(field,value));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Delivery);