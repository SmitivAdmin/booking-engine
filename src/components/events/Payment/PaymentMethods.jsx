import React, {Component, Fragment} from 'react'
import {connect} from "react-redux";
import jQuery from 'jquery'
import {API_KEY, SERVER_URL} from "../../../constants/common-info";
import * as actionCreators from "../../../actions/actionCreator";
import {paymentSuccess} from "../../../actions/actionCreator";
import moment from "moment";
import {Redirect} from 'react-router'
import * as API from "../../../service/events/eventsInfo";
import {Modal, Button} from 'react-bootstrap';

import { PAYMENT_LANG_OBJ, PaymentMethod } from '../../../constants/common-info'


class PaymentMethods extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cardType : [],
            expMonth: [
                'MM',
                '01',
                '02',
                '03',
                '04',
                '05',
                '06',
                '07',
                '08',
                '09',
                '10',
                '11',
                '12'
            ],
            expYear:[],

            // For API
            selectedCardType: '',
            cardNumber: '',
            selectedMonth: 'MM',
            selectedYear: 'YYYY',
            cvvNumber: '',
            nameOnCard: '',

            // For Dialog/Modal
            isShowModel: false,
            modalHead:'',
            modalBody:'',
            isError: false,
            isConfirmOrderResponse: false,


            // For Validation dialog
            validationModel: false,
            validationMessage: '',
            validationTitle: ''
        }
    }

    componentWillMount (){
        let currentYear = new Date().getFullYear();
        let itemCount = 8;
        let lclYear = ["YYYY"];
        for(let i=0;i<=itemCount;i++){
            lclYear.push((currentYear+i).toString());
        }

        this.setState({expYear: lclYear});
    }

    toggleModel = (boolean) =>{
        this.setState({isShowModel:boolean});
    }

    fillDetails() {
        let amount = this.props.totalPriceToBePaid;
        jQuery('.row.credit-card-encl.mt-5.dkt-ccard').removeClass("hide");
        jQuery('.col-md-12.credit-card-encl.px-0.mb-4.mbl-ccard').removeClass("hide");
        jQuery('#credit-card').addClass("active");
    }

    componentDidMount() {
        let paymentString = jQuery("#shoppingCartId").attr("data-commonPaymentMethod");
        if (paymentString !== "") {
            let parsedPaymentString = JSON.parse(paymentString).paymentMethodList;
            console.log("PaymentMetod from java test", parsedPaymentString);
            this.setState({
                cardType: parsedPaymentString
            })
        }
    }

    triggerConfirmPopup = () =>{
        this.setState({
            modalHeadL: 'Confirm Order',
            modalBody: 'Please click OK to proceed with the payment and wait for your confirmation page to be displayed. You will also receive a booking confirmation via email after a successful transaction.',
            isConfirmOrderResponse: true,
            isError: false,
            isShowModel: true
        })
    }

    confirmOrder = async () =>{
        this.toggleModel(false);
        //let terms_condition = jQuery('#terms-conds').is(':checked') ? 'on' : 'off';
        let terms_condition = 'on';
        var userBilingDetails = this.props.userBilingDetails;
        const formData = new FormData();

        console.log("chosenAddon",this.props.chosenAddonMethod);

        formData.append('deliveryMethodCode', this.props.chosenAddonMethod !== "" ? this.props.chosenAddonMethod : "E_TICKET");

        // User Details Should be bind here.
        formData.append('firstName', this.props.firstName);
        formData.append('lastName', this.props.lastName);
        // Want to know where is from.
        formData.append('mailingCountry', userBilingDetails && userBilingDetails.mailingCountry ? userBilingDetails.mailingCountry : '');
        formData.append('mailingBlockHouseStreet', userBilingDetails && userBilingDetails.mailingBlockHouseStreet ? userBilingDetails.mailingBlockHouseStreet : '');
        formData.append('mailingUnitNo', userBilingDetails && userBilingDetails.mailingUnitNo ? userBilingDetails.mailingUnitNo : '');
        formData.append('mailingBuildingName', userBilingDetails && userBilingDetails.mailingBuildingName ? userBilingDetails.mailingBuildingName : '');
        formData.append('mailingPostalCode', userBilingDetails && userBilingDetails.mailingPostalCode ? userBilingDetails.mailingPostalCode : '');
        formData.append('isUpdateMailingAddress', userBilingDetails ? userBilingDetails.isUpdateMailingAddress : false);
        formData.append('purchaseTp', userBilingDetails && (userBilingDetails.purchaseTp && userBilingDetails.purchaseTp !==null) ? userBilingDetails.purchaseTp : false ); // TO confirm with sathish

        // Is E-VOUNCHER Details should be bind here.
        formData.append('evoucherIds', '');
        formData.append('evoucherIds', '');
        formData.append('evoucherIds', '');

        // Cart details
        formData.append('paymentType', 'credit');  // To confirm with sasi
        formData.append('cardType', this.state.selectedCardType);
        formData.append('creditCardNumber', this.state.cardNumber);
        formData.append('cardExpiryMonth', this.state.selectedMonth === "MM" ? "" : parseInt(this.state.selectedMonth));
        formData.append('cardExpiryYear', this.state.selectedYear ==="YYYY" ? "" : parseInt(this.state.selectedYear));
        formData.append('cardSecurityCode', this.state.cvvNumber !== "" ? parseInt(this.state.cvvNumber) : '');
        formData.append('creditCardHolderName', this.state.nameOnCard);

        // formData.append('cardType', "amex");  // amex, visa, mastercard
        // formData.append('creditCardNumber', "375987000000005");
        // formData.append('cardExpiryMonth', "07");
        // formData.append('cardExpiryYear', "2023");
        // formData.append('cardSecurityCode', "123");
        // formData.append('creditCardHolderName', "tester");

        //formData.append('termsAccepted', jQuery('#terms-conds').is(':checked'));
        formData.append('termsAccepted', true);
        formData.append('_termsAccepted', terms_condition);
        
        await API.confirmOrder(formData).then(response => {
            console.log('confirm_order_response', response);
            if(response){
                if (response && response.status === 200) {
                    //window.location.href = response.processPaymentUrl;
                    this.setState({
                        modalHead: "success",
                        modalBody: response.data.statusMessage,
                        isConfirmOrderResponse: false,
                        isError: false
                    },()=>{
                        this.toggleModel(true);
                    });
    
                    window.location.href = response.data.processPaymentUrl;
                    //window.location.href = '/sistic/confirmorder/postconfirmation';
                } else{
                    console.log("confirm_order_fail", response);
                    if(response && response.data && response.data.statusMessage !== ""){
                        this.setState({
                            modalHead: "Error",
                            modalBody: response.data.statusMessage,
                            isError: true
                        },()=>{
                            this.toggleModel(true);
                        });
                    } else{
                        window.location.href = response.data.processPaymentUrl;
                        //window.location.href = '/sistic/confirmorder/postconfirmation';
                    }
                    
                }
            }
            
        })
    }

    validate = () =>{
        debugger
        var errorString = "";
        var self = this;
        return new Promise((resolve, reject) => {
            if(self.state.selectedCardType === ""){
                errorString += "<span>Select Card Type</span></br>";
            }
            if(self.state.cardNumber === ""){
                errorString += "<span>Card Number is empty</span></br>";
            }
            if(self.state.selectedMonth === "MM"){
                errorString += "<span>Card expiry month</span></br>";
            }
            if(self.state.selectedYear === "YYYY"){
                errorString += "<span>Card expiry year</span></br>";
            }
            if(self.state.cvvNumber === ""){
                errorString += "<span>CVV2/CVC2</span></br>";
            }
            if(self.state.nameOnCard === ""){
                errorString += "<span>Name on Card</span></br>";
            }

            if(errorString === ""){
                resolve(true);
            }
            else{
                self.setState({validationModel: true, validationMessage: errorString, validationTitle: 'Incomplete Fields'});
                resolve(false);
            }

        })
    }

    proceedPayment = async () => {
        // Check if terms and condition accepted.
        // TODO check if *selected credit card*.   --- IMPORTANT.
        
        // After Validation.
        //let validateFields = await this.validate();
        let validateFields =true;

        if (validateFields) {
            let promotionDetails = {
                ccNum: this.state.cardNumber,
                //ccNum: "5123450000000008",
                eVouchers: []
            };

            await API.promotion(promotionDetails).then((response) => {
                console.log('Payment promotion hardcore data', response);

                if (response && response.httpStatus === 200) {
                    let attatchFee = PAYMENT_LANG_OBJ[response.lightBoxMessage].replace("{{fee}}", response.lightBoxVariable.fee);
                    this.setState({
                        modalHead: PAYMENT_LANG_OBJ[response.lightBoxTitle],
                        modalBody: attatchFee,
                        isConfirmOrderResponse: false,
                        isError: false
                    }, () => {
                        this.toggleModel(true);
                    });
                }
                else if (response && response.httpStatus) {
                    console.log("response error in promotion api", response);
                    this.setState({
                        modalHead: "Error",
                        modalBody: response.statusMessage,
                        isError: true
                    }, () => {
                        this.toggleModel(true);
                    });
                }
            })
        }

    }

    render() {
        // if (this.props.transactionId) {
        //     return <Redirect to='/sistic/confirmorder/postconfirmation'/>
        // }
        let title = this.props.eventInfo ? this.props.eventInfo.title : '', confirm = false;
        let product = {
            price :  this.props.totalPriceToBePaid,
            description: title
        }
        if(this.state.cardNumber !== "" && this.state.selectedCardType !== "" && this.state.expMonth !== "" && this.state.expYear !== "" && this.state.cvvNumber !== ""  && this.state.nameOnCard !== ""){
            confirm = true;
        } else{
            confirm = false;
        }
        console.log("cardNumber",this.state.cardNumber);
        console.log("selectedCardType",this.state.selectedCardType);

        var cardType = this.state.cardType ;

        // var cardType = [{"creditCardType":null,"code":"AMEX_MBS_SISTIC"},{"creditCardType":null,"code":"MASTER"},{"creditCardType":null,"code":"MASTER_E_VOUCHER"},{"creditCardType":null,"code":"SISTIC_E_VOUCHER"},{"creditCardType":null,"code":"VISA"}];

        

        return (
            <Fragment>
                {/* <form action="/" id="ticket-booking" method="post"> */}
                    <section class="container-fluid delivery-method payment-method pt-4">
                        <div class="container mob-px-0">
                            <h4 class="title-bdr">Select a payment method</h4>
                            <div class="nav nav-pills row">
                                <div class="col-md-3 col-12" onClick={this.fillDetails.bind(this)}>
                                    <a class="nav-item active" id="credit-card">
                                        <div class="radio-check"></div>
                                        <img class="credit-card-icon"
                                             src={process.env.PUBLIC_URL + "/assets/images/credit-card-blue.svg"}
                                             alt="icon"/>
                                        <span class="tag">Credit Card</span>
                                    </a>
                                </div>
                                <div class="col-md-12 credit-card-encl px-0 mb-4 mbl-ccard">
                                    <div class="col-md-9 col-12">
                                        <div class="card-form row">
                                            <div class="rgt-side mt-2">
                                                <p>Accepted Cards</p>
                                                <ul class="accept-card list-inline">
                                                    <li class="list-inline-item"><img class="img-fluid"
                                                        src={process.env.PUBLIC_URL + "/assets/images/master-icon.png"}
                                                        alt="icon" /></li>
                                                    <li class="list-inline-item"><img class="img-fluid"
                                                        src={process.env.PUBLIC_URL + "/assets/images/visa-icon.png"}
                                                        alt="icon" /></li>
                                                    <li class="list-inline-item"><img class="img-fluid"
                                                        src={process.env.PUBLIC_URL + "/assets/images/amex-icon.png"}
                                                        alt="icon" /></li>
                                                </ul>
                                            </div>
                                            {/* responsive starts here */}

                                            <div class="form-group col-md-12">
                                                <label>Card Type</label>
                                                <div className="ng-element el-select-list-wrapper interactive">
                                                    <select class="form-control ng-element el-select-list" id="card-type" value={this.state.selectedCardType} onChange={(e)=>{this.setState({selectedCardType: e.target.value})}}>
                                                        <option value="">-- Select --</option>
                                                        {cardType && cardType.map((item, index) => {
                                                            console.log("code",item.code);
                                                            // Ignore the VOUNCHER from payment methoad TYPE.
                                                            if (item.code !== "SISTIC_E_VOUCHER" && item.code !== "MASTER_E_VOUCHER") {
                                                                var payment_info = PaymentMethod(item.code);
                                                                console.log("payment_info",payment_info);
                                                                return (
                                                                    <option value={payment_info.payment_value}>{payment_info.payment_title}</option>
                                                                )
                                                            }
                                                        })
                                                        }
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label>Card Number</label>
                                                <input type="number" className="form-control card-number" id="card-number" placeholder="0000 - 0000 - 0000 - 0000" value={this.state.cardNumber} onChange={(e)=>this.setState({cardNumber: e.target.value})} />
                                                <img class="input-icon" src={process.env.PUBLIC_URL +"/assets/images/master-small-icon.jpg"} alt="icon" />
                                            </div>
                                            <div class="col-md-6">
                                                <div class="row">
                                                    <div class="form-group col-6">
                                                        <label>Expiry Date</label>
                                                        <select class="form-control ng-element el-select-list" id="card-month" value={this.state.selectedMonth} onChange={(e)=>this.setState({selectedMonth: e.target.value})}>
                                                            {this.state.expMonth.map((item, index) => {
                                                                    return (
                                                                        <option value={item}>{item}</option>
                                                                    )
                                                            })
                                                        }
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-6">
                                                        <label class="screen-reader">Year</label>
                                                        <select class="form-control ng-element el-select-list" id="card-year" value={this.state.selectedYear} onChange={(e)=>this.setState({selectedYear: e.target.value})}>
                                                            {this.state.expYear.map((item, index) => {                                                       
                                                                return (
                                                                    <option value={item}>{item}</option>
                                                                )
                                                            })
                                                            }
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div className="form-group">
                                                    <label>
                                                        CVV/CVC
                                                        <i className="tootltip-icon">
                                                            <img
                                                                src={process.env.PUBLIC_URL + "/assets/images/info-tooltip.svg"}/>
                                                            <span className="cont">Lorem ipsum dolor seit</span>
                                                        </i>
                                                    </label>
                                                    <input type="password" className="form-control cvv" id="cvv-mbl" placeholder=". . ." autocomplete="off" value={this.state.cvvNumber} onChange={(e)=>this.setState({cvvNumber: e.target.value})} />
                                                    <img class="input-icon cvv" src={process.env.PUBLIC_URL +"/assets/images/cvv-icon.svg"} alt="icon" />
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <label>Name on Card</label>
                                                <input type="text" className="form-control" id="cc-name" placeholder="James" value={this.state.nameOnCard} onChange={(e)=>this.setState({nameOnCard: e.target.value})}/>
                                            </div>
                                    
                                            {/* responsive ends here */}
                                        </div>
                                    </div>
                                    <div class="col-12 rgt-side">
                                        <p class="small-cont"><img
                                            src={process.env.PUBLIC_URL + "/assets/images/lock-icon.svg"} alt="icon" />Secure
                                            Credit Card Payment</p>
                                        <p class="small-cont pl">All payment information is encrypted and transmitted
                                            only via a secure SSL connection.</p>
                                    </div>

                                </div>
                                {/* <div class="col-md-3 col-12">
                                    <a class="nav-item">
                                        <div class="radio-check"></div>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/union-pay.png"} alt="icon" />
                                    </a>
                                </div>
                                <div class="col-md-3 col-12">
                                    <a class="nav-item">
                                        <div class="radio-check"> <PayPal product={product} /></div>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/paypal.png"} alt="icon" />
                                    </a>
                                </div>
                                <div class="col-md-3 col-12">
                                    <a class="nav-item">
                                        <div class="radio-check"></div>
                                        <img src={process.env.PUBLIC_URL + "/assets/images/dbs-icon.png"} alt="icon" />
                                    </a>
                                </div> */}
                            </div>
                            <div className="row credit-card-encl mt-5 dkt-ccard">
                                <div className="col-md-9">
                                    <div className="card-form row">
                                        <div class="form-group col-md-12">
                                            <label>Card Type</label>
                                            <div className="ng-element el-select-list-wrapper interactive">
                                                <select class="form-control ng-element el-select-list" id="card-type" value={this.state.selectedCardType} onChange={(e)=>{this.setState({selectedCardType: e.target.value})}}>
                                                    <option value="">-- Select --</option>
                                                    {cardType && cardType.map((item, index) => {
                                                        // Ignore the VOUNCHER from payment methoad TYPE.
                                                        if (item.code !== "SISTIC_E_VOUCHER" && item.code !== "MASTER_E_VOUCHER") {
                                                            var payment_info = PaymentMethod(item.code);
                                                            console.log("payment_info",payment_info);
                                                            return (
                                                                <option value={payment_info.payment_value}>{payment_info.payment_title}</option>
                                                            )
                                                        }
                                                    })
                                                    }
                                                </select>
                                            </div>
                                        </div>
                                        <div className="form-group col-md-12">
                                            <label>Card Number</label>
                                            <input type="number" className="form-control card-number" id="card-number" placeholder="0000 - 0000 - 0000 - 0000" value={this.state.cardNumber} onChange={(e)=>this.setState({cardNumber: e.target.value})} />
                                            <img class="input-icon" src={process.env.PUBLIC_URL +"/assets/images/master-small-icon.jpg"} alt="icon" />
                                        </div>
                                       
                                        <div className="form-group col-md-3">
                                            <label>Expiry Date</label>
                                            <div className="ng-element el-select-list-wrapper interactive">
                                                <select class="form-control ng-element el-select-list" id="card-month" value={this.state.selectedMonth} onChange={(e)=>this.setState({selectedMonth: e.target.value})} >
                                                    {this.state.expMonth.map((item, index) => {
                                                        return (
                                                            <option value={item}>{item}</option>
                                                        )
                                                    })
                                                }
                                                </select>
                                            </div>
                                        </div>
                                        <div style={{position:'relative'}}>
                                            <p style={{fontFamily: 'Rubik', fontSize: '14px', top: '43%', position: 'absolute'}}>/</p>
                                        </div>
                                        <div className="form-group col-md-3">
                                            <label style={{color: 'white'}}>d</label>
                                            <div className="ng-element el-select-list-wrapper interactive">
                                                <select class="form-control ng-element el-select-list" id="card-year" value={this.state.selectedYear} onChange={(e)=>this.setState({selectedYear: e.target.value})}>
                                                    {this.state.expYear.map((item, index) => {                                                       
                                                        return (
                                                            <option value={item}>{item}</option>
                                                        )
                                                    })
                                                    }
                                                </select>
                                            </div>
                                        </div>
                                    
                                        <div className="col-md-6">
                                            <div className="form-group">
                                                <label>
                                                    CVV/CVC
                                                    <i className="tootltip-icon">
                                                        <img
                                                            src={process.env.PUBLIC_URL + "/assets/images/info-tooltip.svg"}/>
                                                        {/* <span className="cont">Lorem ipsum dolor seit</span> */}
                                                    </i>
                                                </label>
                                                <input type="password" className="form-control cvv" id="cvv-mbl" placeholder=". . ." autocomplete="off" value={this.state.cvvNumber} onChange={(e)=>this.setState({cvvNumber: e.target.value})} />
                                                <img class="input-icon cvv" src={process.env.PUBLIC_URL +"/assets/images/cvv-icon.svg"} alt="icon" />
                                            </div>
                                        </div>
                                        <div className="form-group col-md-12">
                                            <label>Name on Card</label>
                                            <input type="text" className="form-control" id="cc-name" placeholder="James" value={this.state.nameOnCard} onChange={(e)=>this.setState({nameOnCard: e.target.value})}/>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3 rgt-side">
                                    <p>Accepted Cards</p>
                                    <ul className="accept-card list-inline">
                                        <li className="list-inline-item">
                                            <img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/master-icon.png"} alt="icon"/></li>
                                        <li className="list-inline-item"><img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/visa-icon.png"} alt="icon"/></li>
                                        <li className="list-inline-item"><img className="img-fluid" src={process.env.PUBLIC_URL + "/assets/images/amex-icon.png"} alt="icon"/></li>
                                    </ul>
                                    <p className="small-cont"><img
                                        src={process.env.PUBLIC_URL + "/assets/images/lock-icon.svg"}
                                        alt="icon"/>Secure Credit Card Payment</p>
                                    <p className="small-cont pl">All payment information is encrypted and transmitted
                                        only via a
                                        secure SSL connection.</p>
                                </div>

                            </div>

                        <div className="row credit-card-encl mt-5 dkt-ccard" style={{ marginBottom: "10%"}}>
                            <div className="col-md-9"></div>
                            <div className="col-md-3 rgt-side">
                                <button className={`btn btn-primary ${confirm === false && 'in-active'}`} onClick={() => this.confirmOrder()}>
                                    Confirm Order
                                    <img className="ml-4" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} />
                                </button>
                            </div>
                        </div>

                        </div>
                    </section>

                <button className={`btn btn-primary proceed ${confirm === false && 'in-active'}`} onClick={() => this.confirmOrder()}>
            Confirm Order
            <img className="ml-3" src={process.env.PUBLIC_URL + "/assets/images/btn-right-arrow.svg"} />
            </button>
                {/* </form> */}

                <Modal show={this.state.isShowModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.toggleModel(false)} centered>
                    <Modal.Header closeButton>
                    <Modal.Title><p className="modal-title">{this.state.modalHead}</p></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <div><span>{this.state.modalBody}</span></div>
                    </Modal.Body>
                    <Modal.Footer>
                        {this.state.isError ?
                            (<Button onClick={()=>this.toggleModel(false)}>Ok</Button>)
                            :
                            (this.state.isConfirmOrderResponse ?
                                (
                                    <Fragment>
                                        <Button onClick={()=>this.confirmOrder()}>Ok</Button>
                                         <Button onClick={()=>this.toggleModel(false)}>Cancel</Button>
                                    </Fragment>
                                ):(
                                    <Fragment>
                                        <Button onClick={()=>this.triggerConfirmPopup()}>Ok</Button>
                                    </Fragment>
                                )
                            )
                        }
                        
                    </Modal.Footer>
                </Modal>


                <Modal show={this.state.validationModel} aria-labelledby="contained-modal-title-vcenter" onHide={()=>this.setState({validationModel: false})} centered>
                    <Modal.Header closeButton>
                    <Modal.Title><p className="modal-title">{this.state.validationTitle}</p></Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    <div style={{textAlign:'center'}}><h6>Please complete the field(s) highlighted in red:</h6></div>
                    <div dangerouslySetInnerHTML={{__html: this.state.validationMessage}} style={{color: "#E83356"}}></div>
                    </Modal.Body>
                    <Modal.Footer>
                    <Button onClick={()=>this.setState({validationModel: false})}>Ok</Button>
                    </Modal.Footer>
                </Modal>

            </Fragment>

        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        totalQuantity: state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        dateChosen: state.dateChosen,
        cartInfo: state.cartInfo,
        update_seat_select: state.update_seat_select,
        insuranceAmount: state.insuranceAmount,
        transactionId: state.transactionId,
        totalPriceToBePaid: state.totalPriceToBePaid,
        firstName: state.firstName,
        lastName: state.lastName,
        chosenAddon: state.chosenAddon,
        chosenAddonPrice: state.chosenAddonPrice,
        chosenAddonMethod: state.chosenAddonMethod,
        userBilingDetails: state.userBilingDetails,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        paymentSuccess: (transactionId, time, date, cardType) => {
            dispatch(actionCreators.paymentSuccess(transactionId, time, date, cardType));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PaymentMethods);
