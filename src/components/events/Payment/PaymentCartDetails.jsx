import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import { SITE_URL, TENANT } from '../../../constants/common-info'
import { Link } from "react-router-dom"
import TimeCounter from './../TimeCounter';
import moment from 'moment';
import * as API from "../../../service/events/eventsInfo"

import he from 'he';
import HTMLParser from 'react-html-parser'

import $ from 'jquery';

class PaymentCartDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cartInfo: [],
        }
    }

    componentDidMount() {

        let cartInfoStr = $("#shoppingCartId").attr("data-lineItems");
        console.log("CartInfo from Delivery as String", cartInfoStr);
        if (cartInfoStr !== "") {
            let cartInfoParsed = JSON.parse(cartInfoStr);
            console.log("CartInfo from Delivery", cartInfoParsed);
            this.setState({
                cartInfo: cartInfoParsed
            })
        }
    }

    viewDetDesk(e) {
        e.preventDefault();
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".total-sec, .ticket-detail-sec, .view_ticket, .close_ticket, .stickyTotal, .stickyTitle, .stickyShopDet").toggleClass('hide');
    }
    viewDetMob(e) {
        e.preventDefault();
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".mob-tic-info").slideToggle();
    }

    // componentDidMount() {
    //     let cartInfoStr = $("#shoppingCartId").attr("data-lineItems");
    //     console.log("CartInfo from Delivery as String", cartInfoStr);
    //     if (cartInfoStr !== "") {
    //         let cartInfoParsed = JSON.parse(cartInfoStr);
    //         console.log("CartInfo from Delivery", cartInfoParsed);
    //         this.setState({
    //             cartInfo: cartInfoParsed
    //         })
    //     }
    // }

    render() {

        let cart_list = this.state.cartInfo;

        // let cart_list = [{"type":"PRODUCT","quantity":2,"unitPrice":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":150,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":150},"subTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":308,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":308},"isGroupBookingLineItem":false,"pkgReqRefNo":null,"pkgReqId":null,"pkgProductType":null,"showSpecialRequest":null,"hideDisplay":null,"cartItemId":"d7afe86d-ca08-4940-9b7a-09f22075a052","isMembership":false,"icc":"","product":{"productId":1141352,"productName":"Patrick The Musical","productDate":"2020-12-26T19:00:00+08:00","productEndDate":"2020-12-26T20:00:00+08:00","level":"STALLS","section":"STALLS LEFT","row":"M","seatNo":[38,39],"productType":"RS","venue":"Sands Theatre at Marina Bay Sands","altDesc":null,"gaSeqNumber":null},"priceclass":{"priceClassName":"Standard","priceClassCode":"A"},"pricecatid":85041,"bookingFee":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":4,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":4},"isIdRedemption":false,"isHidePrice":false,"promoterNameList":["Marina Bay Sands"],"isEvoucherEvent":false,"isEventWithSurvey":false,"productCategory":"Event","liveStreamEnable":false,"isShowEndTime":false}]


        // if(cart_list.length > 0){
        //     console.log('cart_list', cart_list[0].);
        // }
        console.log('cart_list_data', cart_list);

        let title = this.props.eventInfo ? this.props.eventInfo.title : '';
        let venue = this.props.eventInfo ? this.props.eventInfo.venue : undefined;
        let date = this.props.eventInfo ? moment(this.props.eventInfo.updateDate).format("DD MMM YYYY, h:mm a") : undefined;
        let summaryImagePath = this.props.eventInfo ? this.props.eventInfo.summaryImagePath : undefined;
        

        let iccCode = this.props.eventInfo ? this.props.eventInfo.internetContentCode : '';

        title = cart_list.length > 0 && cart_list[0].product.productName;
        if(cart_list.length > 0 && cart_list[0].pkgReqId === null){
            venue = cart_list.length > 0 && cart_list[0].product.venue;
        } else{
            venue = "Various Venues";
        }        

        if (title != "") {
            //console.log('parser123',he.decode(title));
            var decode_title = he.decode(title);
        }

        var product_date = cart_list.length > 0 && cart_list[0].product ? cart_list[0].product.productDate : '', split_book_date, booking_date;

        //console.log('cart_list_product', cart_list.length > 0 && cart_list[0].product);

        if(product_date){
            //console.log('product_date', product_date);
            split_book_date = moment(product_date).format('llll').split(',');
            booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];
        }
        //console.log('booking_date', booking_date);

        var total_price = 0, total_price2 = 0, total_price3 = 0, total_price4 = 0, booking_fee = 0, seat_price_currency = '', ref = this, totalPrice;
        var totalQty = 0, pkgReqId = cart_list.length > 0 && cart_list[0].pkgReqId ? cart_list[0].pkgReqId : null;

        //summaryImagePath = "https://cmsuat.sistic.com.sg/sistic/docroot/sites/default/files/2020-01/TTP-FOP-%281194x556px%29.jpg";

        console.log("upsellCartItems",this.props.upsellCartItems);


        return (
            <Fragment>
                <section className="confirmation_sec mb-5 cart-sec">
                    <div className="container">
                        <div className="confirm_top_sec clearfix">
                            <div className="pull-left w80">
                                <div className="confirm_title">
                                    <h2>Complete your purchase</h2>
                                </div>
                            </div>
                            <div className="pull-right w20">
                                <TimeCounter />
                            </div>
                        </div>

                        <div className="confDetails_sec bdrB_dash lg-none">
                            <div className="confDetail_mbl clearfix">

                                <div className="holder clearfix br_b pt-2">
                                    
                                    { summaryImagePath && (<div className="confDetails_title_img"><img className="img-fluid" src={SITE_URL + summaryImagePath} alt="" /></div>)
                                    }
                                    
                                    <div className={`confDetails_title ${summaryImagePath ? '' : 'full_wdth'}`}>
                                        <b>{HTMLParser(decode_title)}</b>
                                        <p className="mt-1 mb-0">
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" />
                                                {venue}
                                            </span>
                                            <span>
                                            {
                                                pkgReqId === null && (
                                                    <span>
                                                        <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" />
                                                        {booking_date && booking_date}
                                                    </span>
                                                )
                                            } 
                                            </span>
                                        </p>
                                    </div>
                                </div>

                                <div className="mob-tic-info">

                                    {
                                        cart_list.length > 0 && (

                                            cart_list.map((cart_data, index) => {

                                                // total_price += parseInt(cart_data.subTotal.number);
                                                // var seat_price = cart_data.unitPrice.number;

                                                // seat_price_currency = cart_data.unitPrice.currency.currencyCode;

                                                total_price += parseFloat(cart_data.subTotal.number);
                                                var seat_price = cart_data.unitPrice.number;
                                                if(cart_data.priceclass.priceClassName){
                                                    totalQty += cart_data.quantity;
                                                }
                                                seat_price_currency = cart_data.unitPrice.currency.currencyCode;
                                                var priceClassCode = cart_data.priceclass.priceClassCode ? cart_data.priceclass.priceClassCode : '';

                                                var prdType = cart_data.product.productType;
                                                
                                                return (
                                                    <div className="col-12 mb-3 px-0">
                                                        <div className="row title">
                                                            <span className="col-6">{cart_data.priceclass.priceClassName}</span>
                                                            <span className="col-6 text-right">{cart_data.unitPrice.currency.currencyCode} {total_price3}</span>
                                                        </div>
                                                        <div className="row tic-cont">
                                                            <div className="col-4">
                                                                {
                                                                    prdType === "RS" && (
                                                                    <Fragment>
                                                                    <span className="row">
                                                                        <span className="col-4">Level</span>
                                                                        <span className="col-8">{cart_data.product.level}</span>
                                                                    </span>
                                                                    <span className="row">
                                                                        <span className="col-4">Row</span>
                                                                        <span className="col-8">{cart_data.product.row}</span>
                                                                    </span>
                                                                    </Fragment>
                                                                    )
                                                                }
                                                                
                                                                <span className="row">
                                                                    <span className="col-4">Unit Price</span>
                                                                    <span className="col-8">{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</span>
                                                                </span>
                                                            </div>
                                                            <div className="col-8">
                                                                <span className="row">
                                                                    <span className="col-5">Section</span>
                                                                    <span className="col-7 pl-0">{prdType === "RS" ? cart_data.product.section : 'General Admission'}</span>
                                                                </span>
                                                                {
                                                                    prdType === "RS" && (
                                                                    <span className="row">
                                                                        <span className="col-5">Seats(s)</span>
                                                                        <span className="col-7 pl-0">{cart_data.product.seatNo.toString()}</span>
                                                                    </span>
                                                                    )
                                                                }
                                                                <span className="row">
                                                                    <span className="col-5">Quantity</span>
                                                                    <span className="col-7 pl-0">{cart_data.quantity}</span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                )
                                                
                                            })

                                        )
                                        
                                    }
                                </div>
                                
                                <div className="mbl_invoice">
                                 
                                    <div className="ticket_devide">
                                        <div className="left_circle"></div>
                                        <div className="right_circle"></div>
                                        <div className="dot_line"></div>
                                    </div>

                                    <div className="holder">
                                        {
                                            cart_list && cart_list.bookingFee && (
                                                <div className="blk inner_info2 clearfix">
                                                    <span className="pull-left">Booking Fee</span>
                                                    <span className="pull-right">{cart_list.bookingFee.currency && cart_list.bookingFee.currency.currencyCode} {(cart_list.bookingFee.number * totalQty).toFixed(2)}</span>
                                                    <span className="pull-right pr-5">{seat_price_currency} {cart_list.bookingFee.number.toFixed(2)} x{totalQty}</span>
                                                </div>
                                            )
                                        }
                                        
                                        {this.props.crossSellName !== "" &&
                                            <div className="blk inner_info2 clearfix">
                                                <span className="pull-left">{this.props.crossSellName}</span>
                                                <span className="pull-right">{seat_price_currency} {this.props.crossSellAmt.toFixed(2)}</span>
                                            </div>
                                        }

                                        {
                                            this.props.insuranceAmount > 0 && (
                                                <div className="blk inner_info2 clearfix">
                                                    <span className="pull-left">Tickets Insured</span>

                                            <span className="pull-right">{seat_price_currency} {this.props.insuranceAmount}</span>
                                                </div>
                                            )
                                        }
                                        <div className="blk inner_info2 clearfix">
                                            <span className="pull-left">{this.props.chosenAddon}</span>

                                            <span className="pull-right">{seat_price_currency} {this.props.chosenAddonPrice}</span>
                                        </div>
                                        <div className="blk inner_info2 clearfix">
                                            <span className="pull-left">e-Voucher</span>

                                            <span className="pull-right">SGD 0.00</span>
                                        </div>
                                        <div className="have-voucher col-12 px-0">
                                            <button className="btn btn-outline">Have an e-Voucher?</button>
                                            <div className="apply-voucher">
                                                <label>e-Voucher</label>
                                                <div className="form-group">
                                                    <input placeholder="Enter e-Voucher" type="text" name="voucher" className="form-control" />
                                                    <a href="javascript:;" className="add">
                                                        <img src={process.env.PUBLIC_URL + "/assets/images/plus-blue.svg"} alt="icon" />
                                                    </a>
                                                </div>
                                                <button className="btn btn-primary">Apply e-Voucher</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                
                        <div className="confDetails_footer_mbl">
                            <div className="w50 pull-left">
                                <b>Total</b>
                            </div>
                            <div className="w50 pull-right text-right">
                                <b>{seat_price_currency} {this.props.chosenAddonPrice !== "" ? (total_price + parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price + this.props.crossSellAmt
                                ).toFixed(2)}</b>
                            </div>
                        </div>

                        <div className="confDetails_body d-none">
                            {
                                summaryImagePath && (
                                    <div className="media-img">
                                        <img className="img-fluid" src={SITE_URL + summaryImagePath} alt="" />
                                    </div>
                                )
                            }
                            
                            <div className={`confDetails_sec ${summaryImagePath ? '' : 'npad_lft'}`}>

                                <div className="confDetail_mbl">
                                    <div className="confDetails_title">
                                        <b>{HTMLParser(decode_title)}</b>
                                        <p className="mt-1">
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" /> {venue}
                                            </span>
                                            {
                                                pkgReqId === null && (
                                                    <span>
                                                        <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" /> {booking_date && booking_date}
                                                    </span>
                                                )
                                            }
                                        </p>
                                    </div>
                                </div>


                                <div className="confDetails_title">
                                    <b>{HTMLParser(decode_title)}</b>
                                    <p className="mt-1">
                                        <span className="mr-3">
                                            <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" /> {venue}
                                        </span>
                                        {
                                            pkgReqId === null && (
                                                <span>
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" /> {booking_date && booking_date}
                                                </span>
                                            )
                                        }
                                        
                                    </p>
                                </div>
                                <div className="confDetail_tbl">
                                    <table className="table">
                                        <tbody>
                                            {
                                                pkgReqId !== null && (
                                                    <tr className="pkg_incl"><td colSpan={8}>Package includes admission to :</td></tr>
                                                )
                                            }
                                            {
                                                cart_list.length > 0 && (

                                                    cart_list.map((cart_data, index) => {

                                                        total_price2 += parseFloat(cart_data.subTotal.number);
                                                        var seat_price = cart_data.unitPrice.number;
                                                        // if(cart_data.priceclass.priceClassName){
                                                        //     totalQty += cart_data.quantity;
                                                        // }                                                        
                                                        seat_price_currency = cart_data.unitPrice.currency.currencyCode;
                                                        var priceClassCode = cart_data.priceclass.priceClassCode ? cart_data.priceclass.priceClassCode : '';

                                                        var prdType = cart_data.product.productType;
                                                        var pkgReqId = cart_data.pkgReqId;

                                                        var colSpanVal = prdType === "RS" ? 8 : 5; 

                                                        var product_date = cart_data.product.productDate;

                                                        var split_book_date = moment(product_date).format('llll').split(',');
                                                        var booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];

                                                        booking_fee += parseFloat(cart_data.bookingFee && cart_data.bookingFee.number);

                                                        return (
                                                        <Fragment>
                                                            {
                                                                pkgReqId !== null && (
                                                                    <tr className="pkg_prd_name">
                                                                        <td width="2%">{parseInt(index) + 1}.</td>
                                                                        <td width="98%" colSpan={colSpanVal}>{cart_data.product.productName}<br/><span>{cart_data.product.venue},  {booking_date}</span></td>
                                                                    </tr>
                                                                )
                                                            }
                                                            
                                                            <tr>
                                                                {
                                                                    pkgReqId === null ? (
                                                                        <td width="5%"><span>{parseInt(index) + 1}.</span></td>
                                                                    ) : (
                                                                        <td width="5%">&nbsp;</td>
                                                                    )
                                                                }
                                                                <td width="15%"><span>Type</span><p>{cart_data.priceclass.priceClassName}</p></td>
                                                                {
                                                                    prdType === "RS" && (
                                                                    <td width="10%"><span>level</span><p>{cart_data.product.level}</p></td>
                                                                    )
                                                                }
                                                                <td width="15%"><span>Section</span><p>{prdType === "RS" ? cart_data.product.section : 'General Admission'}</p></td>
                                                                {
                                                                    prdType === "RS" && (
                                                                    <Fragment>
                                                                        <td width="5%"><span>Row</span><p>{cart_data.product.row}</p></td>
                                                                        <td width="10%"><span>Seat(s)</span><p>{cart_data.product.seatNo.toString()}</p></td>
                                                                    </Fragment>
                                                                    )
                                                                }
                                                                <td width="15%"><span>Unit Price</span><p>{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</p></td>
                                                                <td width="7%"><span>Quantity</span><p>{cart_data.quantity}</p></td>
                                                                <td width="13%">
                                                                    <b><br />{cart_data.unitPrice.currency.currencyCode} {(seat_price * cart_data.quantity).toFixed(2)}</b>
                                                                </td>
                                                            </tr>
                                                        </Fragment>
                                                        )
                                                    })
                                                )
                                            }

                                            {this.props.upsellCartItems && this.props.upsellCartItems.length > 0 && (
                                            <tr>
                                                <td width="100%" colSpan={5}><img className="addOnsImg" src={process.env.PUBLIC_URL + "/assets/images/addOns.svg"} alt="icon" /> <span className="addOnsTitle">Add-Ons</span></td>
                                                {/* <td width="15%"><span className="addOnsTitle">Add-Ons</span></td> */}
                                            </tr>
                                            )}

                                        </tbody>
                                    </table>

                                    {/* Add-Ons */}
                                    
                                    {this.props.upsellCartItems && this.props.upsellCartItems.length > 0 && (
                                        <div style={{ backgroundColor: '#fff', padding: '5px' }}>
                                        <table className="upSellTable">
                                            <tbody>
                                                {/* Mapped Added cart items */}
                                                {this.props.upsellCartItems.map((upSellcart, index) => {
                                                    total_price2 += parseFloat(upSellcart.amount.replace("SGD", ""));
                                                    return (
                                                        <tr>
                                                            <td width="5%">&nbsp;</td>
                                                            <td width="50%">
                                                                <span className="upSellItemName">
                                                                    {upSellcart.title}
                                                                </span>
                                                                <br />
                                                                <td>
                                                                    <span>Type</span>
                                                                    <p>{upSellcart.type}</p>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <span>Level</span>
                                                                    <p>{upSellcart.level}</p>
                                                                </td>
                                                            </td>
    
                                                            <td width="15%">
                                                                <span>Unit Price</span>
                                                                <p>{upSellcart.currencyCode} {upSellcart.amount}</p>
                                                            </td>
                                                            <td width="16%">
                                                                <span>Quantity</span>
                                                                <p>{upSellcart.qty}</p>
                                                            </td>
                                                            <td width="20%" className="">
                                                                <b><br />{upSellcart.currencyCode} {(upSellcart.amount * upSellcart.qty).toFixed(2)}</b>
                                                            </td>

                                                        </tr>
                                                    )
                                                })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                    )}
                                </div>

                            </div>
                            <div className="ticket_devide">
                                <div className="left_circle"></div>
                                <div className="right_circle"></div>
                                <div className="dot_line"></div>
                            </div>

                            <div className={`confDetails_sec bdrB_dash pt-0 ${summaryImagePath ? '' : 'npad_lft'}`}>
                                <div className="confDetail_tbl">
                                    
                                    <table className="table">                                        
                                        <tbody>
                                            {
                                                booking_fee > 0 && (
                                                    <tr>
                                                        <td width="5%">&nbsp;</td>
                                                        <td width="50%"><span>Booking Fee</span></td>
                                                        <td width="15%"><span>{cart_list[0].bookingFee.currency && cart_list[0].bookingFee.currency.currencyCode} {booking_fee.toFixed(2)}</span></td>
                                                        <td width="17%"><span>x {totalQty} tickets</span></td>
                                                        <td width="13%"><span>{cart_list[0].bookingFee.currency && cart_list[0].bookingFee.currency.currencyCode} {(booking_fee * totalQty).toFixed(2)}</span></td>
                                                        <td width="10%"></td>
                                                    </tr>
                                                )
                                            }
                                            
                                            {this.props.crossSellName !== "" &&
                                                <tr>
                                                    <td width="5%">&nbsp;</td>
                                                    <td width="50%"><span>{this.props.crossSellName}</span></td>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="17%">&nbsp;</td>
                                                    <td width="13%"><span>SGD {this.props.crossSellAmt.toFixed(2)}</span></td>
                                                    <td width="10%">&nbsp;</td>
                                                </tr>
                                            }
                                            {
                                                this.props.insuranceAmount > 0 && (
                                                <tr>
                                                    <td width="5%">&nbsp;</td>
                                                    <td width="50%"><span>Tickets Insured</span></td>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="17%">&nbsp;</td>
                                                    <td width="13%"><span>{seat_price_currency + " " + this.props.insuranceAmount}</span></td>
                                                    <td width="10%"></td>
                                                </tr>
                                                )
                                            }
                                            
                                            <tr>
                                                <td width="5%">&nbsp;</td>
                                                <td width="50%"><span>{this.props.chosenAddon}</span></td>
                                                <td width="15%">&nbsp;</td>
                                                <td width="17%">&nbsp;</td>
                                                <td width="13%"><span>{this.props.chosenAddonPrice !== "" && (seat_price_currency + " " + this.props.chosenAddonPrice.replace("SGD", ""))}</span></td>
                                                <td width="10%"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div className={`have-voucher col-md-12 col-12 no_pad`}>
                                        <button className="btn btn-outline">Have an e-Voucher?</button>
                                        <div className="apply-voucher">
                                            <label>e-Voucher</label>
                                            <div className="form-group">
                                                <input placeholder="Enter e-Voucher" type="text" name="voucher" className="form-control" />
                                                <a href="javascript:;" className="add">
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/plus-blue.svg"} alt="icon" />
                                                </a>
                                            </div>
                                            <button className="btn btn-primary">Apply e-Voucher</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className={`confDetails_footer ${summaryImagePath ? '' : 'npad_lft'}`}>
                                <div className="pull-left w81">
                                    <b>Total</b>
                                </div>

                                <div className="pull-right w15">
                                    <b>{seat_price_currency} {this.props.chosenAddonPrice !== "" ? (total_price2 + parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price2 + this.props.crossSellAmt
                                    ).toFixed(2)}</b>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>


                <section className="show-info seats float-wrap">
                    <div className="container">
                        <div className="row">
                            <div className="media col-md-11 col-12">
                                <div className="media-body">
                                    <span className="stickyShopDet hide">
                                        <h5 className="mt-0 ft">{HTMLParser(decode_title)}</h5>
                                        <p className="ft-1">                                            
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" />
                                                <span>{venue}</span>
                                            </span>
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/ticket-icon.svg"} alt="icon" /> {cart_list && cart_list.length > 0 && cart_list.length} Ticket{cart_list &&  cart_list.length > 1 ? 's' : ''}
                                            </span>
                                            <span>
                                                <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" /> {booking_date && booking_date}
                                            </span>                                            
                                        </p>
                                    </span>
                                    <span className="stickyTitle">
                                        <h5 className="mt-0 ft float-left">Complete your purchase</h5>
                                        <p className="ft-1">
                                            {
                                                total_price2 > 0 && (
                                                    <div className="stickyTotal"><h5 className="mt-0 ft">Total {seat_price_currency} {this.props.chosenAddonPrice !== "" ? (total_price2 + parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price2 + this.props.crossSellAmt).toFixed(2)}</h5></div>
                                                )
                                            }
                                        </p>
                                    </span>
                                    <div className="ticket-detail-sec hide">
                                        <table className="table">
                                            <tbody>
                                                {
                                                    pkgReqId !== null && (
                                                        <tr className="pkg_incl"><td colSpan={8}>Package includes admission to :</td></tr>
                                                    )
                                                }
                                                
                                                {
                                                    cart_list.length > 0 && (
                                                        cart_list.map((cart_data, index) => {
                                                            total_price3 += parseFloat(cart_data.subTotal.number);
                                                            var seat_price = cart_data.unitPrice.number;
                                                            seat_price_currency = cart_data.unitPrice.currency.currencyCode;
                                                            var priceClassCode = cart_data.priceclass.priceClassCode ? cart_data.priceclass.priceClassCode : '';

                                                            var prdType = cart_data.product.productType;
                                                            var pkgReqId = cart_data.pkgReqId;

                                                            var colSpanVal = prdType === "RS" ? 8 : 5; 

                                                            var product_date = cart_data.product.productDate;

                                                            var split_book_date = moment(product_date).format('llll').split(',');
                                                            var booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];

                                                            booking_fee += parseFloat(cart_data.bookingFee && cart_data.bookingFee.number);

                                                            return (
                                                            <Fragment>
                                                                {
                                                                    pkgReqId !== null && (
                                                                        <tr className="pkg_prd_name">
                                                                            <td>{parseInt(index) + 1}.</td>
                                                                            <td colSpan={colSpanVal}>{cart_data.product.productName}<br/><span>{cart_data.product.venue},  {booking_date}</span></td>
                                                                        </tr>
                                                                    )
                                                                }
                                                                <tr>
                                                                    {
                                                                        pkgReqId === null ? (
                                                                            <td><p>{parseInt(index) + 1}.</p></td>
                                                                        ) : (
                                                                            <td width="2%">&nbsp;</td>
                                                                        )
                                                                    }
                                                                    
                                                                    <td width="10%"><p>Type</p><span>{cart_data.priceclass.priceClassName}</span></td>
                                                                    {
                                                                        prdType === "RS" && (
                                                                            <td width="10%"><p>Level</p><span>{cart_data.product.level}</span></td>
                                                                        )
                                                                    }    
                                                                    <td width="15%"><p>Section</p><span>{prdType === "RS" ? cart_data.product.section : 'General Admission'}</span></td>
                                                                    {
                                                                        prdType === "RS" && (
                                                                        <Fragment>
                                                                        <td width="10%"><p>Row</p><span>{cart_data.product.row}</span></td>
                                                                        <td width="10%"><p>Seat(s)</p><span>{cart_data.product.seatNo.toString()}</span></td>
                                                                        </Fragment>
                                                                        )
                                                                    }
                                                                    <td width="15%"><p>Unit Price</p><span>{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</span></td>
                                                                    <td width="10%"><p>Quantity</p><span>{cart_data.quantity}</span></td>
                                                                    <td width="24%" className="shopcartEdit">
                                                                        <span>{cart_data.unitPrice.currency.currencyCode} {(seat_price * cart_data.quantity).toFixed(2)}</span>            
                                                                    </td>
                                                                </tr>
                                                            </Fragment>
                                                            )
                                                        })
                                                    )
                                                }
                                                </tbody>
                                            </table>

                                            <table className="table">
                                                <tbody>
                                                {/* Add-Ons dropdown view */}
                                                    <tr>
                                                        <td colSpan={9}><span style={{ paddingTop: '4px' }} className="addOnsTitle"><img className="addOnsImg" src={process.env.PUBLIC_URL + "/assets/images/addOns.svg"} alt="icon" /> Add-Ons</span></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                            {
                                                this.props.upsellCartItems && this.props.upsellCartItems.length > 0 && (
                                                
                                                <table className="table upCellTbl">
                                                    <tbody>
                                                    {/* Mapped Added cart items */}
                                                    {this.props.upsellCartItems.map((upSellcart, index) => {
                                                        total_price3 += parseFloat(upSellcart.amount.replace("SGD", ""));
                                                        return (
                                                            <tr>
                                                                <td width="5%">&nbsp;</td>
                                                                <td width="60%" colSpan={5}>
                                                                    <span className="upSellItemName">
                                                                        {upSellcart.title}
                                                                    </span>
                                                                    <br />
                                                                    <td>
                                                                        <p>Type</p>
                                                                        <span>{upSellcart.type}</span>
                                                                    </td>
                                                                    <td>&nbsp;</td>
                                                                    <td>
                                                                        <p>Level</p>
                                                                        <span>{upSellcart.level}</span>
                                                                    </td>
                                                                </td>
                                                                <td width="15%">
                                                                    <p>Unit Price</p>
                                                                    <span>{upSellcart.amount}</span>
                                                                </td>
                                                                <td width="20%">
                                                                    <span><b>{upSellcart.currencyCode} {(upSellcart.amount * upSellcart.qty).toFixed(2)}</b></span>
                                                                </td>
                                                            </tr>
                                                        )
                                                    })
                                                    }
                                                </tbody>
                                            </table>
                                            )
                                        }

                                        <table className="table booking_fees">
                                            <tbody>
                                                {
                                                    booking_fee > 0 && (
                                                        <tr className="tfoot">
                                                            <td width="5%">&nbsp;</td>
                                                            <td width="15%" colSpan={4}><span>Booking Fee</span></td>
                                                            <td width="15%"><span>{cart_list[0].bookingFee.currency && cart_list[0].bookingFee.currency.currencyCode} {booking_fee.toFixed(2)}</span></td>
                                                            <td width="10%"><span>x {totalQty} tickets</span></td>
                                                            <td width="4%">&nbsp;</td>
                                                            <td width="16%"><span>{cart_list[0].bookingFee.currency && cart_list[0].bookingFee.currency.currencyCode} {(booking_fee * totalQty).toFixed(2)}</span></td>
                                                        </tr>
                                                    )
                                                }
                                                
                                                {this.props.crossSellName !== "" &&
                                                    <tr>
                                                        <td width="5%">&nbsp;</td>
                                                        <td width="15%" colSpan={5}><span>{this.props.crossSellName}</span></td>
                                                        <td width="15%"><span></span></td>
                                                        <td width="10%"><span></span></td>
                                                        <td width="10%" style={{ textAlign: 'right' }}><span>SGD {this.props.crossSellAmt.toFixed(2)}</span>
                                                            <a href="javascript:;" className="trash-icon" onClick={() => this.deleteCrossSell()}>
                                                                <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                }
                                                {
                                                    this.props.insuranceAmount > 0 && (
                                                        <tr>
                                                            <td width="5%">&nbsp;</td>
                                                            <td width="15%"><span>Tickets Insured</span></td>
                                                            <td width="1%0">&nbsp;</td>
                                                            <td width="15%">&nbsp;</td>
                                                            <td width="5%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="15%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="10%"><span>{seat_price_currency + " " + this.props.insuranceAmount}</span></td>
                                                        </tr>
                                                    )
                                                }
                                                
                                                <tr>
                                                    <td width="5%">&nbsp;</td>
                                                    <td width="15%"><span>{this.props.chosenAddon}</span></td>
                                                    <td width="1%0">&nbsp;</td>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="5%">&nbsp;</td>
                                                    <td width="10%">&nbsp;</td>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="10%">&nbsp;</td>
                                                    <td width="10%"><span>{this.props.chosenAddonPrice !== "" && (seat_price_currency + " " + this.props.chosenAddonPrice.replace("SGD", ""))}</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        {/* <a href="javascript:;" className="expand-detail">Close Details</a> */}
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-1">
                                <TimeCounter />
                            </div>
                            <div className="mob-tic-info d-sm-block d-md-none">
                                <hr />

                                {
                                    cart_list.length > 0 && (


                                        cart_list.map((cart_data, index) => {

                                            // var seat_price = cart_data.unitPrice.number;

                                            // seat_price_currency = cart_data.unitPrice.currency.currencyCode;

                                            total_price4 += parseFloat(cart_data.subTotal.number);
                                            var seat_price = cart_data.unitPrice.number;
                                            // totalQty += cart_data.quantity;
                                            seat_price_currency = cart_data.unitPrice.currency.currencyCode;
                                            var priceClassCode = cart_data.priceclass.priceClassCode ? cart_data.priceclass.priceClassCode : '';

                                            var prdType = cart_data.product.productType;

                                            booking_fee += parseFloat(cart_data.bookingFee && cart_data.bookingFee.number);

                                            return (
                                                <div className="col-12 mb-3 px-0">
                                                    <div className="row title">
                                                        <span className="col-6">{cart_data.priceclass.priceClassName}</span>
                                                        <span className="col-6 text-right">{seat_price_currency} {total_price4}</span>
                                                    </div>
                                                    <div className="row tic-cont">
                                                        <div className="col-4">
                                                            {
                                                                prdType === "RS" && (
                                                                <Fragment>
                                                                <span className="row">
                                                                    <span className="col-4">Level</span>
                                                                    <span className="col-8">{cart_data.product.level}</span>
                                                                </span>
                                                                <span className="row">
                                                                    <span className="col-4">Row</span>
                                                                    <span className="col-8">{cart_data.product.row}</span>
                                                                </span>
                                                                </Fragment>
                                                                )
                                                            }                                                            
                                                            <span className="row">
                                                                <span className="col-4">Unit Price</span>
                                                                <span className="col-8">{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</span>
                                                            </span>
                                                        </div>
                                                        <div className="col-8">
                                                            <span className="row">
                                                                <span className="col-5">Section</span>
                                                                <span className="col-7 pl-0">{prdType === "RS" ? cart_data.product.section : 'General Admission'}</span>
                                                            </span>
                                                            {
                                                                prdType === "RS" && (
                                                                <span className="row">
                                                                    <span className="col-5">Seats(s)</span>
                                                                    <span className="col-7 pl-0">{cart_data.product.seatNo.toString()}</span>
                                                                </span>
                                                                )
                                                            }
                                                            <span className="row">
                                                                <span className="col-5">Quantity</span>
                                                                <span className="col-7 pl-0">{cart_data.quantity}</span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            )
                                        })

                                    )}

                                <div className="ticket_devide bg-transparent">
                                    <div className="dot_line"></div>
                                </div>
                                <div className="holder pt-0">
                                    {
                                        booking_fee > 0 && (
                                            <div className="blk inner_info2 clearfix">
                                                <span className="pull-left">Booking Fee</span>
                                                <span className="pull-right">{cart_list[0].bookingFee.currency.currencyCode} {({booking_fee} * totalQty).toFixed(2)}</span>
                                                <span className="pull-right pr-5">{cart_list[0].bookingFee.currency.currencyCode} {booking_fee.toFixed(2)} x {totalQty}</span>
                                            </div>
                                        )
                                    }
                                    
                                    {this.props.crossSellName !== "" &&
                                        <div className="blk inner_info2 clearfix">
                                            <span className="pull-left">{this.props.crossSellName}</span>
                                            <span className="pull-right"><a href="javascript:;" className="trash-icon" onClick={() => this.deleteCrossSell()}>
                                                <img src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"} alt="icon" />
                                            </a></span>
                                            <span className="pull-right">{seat_price_currency} {this.props.crossSellAmt.toFixed(2)}</span>
                                        </div>
                                    }

                                    {
                                        this.props.insuranceAmount > 0 && (
                                            <div className="blk inner_info2 clearfix">
                                                <span className="pull-left">Tickets Insured</span>
                                                <span className="pull-right">{seat_price_currency} {this.props.insuranceAmount}</span>
                                            </div>
                                        )
                                    }

                                    
                                    <div className="blk inner_info2 clearfix">
                                        <span className="pull-left">{this.props.chosenAddon}</span>

                                        <span className="pull-right">{seat_price_currency} {this.props.chosenAddonPrice}</span>
                                    </div>
                                    <div className="blk inner_info2 clearfix">
                                        <span className="pull-left">e-Voucher</span>

                                        <span className="pull-right">SGD 10.00</span>
                                    </div>
                                    <div className="have-voucher col-12 px-0">
                                        <button className="btn btn-outline">Have an e-Voucher?</button>
                                        <div className="apply-voucher">
                                            <label>e-Voucher</label>
                                            <div className="form-group">
                                                <input placeholder="Enter e-Voucher" type="text" name="voucher" className="form-control" />
                                                <a href="javascript:;" className="add">
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/plus-blue.svg"} alt="icon" />
                                                </a>
                                            </div>
                                            <button className="btn btn-primary">Apply e-Voucher</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div className="total-sec col-12 mob hide">
                        <div className="container">
                            <div className="row align-items-center">
                                <div className="col-md-9 col-sm-6 col-xs-6  booking-fee">
                                    <h6>Total</h6>
                                </div>
                                <div className="col-md-3 col-sm-6 col-xs-6 check-out-sec sticky">
                                    <h6>{seat_price_currency} {this.props.chosenAddonPrice !== "" ? (parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + total_price3 + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price3 + this.props.crossSellAmt).toFixed(2)}</h6>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="view-detail d-md-block d-sm-none">
                        <button className="btn btn-view" onClick={this.viewDetDesk.bind(this)}>
                            <span className="view_ticket">View Ticket Details</span>
                            <span className="close_ticket hide">Close Ticket Details</span>
                            <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon" />
                        </button>
                    </div>

                    <div className="view-detail d-sm-block d-md-none">
                        <button className="btn btn-view" onClick={this.viewDetMob.bind(this)}>
                            <span>View Ticket Details</span>
                            <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon" />
                        </button>
                    </div>
                </section>

            </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    //console.log('cartInfo', state.cartInfo)
    return {
        eventInfo: state.showInfo.eventInfo,
        totalQuantity: state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        dateChosen: state.dateChosen,
        cartInfo: state.cartInfo,
        mergedCartInfo: state.mergedCartInfo,
        checkout_btn_enable: state.checkout_btn_enable,
        update_seat_select: state.update_seat_select,
        chosenAddon: state.chosenAddon,
        chosenAddonPrice: state.chosenAddonPrice,
        insuranceAmount: state.insuranceAmount,
        selected_adult_qty: state.selected_adult_qty,
        selected_child_qty: state.selected_child_qty,
        selected_nsf_qty: state.selected_nsf_qty,
        selected_sectz_qty: state.selected_sectz_qty,
        timeChosen: state.timeChosen,
        crossSellName: state.crossSellName,
        crossSellAmt: state.crossSellAmt,
        upsellCartItems: state.upsellCartItems,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PaymentCartDetails);