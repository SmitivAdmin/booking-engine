import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import {SITE_URL} from '../../../constants/common-info'
import he from "he";
import HTMLParser from "react-html-parser";
// import * as jsPDF from 'jspdf'
import $ from 'jquery'
import html2canvas from 'html2canvas';
import moment from 'moment';

class BookingReference extends Component {
    constructor(props){
        super(props);
    }
    printpage(){
        window.print()
    }
    downloadReference(e){
        window.scrollTo(0,0);
        var elem =document.querySelector("#ticketBill")

        html2canvas(document.querySelector("#ticketBill")).then(canvas => {
            // document.body.appendChild(canvas)
            var image = canvas.toDataURL("image/png", 1.0).replace("image/png", "image/octet-stream"); // here is the most important part because if you dont replace you will get a DOM 18 exception.

            window.location.href=image;
        });
        window.scrollTo(0, document.body.scrollHeight || document.documentElement.scrollHeight);
    }

    render(){
        let title = this.props.eventInfo ? this.props.eventInfo.title : '', cart_list;
        let venue =  this.props.eventInfo? this.props.eventInfo.venue: undefined;
        let summaryImagePath = this.props.eventInfo?this.props.eventInfo.summaryImagePath: undefined;
        let  decodedTitle;
        if(title != ""){
            //console.log('parser123',he.decode(title));
           decodedTitle = he.decode(title);
        }
        //cart_list = this.state.cartInfo;
        let confirmation_transOrder = this.props.confirmation_transOrder;

        // confirmation_transOrder = {"patronType":"I","emailAddress":"sasi@rgmobiles.com","shoppingCartModel":{"lineItemList":[{"type":"PRODUCT","quantity":2,"unitPrice":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":100,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":100},"subTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":208,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":208},"isGroupBookingLineItem":false,"pkgReqRefNo":null,"pkgReqId":null,"pkgProductType":null,"showSpecialRequest":null,"hideDisplay":null,"cartItemId":"13c6ad22-52a0-4be7-9f46-b1aabc19c55a","isMembership":false,"icc":"","product":{"productId":1141352,"productName":"Patrick The Musical","productDate":"2020-12-26T19:00:00+08:00","productEndDate":"2020-12-26T20:00:00+08:00","level":"DRESS CIRCLE","section":"DRESS CIRCLE","row":"C","seatNo":[36,37],"productType":"RS","venue":"Sands Theatre at Marina Bay Sands","altDesc":null,"gaSeqNumber":null},"priceclass":{"priceClassName":"Standard","priceClassCode":"A"},"pricecatid":85050,"bookingFee":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":8,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":8},"isIdRedemption":false,"isHidePrice":false,"promoterNameList":["Marina Bay Sands"],"isEvoucherEvent":false,"isEventWithSurvey":false,"productCategory":"Event","liveStreamEnable":false,"isShowEndTime":false}],"feeLineItems":[{"type":"DELIVERY","quantity":1,"unitPrice":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"subTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"isGroupBookingLineItem":null,"pkgReqRefNo":null,"pkgReqId":null,"pkgProductType":null,"showSpecialRequest":null,"hideDisplay":null,"code":"MAIL","description":"Thank you for choosing Mastercard Pickup. The SGD 0.50 delivery fee for Collection from Singapore Authorised Agents has been waived."}],"addOnLineItems":[],"packageList":null,"lineItemTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":208,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":208},"ticketProtectorAmount":null,"commonPaymentMethod":null,"commonDeliveryMethod":{"deliveryMethodList":[{"code":"E_TICKET","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"order":1,"addressRequired":false,"feeWaived":false},{"code":"MBS_BOX","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"order":2,"addressRequired":false,"feeWaived":false},{"code":"OUTLET_PICKUP","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":0.5},"order":2,"addressRequired":false,"feeWaived":false},{"code":"MAIL","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"order":2,"addressRequired":true,"feeWaived":false},{"code":"REGISTERED_MAIL","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":3,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":3},"order":3,"addressRequired":true,"feeWaived":false},{"code":"COURIER","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":15,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":15},"order":4,"addressRequired":true,"feeWaived":false}]},"currencyUnit":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"membershipBenefit":null,"membershipRedemption":null,"membershipProfileConfig":null,"showSpecialRequestTextarea":null,"transactionExtInfo":null,"idRedemptionTickets":null},"transactionRefNumber":"20200921-000096","firstName":"Sasi","lastName":"Kumar","purchaseDate":"2020-09-21T21:24:27+08:00","acctNum":"5694358","deliveryMethod":{"code":"MAIL","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"order":2,"addressRequired":true,"feeWaived":false},"transactionOrderPaymentDetailsList":[{"paymentType":"AMEX_MBS_SISTIC","subAmount":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":209.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":209.5}}],"totalPayment":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":209.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":209.5},"groupBookingModel":{"expiryDate":"2020-09-22T21:24:28.582+08:00","groupBookingLink":"https://v4be.stixcloudtest.com/sistic/booking/?groupBookingCode=30357f9-c9e0-46c7-97a5-a708e51580","lineItemList":[{"type":"GROUPBOOKING","quantity":1,"unitPrice":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"subTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"isGroupBookingLineItem":true,"pkgReqRefNo":null,"pkgReqId":null,"pkgProductType":null,"showSpecialRequest":null,"hideDisplay":null,"product":{"productId":1141352,"productName":"Patrick The Musical","productDate":"2020-12-26T19:00:00+08:00","productEndDate":"2020-12-26T20:00:00+08:00","level":"DRESS CIRCLE","section":"DRESS CIRCLE","row":"C","seatNo":[38],"productType":"RS","venue":"Sands Theatre at Marina Bay Sands","altDesc":null,"gaSeqNumber":null}}]},"isPackage":false,"idRedemptionCodes":null,"friendsEmail":null}

        console.log('confirmation_transOrder_ref', confirmation_transOrder);

        cart_list = confirmation_transOrder && confirmation_transOrder.shoppingCartModel && confirmation_transOrder.shoppingCartModel.lineItemList;

        console.log('cart_list_book_ref', cart_list);

        var total_price = 0, total_price2 = 0, total_price3 = 0, total_price4 = 0, seat_price_currency = '', ref=this, totalPrice;
        
        var purchase_time = confirmation_transOrder.purchaseDate && moment(confirmation_transOrder.purchaseDate.substring(11, 16), ["HH:mm"]).format("hh:mm A");
        var purchase_date = confirmation_transOrder.purchaseDate && moment(confirmation_transOrder.purchaseDate).format('D MMM, Y');

        var product_date = cart_list && cart_list.length > 0 ? cart_list[0].product.productDate : '', split_book_date, booking_date;
        if(product_date){
            split_book_date = moment(product_date).format('llll').split(',');
            booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];
        }

        return (
        <Fragment>
            {
                confirmation_transOrder && (
                <section className="confirmation_sec_bbg">
                    <div className="container">
                        <div className="confirm_title">
                            <h2>Booking Reference</h2>
                        </div>
                        <div className="bookRefBox clearfix">
                            <div className="clm">Transaction ID <h6>{confirmation_transOrder.transactionRefNumber}</h6></div>
                            <div className="clm2">Date of Purchase <h6>{purchase_date} {purchase_time}</h6></div>
                            <div className="clm3">Name <h6>{confirmation_transOrder.firstName} {confirmation_transOrder.lastName}</h6></div>
                            <div className="clm">Account No<h6>{confirmation_transOrder.acctNum}</h6></div>
                            <div className="clm">Payment Method<h6>{confirmation_transOrder.transactionOrderPaymentDetailsList && confirmation_transOrder.transactionOrderPaymentDetailsList[0].paymentType}</h6></div>
                        </div>

                        <div className="confirm_title mt-5">
                            <h2>Ticket(s) Collection</h2>
                        </div>

                        <div className="ticket_collec_wrap" id="ticketBill">
                            <div className="tick_topsec clearfix">
                                <div className="title_small pull-left">
                                    <span>Mode of Collection</span>
                                    <b className="blk">E-Ticket</b>
                                    <p>You can now conveniently present your e-ticket on your phone for entry.</p>
                                </div>
                                <div  className="pull-right">
                                    <a href="#" className="tDown" onClick={(e)=>this.printpage()}><i className="fa fa-print"></i></a>
                                    <a href="#" className="tPrint" onClick={(e) => this.downloadReference(e, this)}><i className="fa fa-download"></i></a>
                                    <div id="tempData"></div>
                                </div>
                            </div>

                            <div className="ticket_collec">
                                {
                                    cart_list && cart_list.length > 0 && (

                                        cart_list.map((cart_data, index) => {

                                            total_price += parseInt(cart_data.subTotal.number);
                                            var seat_price = cart_data.subTotal.number;

                                            seat_price_currency = cart_data.unitPrice.currency.currencyCode;

                                            var seat_no = cart_data.product.seatNo;

                                            return(
                                                <Fragment>
                                                {   
                                                    seat_no && seat_no.map((seat, index) =>{
                                                    return(
                                                        <div className="row ml-0 mr-0 clearfix br_b">
                                                            <div className="col-sm-8 col-12 col- br_r">
                                                                <li className="etik_list">
                                                                    <div className="etik_title">
                                                                        <h3>{cart_data.product.productName}</h3>
                                                                        <small>{cart_data.product.venue}</small>
                                                                        &nbsp;
                                                                        <small>{booking_date}</small>
                                                                    </div>
                                                                    <div className="etick_details clearfix">
                                                                        <div className="etick_info1">
                                                                            <span>{cart_data.priceclass.priceClassName}</span>
                                                                            <p>{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</p>
                                                                        </div>
                                                                        {
                                                                            cart_data.product.level && (
                                                                                <div className="etick_info2">
                                                                                    <span>Levels</span>
                                                                                    <p>{cart_data.product.level}</p>
                                                                                </div>
                                                                            )
                                                                        }                                                                
                                                                        <div className="etick_info3">
                                                                            <span>Section</span>
                                                                            <p>{cart_data.product.section}</p>
                                                                        </div>
                                                                        {
                                                                            cart_data.product.row && (
                                                                            <div className="etick_info4">
                                                                                <span>Row</span>
                                                                                <p>{cart_data.product.row}</p>
                                                                            </div>
                                                                            )
                                                                        }
                                                                        {
                                                                            seat && (
                                                                            <div className="etick_info4">
                                                                                <span>Seat</span>
                                                                                <p>{seat}</p>
                                                                            </div>
                                                                            )
                                                                        }
                                                                        {/* {
                                                                            cart_data.product.seatNo && (
                                                                            <div className="etick_info4">
                                                                                <span>Seat</span>
                                                                                <p>{cart_data.product.seatNo}</p>
                                                                            </div>
                                                                            )
                                                                        } */}
                                                                    </div>
                                                                </li>
                                                            </div>
                                                            <div className="col-sm-2 col-6 tidTime">
                                                                <div className="blk mb-1">
                                                                    <span>Transaction ID </span>
                                                                    <span>{confirmation_transOrder.transactionRefNumber}</span>
                                                                </div><br />
                                                                <div className="blk">
                                                                    <span>Transaction Time</span>
                                                                    <span>{purchase_date} {purchase_time}</span>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-2 col-6">
                                                                <div className="barcode">
                                                                    <img src={process.env.PUBLIC_URL + "/assets/images/barcode.png"} />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    )
                                                })

                                            }
                                            </Fragment>
                                            )

                                        })

                                    )
                                }

                            </div>

                            <div className="blk mb-5 mt-4">
                                <div className="clearfix">
                                    <a href="javascript;" className="inblock_btn voh_btn pull-right">View Order History</a>
                                </div>
                            </div>

                            <hr/>


                            <div className="notes">
                                <div>
                                    <p className="blk">	NOTE</p>
                                    <p>For verification purposes, kindly present the following upon collection :</p>

                                    <li className="romen_list">Credit card used in this transaction</li>

                                    <li className="romen_list">Your NRIC/Passport/FIN Card</li>

                                    <li className="romen_list">Clear print out of this email confirmation</li>
                                    <p>For a proxy to collect on your behalf, he/she needs to present :</p>

                                    <p>*Strictly no replacement of tickets for General Admission events</p>

                                    <li className="romen_list">This Letter of Authorisation (duly completed and signed by Credit Cardholder)</li>

                                    <li className="romen_list">Clear photocopy of Credit Cardholder's photo identification card such as NRIC/Passport/FIN Card</li>

                                    <li className="romen_list">Clear photocopy of the front of Credit Card used for the purchase</li>

                                    <li className="romen_list mb-3">Proxy's original photo identification card such as NRIC/Passport/FIN Card</li>

                                    <b>
                                    <p className="blk mb-1">	IMPORTANT NOTE</p>
                                    SISTIC reserves the right not to release tickets if the above documents are not in order.
                                    </b>
                                </div>
                            </div>

                        </div>

                        <div className="confirm_title mt-5">
                            <h2> Refund Exchange Policy </h2>
                            <p className="f-rubik">No refund or exchange of tickets is allowed once booking is confirmed.</p>
                        </div>

                    </div>
                </section>

                )
            }
            
        </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        totalQuantity:  state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        propsObj: ownProps,
        transactionId:state.transactionId,
        transactionTime: state.transactionTime,
        transactionDate: state.transactionDate,
        cardType: state.cardType,
        cartInfo: state.cartInfo,
        confirmation_transOrder: state.confirmation_transOrder,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        // fetchData: (dataObject) => {
        //     dispatch(actionCreators.fetchShowInfo(dataObject));
        // }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingReference);
