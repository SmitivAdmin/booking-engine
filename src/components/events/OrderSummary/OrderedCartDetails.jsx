import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { SITE_URL, ADD_ON, DeliverMethodTitle, DeliveryMethodDesc } from '../../../constants/common-info'
import TimeCounter from './../TimeCounter'
import moment from 'moment';
import he from 'he';
import HTMLParser from 'react-html-parser'
import $ from "jquery";
import * as actionCreators from "../../../actions/actionCreator";

class BookingCartDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cartInfo: [],
            // Upsell States ***Start**
            // availabelUpsell: [
            //     {
            //         title: "Aladdin - The Hit Broadway Musical",
            //         location: "Sands Theatre, Marina Bay Sands ",
            //         type: "PRODUCT",
            //         more: true,
            //         section: "A Reserve",
            //         date: "17 Jun 2020, 12.00 PM ",
            //         amount: "SGD 80.00",
            //         row: "M",
            //         qty: 1,
            //         seatNo: "12,13",
            //         type: "Merchandise",
            //         level: "Basement 1"
            //     },
            // ]

            availabelUpsell: []
        }
    }

    viewDetDesk(e) {
        e.preventDefault();
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".total-sec, .ticket-detail-sec, .view_ticket, .close_ticket").toggleClass('hide');
    }
    viewDetMob(e) {
        e.preventDefault();
        $('.view-detail .btn-view').toggleClass("arrow");
        $(".mob-tic-info").slideToggle();
    }

    // componentDidMount() {
    //     let cartInfoStr = $("#shoppingCartId").attr("data-lineItems");
    //     console.log("CartInfo from Payment as string", cartInfoStr);
    //     if (cartInfoStr !== "") {
    //         let cartInfoParsed = JSON.parse(cartInfoStr);
    //         console.log("CartInfo from Payment", cartInfoParsed);
    //         this.setState({
    //             cartInfo: cartInfoParsed
    //         })
    //     }
    // }



    addUpSellToCart = (index) => {
        // Add to UpSell In Cart
        this.props.updateUpSellItem(this.state.availabelUpsell[index], "ADD");

        // Delete from availabel Cart.
        var lclItem = this.state.availabelUpsell;
        lclItem.splice(index, 1);
        this.setState({ availabelUpsell: lclItem });
    }

    upSellDelete = (index) => {
        var delItem = this.props.upsellCartItems[index];
        // Add the item to local available items
        var newAvailItem = this.state.availabelUpsell;
        newAvailItem.push(delItem);

        this.setState({ availabelUpsell: newAvailItem }, () => {
            // Delete from redux.
            this.props.updateUpSellItem(index, "DELETE");
        });
    }

    render() {
        //let cart_list = this.state.cartInfo;
        var decode_title = '';
        let confirmation_transOrder = this.props.confirmation_transOrder;

        // let confirmation_transOrder = {"patronType":"I","emailAddress":"sasi@rgmobiles.com","shoppingCartModel":{"lineItemList":[{"type":"PRODUCT","quantity":2,"unitPrice":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":100,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":100},"subTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":208,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":208},"isGroupBookingLineItem":false,"pkgReqRefNo":null,"pkgReqId":null,"pkgProductType":null,"showSpecialRequest":null,"hideDisplay":null,"cartItemId":"13c6ad22-52a0-4be7-9f46-b1aabc19c55a","isMembership":false,"icc":"","product":{"productId":1141352,"productName":"Patrick The Musical","productDate":"2020-12-26T19:00:00+08:00","productEndDate":"2020-12-26T20:00:00+08:00","level":"DRESS CIRCLE","section":"DRESS CIRCLE","row":"C","seatNo":[36,37],"productType":"RS","venue":"Sands Theatre at Marina Bay Sands","altDesc":null,"gaSeqNumber":null},"priceclass":{"priceClassName":"Standard","priceClassCode":"A"},"pricecatid":85050,"bookingFee":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":8,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":8},"isIdRedemption":false,"isHidePrice":false,"promoterNameList":["Marina Bay Sands"],"isEvoucherEvent":false,"isEventWithSurvey":false,"productCategory":"Event","liveStreamEnable":false,"isShowEndTime":false}],"feeLineItems":[{"type":"DELIVERY","quantity":1,"unitPrice":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"subTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"isGroupBookingLineItem":null,"pkgReqRefNo":null,"pkgReqId":null,"pkgProductType":null,"showSpecialRequest":null,"hideDisplay":null,"code":"MAIL","description":"Thank you for choosing Mastercard Pickup. The SGD 0.50 delivery fee for Collection from Singapore Authorised Agents has been waived."}],"addOnLineItems":[],"packageList":null,"lineItemTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":208,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":208},"ticketProtectorAmount":null,"commonPaymentMethod":null,"commonDeliveryMethod":{"deliveryMethodList":[{"code":"E_TICKET","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"order":1,"addressRequired":false,"feeWaived":false},{"code":"MBS_BOX","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"order":2,"addressRequired":false,"feeWaived":false},{"code":"OUTLET_PICKUP","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":0.5},"order":2,"addressRequired":false,"feeWaived":false},{"code":"MAIL","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"order":2,"addressRequired":true,"feeWaived":false},{"code":"REGISTERED_MAIL","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":3,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":3},"order":3,"addressRequired":true,"feeWaived":false},{"code":"COURIER","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":15,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":15},"order":4,"addressRequired":true,"feeWaived":false}]},"currencyUnit":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"membershipBenefit":null,"membershipRedemption":null,"membershipProfileConfig":null,"showSpecialRequestTextarea":null,"transactionExtInfo":null,"idRedemptionTickets":null},"transactionRefNumber":"20200921-000096","firstName":"Sasi","lastName":"Kumar","purchaseDate":"2020-09-21T21:24:27+08:00","acctNum":"5694358","deliveryMethod":{"code":"MAIL","charge":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":1.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":1.5},"order":2,"addressRequired":true,"feeWaived":false},"transactionOrderPaymentDetailsList":[{"paymentType":"AMEX_MBS_SISTIC","subAmount":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":209.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":209.5}}],"totalPayment":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":209.5,"zero":false,"negative":false,"positive":true,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":false,"positiveOrZero":true,"numberStripped":209.5},"groupBookingModel":{"expiryDate":"2020-09-22T21:24:28.582+08:00","groupBookingLink":"https://v4be.stixcloudtest.com/sistic/booking/?groupBookingCode=30357f9-c9e0-46c7-97a5-a708e51580","lineItemList":[{"type":"GROUPBOOKING","quantity":1,"unitPrice":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"subTotal":{"currency":{"context":{"empty":false,"providerName":"java.util.Currency"},"defaultFractionDigits":2,"currencyCode":"SGD","numericCode":702},"number":0,"zero":true,"negative":false,"positive":false,"factory":{"defaultMonetaryContext":{"precision":0,"maxScale":63,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"maxNumber":null,"minNumber":null,"amountType":"org.javamoney.moneta.Money","maximalMonetaryContext":{"precision":0,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null}},"context":{"precision":256,"maxScale":-1,"amountType":"org.javamoney.moneta.Money","fixedScale":false,"empty":false,"providerName":null},"negativeOrZero":true,"positiveOrZero":true,"numberStripped":0},"isGroupBookingLineItem":true,"pkgReqRefNo":null,"pkgReqId":null,"pkgProductType":null,"showSpecialRequest":null,"hideDisplay":null,"product":{"productId":1141352,"productName":"Patrick The Musical","productDate":"2020-12-26T19:00:00+08:00","productEndDate":"2020-12-26T20:00:00+08:00","level":"DRESS CIRCLE","section":"DRESS CIRCLE","row":"C","seatNo":[38],"productType":"RS","venue":"Sands Theatre at Marina Bay Sands","altDesc":null,"gaSeqNumber":null}}]},"isPackage":false,"idRedemptionCodes":null,"friendsEmail":null}

        console.log('confirmation_transOrder_cart', confirmation_transOrder);

        let cart_list = confirmation_transOrder && confirmation_transOrder.shoppingCartModel && confirmation_transOrder.shoppingCartModel.lineItemList;
        console.log('cart_list_book_cart', cart_list);

        let reserved_groupbooking = confirmation_transOrder && confirmation_transOrder.groupBookingModel;
        let reserved_cart_list = reserved_groupbooking && reserved_groupbooking.lineItemList;
            
        let title = this.props.eventInfo ? this.props.eventInfo.title : '';
        let venue = this.props.eventInfo ? this.props.eventInfo.venue : undefined;
        let summaryImagePath = this.props.eventInfo ? this.props.eventInfo.summaryImagePath : undefined;

        title = cart_list && cart_list.length > 0 && cart_list[0].product.productName;
        venue = cart_list && cart_list.length > 0 && cart_list[0].product.venue;

        if (title) {
            //console.log('parser123',he.decode(title));
            decode_title = he.decode(title);
        }
        // let cart_list = this.props.mergedCartInfo;

        var total_price = 0, total_price2 = 0, total_price3 = 0, total_price4 = 0, booking_fee = 0, seat_price_currency = '', ref = this;
        var totalQty = 0, upsell_amount = 0, pkgReqId = cart_list.length > 0 && cart_list[0].pkgReqId ? cart_list[0].pkgReqId : null;

        var product_date = cart_list && cart_list.length > 0 ? cart_list[0].product.productDate : '', split_book_date, booking_date;
        if(product_date){
            split_book_date = moment(product_date).format('llll').split(',');
            booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];
        }
        console.log('split_book_date_data', split_book_date);

        summaryImagePath = "http://cmsuat.sistic.com.sg/sistic/docroot/sites/default/files/2019-12/SkyPark_436x326.jpg";

        return (
            
            <Fragment>
                {
                    cart_list && (
                    <Fragment>
                        <section class="confirmation_sec mb-5 cart-sec">
                         <div class="container">
                        <div class="confirm_top_sec clearfix">
                            <div class="pull-left w80">
                                <div class="confirm_title">
                                    <h2>Complete your purchase</h2>
                                </div>
                            </div>
                            <div class="pull-right w20">
                                <TimeCounter />
                            </div>
                        </div>

                        <div className="confDetails_sec bdrB_dash lg-none">
                            <div className="confDetail_mbl clearfix">
                                <div className="holder clearfix br_b pt-2">
                                    <div className="confDetails_title_img">
                                        {summaryImagePath ?
                                            (<div className="media-img mr-4">
                                                <img className="img-fluid" src={SITE_URL + summaryImagePath} alt="" />
                                            </div>) : 'pl-3'
                                        }
                                    </div>
                                    <div className="confDetails_title">
                                        <b>{HTMLParser(decode_title)}</b>
                                        <p className="mt-1 mb-0">
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"}
                                                    alt="icon" />
                                                {venue}
                                            </span>
                                            <span>
                                                <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"}
                                                    alt="icon" />
                                                {booking_date}
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                <div className="mob-tic-info">

                                    {
                                        cart_list.length > 0 && (

                                            cart_list.map((cart_data, index) => {

                                                total_price += parseFloat(cart_data.subTotal.number);
                                                var seat_price = cart_data.unitPrice.number;
                                                if(cart_data.priceclass.priceClassName){
                                                    totalQty += cart_data.quantity;
                                                }

                                                seat_price_currency = cart_data.unitPrice.currency.currencyCode;
                                                var priceClassCode = cart_data.priceclass.priceClassCode ? cart_data.priceclass.priceClassCode : '';

                                                var prdType = cart_data.product.productType;
                                                var pkgReqId = cart_data.pkgReqId;

                                                var colSpanVal = prdType === "RS" ? 8 : 5; 

                                                var product_date = cart_data.product.productDate;

                                                var split_book_date = moment(product_date).format('llll').split(',');
                                                var booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];

                                                return (
                                                    <div className="col-12 mb-3 px-0">
                                                        <div className="row title">
                                                            <span className="col-6">{cart_data.priceclass.priceClassName}</span>
                                                            <span
                                                                className="col-6 text-right">{cart_data.unitPrice.currency.currencyCode} {seat_price}</span>
                                                        </div>
                                                        <div className="row tic-cont">
                                                            {
                                                                prdType === "RS" && (
                                                                    <div className="col-4">
                                                                        <span className="row">
                                                                            <span className="col-4">Level</span>
                                                                            <span className="col-8">{cart_data.product.level}</span>
                                                                        </span>
                                                                        <span className="row">
                                                                            <span className="col-4">Row</span>
                                                                            <span className="col-8">{cart_data.product.row}</span>
                                                                        </span>
                                                                    </div>
                                                                )
                                                            }
                                                            
                                                            <div className="col-8">
                                                                <span className="row">
                                                                    <span className="col-5">Section</span>
                                                                    <span className="col-7 pl-0">{cart_data.product.section}</span>
                                                                </span>
                                                                {
                                                                    prdType === "RS" && (
                                                                    <span className="row">
                                                                        <span className="col-5">Seats(s)</span>
                                                                        <span className="col-7 pl-0">{cart_data.product.seatNo.toString()}</span>
                                                                    </span>
                                                                    )
                                                                }
                                                                <span className="row">
                                                                    <span className="col-5">Unit Price</span>
                                                                    <span
                                                                        className="col-7 pl-0">{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</span>
                                                                </span>
                                                            </div>
                                                            <a href="javascript:;" className="trash-icon"
                                                                onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCart(e, index, cart_data.inventry_id, cart_data.type)}>
                                                                <img
                                                                    src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"}
                                                                    alt="icon" />
                                                            </a>
                                                        </div>
                                                    </div>
                                                )
                                            })

                                        )
                                    }
                                </div>
                                <div className="mbl_invoice">

                                    <div className="holder">
                                        <div className="blk inner_info2 clearfix">
                                            <span className="pull-left">Booking Fee</span>
                                            <span className="pull-right">SGD 4.00</span>
                                            <span
                                                className="pull-right pr-5">SGD 4.00 x{totalQty}</span>
                                        </div>
                                        {this.props.crossSellName !== "" &&
                                            <div className="blk inner_info2 clearfix">
                                                <span className="pull-left">{this.props.crossSellName}</span>

                                                <span className="pull-right">SGD {this.props.crossSellAmt.toFixed(2)}</span>
                                            </div>
                                        }
                                        <div className="blk inner_info2 clearfix">
                                            <span className="pull-left">{this.props.chosenAddon}</span>

                                            <span className="pull-right">{this.props.chosenAddonPrice}</span>
                                        </div>
                                        <div className="blk inner_info2 clearfix">
                                            <span className="pull-left">e-Voucher</span>

                                            <span className="pull-right">SGD 00.00</span>
                                        </div>
                                        <div className="have-voucher col-12 px-0">
                                            <button className="btn btn-outline">Have an e-Voucher?</button>
                                            <div className="apply-voucher">
                                                <label>e-Voucher</label>
                                                <div className="form-group">
                                                    <input placeholder="Enter e-Voucher" type="text" name="voucher"
                                                        className="form-control" />
                                                    <a href="javascript:;" className="add">
                                                        <img
                                                            src={process.env.PUBLIC_URL + "/assets/images/plus-blue.svg"}
                                                            alt="icon" />
                                                    </a>
                                                </div>
                                                <button className="btn btn-primary">Apply e-Voucher</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="confDetails_footer_mbl">
                            <div class="w50 pull-left">
                                <b>Total</b>
                            </div>
                            {this.props.updateTotalPrice(this.props.chosenAddonPrice !== "" ? (parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + total_price + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price).toFixed(2) + this.props.crossSellAmt)
                            }
                            <div class="w50 pull-right text-right">
                                <b>{seat_price_currency} {this.props.chosenAddonPrice !== "" ? (total_price + parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price + this.props.crossSellAmt).toFixed(2)}</b>
                            </div>
                        </div>


                        <div className="confDetails_body d-none">
                            {
                                summaryImagePath && (
                                    <div className="media-img">
                                        <img className="img-fluid" src={summaryImagePath} alt="" />
                                    </div>
                                )
                            }
                            
                            <div className={`confDetails_sec ${summaryImagePath ? '' : 'npad_lft'}`}>

                                <div className="confDetail_mbl">
                                    <div className="confDetails_title">
                                        <b>{HTMLParser(decode_title)}</b>
                                        <p className="mt-1 mb-0">
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" /> {venue}
                                            </span>
                                            {
                                                pkgReqId === null && (
                                                    <span>
                                                        <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" /> {booking_date && booking_date}
                                                    </span>
                                                )
                                            }
                                        </p>
                                    </div>
                                </div>


                                <div className="confDetails_title">
                                    <b>{HTMLParser(decode_title)}</b>
                                    <p className="mt-1 mb-0">
                                        <span className="mr-3">
                                            <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" /> {venue}
                                        </span>
                                        {
                                            pkgReqId === null && (
                                                <span>
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" /> {booking_date && booking_date}
                                                </span>
                                            )
                                        }
                                        
                                    </p>
                                </div>
                                <div className="confDetail_tbl">
                                    <table className="table">
                                        <tbody>
                                            {
                                                pkgReqId !== null && (
                                                    <tr className="pkg_incl"><td colSpan={8}>Package includes admission to :</td></tr>
                                                )
                                            }
                                            {
                                                cart_list.length > 0 && (

                                                    cart_list.map((cart_data, index) => {

                                                        total_price2 += parseFloat(cart_data.subTotal.number);
                                                        var seat_price = cart_data.unitPrice.number;
                                                        // if(cart_data.priceclass.priceClassName){
                                                        //     totalQty += cart_data.quantity;
                                                        // }                                                        
                                                        seat_price_currency = cart_data.unitPrice.currency.currencyCode;
                                                        var priceClassCode = cart_data.priceclass.priceClassCode ? cart_data.priceclass.priceClassCode : '';

                                                        var prdType = cart_data.product.productType;
                                                        var pkgReqId = cart_data.pkgReqId;

                                                        var colSpanVal = prdType === "RS" ? 8 : 5; 

                                                        var product_date = cart_data.product.productDate;

                                                        var split_book_date = moment(product_date).format('llll').split(',');
                                                        var booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];

                                                        booking_fee += parseFloat(cart_data.bookingFee && cart_data.bookingFee.number);

                                                        return (
                                                        <Fragment>
                                                            {
                                                                pkgReqId !== null && (
                                                                    <tr className="pkg_prd_name">
                                                                        <td width="2%">{parseInt(index) + 1}.</td>
                                                                        <td width="98%" colSpan={colSpanVal}>{cart_data.product.productName}<br/><span>{cart_data.product.venue},  {booking_date}</span></td>
                                                                    </tr>
                                                                )
                                                            }
                                                            
                                                            <tr>
                                                                {
                                                                    pkgReqId === null ? (
                                                                        <td width="5%"><span>{parseInt(index) + 1}.</span></td>
                                                                    ) : (
                                                                        <td width="5%">&nbsp;</td>
                                                                    )
                                                                }
                                                                <td width="12%"><span>Type</span><p>{cart_data.priceclass.priceClassName}</p></td>

                                                                {
                                                                    prdType === "RS" && (
                                                                    <td width="15%"><span>level</span><p>{cart_data.product.level}</p></td>
                                                                    )
                                                                }
                                                                <td width="15%"><span>Section</span><p>{prdType === "RS" ? cart_data.product.section : 'General Admission'}</p></td>
                                                                {
                                                                    prdType === "RS" && (
                                                                    <Fragment>
                                                                        <td width="5%"><span>Row</span><p>{cart_data.product.row}</p></td>
                                                                        <td width="10%"><span>Seat(s)</span><p>{cart_data.product.seatNo.toString()}</p></td>
                                                                    </Fragment>
                                                                    )
                                                                }
                                                                <td width="15%"><span>Unit Price</span><p>{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</p></td>
                                                                <td width="7%"><span>Quantity</span><p>{cart_data.quantity}</p></td>
                                                                <td width="13%">
                                                                    <b><br />{cart_data.unitPrice.currency.currencyCode} {(seat_price * cart_data.quantity).toFixed(2)}</b>
                                                                </td>
                                                            </tr>
                                                        </Fragment>
                                                        )
                                                    })
                                                )
                                            }

                                            {this.props.upsellCartItems && this.props.upsellCartItems.length > 0 && (
                                            <tr>
                                                <td width="100%" colSpan={5}><img className="addOnsImg" src={process.env.PUBLIC_URL + "/assets/images/addOns.svg"} alt="icon" /> <span className="addOnsTitle">Add-Ons</span></td>
                                                {/* <td width="15%"><span className="addOnsTitle">Add-Ons</span></td> */}
                                            </tr>
                                            )}

                                        </tbody>
                                    </table>

                                    {/* Add-Ons */}
                                    
                                    {this.props.upsellCartItems && this.props.upsellCartItems.length > 0 && (
                                        <div style={{ backgroundColor: '#fff', padding: '5px' }}>
                                        <table className="upSellTable">
                                            <tbody>
                                                {/* Mapped Added cart items */}
                                                {this.props.upsellCartItems.map((upSellcart, index) => {
                                                    total_price2 += parseFloat(upSellcart.amount.replace("SGD", ""));
                                                    return (
                                                        <tr>
                                                            <td width="5%">&nbsp;</td>
                                                            <td width="50%">
                                                                <span className="upSellItemName">
                                                                    {upSellcart.title}
                                                                </span>
                                                                <br />
                                                                <td>
                                                                    <span>Type</span>
                                                                    <p>{upSellcart.type}</p>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <td>
                                                                    <span>Level</span>
                                                                    <p>{upSellcart.level}</p>
                                                                </td>
                                                            </td>
    
                                                            <td width="15%">
                                                                <span>Unit Price</span>
                                                                <p>{upSellcart.currencyCode} {upSellcart.amount}</p>
                                                            </td>
                                                            <td width="16%">
                                                                <span>Quantity</span>
                                                                <p>{upSellcart.qty}</p>
                                                            </td>
                                                            <td width="20%" className="">
                                                                <b><br />{upSellcart.currencyCode} {(upSellcart.amount * upSellcart.qty).toFixed(2)}</b>
                                                            </td>

                                                        </tr>
                                                    )
                                                })
                                                }
                                            </tbody>
                                        </table>
                                    </div>
                                    )}
                                </div>

                            </div>
                            <div className="ticket_devide">
                                <div className="left_circle"></div>
                                <div className="right_circle"></div>
                                <div className="dot_line"></div>
                            </div>

                            <div className={`confDetails_sec bdrB_dash pt-0 ${summaryImagePath ? '' : 'npad_lft'}`}>
                                <div className="confDetail_tbl">
                                    
                                    <table className="table">                                        
                                        <tbody>
                                            {
                                                booking_fee > 0 && (
                                                    <tr>
                                                        <td width="5%">&nbsp;</td>
                                                        <td width="50%"><span>Booking Fee</span></td>
                                                        <td width="15%"><span>{cart_list[0].bookingFee.currency && cart_list[0].bookingFee.currency.currencyCode} {booking_fee.toFixed(2)}</span></td>
                                                        <td width="17%"><span>x {totalQty} tickets</span></td>
                                                        <td width="13%"><span>{cart_list[0].bookingFee.currency && cart_list[0].bookingFee.currency.currencyCode} {(booking_fee * totalQty).toFixed(2)}</span></td>
                                                        <td width="10%"></td>
                                                    </tr>
                                                )
                                            }
                                            
                                            {this.props.crossSellName !== "" &&
                                                <tr>
                                                    <td width="5%">&nbsp;</td>
                                                    <td width="50%"><span>{this.props.crossSellName}</span></td>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="17%">&nbsp;</td>
                                                    <td width="13%"><span>SGD {this.props.crossSellAmt.toFixed(2)}</span></td>
                                                    <td width="10%">&nbsp;</td>
                                                </tr>
                                            }
                                            {
                                                this.props.insuranceAmount > 0 && (
                                                <tr>
                                                    <td width="5%">&nbsp;</td>
                                                    <td width="50%"><span>Tickets Insured</span></td>
                                                    <td width="15%">&nbsp;</td>
                                                    <td width="17%">&nbsp;</td>
                                                    <td width="13%"><span>{seat_price_currency + " " + this.props.insuranceAmount}</span></td>
                                                    <td width="10%"></td>
                                                </tr>
                                                )
                                            }
                                            
                                            <tr>
                                                <td width="5%">&nbsp;</td>
                                                <td width="50%"><span>{DeliverMethodTitle(confirmation_transOrder.deliveryMethod.code)}</span></td>
                                                <td width="15%">&nbsp;</td>
                                                <td width="17%">&nbsp;</td>
                                                <td width="13%"><span>{confirmation_transOrder.deliveryMethod.charge.number !== "" && (confirmation_transOrder.deliveryMethod.charge.currency.currencyCode + " " + (confirmation_transOrder.deliveryMethod.charge.number).toFixed(2))}</span></td>
                                                <td width="10%"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <div className={`have-voucher col-md-12 col-12 no_pad`}>
                                        <button className="btn btn-outline">Have an e-Voucher?</button>
                                        <div className="apply-voucher">
                                            <label>e-Voucher</label>
                                            <div className="form-group">
                                                <input placeholder="Enter e-Voucher" type="text" name="voucher" className="form-control" />
                                                <a href="javascript:;" className="add">
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/plus-blue.svg"} alt="icon" />
                                                </a>
                                            </div>
                                            <button className="btn btn-primary">Apply e-Voucher</button>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div className={`confDetails_footer ${summaryImagePath ? '' : 'npad_lft'}`}>
                                <div className="pull-left w81">
                                    <b>Total</b>
                                </div>

                                <div className="pull-right w15">
                                    <b>{seat_price_currency} {this.props.chosenAddonPrice !== "" ? (total_price2 + parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price2 + this.props.crossSellAmt
                                    ).toFixed(2)}</b>
                                </div>
                            </div>

                        </div>

                        {
                            reserved_cart_list.length > 0 && (

                            <div className="confDetails_body d-none reserved_frnd">

                                <div className={`confDetails_sec ${summaryImagePath ? '' : 'npad_lft'}`}>

                                    <div className={`reservedTtl ${summaryImagePath ? '' : 'npad_lft'}`}>Reserved for Friends</div>

                                    {
                                        summaryImagePath && (
                                            <div className="media-img">
                                                <img className="img-fluid" src={summaryImagePath} alt="" />
                                            </div>
                                        )
                                    }

                                    <div className="confDetail_mbl">
                                        <div className="confDetails_title">
                                            <b>{HTMLParser(decode_title)}</b>
                                            <p className="mt-1 mb-0">
                                                <span className="mr-3">
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" /> {venue}
                                                </span>
                                                {
                                                    pkgReqId === null && (
                                                        <span>
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" /> {booking_date && booking_date}
                                                        </span>
                                                    )
                                                }
                                            </p>
                                        </div>
                                    </div>


                                    <div className="confDetails_title">
                                        <b>{HTMLParser(decode_title)}</b>
                                        <p className="mt-1 mb-0">
                                            <span className="mr-3">
                                                <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" /> {venue}
                                            </span>
                                            {
                                                pkgReqId === null && (
                                                    <span>
                                                        <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"} alt="icon" /> {booking_date && booking_date}
                                                    </span>
                                                )
                                            }
                                            
                                        </p>
                                    </div>
                                    <div className="confDetail_tbl">
                                        <table className="table">
                                            <tbody>
                                                {
                                                    pkgReqId !== null && (
                                                        <tr className="pkg_incl"><td colSpan={8}>Package includes admission to :</td></tr>
                                                    )
                                                }
                                                {
                                                    reserved_cart_list.length > 0 && (

                                                        reserved_cart_list.map((cart_data, index) => {

                                                            total_price2 += parseFloat(cart_data.subTotal.number);
                                                            var seat_price = cart_data.unitPrice.number;
                                                            // if(cart_data.priceclass.priceClassName){
                                                            //     totalQty += cart_data.quantity;
                                                            // }                                                        
                                                            seat_price_currency = cart_data.unitPrice.currency.currencyCode;
                                                            var priceClassCode = cart_list[0].priceclass.priceClassCode ? cart_list[0].priceclass.priceClassCode : '';

                                                            var prdType = cart_data.product.productType;
                                                            var pkgReqId = cart_data.pkgReqId;

                                                            var colSpanVal = prdType === "RS" ? 8 : 5; 

                                                            var product_date = cart_data.product.productDate;

                                                            var split_book_date = moment(product_date).format('llll').split(',');
                                                            var booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];

                                                            booking_fee += parseFloat(cart_data.bookingFee && cart_data.bookingFee.number);

                                                            return (
                                                            <Fragment>
                                                                {
                                                                    pkgReqId !== null && (
                                                                        <tr className="pkg_prd_name">
                                                                            <td width="2%">{parseInt(index) + 1}.</td>
                                                                            <td width="98%" colSpan={colSpanVal}>{cart_data.product.productName}<br/><span>{cart_data.product.venue},  {booking_date}</span></td>
                                                                        </tr>
                                                                    )
                                                                }
                                                                
                                                                <tr>
                                                                    {
                                                                        pkgReqId === null ? (
                                                                            <td width="5%"><span>{parseInt(index) + 1}.</span></td>
                                                                        ) : (
                                                                            <td width="5%">&nbsp;</td>
                                                                        )
                                                                    }
                                                                    <td width="12%"><span>Type</span><p>-</p></td>
                                                                    
                                                                    {
                                                                        prdType === "RS" && (
                                                                        <td width="15%"><span>level</span><p>{cart_data.product.level}</p></td>
                                                                        )
                                                                    }
                                                                    <td width="15%"><span>Section</span><p>{prdType === "RS" ? cart_data.product.section : 'General Admission'}</p></td>
                                                                    {
                                                                        prdType === "RS" && (
                                                                        <Fragment>
                                                                            <td width="5%"><span>Row</span><p>{cart_data.product.row}</p></td>
                                                                            <td width="10%"><span>Seat(s)</span><p>{cart_data.product.seatNo.toString()}</p></td>
                                                                        </Fragment>
                                                                        )
                                                                    }
                                                                    <td width="15%"><span>Unit Price</span><p>{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</p></td>
                                                                    <td width="7%"><span>Quantity</span><p>{cart_data.quantity}</p></td>
                                                                    <td width="13%">
                                                                        <b><br />{cart_data.unitPrice.currency.currencyCode} {(seat_price * cart_data.quantity).toFixed(2)}</b>
                                                                    </td>
                                                                </tr>
                                                            </Fragment>
                                                            )
                                                        })
                                                    )
                                                }

                                            </tbody>
                                        </table>


                                    </div>

                                </div>

                                <div className={`confDetails_sec no_bdr ${summaryImagePath ? '' : 'npad_lft'}`}>
                                    <p className={`resSeatLink`}>Your reserved seats will be expired on Tue 05 May 2020 05:26 PM. Invite your friends to watch with you by sharing the following link:<br />
                                    <a href={reserved_groupbooking.groupBookingLink} target="_blank">{reserved_groupbooking.groupBookingLink}</a></p>
                                </div>

                            </div>
                            )
                        }
                    </div>

                </section>

                        <section className="show-info seats float-wrap">
                            <div className="container">
                                <div className="row">
                                    <div className="media col-md-11 col-12">
                                        <div className="media-body">
                                            <h5 className="mt-0 ft">{HTMLParser(decode_title)}</h5>
                                            <p className="ft-1">
                                                <span className="mr-3">
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/pin-icon.svg"} alt="icon" />
                                                    <span className="label-primary">{venue}</span>
                                                </span>
                                                <span className="mr-3">
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/ticket-icon.svg"}
                                                        alt="icon" />
                                                    {this.props.totalQuantity} Ticket{this.props.totalQuantity > 1 ? 's' : ''}
                                                </span>
                                                <span>
                                                    <img src={process.env.PUBLIC_URL + "/assets/images/calendar-icon.svg"}
                                                        alt="icon" />
                                                    {this.props.dateChosen + ", " + this.props.timeChosen}
                                                </span>
                                                
                                            </p>
                                            <div className="ticket-detail-sec hide">
                                                <table className="table">
                                                    <tbody>


                                                        {
                                                            cart_list.length > 0 && (

                                                                cart_list.map((cart_data, index) => {

                                                                    total_price3 += parseInt(cart_data.subTotal.number);

                                                                    var seat_price = cart_data.unitPrice.number;
                                                                    seat_price_currency = cart_data.unitPrice.currency.currencyCode;

                                                                    var prdType = cart_data.product.productType;
                                                                    var pkgReqId = cart_data.pkgReqId;

                                                                    var colSpanVal = prdType === "RS" ? 8 : 5; 

                                                                    var product_date = cart_data.product.productDate;

                                                                    var split_book_date = moment(product_date).format('llll').split(',');
                                                                    var booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];
                                                                    return (
                                                                        <tr>
                                                                            <td width="2%"><p>{parseInt(index) + 1}.</p></td>
                                                                            <td width="10%"><p>Type</p>
                                                                                <span>{cart_data.priceclass.priceClassName}</span></td>
                                                                            {
                                                                                prdType === "RS" && (
                                                                                <td width="10%"><p>Level</p>
                                                                                    <span>{cart_data.product.level}</span></td>
                                                                                )
                                                                            }
                                                                            
                                                                            <td width="15%"><p>Section</p>
                                                                                <span>{cart_data.product.section}</span></td>
                                                                            {
                                                                                prdType === "RS" && (
                                                                                <Fragment>
                                                                                    <td width="10%"><p>Row</p>
                                                                                        <span>{cart_data.product.row}</span></td>
                                                                                    <td width="10%"><p>Seat(s)</p>
                                                                                        <span>{cart_data.product.seatNo.toString()}</span>
                                                                                    </td>
                                                                                </Fragment>
                                                                                )
                                                                            }
                                                                            <td width="15%"><p>Unit Price</p>
                                                                                <span>{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</span>
                                                                            </td>
                                                                            <td width="10%"><p>Quantity</p><span>1</span></td>
                                                                            <td width="15%" className="text-right">
                                                                                <b>{cart_data.unitPrice.currency.currencyCode} {(seat_price * cart_data.quantity).toFixed(2)}</b>
                                                                                <i className="trash-icon"
                                                                                    onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCart(e, index, cart_data.inventry_id, cart_data.type)}>
                                                                                    <img
                                                                                        src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"}
                                                                                        alt="icon" />
                                                                                </i>
                                                                            </td>
                                                                        </tr>
                                                                    )
                                                                })
                                                            )
                                                        }

                                                        {/* Add-Ons dropdown view */}
                                                        <tr>
                                                            <td width="2%"><img className="addOnsImg" src={process.env.PUBLIC_URL + "/assets/images/addOns.svg"} alt="icon" /></td>
                                                            <td width="15%"><span style={{ paddingTop: '4px' }} className="addOnsTitle">Add-Ons</span></td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="15%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="15%" className="text-right">
                                                                &nbsp;
                                                    </td>
                                                        </tr>
                                                        {/* Mapped Added cart items */}

                                                        {this.props.upsellCartItems.map((upSellcart, index) => {
                                                            total_price3 += parseFloat(upSellcart.amount.replace("SGD", ""));
                                                            return (
                                                                <tr>
                                                                    <td width="5%">&nbsp;</td>
                                                                    <td width="50%">
                                                                        <span className="upSellItemName">
                                                                            {upSellcart.title}
                                                                        </span>
                                                                        <br />
                                                                        <td>
                                                                            <p>Type</p>
                                                                            <span>{upSellcart.type}</span>
                                                                        </td>
                                                                        <td>&nbsp;</td>
                                                                        <td>
                                                                            <p>Level</p>
                                                                            <span>{upSellcart.level}</span>
                                                                        </td>
                                                                    </td>
                                                                    <td width="10%">&nbsp;</td>
                                                                    <td width="10%">&nbsp;</td>
                                                                    <td width="10%">&nbsp;</td>
                                                                    <td width="10%">&nbsp;</td>
                                                                    <td width="15%">
                                                                        <p>Unit Price</p>
                                                                        <span>{upSellcart.amount}</span>
                                                                    </td>
                                                                    <td width="10%">
                                                                        <p>Quantity</p>
                                                                        <span>{upSellcart.qty}</span>
                                                                    </td>
                                                                    <td width="20%" className="text-right">
                                                                        <span><b>{upSellcart.amount}</b></span>
                                                                    </td>
                                                                </tr>
                                                            )
                                                        })
                                                        }

                                                        <tr className="tfoot">
                                                            <td width="2%">&nbsp;</td>
                                                            <td width="15%"><span>Booking Fee</span></td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="15%">&nbsp;</td>
                                                            <td width="5%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="15%"><span>SGD 4.00</span></td>
                                                            <td width="10%"><span>x {totalQty} tickets</span>
                                                            </td>
                                                            <td width="10%">
                                                                <span>SGD {(4 * totalQty).toFixed(2)}</span>
                                                            </td>
                                                        </tr>
                                                        {this.props.crossSellName !== "" &&
                                                            <tr>
                                                                <td width="2%">&nbsp;</td>
                                                                <td width="15%"><span>{this.props.crossSellName}</span></td>
                                                                <td width="10%">&nbsp;</td>
                                                                <td width="15%">&nbsp;</td>
                                                                <td width="5%">&nbsp;</td>
                                                                <td width="10%">&nbsp;</td>
                                                                <td width="15%"><span></span></td>
                                                                <td width="10%"><span></span></td>
                                                                <td width="10%"><span>SGD {this.props.crossSellAmt.toFixed(2)}</span>
                                                                </td>
                                                            </tr>
                                                        }
                                                        <tr>
                                                            <td width="2%">&nbsp;</td>
                                                            <td width="15%"><span>{this.props.chosenAddon}</span></td>
                                                            <td width="1%0">&nbsp;</td>
                                                            <td width="15%">&nbsp;</td>
                                                            <td width="5%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="15%">&nbsp;</td>
                                                            <td width="10%">&nbsp;</td>
                                                            <td width="10%">
                                                                <span>{this.props.chosenAddonPrice !== "" && (seat_price_currency + " " + this.props.chosenAddonPrice.replace("SGD", ""))}</span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <a href="javascript:;" className="expand-detail">Close Details</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-1">
                                        <TimeCounter />
                                    </div>
                                    <div className="mob-tic-info">
                                        <hr />

                                        {
                                            cart_list.length > 0 && (


                                                cart_list.map((cart_data, index) => {

                                                    var seat_price = cart_data.unitPrice.number;
                                                    // total_price3 += parseInt(cart_data.subTotal.number);
                                                    seat_price_currency = cart_data.unitPrice.currency.currencyCode;

                                                    var prdType = cart_data.product.productType;
                                                    var pkgReqId = cart_data.pkgReqId;

                                                    var colSpanVal = prdType === "RS" ? 8 : 5; 

                                                    var product_date = cart_data.product.productDate;

                                                    var split_book_date = moment(product_date).format('llll').split(',');
                                                    var booking_date = (split_book_date[1].trim()).split(' ')[1]+' '+(split_book_date[1].trim()).split(' ')[0]+' '+(split_book_date[2].trim()).split(' ')[0]+', '+(split_book_date[2].trim()).split(' ')[1]+(split_book_date[2].trim()).split(' ')[2];

                                                    return (
                                                        <div className="col-12 mb-3 px-0">
                                                            <div className="row title">
                                                                <span className="col-6">{cart_data.priceclass.priceClassName}</span>
                                                                <span
                                                                    className="col-6 text-right">{cart_data.unitPrice.currency.currencyCode} {seat_price}</span>
                                                            </div>
                                                            <div className="row tic-cont">
                                                                {
                                                                    prdType === "RS" && (
                                                                        <div className="col-4">
                                                                            <span className="row">
                                                                                <span className="col-4">Level</span>
                                                                                <span className="col-8">{cart_data.product.level}</span>
                                                                            </span>
                                                                            <span className="row">
                                                                                <span className="col-4">Row</span>
                                                                                <span className="col-8">{cart_data.product.row}</span>
                                                                            </span>
                                                                        </div>
                                                                    )
                                                                }
                                                                
                                                                <div className="col-8">
                                                                    <span className="row">
                                                                        <span className="col-5">Section</span>
                                                                        <span className="col-7 pl-0">{cart_data.product.section}</span>
                                                                    </span>
                                                                    {
                                                                        prdType === "RS" && (
                                                                        <span className="row">
                                                                            <span className="col-5">Seats(s)</span>
                                                                            <span className="col-7 pl-0">{cart_data.product.seatNo.toString()}</span>
                                                                        </span>
                                                                        )
                                                                    }
                                                                    <span className="row">
                                                                        <span className="col-5">Unit Price</span>
                                                                        <span
                                                                            className="col-7 pl-0">{cart_data.unitPrice.currency.currencyCode} {seat_price.toFixed(2)}</span>
                                                                    </span>
                                                                </div>
                                                                <a href="javascript:;" className="trash-icon"
                                                                    onClick={e => window.confirm("Are you sure you wish to delete this item?") && this.delCart(e, index, cart_data.inventry_id, cart_data.type)}>
                                                                    <img
                                                                        src={process.env.PUBLIC_URL + "/assets/images/trash-icon.svg"}
                                                                        alt="icon" />
                                                                </a>
                                                            </div>
                                                        </div>
                                                    )
                                                })

                                            )}

                                        <div className="ticket_devide bg-transparent">
                                            <div className="dot_line"></div>
                                        </div>
                                        <div className="holder pt-0">
                                            <div className="blk inner_info2 clearfix">
                                                <span className="pull-left">Booking Fee</span>
                                                <span className="pull-right">SGD 4.00</span>
                                                <span
                                                    className="pull-right pr-5">SGD 4.00 x {totalQty}</span>
                                            </div>
                                            {this.props.crossSellName !== "" &&
                                                <div className="blk inner_info2 clearfix">
                                                    <span className="pull-left">{this.props.crossSellName}</span>

                                                    <span className="pull-right">SGD {this.props.crossSellAmt.toFixed(2)}</span>
                                                </div>
                                            }
                                            <div className="blk inner_info2 clearfix">
                                                <span className="pull-left">{this.props.chosenAddon}</span>

                                                <span className="pull-right">{this.props.chosenAddonPrice}</span>
                                            </div>
                                            <div className="blk inner_info2 clearfix">
                                                <span className="pull-left">e-Voucher</span>

                                                <span className="pull-right">SGD 00.00</span>
                                            </div>
                                            <div className="have-voucher col-12 px-0">
                                                <button className="btn btn-outline">Have an e-Voucher?</button>
                                                <div className="apply-voucher">
                                                    <label>e-Voucher</label>
                                                    <div className="form-group">
                                                        <input placeholder="Enter e-Voucher" type="text" name="voucher"
                                                            className="form-control" />
                                                        <a href="javascript:;" className="add">
                                                            <img src={process.env.PUBLIC_URL + "/assets/images/plus-blue.svg"}
                                                                alt="icon" />
                                                        </a>
                                                    </div>
                                                    <button className="btn btn-primary">Apply e-Voucher</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div className="total-sec col-12 mob hide">

                                <div className="container">
                                    <div className="row align-items-center">
                                        <div className="col-6 booking-fee">
                                            <h6>Total</h6>
                                        </div>
                                        {this.props.updateTotalPrice(this.props.chosenAddonPrice !== "" ? (parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + total_price3 + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt + upsell_amount).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price3 + this.props.crossSellAmt + upsell_amount).toFixed(2))
                                        }
                                        <div className="col-6 check-out-sec">
                                            <h6>{seat_price_currency} {this.props.chosenAddonPrice !== "" ? (parseFloat(this.props.chosenAddonPrice.replace("SGD", "")) + total_price3 + parseFloat(this.props.insuranceAmount) + this.props.crossSellAmt + upsell_amount).toFixed(2) : (parseFloat(this.props.insuranceAmount) + total_price3 + this.props.crossSellAmt + upsell_amount).toFixed(2)}</h6>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {/* <div className="view-detail d-block d-sm-block d-md-none">
                                <button className="btn btn-view" onClick={this.viewDetails.bind(this)}>
                                    <span>View Ticket Details</span>
                                    <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"}
                                        alt="icon"/>
                                </button>
                            </div> */}

                            <div className="view-detail d-block d-sm-none d-md-block">
                                <button className="btn btn-view" onClick={this.viewDetDesk.bind(this)}>
                                    <span className="view_ticket">View Ticket Details</span>
                                    <span className="close_ticket hide">Close Ticket Details</span>
                                    <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon" />
                                </button>
                            </div>

                            <div className="view-detail d-block d-sm-block d-md-none">
                                <button className="btn btn-view" onClick={this.viewDetMob.bind(this)}>
                                    <span>View Ticket Details</span>
                                    <img className="ml-1" src={process.env.PUBLIC_URL + "/assets/images/view-detail-arrow.svg"} alt="icon" />
                                </button>
                            </div>
                        </section>

                    </Fragment>
                    )
                }
                
            </Fragment>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        eventInfo: state.showInfo.eventInfo,
        totalQuantity: state.adult_qty + state.child_qty + state.nsf_qty + state.sectz_qty,
        chosenAddon: state.chosenAddon,
        chosenAddonPrice: state.chosenAddonPrice,
        cartInfo: state.cartInfo,
        mergedCartInfo: state.mergedCartInfo,
        update_seat_select: state.update_seat_select,
        dateChosen: state.dateChosen,
        insuranceAmount: state.insuranceAmount,
        timeChosen: state.timeChosen,
        crossSellName: state.crossSellName,
        crossSellAmt: state.crossSellAmt,
        upsellCartItems: state.upsellCartItems,
        confirmation_transOrder: state.confirmation_transOrder,
        propsObj: ownProps
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateInsuredAmount: (amount) => {
            dispatch(actionCreators.updateInsured(amount));
        },
        updateTotalPrice: (sum) => {
            dispatch(actionCreators.updateSum(sum));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(BookingCartDetails);
