import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import Footer from '../common/Footer'
import Header from '../common/Header'
import CartDetails from './Booking/CartDetails'
import SeatSelection from './Booking/SeatSelection'
import ListViewCalendar from './Booking/ListViewCalendar'
import ShowDateTime from './Booking/ShowDateTime'
import TicketType from './Booking/TicketType'

import _ from "lodash";

class Booking extends Component {

    constructor(props) {
        super(props);
    }

    render() {
        let form;

        form = (
            <Fragment>
                <Header/>

                <main>

                    {/* <Breadcrumbs/> */}

                    <CartDetails />

                    <ListViewCalendar />

                    <ShowDateTime />

                    <SeatSelection />

                    <TicketType />

                </main>


                {/* <Footer/> */}
            </Fragment>
        );
        return (
            <Fragment>
                {form}
            </Fragment>

        );
    }
}

const mapStateToProps = (state) => {
    return {

    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Booking);

