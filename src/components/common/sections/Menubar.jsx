import React, {Component} from 'react'


export default class Menubar extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
            <div className="row mt-3">
                <div className="col-6">
                    <nav className="navbar navbar-expand-sm">
                        <div className="collapse navbar-collapse" id="custom-left-nav">
                            <ul className="navbar-nav">
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Events</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Attractions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Promotions</a>
                                </li>
                                <li className="nav-item">
                                    <a className="nav-link" href="#">Explore</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
             
        </div>
      )
    }
}