import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../../actions/actionCreator"
import jQuery from 'jquery';

class Topbar extends Component {
    constructor(props){
        super(props);
    }
    
    logoutFunc(e){
        e.preventDefault();

        // localStorage.setItem("access_token", '');
        // localStorage.setItem("token_type", '');
        // localStorage.setItem("refresh_token", '');
        // localStorage.setItem("expires_in", '');
        // localStorage.setItem("scope", '');
        // localStorage.setItem("patronId", '');
        // localStorage.setItem("email", '');
        // localStorage.setItem("firstName", '');
        // localStorage.setItem("lastName", '');
        // localStorage.setItem("accountNo", '');
        // localStorage.setItem("hasMembership", '');
        // localStorage.setItem("accountType", '');
        // localStorage.setItem("jti", '');

        // jQuery(".login_indicate").removeClass("green_clr");

        // this.props.userData({access_token:'', token_type: '', refresh_token: '', expires_in: '', scope: '', patronLogin:{patronId: '', email:'', firstName: '', lastName: '', accountNo: '', hasMembership: '', accountType: '', jti: ''}});
        
        localStorage.setItem("access_token", '');
        localStorage.setItem("patronName", '');
        localStorage.setItem("email", '');
        localStorage.setItem("firstName", '');
        localStorage.setItem("lastName", '');
        localStorage.setItem("accountNo", '');
        localStorage.setItem("accountType", '');

        jQuery(".login_indicate").removeClass("green_clr");

        this.props.userData({access_token:'', patronName: '', email:'', firstName: '', lastName: '', accountNo: '', accountType: ''});

        //window.location.href = window.location.origin.toString();
        window.location.href = window.location.origin.toString()+'/sistic/logout'; 
    }


    render(){
        console.log('patronEmail',this.props.patronEmail);
        return (
        <div className="row">
            <div className="col-8">
                <a href="#" className="logo-sec">
                    <img src={process.env.PUBLIC_URL + '/assets/images/sistic_logo.svg'} alt="sistic logo" className="img-fluid" />
                </a>
                <div className="search-sec">
                    <form className="search-main" id="search-form">
                        <input type="text" name="search" id="search" placeholder="Search experiences..." className="form-control" />
                        <img src={process.env.PUBLIC_URL + "/assets/images/search_icon.svg"} alt="search" className="img-fluid search_icon" />
                    </form>
                </div>
            </div>
            <div className="col-4 text-right ticket-main">
                {
                    this.props.patronEmail !== undefined && this.props.patronEmail !== "" ?
                    (<a href="javascript:;" className="loginAuthr">
                        <img src={process.env.PUBLIC_URL + "/assets/images/user_icon.svg"} alt="user" className="img-fluid" />
                        <span className={`login_indicate green_clr`}></span>

                        <ul className="header-submenu">
                            <li><a href="https://ticketing.stixcloudtest.com/sistic/patron/management" className="login-board-myaccount">My Account</a></li>
                            <li><a href="javascript:;" className="login-board-logout" onClick={this.logoutFunc.bind(this)}>Logout</a></li>
                        </ul>
                    </a>) : (<a href="javascript:;" className="loginAuthr" data-toggle="modal" data-target="#sistic-login-modal">
                        <img src={process.env.PUBLIC_URL + "/assets/images/user_icon.svg"} alt="user" className="img-fluid" />
                        <span className={`login_indicate`}></span>
                    </a>)
                }
                
                <img src={process.env.PUBLIC_URL + "/assets/images/cart_icon.svg"} alt="cart" className="img-fluid" />
                <div className="ticket-with-us">
                    <a href="#">Ticket with us</a>
                </div>
            </div>
        </div>
      )
    }
}


const mapStateToProps = (state, ownProps) => {
    //console.log('timer_state',state.seconds);
    return {
        access_token: state.access_token,
        patronName: state.patronName,
        patronEmail: state.email,
        firstName: state.firstName,
        lastName: state.lastName,
        accountNo: state.accountNo,
        accountType: state.accountType
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        userData: (dataObject) => {
            dispatch(actionCreators.userInfo(dataObject));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Topbar);