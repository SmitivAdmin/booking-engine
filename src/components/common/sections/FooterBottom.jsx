import React, {Component} from 'react'

export class FooterBottom extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
        <div className="footer-bottom" >
            <div className="container-fluid" >
                <div className="row align-items-center hide-mob" >
                    <div className="col-lg-6 col-md-4 stix-sec">
                        <a href="#">
                            <img src={process.env.PUBLIC_URL + "/assets/images/footer_stix.gif"} alt="Stix" className="img-fluid" />
                        </a>
                        <a href="#">
                            <img src={process.env.PUBLIC_URL + "/assets/images/google_trust.JPG"} alt="Google trust" className="img-fluid" />
                        </a>
                        <p className="copyright">Copyright 1998 - 2019. &copy; SISTIC.com Pte Ltd</p>
                    </div>
                    <div className="col-lg-3 col-md-4 privacy-sec">
                        <a href="#">
                            Privacy Policy
                        </a>
                        <a href="#">
                            Terms & Conditions
                        </a>
                    </div>
                    <div className="col-lg-3 col-md-4 sistic-phone">
                        <p>
                            SISTIC Hotline: <a href="tel:+65 63485555">+65 63485555</a>
                        </p>
                    </div>
                </div>
                <div className="row align-items-center show-mob" >
                    <div className="stix-sec mb-3">
                        <a href="#">
                            <img src={process.env.PUBLIC_URL + "/assets/images/footer_stix.gif"} alt="Stix" className="img-fluid" />
                        </a>
                        <a href="#">
                            <img src={process.env.PUBLIC_URL + "/assets/images/google_trust.JPG"} alt="Google trust" className="img-fluid" />
                        </a>
                    </div>
                    <div className="privacy-sec mb-3">
                        <p className="copyright">Copyright 1998 - 2019. &copy; SISTIC.com Pte Ltd</p><br/>
                        <a href="#">
                            Privacy Policy
                        </a>
                        <a href="#">
                            Terms & Conditions
                        </a><br/>
                        <p>
                            SISTIC Hotline: <a href="tel:+65 63485555">+65 63485555</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}
