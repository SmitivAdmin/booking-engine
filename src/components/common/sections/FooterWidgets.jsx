import React, {Component} from 'react'
import FacebookLogin from 'react-facebook-login';
import * as API from "../../../service/events/eventsInfo";

export class FooterWidgets extends Component {
    constructor(props){
        super(props);
    }

    render(){
        const responseFacebook = (response) => {
            console.log(response);
            //API.loginWithFaceBook
            API.loginWithFaceBook(response).then(response =>{
                console.log("User logged in successfully", response)
            });
        }
        return (
            <div className="footer-top mt-5 p-4 mob-px-0">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-lg-2 col-md-4 col-12">
                                <h4>Our Company</h4>
                                <div className="company-list">
                                    <ul className="list-inline">
                                        <li>
                                            <a href="#">About us</a>
                                        </li>
                                        <li>
                                            <a href="#">Sell with Us</a>
                                        </li>
                                        <li>
                                            <a href="#">Ticketing Technology</a>
                                        </li>
                                        <li>
                                            <a href="#">Partner with Us</a>
                                        </li>
                                        <li>
                                            <a href="#">Careers</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-lg-4 col-md-8 col-12">
                                <h4>Helpful Links</h4>
                                <div className="help-list">
                                    <ul className="list-inline">
                                        <li>
                                            <a href="#">Where to Buy Tickets</a>
                                        </li>
                                        <li>
                                            <a href="#">Locate an Agent</a>
                                        </li>
                                        <li>
                                            <a href="#">Locate an Venue</a>
                                        </li>
                                        <li>
                                            <a href="#">Blog</a>
                                        </li>
                                        <li>
                                            <a href="#">Media</a>
                                        </li>
                                    </ul>
                                </div>
                                <div className="account-list">
                                    <ul className="list-inline">
                                        <li>
                                            <a href="#">My Account</a>
                                        </li>
                                        <li>
                                            <a href="#">Gift Vouchers</a>
                                        </li>
                                        <li>
                                            <a href="#">FAQ</a>
                                        </li>
                                        <li>
                                            <a href="#">Cancellations/Refunds</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact Us</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-12">
                                <h4>SISTIC on Mobile</h4>
                                <div className="mobile-app">
                                    <a href="#" className="clearfix">
                                        <img src={process.env.PUBLIC_URL + "/assets/images/apple_bg.png"} alt="apple" className="img-fluid appl-img" />
                                    </a>
                                    <a href="#">
                                    <img src={process.env.PUBLIC_URL + "/assets/images/play_bg.png"} alt="play store" className="img-fluid play-img" />
                                </a>
                            </div>
                            </div>

                            <div className="col-lg-3 col-md-6 col-12">
                                <h4>Stay Connected</h4>
                                <div className="mail-inner">
                                    <form className="mail-sub" id="mail-form">
                                        <input type="email" name="user_email" id="user_email" placeholder="Enter your email" className="form-control" />
                                        <img src={process.env.PUBLIC_URL + "/assets/images/button_arrow.png"} alt="Button arrow" className="img-fluid button-arrow" />
                                    </form>
                                </div>
                                <div className="follow-us">
                                <h4>Follow us on</h4>
                                <ul className="list-inline">
                                    <li>
                                        <a href="#">
                                            <img src={process.env.PUBLIC_URL + "/assets/images/footer_fb.png"} alt="Facebook" className="img-fluid" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img src={process.env.PUBLIC_URL + "/assets/images/footer_insta.png"} alt="Instagram" className="img-fluid" />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                    </div>

                </div>
            </div>
        </div>
        )
    }
}
