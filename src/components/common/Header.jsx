import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import * as actionCreators from "../../actions/actionCreator"

import Topbar from './sections/Topbar'
import Menubar from './sections/Menubar'
import MobileHeader from './sections/MobileHeader'
import TimeCounter from '../events/TimeCounter'
import FacebookLogin from 'react-facebook-login';


import jQuery from 'jquery';
import * as API from "../../service/events/eventsInfo";

class Header extends Component {
    constructor(props){
        super(props);
    }

    extendTime(e){
        e.preventDefault();
        let update_seconds = this.props.seconds + 300000;
        this.props.updateTimer('seconds',update_seconds);
        window.jQuery("#time-up").modal('hide');
    }

    handleChange(e){
        e.preventDefault();
        var getId = e.target.id;
        if(e.target.value != ""){
            jQuery("#"+getId).removeClass("error_bdr");
            jQuery("#"+getId).siblings(".error_msg").addClass('hide');
        } else{
            jQuery("#"+getId).addClass("error_bdr");
            jQuery("#"+getId).siblings(".error_msg").removeClass('hide');
        }
    }

    async loginFormSubmit(e){
        e.preventDefault();
    
        var error_msg = true;
        var user_email = jQuery("#sistic_user_email").val();
        var user_password = jQuery("#sistic_user_pass").val();
    
        if(user_email != ""){      
          jQuery("#sistic_user_email").removeClass("error_bdr");
          jQuery("#sistic_user_email").siblings(".error_msg").html("");
        } else{
          jQuery("#sistic_user_email").siblings(".error_msg").html("Please enter the email-id.");
          jQuery("#sistic_user_email").addClass("error_bdr");
          error_msg = false;
        }
    
        if(user_password != ""){
          jQuery("#sistic_user_pass").removeClass("error_bdr");
          jQuery("#sistic_user_pass").siblings(".error_msg").html("");
        } else{
          jQuery("#sistic_user_pass").siblings(".error_msg").html("Please enter the password.");
          jQuery("#sistic_user_pass").addClass("error_bdr");
          error_msg = false;      
        }
    
        if(error_msg === true){
          //console.log("Success!");
            // await API.loginUser(user_email, user_password).then((response) =>{
            //     //console.log("status", response.status);
            //     //console.log("response", response.data);
            //     if(response !== undefined){
            //         if(response.status === 200){
            //             localStorage.setItem("access_token", response.data.access_token);
            //             localStorage.setItem("token_type", response.data.token_type);
            //             localStorage.setItem("refresh_token", response.data.refresh_token);
            //             localStorage.setItem("expires_in", response.data.expires_in);
            //             localStorage.setItem("scope", response.data.scope);
            //             localStorage.setItem("patronId", response.data.patronLogin.patronId);
            //             localStorage.setItem("email", response.data.patronLogin.email);
            //             localStorage.setItem("firstName", response.data.patronLogin.firstName);
            //             localStorage.setItem("lastName", response.data.patronLogin.lastName);
            //             localStorage.setItem("accountNo", response.data.patronLogin.accountNo);
            //             localStorage.setItem("hasMembership", response.data.patronLogin.hasMembership);
            //             localStorage.setItem("accountType", response.data.patronLogin.accountType);
            //             localStorage.setItem("jti", response.data.jti);
    
            //             this.props.userData(response.data);
    
            //             jQuery(".login_indicate").addClass("green_clr");
            //             window.jQuery('#sistic-login-modal').modal('hide');
            //             jQuery(".login_err").html('');
            //         } else{
            //             //console.log("response_error_1", response);
            //             if(response.status === 401){
            //                 //console.log("response_error", response.data.message);
            //                 jQuery(".login_err").html(response.data.message);
            //             } else{
            //                 //console.log("response_error", response.data.error_description);
            //                 jQuery(".login_err").html(response.data.error_description);
            //             }
                        
            //         }
            //     } else{
            //         jQuery(".login_err").html("Response is undefined");
            //     }
            // });

            var patron_type = "I";
            await API.loginUser(user_email, user_password, patron_type).then((response) =>{
                jQuery("#confirm-type").trigger("click");

                console.log('login_response', response);

                if(response && response.status === 200){
                    localStorage.setItem("access_token", response.data.token);
                    localStorage.setItem("patronName", response.data.patronName);
                    localStorage.setItem("email", response.data.email);
                    localStorage.setItem("firstName", response.data.firstName);
                    localStorage.setItem("lastName", response.data.lastName);
                    localStorage.setItem("accountNo", response.data.accountNo);
                    localStorage.setItem("accountType", response.data.patronType);

                    this.props.userData(response.data);

                    jQuery(".login_indicate").addClass("green_clr");
                    window.jQuery('#sistic-login-modal').modal('hide');
                    jQuery(".login_err").html('');

                    // jQuery("#confirm-type").trigger("click");
                    API.checkCart().then(response =>{
                        console.log(response);
                    }); 
                } else{
                    if(response && response.status === 401){
                        //console.log("response_error", response.data.message);
                        jQuery(".login_err").html(response && response.data && response.data.message);
                    } else{
                        //console.log("response_error", response.data.error_description);
                        jQuery(".login_err").html(response && response.data && response.data.error_description);
                    }
                }
                
            });
        } else{
          jQuery("#login-form .error_msg").removeClass("hide");
        }
    }


    forgetPwdSubmit(e){
        e.preventDefault();

        var error_msg = true;
        var user_email = jQuery("#forget_email").val();
    
        if(user_email != ""){      
          jQuery("#forget_email").removeClass("error_bdr");
          jQuery("#forget_email").siblings(".error_msg").html("");
        } else{
          jQuery("#forget_email").siblings(".error_msg").html("Please enter the email-id.");
          jQuery("#forget_email").addClass("error_bdr");
          error_msg = false;
        }

        if(error_msg === true){
            //console.log("Success!");
            API.forgetPwd(user_email).then((response) =>{
                if(response.status === 200){
                    jQuery(".forget_err").html("We have sent the link to reset your password to "+user_email+".<br/><br/>Please contact us if you do not receive it within the next few minutes at +65 6348 5555 or feedback@sistic.com.sg");
                } else{
                    if(response.status === 400){
                        jQuery(".forget_err").html(response.data.statusMessage);
                    } else{
                        jQuery(".forget_err").html(response.data.error_description);
                    }
                    
                }
            })
        }
    }

    IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
            return false;
        }else{
            return true;
        }
    }

    regFrmSubmit(e){
        e.preventDefault();

        var error_msg = true;
        var user_email = jQuery("#reg_email").val();
    
        if(user_email != ""){      
            if(this.IsEmail(user_email) === false){
                jQuery("#reg_email").siblings(".error_msg").html("Please enter the valid email-id.");
                error_msg = false;
            } else{
                jQuery("#reg_email").removeClass("error_bdr");
                jQuery("#reg_email").siblings(".error_msg").html("");
                error_msg = true;
            }          
        } else{
          jQuery("#reg_email").siblings(".error_msg").html("Please enter the email-id.");
          jQuery("#reg_email").addClass("error_bdr");
          error_msg = false;
        }

        

        if(error_msg === true){
            //console.log("Success!");
            API.checkEmail(user_email).then((response) =>{
                if(response.status === 200){
                    jQuery(".create_acc_err").html("Success");
                } else{
                    if(response.status === 400){
                        jQuery(".create_acc_err").html(response.data.statusMessage);
                    } else{
                        jQuery(".create_acc_err").html(response.data.error_description);
                    }
                    
                }
            })
        }
    }

    loginForgetForm(get_form, e){
        jQuery(".forget_err, .login_err").html('');
        if(get_form === "forgetpwd_form_cnt"){
            jQuery(".forgetpwd_form_cnt").removeClass("hide");
            jQuery(".login_form_cnt").addClass("hide");
        } else{
            jQuery(".forgetpwd_form_cnt").addClass("hide");
            jQuery(".login_form_cnt").removeClass("hide");
        }
    }

    createAccount(get_form, e){
        jQuery(".create_acc_err").html('');
        if(get_form === "create_btn"){
            jQuery(".create_btn").addClass("hide");
            jQuery(".create_acc_form_cnt").removeClass("hide");
        } else{
            jQuery(".create_btn").removeClass("hide");
            jQuery(".create_acc_form_cnt").addClass("hide");
        }
    }

    render(){
        const responseFacebook = (response) => {
            console.log(response);
            //API.loginWithFaceBook
            API.loginWithFaceBook(response.accessToken).then(response =>{
                console.log("User logged in successfully", response)
            });
        }
        return (
        <Fragment>
            
            <header>
                <div className="header" id="desktop-header">
                    <div className="container-fluid pad-x-30">
                        <Topbar />

                        <Menubar />

                    </div>
                </div>

                <div className="header" id="mobile-header">
                    <div className="container-fluid">
                        <MobileHeader />
                    </div>
                </div>
            </header>

            {/* Login and Signup Modal*/}
            <div className="modal fade sistic-login" id="sistic-login-modal" tabindex="-1" role="dialog">
                <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div className="modal-body text-center">
                        <img src={process.env.PUBLIC_URL + "/assets/images/sistic_color_logo.png"} alt="Sistic logo" className="img-fluid" />
                        <p className="ticket-entertain mt-1">Your Ticket To Great Entertainment</p>

                        <div className="login-form-sec login_form_cnt">
                            <form action="javascript:;" id="login-form" name="login-form" method="post" autoComplete="off" onSubmit={this.loginFormSubmit.bind(this)}>

                                <div className="login_err"></div>

                                <div className="form-group">
                                    <input type="text" className="form-control" name="sistic_user_email" id="sistic_user_email" placeholder="Email"  onChange={this.handleChange.bind(this)} autoComplete="off"/>
                                    <div className="error_msg"></div>
                                </div>
                                <div className="form-group">
                                    <input type="password" className="form-control" name="sistic_user_pass" id="sistic_user_pass" placeholder="Password" onChange={this.handleChange.bind(this)} />
                                    <div className="error_msg"></div>
                                </div>
                                <button type="submit" className="btn primary-btn login-btn custom-btn mb-2">Login</button>
                            </form>

                            <p><a href="javascript:;" onClick={this.loginForgetForm.bind(this, 'forgetpwd_form_cnt')}>Forget Password?</a></p>
                        </div>

                        <div className="login-form-sec forgetpwd_form_cnt hide">
                            <form action="javascript:;" id="forget-form" name="forget-form" method="post" autoComplete="off" onSubmit={this.forgetPwdSubmit.bind(this)}>
                                <div className="forget_err"></div>

                                <div className="form-group">
                                    <input type="text" className="form-control" name="forget_email" id="forget_email" placeholder="Email"  onChange={this.handleChange.bind(this)} autoComplete="off"/>
                                    <div className="error_msg"></div>
                                </div>
                                <button type="submit" className="btn primary-btn login-btn custom-btn mb-2">Continue</button>
                            </form>

                            <p><a href="javascript:;" onClick={this.loginForgetForm.bind(this, 'login_form_cnt')}>Login</a></p>
                        </div>
                        
                        <p>Or</p>
                        <a href="javascript:;" className="btn primary-btn custom-btn mb-2">Checkout as <span className="bold-txt">Guest</span></a>
                        
                        <a href="javascript:;" className="btn custom-btn facebook-btn mb-3">Login with <span className="bold-txt">Facebook</span>
                            <div className="fb_btn">
                                <FacebookLogin
                                    appId="169825514068512"
                                    fields="name,email,picture"
                                    callback={responseFacebook}
                                />
                            </div>                            
                        </a>

                        <p className="disconnect-txt">Inactivity after logging in for a period of over 15 minutes will disconnect your web browser session.</p>
                        <a href="javascript:;" className="btn secondary-btn custom-btn create_btn" onClick={this.createAccount.bind(this, 'create_btn')}>Create a <span className="bold-txt">SISTIC Account</span></a>

                        <div className="login-form-sec create_acc_form_cnt hide">
                            <form action="javascript:;" id="register-form" name="register-form" method="post" autoComplete="off" onSubmit={this.regFrmSubmit.bind(this)}>
                                <div className="create_acc_err"></div>

                                <div className="form-group">
                                    <input type="text" className="form-control" name="reg_email" id="reg_email" placeholder="Email"  autoComplete="off"/>
                                    <div className="error_msg"></div>
                                </div>
                                <button type="submit" className="btn primary-btn login-btn custom-btn mb-2">Create an Account</button>
                            </form>

                            <p><a href="javascript:;" onClick={this.createAccount.bind(this, 'create_acc_form_cnt')}>Cancel</a></p>
                        </div>
                    </div>
                </div>
                </div>
            </div>

            {/* Timer Extend Modal */}
            <div className="modal fade sistic-login" id="time-up" tabindex="-1" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div className="modal-body text-center">
                        <h4>Time is Almost Up!</h4>
                        <p>Your session is expiring. Would you like to extend for 5 more minutes?</p>
                        <div className="timer-encl">
                            <TimeCounter />
                        </div>
                        <button className="btn btn-primary mb-2" onClick={this.extendTime.bind(this)}>Extend</button>
                        <button className="btn btn-gray" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
                </div>
            </div>
        </Fragment>
      )
    }
}

const mapStateToProps = (state, ownProps) => {
    //console.log('timer_state',state.seconds);
    return {
        time: state.time,
        seconds: state.seconds,
        propsObj: ownProps,
        checkcart_info: state.checkcart_info
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateTimer: (field, value) => {
            dispatch(actionCreators.updateTimer(field, value));
        },
        userData: (dataObject) => {
            dispatch(actionCreators.userInfo(dataObject));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);

