import React, {Component} from 'react'
import {FooterBottom} from './sections/FooterBottom'
import {FooterWidgets} from './sections/FooterWidgets'

export default class Footer extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return (
        <footer>
            <FooterWidgets />        

            <FooterBottom />

        </footer>
        )
    }
}
