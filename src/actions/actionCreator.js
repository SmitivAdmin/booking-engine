
import {
    CHOOSE_DATE,
    SHOW_INFO,
    UPDATE_QUANTITY,
    CONFIRM_QUANTITY,
    BREAD_CRUMBS,
    CAL_VIEW,
    UPDATE_TIMER,
    SEAT_MAP_OVERVIEW,
    SEAT_SELECTION,
    ADD_TO_CART,
    ADD_TO_RESERVED_CART,
    EMPTY_CART,
    REMOVE_CART,
    REMOVE_MULTIPLE_ITEM_CART,
    REMOVE_RESERVED_CART,
    PAYMENT_SUCCESS, ON_ADDON_SELECT, PAYMENT_METHODS, INSURANCE_CHOSEN, UPDATE_TOTAL_PRICE, CHOOSE_TIME,
    SEAT_STAGE_VIEW, SEAT_STAGE_COORDS,
    USER_DATA, ADD_QUANTITY, UPDATE_TICKET_TYPE, UPDATE_CURRENT_QUANTITY, COMBINE_CART_ON_TICKETTYPE,
    PRICE_TABLE, OVERVIEW_SELECTED_AREA, SHOPPING_CART,
    UPDATE_SEAT_FRND,
    UPDATE_USER_BILING_DETAILS,
    UPDATE_CROSS_SELL, UPDATE_UP_SELL,
    RESERVED_FRIENDS_GA,
    PACAKAGE_SHOW_LIST,
    PACK_SHOWTIME_LIST,
    CHOOSE_PACKAGE,
    PACKAGE_CART_INFO, UPDATE_EVENT_TYPE, CONFIRM_ORDER, CHECKCART, CHOOSE_CAL_DATE
} from './actionsTypes'


export function fetchShowInfo(info) {
    console.log('showinfo_data', info)
    return {
        type: SHOW_INFO,
        showInfo: info
    }
}

export function updateQty(obj,currentQuantity, factor ) {
    console.log("update_qty", obj,currentQuantity, factor);
    return {
        type: UPDATE_QUANTITY,
        ticketTypesSelected:obj,
        currentQuantity:currentQuantity + factor
    }
}

export function updateconfirmQty(obj,currentQuantity, factor ) {
    console.log("update_conf_qty", obj,currentQuantity, factor);
    return {
        type: CONFIRM_QUANTITY,
        ticketTypesSelected:obj,
        currentQuantity:currentQuantity + factor
    }
}

export function addQuantity(currentQuantity, factor) {
    return {
        type: ADD_QUANTITY,
        updatedQuantity: currentQuantity + factor
    }
}
export function decrementQuantity(currentQuantity, factor) {
    return {
        type: ADD_QUANTITY,
        updatedQuantity: currentQuantity - factor
    }
}
export function chooseDate(flag, times, productId,showActive, dateChosen, prdTicketType, isEventWithSurvey, showTitle, venue, show_date_time) {
    return {
        type: CHOOSE_DATE,
        showTimeFlag: flag,
        showTimes: times,
        productId: productId,
        showActive: showActive,
        dateChosen: dateChosen,
        prdTicketType: prdTicketType,
        isEventWithSurvey: isEventWithSurvey,
        choose_showTitle: showTitle,
        choose_venue: venue,
        choose_showDate: show_date_time
    }
}
export function chooseTime(time, chooseProductId) {
    return {
        type: CHOOSE_TIME,
        chosenTime: time,
        chosenProductId: chooseProductId
    }
}
export function chooseShowDate(flag, dateChosen, showTitle, venue, showTimes){
    return {
        type: CHOOSE_CAL_DATE,
        showTimeFlag: flag,
        dateChosen: dateChosen,
        choose_showTitle: showTitle,
        choose_venue: venue,
        showTimes: showTimes
    }
}
export function showCalendarView(flag, hide, showTimeFlag) {
    return {
        type: CAL_VIEW,
        showCalFlag: flag,
        hideDefaultDateView: hide,
        showTimeFlag: showTimeFlag
    }
}

export function breadCrumbs(field,value) {
    return {
        type: BREAD_CRUMBS,
        field,value
    }
}

export function updateTimer(field,value) {
    return {
        type: UPDATE_TIMER,
        field,value
    }
}

export function fetchSeatMapInfo(seat_map_info) {
    if(seat_map_info === ""){
        return {
            type: SEAT_MAP_OVERVIEW,
            seatMapInfo: {}
        }
    }else{
    return {
        type: SEAT_MAP_OVERVIEW,
        seatMapInfo: seat_map_info
    }
}
}

export function fetchSeatSelInfo(seat_selection_info) {
    console.log('seat_selection_info',seat_selection_info)
    if(seat_selection_info && seat_selection_info.seatSelection){
        return {
            type: SEAT_SELECTION,
            seatSelectionInfo: seat_selection_info
        }
    }
    
}

export function addCartInfo(cart_info) {
    
    return {
        type: ADD_TO_CART,
        cartInfo: cart_info
    }
}

export function addReservedCartInfo(cart_info) {
    console.log('rs_cart_info',cart_info);
    return {
        type: ADD_TO_RESERVED_CART,
        reservedcartInfo: cart_info
    }
}

export function addReservedCartInfoGA(obj,currentQuantity, factor){
    
    return {
        type: RESERVED_FRIENDS_GA,
        reservedcartInfo:obj,
        currentQuantity:currentQuantity + factor
    }
}

export function emptyCartInfo() {
    return {
        type: EMPTY_CART,
        cartInfo: [],
        reservedcartInfo: [],
        currentQuantity: 0
    }
}

export function chosenAddon(addon, addonPrice, addonMethod) {
    return {
        type: ON_ADDON_SELECT,
        chosenAddon: addon,
        chosenAddonPrice: addonPrice,
        chosenAddonMethod: addonMethod
    }
}
export function removeCartInfo(index) {
    return {
        type: REMOVE_CART,
        index
    }
}

export function removeMultiCartInfo(index, isFrnd) {
    return {
        type: REMOVE_MULTIPLE_ITEM_CART,
        index: index, 
        isFrnd: isFrnd
    }
}

export function removeReservedCartInfo(index) {
    return {
        type: REMOVE_RESERVED_CART,
        index
    }
}

export function paymentSuccess(transactionId, time, transDate,cardType) {
    return {
        type: PAYMENT_SUCCESS,
        transactionId: transactionId,
        transactionTime: time,
        transactionDate:transDate,
        cardType:cardType
    }
}

export function getPayments(methods) {
    return {
        type: PAYMENT_METHODS,
        paymentMethods: methods
    }
}

export function updateInsured(amount) {
    return {
        type: INSURANCE_CHOSEN,
        insuranceAmount: amount
    }
}

export function updateSum(amount) {
    return {
        type: UPDATE_TOTAL_PRICE,
        totalPriceToBePaid: amount
    }
}

export function fetchSeatStageViewInfo(seat_selection_info) {
    //console.log('seat_selection_info',seat_selection_info)
    return {
        type: SEAT_STAGE_VIEW,
        seatSelectionArr: seat_selection_info
    }
}

export function fetchSeatLevelsCoords(levels_coordinates_list) {
    //console.log('levels_coordinates_list_2',levels_coordinates_list)
    return {
        type: SEAT_STAGE_COORDS,
        levelsCoordinatesList:levels_coordinates_list
    }
}

export function userInfo(user_data) {
    console.log('user_data',user_data)
    // return {
    //     type: USER_DATA,
    //     access_token: user_data.access_token,
    //     token_type: user_data.token_type,
    //     refresh_token: user_data.refresh_token,
    //     expires_in: user_data.expires_in,
    //     scope: user_data.scope,
    //     patronId: user_data.patronLogin.patronId,
    //     email: user_data.patronLogin.email,
    //     firstName: user_data.patronLogin.firstName,
    //     lastName: user_data.patronLogin.lastName,
    //     accountNo: user_data.patronLogin.accountNo,
    //     hasMembership: user_data.patronLogin.hasMembership,
    //     accountType: user_data.patronLogin.accountType,
    //     jti: user_data.jti,
    // }

    return {
        type: USER_DATA,
        access_token: user_data.token,
        patronName: user_data.patronName,
        email: user_data.email,
        firstName: user_data.firstName,
        lastName: user_data.lastName,
        accountNo: user_data.accountNo,
        accountType: user_data.patronType
    }
}

export function updateTicketType(list, chosenPriceCatId) {
    return {
        type: UPDATE_TICKET_TYPE,
        ticketTypeList:list,
        chosenPriceCatId: chosenPriceCatId
    }
}

export function updateCurrentQuantity(currentQuantity, factor) {
    return {
        type: UPDATE_CURRENT_QUANTITY,
        currentQuantity:currentQuantity + factor
    }
}

export function combineCartDetails(mergedCartInfo) {
    return {
        type: COMBINE_CART_ON_TICKETTYPE,
        mergedCartInfo:mergedCartInfo
    }
}

export function fetchPriceTabele(price_table_obj) {
    return {
        type: PRICE_TABLE,
        priceTableObj:price_table_obj
    }
}

export function overviewSelectedArea(area) {
    return {
        type: OVERVIEW_SELECTED_AREA,
        selectedFilterArea:area
    }
}

export function shoppingCartInfo(info) {
    return {
        type: SHOPPING_CART,
        shoppingCart:info
    }
}

export function updateCartForFrnd(boolean) {
    return {
        type: UPDATE_SEAT_FRND,
        boolean:boolean
    }
}

export function updateUserBillingDetails(details) {
    return {
        type: UPDATE_USER_BILING_DETAILS,
        userData: details
    }
}

export function updateCrossSellDetails(name, amount) {
    return {
        type: UPDATE_CROSS_SELL,
        name: name,
        amount: amount
    }
}

export function updateUpSellItem(item, type) {
    return {
        type: UPDATE_UP_SELL,
        item: item,
        actionType: type
    }
}

export function packageShowList(packageShowListObj) {
    return {
        type: PACAKAGE_SHOW_LIST,
        packageShowListObj: packageShowListObj
    }
}
export function packShowTimeList(packShowTimeListObj) {
    return {
        type: PACK_SHOWTIME_LIST,
        packShowTimeListObj: packShowTimeListObj
    }
}

export function choosePackage(pkgReqId, prdtIdList) {
    return {
        type: CHOOSE_PACKAGE,
        pkgReqId: pkgReqId,
        prdtIdList: prdtIdList
    }
}

export function packageQtyInfo(packageCartInfo) {
    return {
        type: PACKAGE_CART_INFO,
        packageCartInfo:packageCartInfo
    }
}

export function updateEventType(event_type) {
    return {
        type: UPDATE_EVENT_TYPE,
        prdTicketType:event_type
    }
}
export function confirmOrder(field,value) {
    return {
        type: CONFIRM_ORDER,
        field,value
    }
}
export function checkCartInfo(checkcart_info) {
    return {
        type: CHECKCART,
        checkcart_info:checkcart_info
    }
}

