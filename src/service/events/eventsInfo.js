import {
    AUTH_TOKEN, AUTH_TOKEN2, LOGIN_AUTH_KEY, BASE_URL, LOGIN_BASE_URL, TICKETING_URL, TENANT, 
    SEAT_CONFIRMATION, CAT_SECTION, PRODUCT_ID, ADD_CART, LOGIN_API, SHOPPING_CART, CHECKCART,
    PROMOTION, CONFIRM_ORDER, USER_LOGIN,EVOUCHERVALIDATION, VERSION_API, PROP_API, SERVER_API, LANG_API, CART_DELETE, SEAT_AVAILABLE, SELECT_PACKAGE, SELECT_PACKAGE_PRODUCTID, PACKAGE_AVAILABILITY, PACKAGE_CONFIRMATION, PACKAGE_SELECTION, GET_PACKAGE_SELECTION, REMOVE_SELECTION, CART_PACKAGE, millisecods
} from '../../constants/common-info';

import CookieUtil from '../../constants/CookieUtil';
import { fromPairs } from 'lodash';

const config = {
    headers: {
        Authorization: `Bearer ${AUTH_TOKEN}`,
        'Content-type': 'application/json; charset=UTF-8',
    }
};
const config2 = {
    headers: {
        Authorization: `Bearer ${AUTH_TOKEN2}`,
        'Content-type': 'application/json; charset=UTF-8',
    }
};

export async function getInfo(icc_code, event_type, group_booking){
   
    var resObj = {};
    CookieUtil.setCookie('lang','en_us',10);

    // getShowDateTime(icc_code, event_type, group_booking);
    
    // return await fetch(BASE_URL + "/icc/?internetContentCode="+icc_code, config).then((res) => {
    //     return res.json()
    // }).then(function (data) {
    //     //TODO: Deepa use lodash
    //     console.warn('icc_data',data);
    //     resObj.eventInfo = data.content[0]; 
    //     return resObj;      
    // }).catch(function (error) {
    //     console.warn('error_msg',error);
    // });
    if(event_type === "booking"){
        if(group_booking !== ""){
            var api_url = BASE_URL + "/groupbooking/"+icc_code+group_booking;
            var config_data = config;
        } else{
            var api_url = BASE_URL + "/icc/"+icc_code;
            var config_data = config;
        }
    } else{
        var api_url = BASE_URL + "/package/"+icc_code;
        var config_data = config2;
    }

    return await fetch(api_url, config_data).then((res) => {
        return res.json()
    }).then(function (data) {
        //TODO: Deepa use lodash
        resObj.productsObj = data;
        return resObj;
    }).catch(function (error) {
        console.warn(error);
    });
}

// export async function getShowDateTime(icc_code, event_type, group_booking){
   
//     var resObj = {}, api_url = BASE_URL + "/icc/"+icc_code, config_data = config;

//     if(event_type === "booking"){
//         if(group_booking !== ""){
//             var api_url = BASE_URL + "/groupbooking/"+icc_code+group_booking;
//             var config_data = config;
//         } else{
//             var api_url = BASE_URL + "/icc/"+icc_code;
//             var config_data = config;
//         }
//     } else{
//         var api_url = BASE_URL + "/package/"+icc_code;
//         var config_data = config2;
//     }

//     return await fetch(api_url, config_data).then((res) => {
//         return res.json()
//     }).then(function (data) {
//         //TODO: Deepa use lodash
//         resObj.productsObj = data;
//         return resObj;
//     }).catch(function (error) {
//         console.warn(error);
//     });
// }


export async function getPaymentMethods(product_id,price_cat_id,mode, priceClassList){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN}`,
            'Content-type': 'application/json',
        },
        body: JSON.stringify([{
            "productId": product_id,
            "priceCatId": price_cat_id,
            "mode": mode,
            "priceClassCodeList": [
                "A"
            ]
        }])
    };
   
    var resObj = {};
    return await fetch(BASE_URL + "/cart/deliverymethods", settings).then((res) => {
        return res.json()
    }).then(function (data) {
        resObj.paymentMethods = data;
        return resObj;
    }).catch(function (error) {
        console.warn(error);
    });
}

export async function orderSuccess(refNo, cardType, cardNumber, amount, currency){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN}`,
            'Content-type': 'application/json',
        },
        body:JSON.stringify({
            "confirmOrder":true,
            "externalTransactionId": "externalTxnID:"+refNo+"|octopus:"+refNo,
            "payment":{
                "paymentRefNo":refNo,
                "paymentMethod":cardType,
                "ccNumber" : cardNumber,
                "charge":{
                    "amount":amount,
                    "currency":"SGD"
                }
            }
        })
    };
   
    // var resObj = {};
    return await fetch(BASE_URL + "/orders/"+refNo, settings).then((res) => {
        return res.json()
    }).then(function (data) {
        return data;
    }).catch(function (error) {
        console.warn(error);
    });
}

export async function loginWithFaceBook(token){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Basic ${LOGIN_AUTH_KEY}`,
            'Content-type': 'application/json',
        },
        body:JSON.stringify({
            "facebook_token":token,
        })
    };
   
    // var resObj = {};
    return await fetch(LOGIN_BASE_URL + "/login?grant_type=password", settings).then((res) => {
        return res.json()
    }).then(function (data) {
        return data;
    }).catch(function (error) {
        console.warn(error);
    });
}

export async function userLogin(user_email,user_pwd){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Basic ${LOGIN_AUTH_KEY}`,
            'Content-type': 'application/json;',
        },
        body: JSON.stringify({
            username: user_email,
            password: user_pwd
        })
    };
   
    return await fetch(LOGIN_BASE_URL + "/login?grant_type=password", settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

export async function forgetPwd(user_email){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN}`,
            'Content-type': 'application/json',
        }
    };
   
    return await fetch(BASE_URL + "/patrons/forgotpassword/"+user_email, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

export async function checkEmail(user_email){
    const settings = {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        }
    };
   
    return await fetch(TICKETING_URL + "/patrons/registration/availability/"+user_email, settings).then((res) => {
       //return res.json()
        const statusCode = res.httpStatus;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}
export async function preOrder(patronInfo,cartItemList,deliveryMethod, cartItemTotal, remarks){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN}`,
            'Content-type': 'application/json',
        },
        body: JSON.stringify({
            patronInfo: patronInfo,
            cartItemList: cartItemList,
            deliveryMethod: deliveryMethod,
            cartItemTotal: cartItemTotal,
            remarks: remarks
        })
    };
   
    return await fetch(BASE_URL + "/orders/", settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log('pre_order_data',data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

export async function ticketType(productId,priceCatId,ticket_type, pkgReqId){

    //var AUTH_TKN = CookieUtil.getCookie(TENANT + '_token');
    //console.log('AUTH_TKN1',AUTH_TKN);
    //alert("TOKEN...  "+(all_token.sistic_token));
    //alert("TOKEN...  "+(all_token.sistic_token));
    //console.log('AUTH_TKN2',all_token.sistic_token);
    var pkgReqId_Data = '';
    if(pkgReqId){
        pkgReqId_Data = '&pkgReqId='+pkgReqId;
    }
    const settings = {
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        }
    };
   
    return await fetch(BASE_URL + "/products/"+productId+"/tickettype?priceCatId="+priceCatId+pkgReqId_Data+"&_="+millisecods, settings).then((res) => {
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        //console.log('ticketType',data);
        var select_product = {
            productId,
            priceCatId,
            ticket_type,
            pkgReqId
        };
        Object.keys(select_product).map(o => data[o]= select_product[o])
        return { status, data }
    }).catch(function (error) {
        console.warn(error);
    });
}

/* ========== SeatMap==========*/

// export async function getSeatMapOverView(product_id){
//     var resObj = {};
//     return await fetch(BASE_URL + "/products/"+product_id+"/seatmap/overview", config).then((res) => {
//         return res.json()
//     }).then(function (data) {
//         resObj.seatMapInfo = data;
//         //return resObj;

//         return fetch(BASE_URL + "/products/"+product_id+"/seatmap/availability", config);

//     }).then(function (response) {
//         return response.json();
//     }).then(function (data) {
//         resObj.availabilityObj = data;

//         return fetch(BASE_URL + "/products/"+product_id+"/seatmap/pricetable", config);
//         //return resObj;
//     }).then(function (response) {
//         return response.json();
//     }).then(function (data) {
//         resObj.priceTableObj = data;
//         return resObj;
        
//     }).catch(function (error) {
//         console.warn(error);
//     });
// }
export async function getSeatMapOverView(product_id, pkgReqId){
    // var AUTH_TKN = CookieUtil.getCookie(TENANT + '_token');
    // console.log('AUTH_TKN',AUTH_TKN);

    var resObj = {}, pkgReqIdData = '';
    if(pkgReqId !== "" && pkgReqId !== undefined){
        pkgReqIdData = "?pkgReqId="+pkgReqId;
    }
    return await fetch(BASE_URL + "/products/"+product_id+"/seatmap/overview"+pkgReqIdData, config2).then((res) => {
        return res.json()
    }).then(function (data) {
        resObj.seatMapInfo = data;
        return resObj;
        //return fetch(BASE_URL + "/products/"+product_id+"/seatmap/availability", config);
    }).catch(function (error) {
        console.warn(error);
    });
}

export async function getPriceTable(product_id){
    return await fetch(BASE_URL + "/products/"+product_id+"/seatmap/pricetable", config).then((res) => {
        return res.json()
    }).then(function (data) {
        return data;

        //return fetch(BASE_URL + "/products/"+product_id+"/seatmap/availability", config);
    }).catch(function (error) {
        console.warn(error);
    });
}

export async function getSeatSelection(product_id, price_cat_id, seat_section_id, mode, quantity, price_cat_alias, seat_section_alias, seat_section_type, price_amount, price_currency, price_formatted, seat_level_alias, pkgReqId){
    var resObj = {}, availability_link = '';

    if(pkgReqId != ""){
        availability_link = BASE_URL + "/products/"+product_id+"/seatmap/availability?priceCatId="+price_cat_id+"&seatSectionId="+seat_section_id+"&mode="+mode+"&quantity="+quantity+"&pkgReqId="+pkgReqId;
    } else{
        availability_link = BASE_URL + "/products/"+product_id+"/seatmap/availability?priceCatId="+price_cat_id+"&seatSectionId="+seat_section_id+"&mode="+mode+"&quantity="+quantity;
    }
    

    return await fetch(availability_link, config).then((res) => {
        return res.json()
    }).then(function (data) {
        var seat_details = {
            product_id,
            price_cat_id,
            seat_section_id,
            mode,
            quantity,
            price_cat_alias,
            seat_section_alias,
            seat_section_type,
            price_amount,
            price_currency,
            price_formatted,
            seat_level_alias
        };
        Object.keys(seat_details).map(o => data[o]= seat_details[o])
        resObj.seatSelection = data;
        return resObj;
    }).catch(function (error) {
        console.warn(error);
    });
}

// export async function getSeatsAvailable(product_id, price_cat_id, seat_section_id, mode, quantity, price_cat_alias, seat_section_alias, seat_section_type, price_amount, price_currency, price_formatted, seat_level_alias){
//     var resObj = {};
//     return await fetch(BASE_URL + "/products/"+product_id+"/seatmap/availability?priceCatId="+price_cat_id+"&seatSectionId="+seat_section_id+"&mode="+mode+"&quantity="+quantity, config).then((res) => {
//         return res.json()
//     }).then(function (data) {
//         if(data.seatsAvailableList){
//             return data.seatsAvailableList.length;
//         } else{
//             return 0;
//         }
        
//     }).catch(function (error) {
//         console.warn(error);
//     });
// }

export async function getSeatAvailableGroup(product_id, group_booking){
    return await fetch(BASE_URL + "/products/"+product_id+"/seatmap/availability"+group_booking, config).then((res) => {
        return res.json()
    }).then(function (data) {
        return data;        
    }).catch(function (error) {
        console.warn(error);
    });
}
/* ========== SeatMap==========*/

/* ======== Package Flow Seat Availability ========= */
export async function getPackageSeatsAvailable(product_id, pkgReqId){
    var resObj = {};
    return await fetch(BASE_URL + "/products/"+product_id+"/seatmap/availability?pkgReqId="+pkgReqId, config).then((res) => {
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}



// Java API Calls Start

/* ========== Set Product ID on the session ==========*/
export async function setProductId(product_id, icc_code, grp_booking){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify({"productId":product_id,"internetContentCode":icc_code,"groupBookingCode":grp_booking});
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+PRODUCT_ID, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}


/* ========== Cat Section ==========*/
export async function catSection(price_cat_id, seat_section_id, mode, isMembership, isEventWithSurvey){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify({"priceCatId":price_cat_id,"seatSectionId":seat_section_id,"mode":mode,"isMembership":isMembership,"isEventWithSurvey":isEventWithSurvey});
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+CAT_SECTION, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== Check Seat Confirm ==========*/
export async function checkConfirmSeats(confirmSeats){
    var params = JSON.stringify(confirmSeats);
    console.log('api_url_confirmSeats',params);

    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+SEAT_CONFIRMATION, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== Add Cart ==========*/
export async function addCart(cartinfo){
    var params = JSON.stringify(cartinfo);
    console.log('api_url_confirmSeats',params);

    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+ADD_CART, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== User Login  ==========*/
export async function loginUser(user_email,user_pwd, patron_type){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Basic ${AUTH_TOKEN2}`,
            'Content-type': 'application/json;',
        },
        body: JSON.stringify({
            username: user_email,
            password: user_pwd,
            patronType: patron_type
        })
    };
    
    return await fetch("/"+USER_LOGIN, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== Confirm Shoppingcart  ==========*/
export async function confirmCart(){
    return await fetch("/"+SHOPPING_CART, config2).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== Checkcart  ==========*/
export async function checkCart(){
    return await fetch("/"+CHECKCART, config2).then(async (res) => {
        await fetch("/"+VERSION_API, config2).then();
        await fetch("/"+PROP_API, config2).then();
        await fetch("/"+SERVER_API, config2).then();
        await fetch("/"+LANG_API, config2).then();
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

export async function promotion(promotionDetails){
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN}`,
            'Content-type': 'application/json;',
        },
        body: JSON.stringify(promotionDetails)
    };

   
    return await fetch("/"+PROMOTION, settings).then((res) => {
        console.log("raw PROMOTION parsed", res);
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}
/*=====Apply E-voucher === */
export async function applyevoucher(formData){
    var reslutvalue=[];
    reslutvalue.push(formData);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`
        },
        body: reslutvalue
    };
    return await fetch(EVOUCHERVALIDATION, settings).then((res) => {
        console.log("Confirm_response", res);
        const statusCode = res.exceedMaxQtyAllowed;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}



/* ======= Confirm Order ========== */
export async function confirmOrder(formData){

    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN}`
        },
        body: formData
    };
    return await fetch("/"+CONFIRM_ORDER, settings).then((res) => {
        console.log("Confirm_response", res);
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        console.log(data)
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ============ Delete Cart ========== */
export async function deleteCart(cartItemId, priceClassCode){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify({"cartItemId":cartItemId,"priceClassCode":priceClassCode});
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+CART_DELETE, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* =========== Get seat available ============ */
export async function getSeatAvailable(product_id, price_cat_id, seat_section_id, quantity, mode, price_cat_alias, seat_section_alias, seat_section_type, price_amount, price_currency, price_formatted, seat_level_alias, pkgReqId, groupBookingCode){
    var resObj = {};
    var params = JSON.stringify({"quantity":0,"mode":mode, "groupBookingCode":groupBookingCode});
    var pkgReqId = pkgReqId !== '' ? "?pkgReqId="+pkgReqId : '';
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
    return await fetch("/"+SEAT_AVAILABLE+pkgReqId, settings).then((res) => {
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        var seat_details = {
            product_id,
            price_cat_id,
            seat_section_id,
            mode,
            quantity,
            price_cat_alias,
            seat_section_alias,
            seat_section_type,
            price_amount,
            price_currency,
            price_formatted,
            seat_level_alias
        };
        Object.keys(seat_details).map(o => data.detailSeatmapResponse[o]= seat_details[o])
        resObj.seatSelection = data.detailSeatmapResponse;
        return { status, resObj };
    }).catch(function (error) {
        console.warn(error);
    });
}


/* =========== Select Package ============ */
export async function getPackageShow(pkgId, pkgAlias, info, description){
    var resObj = {};
    var params = JSON.stringify({"pkgId":pkgId,"pkgAlias":pkgAlias, "info":info, "description":description});
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
    return await fetch("/"+SELECT_PACKAGE, settings).then((res) => {
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (error) {
        console.warn(error);
    });
}


/* =========== Package Prod Option ============ */
export async function addPackPrd(pkgReqId, prdtIdList){
    var resObj = {};
    var params = JSON.stringify({"pkgReqId":pkgReqId,"prdtIdList":prdtIdList});
    console.log('prd_params', params);

    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
    return await fetch(BASE_URL+"/package/prdtOpt/", settings).then((res) => {
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (error) {
        console.warn(error);
    });
}

/* ========== Set Product ID on the session ========== */
export async function setPackageProductId(product_id, icc_code, pkgReqId, showDateTime, productAlias, venueAlias, prdtIdList){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify({"productId":product_id,"internetContentCode":icc_code,"pkgReqId":pkgReqId, "showDateTime":showDateTime, "productAlias": productAlias, "venueAlias": venueAlias, "prdtIdList": prdtIdList});
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+SELECT_PACKAGE_PRODUCTID, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== Package Availability ========== */
export async function packageAvailability(post_data){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify(post_data);
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+PACKAGE_AVAILABILITY, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== Package Confirmation ========== */
export async function packageConfirmation(post_data){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify(post_data);
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+PACKAGE_CONFIRMATION, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ========== Package SELECTION ========== */
export async function packageSelection(post_data){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify(post_data);
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };
   
    return await fetch("/"+PACKAGE_SELECTION, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ============== Get Package Selecction ============= */
export async function getPkgSlctn(){
    return await fetch("/"+GET_PACKAGE_SELECTION, config2).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ============== Get Package Selecction ============= */
export async function removeSelection(post_data){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify(post_data);
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };

    return await fetch("/"+REMOVE_SELECTION, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}

/* ============== Get Package Selecction ============= */
export async function cartPackage(post_data){
    //console.log('price_cat_id', price_cat_id);
    var params = JSON.stringify(post_data);
    //console.log('params', params);
    const settings = {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${AUTH_TOKEN2}`,
            'Content-type': 'application/json',
        },
        body: params
    };

    return await fetch("/"+CART_PACKAGE, settings).then((res) => {
       //return res.json()
        const statusCode = res.status;
        const data = res.json();
        return Promise.all([statusCode, data]);
    }).then(function ([status, data]) {
        return { status, data };
    }).catch(function (status, error) {
        console.warn(error);
    });
}
// Java API Calls End


