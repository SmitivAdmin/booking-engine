$(document).ready(function(){
    $(".search_icon").click(function(){
        $(this).parent("form").find("input").toggle();
    });

    /*button submit on footer */
    $(".button-arrow").click(function(){
        $(this).parent("form").submit();
    });

    /*seats get seat btn */
    // $('.get-seat-btn').click(function(){
    //     if($(window).width() > 768){
    //         if($(this).text() == "Choose my own seats"){
    //             $(this).text('Get the best seats available');
    //         }else{
    //             $(this).text('Choose my own seats');
    //         }
    //     }
       
    if($(window).width() < 768){
        if($(this).text() == "Best seats"){
            $(this).text('Cancel');
            $(this).addClass('btn-bg');

        }else{
            $(this).text('Best seats');
            $(this).removeClass('btn-bg');
        }
    }

    /*filter level change image */
    // $(".filter-encl").mouseenter(function(){
    //     var newimg = $(this).find("img").data("newsrc");
    //     $(this).find("img").attr("src", newimg);
    // });
    // $(".filter-encl").mouseleave(function() {
    //     var oldimg = $(this).find("img").data("oldsrc");
    //     $(this).find("img").attr("src", oldimg);
    // });

    
    $('#filter-layer-slider').owlCarousel({
        loop: false,
        margin: 0,
        nav: false,
        dots: false,
        margin: 10,
        responsive:{
            0:{
                items:3
            },
            600:{
                items:4
            },
            1000:{
                items:0
            }
        }
    })
    /*filter levels in mobile */
    // $(".filter-layers").click(function(){
    //     $(this).parents(".filter-mobile").find(".filter-level").toggle();
    // });

    $(".filter-main").click(function(){
        $(".filter-level-inner .filter-main").css({"opacity":"0.4"});
        $(".filter-level-inner .filter-main").find("p").css({"color":"#9e9e9e"});
        $(this).css({"opacity":"1"});
        $(this).find("p").css({"color":"#0098ff"});
    });
    $(".filter-main.all-filter").click(function(){
        $(".filter-level-inner .filter-main").css({"opacity":"1"});
        $(this).find("p").css({"color":"#0098ff"});
    });

});

// ligh-slider

// optional
// $('#blogCarousel').carousel({
//     interval: 5000
// });